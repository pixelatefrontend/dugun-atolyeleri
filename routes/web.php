<?php
Route::group( ['domain' => 'www.'.config('app.baseURL')], function(){
	Route::get('/', function() {
    	//TODO: redirect this to the main domain + the slug that came in -- somehow :/
	    return redirect(config('app.baseURL'));
	});

	Route::get('/{slug}/{slug2}', function($slug, $slug2) {
    	//TODO: redirect this to the main domain + the slug that came in -- somehow :/
	    return redirect(config('app.baseURL').'/'.$slug.'/'.$slug2);
	});

	Route::get('/blog', function() {
    	//TODO: redirect this to the main domain + the slug that came in -- somehow :/
	    return redirect(config('app.baseURL').'/blog');
	});
});
// Atölye Routerları (atolye.dugunatolyeleri.com)
// Controller dizini (app/Http/Controllers/Workshop/..)
Route::group( ['domain' => 'atolye.'.config('app.baseURL'), 'namespace' => 'Workshop'], function(){

	Route::group( [ 'middleware' => 'workshop' ],function(){

		Route::get('/', 'HomeController@getIndex')->name('workshop.dashboard');
		// Genel Bilgiler
		Route::get('panel', 'GeneralsController@getIndex')->name('workshop.panel');
		Route::put('panel', 'GeneralsController@postGeneralUpdate')->name('workshop.panel.update.post');
		Route::put('atolye-gorseli', 'GeneralsController@postImageUpdate')->name('workshop.image.update.post');
		Route::put('atolye-detay', 'GeneralsController@postDetailUpdate')->name('workshop.detail.update.post');
		Route::put('sorular', 'GeneralsController@postQuestionsUpdate')->name('workshop.questions.update.post');
		// Paket Sayfası
		Route::get('packages', 'PaymentsController@getPackages')->name('get.packages');
		// Paket fiyatlarının post edildiği sayfa
		Route::post('packages', 'PaymentsController@postPackages')->name('post.packages');
		// Premium info
		Route::get('premium', 'PremiumController@getIndex')->name('get.premium');
		// Fatura işlemleri
		Route::get('bill', 'PaymentsController@getBill')->name('get.bill');
		Route::post('bill', 'PaymentsController@postBill')->name('post.bill');
		// Ödeme sayfası
		Route::get('card', 'PaymentsController@getCardForm')->name('get.card.form');
		Route::post('card', 'PaymentsController@postCard')->name('post.card');
		// Ödeme geri dönüş sayfası
		Route::post('completed', 'PaymentsController@getSubscribeSuccess')->name('subscribe.success');
		// Testinh
		Route::get('demo', 'PaymentsController@foo');
		// Route::post('demo', 'PaymentsController')->name('')
		
		// Galeri
		Route::get('galeri', 'GalleryController@getIndex')->name('workshop.gallery');
		Route::post('galeri', 'GalleryController@postUpdate')->name('workshop.gallery.update.post');
		Route::post('image/destroy/{id}', 'GalleryController@imageDestroy')->name('gallery.image.destroy');
		Route::post('image/sortable', 'GalleryController@imageSortable')->name('workshop.image.sortable');
		Route::post('image/conf/{id}', 'GalleryController@imageConf');
		Route::post('image/detail', 'GalleryController@postImageDetail')->name('workshop.image.detail');
		// Video Galeri
		// Route::get('video', 'VideosController@getIndex')->name('workshop.video');
		// Route::put('video', 'VideosController@postUpdate')->name('workshop.video.post');
		Route::resource('videos', 'VideosController');
		// Teklifler
		Route::get('teklifler', 'OffersController@getIndex')->name('workshop.offers');
		Route::post('offer/addcontract', 'OffersController@addContract')->name('workshop.offer.add');
		Route::post('offer/destroyContract', 'OffersController@destroyContract')->name('workshop.offer.destroy');
		// Yorumlar
		Route::get('yorumlar', 'CommentsController@getIndex')->name('workshop.comments');
		Route::delete('comment/{id}', 'CommentsController@destroyOffer')->name('workshop.comment.destroy');
		Route::post('comment/confirm/{id}', 'CommentsController@confirmComment')->name('workshop.comment.confirm');
		Route::post('comment/disabled/{id}', 'CommentsController@disabledComment')->name('workshop.comment.disabled');
		// Kampanyalar
		Route::resource('campaigns', 'CampaignsController');
		// İstatistikler
		Route::get('istatistikler', 'AnalyticsController@getIndex')->name('workshop.analytics');
		// Kişiler
		Route::get('kisileri-yonet', 'PersonsController@getIndex')->name('workshop.persons');
		// Ödeme işlemleri
		Route::get('odeme', 'PaymentsController@getIndex')->name('workshop.payment');
		// Çıkış Yap
		Route::get('cikis', 'AuthController@getLogout')->name('workshop.logout');

	});

	// Atöyle Giriş Routerları

	Route::get('giris', 'AuthController@getLogin')->name('workshop.login');
	Route::post('giris', 'AuthController@postLogin')->name('workshop.login.post');

	//  Atölye Kayıt Routerları

	Route::get('/kayit', 'AuthController@getRegister')->name('workshop.register');
	Route::post('/kayit', 'AuthController@postRegister')->name('workshop.register.post');

});

// Kullanıcı Routerları (kullanici.dugunatolyeleri.com)
// Controller dizini (app/Http/Controllers/User/..)

Route::group( ['domain' => 'kullanici.'.config('app.baseURL'), 'namespace' => 'User'], function(){

	Route::group( ['middleware' => 'auth'],function(){

		Route::get('/', 'HomeController@getIndex')->name('user.dashboard');
		Route::put('/user/{id}', 'UserController@postUpdate')->name('user.update.post');
		Route::get('tekliflerim', 'OffersController@getOffers')->name('user.offers');
		Route::get('listelerim', 'ListsController@getLists')->name('user.lists');
		Route::get('favorilerim', 'FavoritesController@getFavorites')->name('user.favorites');
		Route::post('user/favorites', 'FavoritesController@addFavorite')->name('add.user.favorite');
		Route::get('yorumlarim', 'CommentsController@getComments')->name('user.comments');
		Route::delete('comment/destroy/{id}', 'CommentsController@commentDestroy')->name('user.comment.destroy');
		Route::get('ayarlarim', 'HomeController@getIndex')->name('user.settings');
		// Kullanıcı Atölye Beğenme
		// Kullanıcı Oturum Sonlandırma
		Route::get('logout', 'AuthController@getLogout')->name('user.logout');

	});



		// Kullanıcı Giriş Routerları
		Route::get('giris', 'AuthController@getLogin')->name('user.login');
		Route::post('giris', 'AuthController@postLogin')->name('user.login.post');
		// Facebook ile giriş
		Route::get('facebook/login', 'AuthController@getFacebookLogin')->name('get.facebook.login');
		Route::get('facebook/login/callback', 'AuthController@handleProviderCallback')->name('redirect.facebook.login');

		//  Kullanıcı Kayıt Routerları
		Route::get('kayit', 'AuthController@getRegister')->name('user.register');
		Route::post('kayit', 'AuthController@postRegister')->name('user.register.post');
		Route::get('register/activation/{token}', 'AuthController@confirmation')->name('user.activation');

		// Şifre sıfırlama
		Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('get.password.reset');
		Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('post.password.reset');
		Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('get.password.token');
		Route::post('password/reset', 'ResetPasswordController@reset')->name('post.password');

	
});
// Admin Paneli Routerları (admin.dugunatolyeleri.com)
// Controller dizini (app/Http/Controllers/Admin/..)

Route::group( ['domain' => 'admin.'.config('app.baseURL'), 'namespace' => 'Admin'], function(){	
	Route::group([ 'middleware' => 'admin'], function(){
		// Backup Route
		Route::get('getBackup', 'BackupController@getBackup')->name('get.backup');

		Route::get('workshopfoo', 'HomeController@getFoo');

		Route::get('/', 'HomeController@getIndex')->name('admin.dashboard');
		Route::post('workshopbycateogory', 'HomeController@postWorkshopByCategory')->name('workshopByCategory');
		// Site Ayarları
		Route::get('/settings', 'SettingController@getIndex')->name('get.settings');
		Route::post('/settings', 'SettingController@postUpdate')->name('post.setting');
		// Slider Yönetimi
		Route::resource('sliders', 'SliderController');
		// Kategoriler
		Route::resource('categories', 'CategoryController');
		Route::post('categorie/sortable', 'CategoryController@postSortable')->name('categorie.sortable');
		// Admins
		Route::resource('admins', 'AdminController');
		// Atöylyeler
		Route::resource('workshops', 'WorkshopController');
		Route::get('/workshop-list', 'WorkshopController@getData')->name('workshops.data');
		Route::get('workshop/disabled', 'WorkshopController@getDisabledWorkshops')->name('disabled.workshops');
		Route::get('workshop/disabled/list', 'WorkshopController@getDisabledData')->name('disabled.workshops.data');
		Route::get('workshop/waiting/list', 'WorkshopController@getWaitingData')->name('waiting.workshops.data');
		Route::post('image/destroy/{id}', 'WorkshopController@imageDestroy');
		Route::post('image/conf/{id}', 'WorkshopController@imageConf');
		Route::post('image/sortable', 'WorkshopController@imageSortable')->name('image.sortable');
		Route::put('create/acc/{id}', 'WorkshopController@createAcc')->name('create.acc');

		Route::post('questions/{id}', 'WorkshopController@postQuestions')->name('post.questions');
		Route::put('images/{id}', 'WorkshopController@postImages')->name('post.images');
		Route::put('details/{id}', 'WorkshopController@postWorkshopDetail')->name('post.details');
		Route::put('gallery/{id}', 'WorkshopController@postGallery')->name('post.gallery');
		Route::put('video/{id}', 'WorkshopController@postVideo')->name('post.video');
		Route::put('other/{id}', 'WorkshopController@postOther')->name('post.other');
		// Teklifler
		Route::resource('offers', 'OfferController');
		// Üyeler
		Route::resource('users', 'UserController');
		// Panel Kullanıcıları
		Route::resource('admins', 'AdminController');
		// Blog
		Route::resource('blogs', 'BlogController');
		Route::post('blog-image-destroy', 'BlogController@imageDestroy')->name('blog.image.destroy');
		Route::resource('blog-categories', 'BlogCategoryController');
		Route::post('blog-categories/sortable', 'BlogCategoryController@postSortable')->name('blog.categorie.sortable');
		Route::resource('blog-comments', 'BlogCommentsController');
		Route::get('blog-comments/active/list', 'BlogCommentsController@getActiveComments')->name('get.active.blog.comments');
		Route::get('blog-comments/active/{id}', 'BlogCommentsController@getActive')->name('blog-comments.active');
		// Yorumlar
		Route::resource('comments', 'CommentsController');
		Route::get('comments/active/list', 'CommentsController@getActiveComments')->name('get.active.comments');
		Route::get('comments/active/{id}', 'CommentsController@getActive')->name('comments.active');
		// Kampanya Yönetimi
		Route::get('campaigns', 'CampaignsController@getIndex')->name('get.campaigns');
		Route::get('campaigns/{id}/edit', 'CampaignsController@getUpdate')->name('get.campaigns.update');
		Route::put('campaigns/{id}/edit', 'CampaignsController@postUpdate')->name('post.campaigns.update');
		// İletişim Formu
		Route::get('contact-form', 'ContactController@getList')->name('get.contact-list');
		Route::post('contact-form/{id}', 'ContactController@getActive');
		Route::delete('contact-form/{id}', 'ContactController@destroy')->name('contact.destroy');
		// Admin Çıkış
		Route::get('logout', 'AuthController@getLogout')->name('get.logout');
	});

	Route::group([ 'middleware' => 'admin_guest' ], function(){
		Route::get('login', 'AuthController@getLogin')->name('admin.get.login');
		Route::post('login', 'AuthController@postLogin')->name('admin.post.login');
	});
});

// Anadomain Routerları (dugunatolyeleri.com || http://dugunatolyeleri.com || www.dugunatolyeleri.com)
// Controller dizini (app/Http/Controllers/Site/..)

Route::group(['namespace' => 'Site', 'domain' =>  env('URL') ], function(){

	Route::get('logdemo', 'PagesController@getLog');

	Route::get('facebook/login', 'HomeController@getFacebookLogin')->name('get.facebook.login');
	Route::get('facebook/login/callback', 'HomeController@handleProviderCallback')->name('redirect.facebook.login');

	Route::get('/', 'HomeController@getIndex')->name('website.home');
	Route::get('tum-atolyeler', 'HomeController@getAllWorkshop')->name('workshop.all');
	// Arama 
	Route::get('search', 'SearchController@getSearch')->name('search');

	// Category
	Route::get('kategori/{slug}', 'CategoryController@getSlug')->name('category.single');
	// Tüm Atölyeler
	Route::get('/atolyeler', 'WorkshopController@getList')->name('workshop.list');
	// Blog
	Route::get('blog', 'BlogController@getBlog')->name('get.blog');
	Route::get('blog/{slug}_{id}', 'BlogController@getDetailBlog')->name('get.blog.detail');
	Route::get('blog/kategori/{slug}', 'BlogController@getCategory')->name('get.blog.category');
	Route::post('blog/{id}/comment', 'BlogController@postBlogComment')->name('add.comment.blog');
	Route::get('blog/search', 'BlogController@postBlogSearch')->name('blog.search');
	// Atölye Detay
	Route::get('/atolye/{slug}', 'WorkshopController@getWorkshop')->name('workshop.single');
	Route::post('workshop/visitor', 'WorkshopController@postWorkshopVisitor')->name('workshop.visitor.count');
	// Yorum işlemleri
	Route::post('comment/{slug}', 'CommentsController@addComment')->name('add.comment');
	Route::delete('comment/{id}', 'CommentsController@deleteComment')->name('comment.destroy');
	// Atölye Beğen
	Route::post('addFavorite', 'FavoritesController@addFavorite')->name('favorite.add');
	Route::post('destroyFavorite', 'FavoritesController@unAuthFavorite')->name('favorite.destroy');
	// Liste Oluştur & Listeye Ekle
	Route::post('list/add/{slug}', 'ListsController@addList')->name('list.add');
	Route::post('list/{slug}', 'ListsController@addListContent')->name('add.list.content');
	// Teklif iste 
	Route::post('offer/{slug}', 'OffersController@addOffer')->name('add.offer.post');

	// Statik sayfalar
	Route::get('sss', 'PagesController@getSss')->name('get.sss');
	Route::get('sss-atolye', 'PagesController@getSssWorkshop')->name('get.sss.workshop');
	Route::get('gizlilik', 'PagesController@getPrivacy')->name('get.privacy');
	Route::get('kosullar', 'PagesController@getTerms')->name('get.terms');
	Route::get('iletisim', 'PagesController@getContact')->name('get.contact');
	Route::post('iletisim', 'PagesController@postContact')->name('post.contact');
	Route::get('biz-kimiz', 'PagesController@getWhoAreWe')->name('get.whoarewe');
});
