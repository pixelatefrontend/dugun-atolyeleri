<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Workshop;
use App\Models\Card;
use App\Models\Bill;
use App\Models\Subscription;
use Carbon\Carbon;

class SubscriptionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:subscription-control';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription Payment Control';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function config()
    {
        $options = new \Iyzipay\Options();
        $options->setApiKey(env('IYZIPAY_API_KEY'));
        $options->setSecretKey(env('IYZIPAY_SECRET_KEY'));
        $options->setBaseUrl(env('IYZIPAY_BASE_URL'));
        return $options;
    }
    
    public function handle()
    {
        $workshops = Workshop::whereType('premium')->with('subscriptions', 'card')->get();
        foreach($workshops as $workshop):
                if($workshop->subscriptions->count() > 0)
                {
                    if($workshop->subscriptions->last()->paymentTime())
                    {
                        if($workshop->subscriptions->last()->cancelled()){
                            $request = new \Iyzipay\Request\CreatePaymentRequest();
                            $request->setLocale(\Iyzipay\Model\Locale::TR);
                            $request->setPrice($workshop->subscriptions->last()->plan->price());
                            $request->setPaidPrice($workshop->subscriptions->last()->plan->price());
                            $request->setCurrency(\Iyzipay\Model\Currency::TL);
                            $request->setInstallment(1);
                            $request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
                            $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);


                            $paymentCard = new \Iyzipay\Model\PaymentCard();
                            $paymentCard->setCardUserKey($workshop->card->card_user_key);
                            $paymentCard->setCardToken($workshop->card->card_token);
                            $request->setPaymentCard($paymentCard);

                            $buyer = new \Iyzipay\Model\Buyer();
                            $buyer->setId($workshop->id);
                            $buyer->setName($workshop->bill->name);
                            $buyer->setSurname($workshop->bill->surname);
                            $buyer->setGsmNumber($workshop->bill->phone);
                            $buyer->setEmail($workshop->bill->email);
                            $buyer->setIdentityNumber($workshop->bill->identity_number);
                            $buyer->setRegistrationAddress($workshop->bill->address);
                            $buyer->setIp($workshop->bill->ip);
                            $buyer->setCity($workshop->bill->city);
                            $buyer->setCountry($workshop->bill->country);
                            $request->setBuyer($buyer);

                            $billingAddress = new \Iyzipay\Model\Address();
                            $billingAddress->setContactName($workshop->member);
                            $billingAddress->setCity($workshop->city);
                            $billingAddress->setCountry("Turkey");
                            $billingAddress->setAddress($workshop->address);
                            $request->setBillingAddress($billingAddress);

                            $basketItems = array();
                            $firstBasketItem = new \Iyzipay\Model\BasketItem();
                            $firstBasketItem->setId($workshop->subscriptions->last()->plan_id);
                            $firstBasketItem->setName($workshop->subscriptions->last()->plan->name);
                            $firstBasketItem->setCategory1($workshop->subscriptions->last()->plan->slug);
                            $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
                            $firstBasketItem->setPrice($workshop->subscriptions->last()->plan->price());
                            $basketItems[0] = $firstBasketItem;
                            $request->setBasketItems($basketItems);

                            $payment = \Iyzipay\Model\Payment::create($request, $this->config());

                            if($payment->getStatus() == 'success'){
                                $subscribeTable = new Subscription;
                                $subscribeTable->plan_id = $workshop->subscriptions->last()->plan_id;
                                $subscribeTable->start_date = Carbon::now();
                                if($workshop->subscriptions->last()->plan->slug == 'monthly'){
                                    $subscribeTable->end_date = Carbon::now()->addMonth();
                                }else if($workshop->subscriptions->last()->plan->slug == 'yearly'){
                                    $subscribeTable->end_date = Carbon::now()->addYear();
                                }
                                $workshop->subscribe()->save($subscribeTable);
                                $this->info($workshop->title. ' : Ödeme Alındı!');
                            }else{
                                $this->info($payment->getErrorMessage());
                            }
                        }else{
                            $workshop->type = 'normal';
                            $workshop->save();
                        }
                    }
                }
        endforeach;
    }
}
