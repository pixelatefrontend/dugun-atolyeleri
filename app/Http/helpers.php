<?php
use Illuminate\Support\Str;

if(! function_exists('stuff_destroy')) {

	function stuff_destroy($params, $title = '') {
		$form = Form::open([ 'method' => 'DELETE', 'route' => $params, 'class' => 'form-options', 'id' => 'delete_stuff']);
		$form .= "<button type='button' onclick='deleteStuff(this)' class='btn btn-trash'>".$title." <i class='fa fa-trash'></i></button>";
		$form .= Form::close();
		return $form;
	}
}

if(! function_exists('confirmComment')) {

	function confirmComment($params) {
		$form = Form::open([ 'method' => 'POST', 'route' => $params, 'class' => 'form-options', 'id' => 'delete_stuff']);
		$form .= "<button type='button' onclick='confirmComment(this)' class='btn btn-edit'> <i class='fa fa-check'></i> Onayla</button>";
		$form .= Form::close();
		return $form;
	}
}

if(! function_exists('disabledComment')) {

	function disabledComment($params) {
		$form = Form::open([ 'method' => 'POST', 'route' => $params, 'class' => 'form-options', 'id' => 'delete_stuff']);
		$form .= "<button type='button' onclick='disabledComment(this)' class='btn btn-warning'><i class='fa fa-eye-slash'></i> Yayından Al </button>";
		$form .= Form::close();
		return $form;
	}
}


if (! function_exists('setActive')) {
	function setActive($path, $active = 'active') {
		return Request::is($path) ? $active : '';
	}
}

if (! function_exists('str_slug')) {
   function str_slug($title, $separator = '-')
	{
	    $title = str_replace(
	        ['ü', 'Ü', 'ö', 'Ö', 'ş', 'Ş'],
	        ['u', 'U', 'o', 'O', 's', 'S'],
	        $title
	    );
	    
	    return Str::slug($title, $separator);
	}
}
