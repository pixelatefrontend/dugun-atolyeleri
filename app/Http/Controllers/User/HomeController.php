<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\City;

class HomeController extends Controller
{
	protected $viewPath = '_user.pages.';

    public function getIndex()
    {
    	$cities = City::pluck('city', 'city');
    	return view($this->viewPath.'dashboard', compact('cities'));
    }
}
