<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ListsController extends Controller
{
	protected $viewPath =  '_user.pages.lists.index';

    public function getLists()
    {
    	$user = Auth::user()->load('lists.workshops.images');
    	return view($this->viewPath, compact('user'));
    }
}
