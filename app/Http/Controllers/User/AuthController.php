<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;
use App\Models\City;
use Carbon\Carbon;
use Mail;
use Socialite;
use Illuminate\Support\Facades\Password;


class AuthController extends Controller
{
    protected $rules = [ 
            'name' => 'required|min:3|alpha_num',
            'lname' => 'required|min:3|alpha_num',
            'phone' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm-password' => 'required|same:password',
            'date' => 'required',
            'city' => 'required',
         ];
	protected $registerURL  = '_user.auth.register';
	protected $loginURL 	= '_user.auth.login';
    protected $resetURL     = '_user.auth.password';

    // Kullanıcı giriş sayfası
    public function getLogin()
    {
    	return view($this->loginURL);
    }

    // Kullanıcı kayıt sayfası
    public function getRegister()
    {
        $cities = City::all();
    	return view($this->registerURL, compact('cities'));
    }

    // Kullanıcı Giriş İşlemleri
    public function postLogin(Request $request)
    {
        $infos = [ 'email' => $request->email, 'password' => $request->password ];
        if(Auth::attempt($infos))
        {
            if(Auth::user()->active == 0) {
                \Session::flash('loginMessage', 'E-mail adresi onaylanmamış');
                return back();
            }
            return redirect()->intended(route('user.dashboard'));
        }else{
            \Session::flash('loginMessage', 'E-mail veya Şifre Hatalı');
            return redirect()->back();
        }
    }

    // Kullanıcı kayıt işlemleri
    public function postRegister(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if($user){  
            $table = User::findOrFail($user->id);
            $table->password = bcrypt($request->password);
            $table->active = 1;
            $table->save();
        }else{
            $this->validate($request, $this->rules);
            $table = new User;
            $table->name = $request->name;
            $table->lname = $request->lname;
            $table->phone = $request->phone;
            $table->email = $request->email;
            $table->password = bcrypt($request->password);
            $table->date = Carbon::createFromFormat('d/m/Y', $request->date);
            $table->city = $request->city;
            $table->active = 1;
            $table->save();
        }
        
        Auth::loginUsingId($table->id);
        return redirect(route('user.dashboard'));
    }

    // Auth sonlandırma
    public function getLogout()
    {
        Auth::logout();
        return redirect(route('user.login'));
    }

    public function confirmation($token)
    {
        $user = User::where('activation_token', $token)->first();
        if(!is_null($user)){
            $user->active = 1;
            $user->activation_token = '';
            $user->save();
            return redirect(route('user.login'))->with('message', 'Üyelik işleminiz başarıyla gerçekleşti');
        }
    }

    public function getFacebookLogin()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('facebook')->user();
        $checkUser = User::where('email', $user->email)->first();
        $member = explode(' ', $user->name);
        if(!$checkUser){
            $table = new User;
            $table->name = $member[0];
            $table->lname = $member[1];
            $table->email = $user->email;
            $table->date = Carbon::now();
            $table->image = $user->avatar;
            $table->active = 1;
            $table->save();
            Auth::login($table);
            return redirect(route('user.dashboard'));
        }else{
            Auth::login($checkUser);
            return redirect(route('user.dashboard'));
        }

        // $user->token;
    }
}
