<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Comment;

class CommentsController extends Controller
{
    protected $viewPath = '_user.pages.comments.index';

    public function getComments()
    {
    	$user = Auth::user()->load('comments.workshop');
		return view($this->viewPath, compact('user'));
    }

    public function commentDestroy($id)
    {
    	$item = Comment::where('id', $id)->delete();
    	return back();
    }
}
