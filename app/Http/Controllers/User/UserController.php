<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Workshop;
use App\Models\Favorite;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
    public function postUpdate(Request $request, $id)
    {
        // dd($request);
    	$table = User::find($id);
    	$table->name = $request->name;
    	$table->lname = $request->lname;
    	$table->phone = $request->phone;
        if($request->password != null) $table->password = bcrypt($request->password);
    	$table->date = Carbon::createFromFormat('d/m/Y', $request->date);
    	$table->city = $request->city;
    	$table->save();
    	\Session::flash('message', 'Kişisel bilgileriniz güncellenmiştir.');
    	return redirect()->back();
    }
    
}
