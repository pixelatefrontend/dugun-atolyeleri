<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Models\User;
use Auth;
//Password Broker Facade
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{

    protected $redirectTo = '/login';

    // trait for handling reset Password
    use ResetsPasswords;

    public function showResetForm(Request $request, $token = null)
    {
        return view('_user.auth.password.reset')->with(
            ['token' => $token, 'email' => $request->email,]
        );
    }

    //returns Password broker of seller
    public function broker()
    {
        return Password::broker('users');
    }

    //returns authentication guard of seller
    protected function guard()
    {
        return Auth::guard('web');
    }
}
