<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class OffersController extends Controller
{
	protected $viewPath = '_user.pages.offers.index';

	public function getOffers()
	{
		$offers = Auth::user()->offers;
		return view($this->viewPath, compact('offers'));
	}
}
