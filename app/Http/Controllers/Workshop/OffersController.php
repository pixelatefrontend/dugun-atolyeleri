<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Offer;
use App\Models\Workshop;


class OffersController extends Controller
{
	protected $viewPath = "_workshop.pages.offers.";

	public function getUser()
	{
		return Auth::guard('workshop')->user();
	}

    public function getIndex()
    {
    	$item = $this->getUser()->load('offers');
        $offers = Offer::where('workshop_id', $this->getUser()->id)->get();
        return view($this->viewPath.'index', compact('item', 'offers'));
    }

    public function destroyOffer($id)
    {
    	$item = Offer::findOrFail($id);
        $item->delete();
        sleep(1);
        return back();
    }

    public function addContract(Request $request)
    {  
        if($request->ajax())
        {
            $offer = Offer::findOrFail($request->id);
            $offer->completed = 1;
            $offer->save();
            return response()->json(['success' => true]);
        }
    }

    public function destroyContract(Request $request)
    {  
        if($request->ajax())
        {
            $offer = Offer::findOrFail($request->id);
            $offer->completed = 0;
            $offer->save();
            return response()->json(['success' => true]);
        }
    }
}
