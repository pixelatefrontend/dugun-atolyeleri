<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Auth;

class CommentsController extends Controller
{
	protected $viewPath = "_workshop.pages.comments.";

	protected function getUser()
	{
		return Auth::guard('workshop')->user();
	}
    public function getIndex()
    {
    	$item = $this->getUser()->load('allComments');
    	return view($this->viewPath.'index', compact('item'));
    }

    public function confirmComment($id)
    {
        $item = Comment::findOrFail($id);
        $item->active = 1;
        $item->save();
        sleep(1);
        return back()->with('message', 'Yorum onaylandı');
    }

    public function disabledComment($id)
    {
        $item = Comment::findOrFail($id);
        $item->active = 0;
        $item->save();
        sleep(1);
        return back()->with('message', 'Yorum yayından alındı');
    }

    public function destroyOffer($id)
    {
    	$item = Comment::findOrFail($id);
        $item->delete();
        sleep(1);
        return back();
    }
}
