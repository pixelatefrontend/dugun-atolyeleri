<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Workshop;
use App\Models\Video;
use Intervention\Image\ImageManagerStatic as Image;

class VideosController extends Controller
{
    protected $viewPath = '_workshop.pages.videos.';

    public function getUser()
    {
        return Auth::guard('workshop')->user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->getUser()->load('videos');
        return view($this->viewPath.'list', compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewPath.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Video;
        $table->workshop_id = $this->getUser()->id;
        $table->description = $request->description;
        $table->url = $request->url;
        $table->site = $request->site;
        $table->active = $request->active;
        if($request->hasFile('videoCover')){
            $images = $request->file('videoCover');
            $fileName = time().rand().'.'.$images->getClientOriginalExtension();
            $filePath = public_path('uploads/workshops/'.$this->getUser()->id.'/');
            $image = Image::make($images->getRealPath());
            \File::exists($filePath) or \File::makeDirectory($filePath);
            $image->resize(600, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
            if($image->height() > 800) {
                $image->crop(600, 800);
            }
            $image->save($filePath . $fileName);
        }else{
            $fileName = '';
        }
        $table->cover = $fileName;
        $table->save();
        return redirect(route('videos.edit', $table->id))->with('message', 'Atölye başarıyla eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Video::findOrFail($id);
        return view($this->viewPath.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Video::findOrFail($id);
        $table->description = $request->description;
        $table->url = $request->url;
        $table->site = $request->site;
        $table->active = $request->active;
        if($request->hasFile('videoCover')){
            $images = $request->file('videoCover');
            $fileName = time().rand().'.'.$images->getClientOriginalExtension();
            $filePath = public_path('uploads/workshops/'.$this->getUser()->id.'/');
            $image = Image::make($images->getRealPath());
            \File::exists($filePath) or \File::makeDirectory($filePath);
            $image->resize(600, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
            if($image->height() > 800) {
                $image->crop(600, 800);
            }
            $image->save($filePath . $fileName);
            $table->cover = $fileName;
        }
        $table->save();
        return back()->with('message', 'Video başarıyla düzenlendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Video::findOrFail($id);
        $item->delete();
        sleep(1);
        return back()->with('message', 'Video silme işlemi başarılı');
    }
}
