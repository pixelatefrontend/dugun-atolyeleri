<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\City;
use App\Models\Workshop;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Service;
use App\Models\Map;
use App\Models\Question;

class GeneralsController extends Controller
{
	protected $viewPath = "_workshop.pages.generals.";

	protected function getID()
	{
		return Auth::guard('workshop')->user()->id;
	}

    public function getIndex()
    {
    	$item = Auth::guard('workshop')->user()->load('services', 'questions', 'map');
    	$cities = City::pluck('city', 'city');
    	return view($this->viewPath.'index', compact('item', 'cities'));
    }

    public function postGeneralUpdate(Request $request)
    {
    	$item = Workshop::findOrFail($this->getID());
    	$item->title = $request->title;
    	$item->instagram_username = $request->instagram_username;
    	$item->description = $request->description;
    	$item->email = $request->email;
    	$item->city = $request->city;
    	$item->save();

    	return redirect()->back()->with('message', 'Bilgileriniz başarıyla düzenlendi.');
    }

    public function postImageUpdate(Request $request)
    {
    	$item = Workshop::findOrFail($this->getID());
    	$filePath = public_path('uploads/workshops/'.$item->id.'/');
        \File::exists($filePath) or \File::makeDirectory($filePath);
        // AVatar
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $fileNameAvatar = time().rand().'.'.$avatar->getClientOriginalExtension();
            $avatarImage = Image::make($avatar->getRealPath());
            $avatarImage->resize(150, null, function($constraint){
                $constraint->aspectRatio();
            })->save($filePath . $fileNameAvatar);
            $item->avatar = '/uploads/workshops/'.$item->id.'/'.$fileNameAvatar;
            $item->save();
        }
        // Cover
        if($request->hasFile('cover')){
            $cover  = $request->file('cover');
            $fileNameCover = time().rand().'.'.$cover->getClientOriginalExtension();
            $coverImage  = Image::make($cover->getRealPath());
            $item->cover = '/uploads/workshops/'.$item->id.'/'. $fileNameCover;
            $coverImage->save($filePath . $fileNameCover);
            $item->save();
        }
        return back()->with('message', 'Bilgileriniz başarıyla düzenlendi.');
    }

    public function postDetailUpdate(Request $request)
    {
    	$item = Workshop::findOrFail($this->getID());
    	$item->member = $request->member;
        $item->address = $request->address;
        $item->phone = $request->phone;
        $item->save();
        if($item->services === null)
        {
            $services = new Service;
            $services->price = $request->price;
            $services->delivery_time = $request->delivery_time;
            $item->services()->save($services);
        }else{
            $item->services()->update([ 'price' => $request->price, 'delivery_time' => $request->delivery_time ]);
        }

        if($item->map === null)
        {
            $map = new Map;
            $map->x = $request->x;
            $map->y = $request->y;
            $item->map()->save($map);
        }else{
            $item->map()->update([ 'x' => $request->x, 'y' => $request->y ]);
        }
        return back()->with('message', 'Bilgileriniz başarıyla düzenlendi.');
    }

    public function postQuestionsUpdate(Request $request)
    {
        $ids = explode(',', $this->getID());
        foreach($ids as $id){
            $item = Question::where('workshop_id', $id);
            try {
                $item->delete();
            } catch (Exception $e) {
                return Misc::exceptionalJson($e);
            }
        }
        if($request->has('questions')){
            foreach ($request->questions as $key => $v) {
                $data = array(
                    'workshop_id' => $id,
                    'question' => $request->questions[$key],
                    'reply' => $request->replies[$key],
                    );
               Question::insert($data);
            }
        }

        \Session::flash('message', 'Atölye başarıyla düzenlendi!');
         return back();
    }
}
