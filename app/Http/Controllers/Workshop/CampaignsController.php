<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Campaign;

class CampaignsController extends Controller
{

    protected $viewPath = '_workshop.pages.campaigns.';

    protected function getUser()
    {
        return Auth::guard('workshop')->user();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = $this->getUser()->load('campaigns');
        return view($this->viewPath.'list', compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewPath.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Campaign;
        $table->workshop_id = $this->getUser()->id;
        $table->title = $request->title;
        $table->discount = $request->discount;
        $table->active = $request->active;
        $table->end_date = $request->end_date;
        $table->content = $request->content;
        $table->save();
        session()->flash('message', 'Kampanya başarıyla eklendi!');
        return redirect(route('campaigns.edit', $table->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Campaign::where('workshop_id', $this->getUser()->id)->first();
        return view($this->viewPath.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Campaign::findOrFail($id);
        $table->title = $request->title;
        $table->discount = $request->discount;
        $table->active = $request->active;
        $table->end_date = $request->end_date;
        $table->content = $request->content;
        $table->save();
        session()->flash('message', 'Kampanya başarıyla düzenlendi!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Campaign::findOrFail($id)->delete(); 
        sleep(1);
        return back();
    }
}
