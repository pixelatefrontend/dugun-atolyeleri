<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Workshop;
use App\Models\Category;
use App\Models\City;
use Mail;

class AuthController extends Controller
{
    protected $rules = [
            'title' => 'required|unique:workshops|max:80',
//            'instagram_username' => 'required|alpha_num|regex:/^[\pL\s\-]+$/u',
            'instagram_username' => 'required',
//            'name' => 'required|alpha_num|min:3|max:120',
            'name' => 'required|min:3|max:120',
            'lname' => 'required|max:25',
            'number' => 'required',
            'email' => 'required|email|unique:workshops,email',
            'city' => 'required',
    ];
    protected $validateMessage = [
        'email.unique' => 'Bu E-mail adresi daha önceden alınmış.',
        'title' => 'Bu Atölye adı daha önceden alınmış',
    ];
	protected $viewPath = '_workshop.auth.';

    public function getLogin()
    {
    	return view($this->viewPath.'login');
    }

    public function getRegister()
    {
    	$categories = Category::active()->get();
        $cities = City::all();
    	return view($this->viewPath.'register', compact('categories', 'cities'));
    }

    public function postLogin(Request $r)
    {
      $credentials = [ 'username' => $r->username, 'password' => $r->password, 'active' => 1 ];
      if(Auth::guard('workshop')->attempt($credentials))
        {
            return redirect(route('workshop.dashboard'));
        }
        return back()->with('message', 'Kullanıcı Adı veya Şifre hatalı');
    }
    
    public function postRegister(Request $request)
    {
        $this->validate($request, $this->rules, $this->validateMessage);
        $table = new Workshop;
        $table->type = 'normal';
        $table->title = $request->title;
        $table->slug = str_slug($request->title);
        $table->instagram_username = $request->instagram_username;
        $table->member = $request->name.' '.$request->lname;
        $table->phone = $request->phone;
        $table->email = $request->email;
        $table->password = bcrypt('secret');
        $table->city = $request->city;
        $table->address = $request->address;
        $table->active = 2;
        $table->save();
        $table->categories()->sync($request->workshop_categories);
        $data = $table->toArray();
        Mail::send('emails.workshop_confirmation', $data, function($message) use($data){
            $message->to($data['email']);
            $message->subject('Düğün Atölyelerine Hoşgeldiniz');
        });

        \Session::flash('workshop_register', 'Kaydınız alınmıştır. Teşekkür Ederiz');
        return redirect(route('website.home'));
    }

    public function getLogout()
    {
        Auth::guard('workshop')->logout();
        return redirect(route('website.home'));
    }
}
