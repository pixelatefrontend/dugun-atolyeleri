<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\User;

class HomeController extends Controller
{
	protected $indexPath = "_workshop.pages.index";

    public function getIndex()
    {
        $visitors = [];
        $data_users = [];
    	$workshop = Auth::guard('workshop')->user()->load('offers','comments', 'visitors', 'lists');

        foreach($workshop->lists as $list)
        {
            $users = User::where('id', $list->user_id)->get();
            foreach($users as $user)
            {
                $data_users[] = $user;
            }
        }
    	
    	foreach($workshop->visitors as $data)
    	{
    		$visitors[] = [ 'date' => $data->visitor_date->format('Y-m-d'), 'value' => $data->count ];
    	}

    	return view($this->indexPath, compact('workshop', 'visitors', 'data_users'));
    }
}
