<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Image as Gallery;
use App\Models\Workshop;
use Intervention\Image\ImageManagerStatic as Image;


class GalleryController extends Controller
{
    protected $viewPath = '_workshop.pages.gallery.index';

    public function getUser()
    {
    	return Auth::guard('workshop')->user();
    }

    public function getIndex()
    {
    	$item = $this->getUser()->load('images');
    	return view($this->viewPath, [ 'item' => $item ]);
    }

    public function postUpdate(Request $request)
    {
    	$item = Workshop::findOrFail($this->getUser()->id);
    	$images = $request->file('file');
       
            $fileName = time().rand().'.'.$images->getClientOriginalExtension();
            $filePath = public_path('uploads/workshops/'.$item->id.'/');
            $image = Image::make($images->getRealPath());
            \File::exists($filePath) or \File::makeDirectory($filePath);
            $image->resize(600, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
            if($image->height() > 800) {
                $image->crop(600, 800);
            }
            $image->save($filePath . $fileName);
            $data = array(
                    'workshop_id' => $item->id,
                    'filename' => $fileName,
                );
            Gallery::insert($data);

        return response()->json(['success' => true]);
    }

    public function imageDestroy(Request $request, $id)
    {
        if($request->ajax()) {
            $item = Gallery::find($id)->delete();
            return response()->json([ 
                'success' => true,
             ]);
        }
    }

    public function imageSortable(Request $request)
    {
        $updateRecordsArray = $request->order;
        $i = 1;
 
        foreach ($updateRecordsArray as $recordID) {
            Gallery::where('id', '=', $recordID)->update(array('sort' => $i));
            $i++;
        }
        return \Response::json('ok');
    }

    public function imageConf(Request $request, $id)
    {
        if($request->ajax()) {
            $item = Workshop::findOrFail($id);
            $item->first_picture = $request->path;
            $item->save();
            return response()->json([
                'success' => true
            ]);
        }
    }

    public function postImageDetail(Request $request)
    {
        if($request->ajax())
        {
            $table = Gallery::findOrFail($request->image_id);
            $table->title = $request->title;
            $table->code = $request->code;
            $table->description = $request->description;
            $table->save();
            return response()->json([ 'success' => true ]);
        }
    }
}
