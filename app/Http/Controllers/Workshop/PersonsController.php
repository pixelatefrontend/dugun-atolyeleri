<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PersonsController extends Controller
{
    protected $viewPath = "_workshop.pages.persons.";

    public function getIndex()
    {
    	return view($this->viewPath.'index');
    }
}
