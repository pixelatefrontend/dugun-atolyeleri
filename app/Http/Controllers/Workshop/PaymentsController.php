<?php

namespace App\Http\Controllers\Workshop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use Iyzico\IyzipayLaravel\StorableClasses\Address;
use Iyzico\IyzipayLaravel\StorableClasses\BillFields;
use App\Models\Workshop;
use App\Models\Plan;
use App\Models\Bill;
use App\Models\Card;
use App\Models\Subscription;
use Carbon\Carbon;
use Auth;

class PaymentsController extends Controller
{
	protected $viewPath = '_workshop.pages.payment.';

	protected function getUser()
	{
		return Auth::guard('workshop')->user();
	}

	protected function config()
	{
		$options = new \Iyzipay\Options();
		$options->setApiKey(env('IYZIPAY_API_KEY'));
		$options->setSecretKey(env('IYZIPAY_SECRET_KEY'));
		$options->setBaseUrl(env('IYZIPAY_BASE_URL'));
		return $options;
	}

	protected function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function getPackages()
	{
		session()->forget('package');
		$plans = Plan::all();
		return view($this->viewPath.'index', compact('plans'));
	}

	public function postPackages(Request $request)
	{
		session()->put(['plan' => $request->plan, 'plan_id' => $request->plan_id]);
		return redirect(route('get.bill'));
	}

	public function getBill()
	{
		return view($this->viewPath.'bill');
	}

	public function postBill(Request $request)
	{
		$workshop = Workshop::find($this->getUser()->id);
		$table = new Bill;
		$table->name = $request->name;
		$table->surname = $request->surname;
		$table->address = $request->address;
		$table->identity_number = $request->identity_number;
		$table->email = $request->email;
		$table->phone = $request->phone;
		$table->country = $request->country;
		$table->city = $request->city;
		$table->ip = $request->ip();
		$workshop->bill()->save($table);

		return redirect(route('get.card.form'));
	}

	public function getCardForm()
	{
		return view($this->viewPath.'payment');
	}

	public function postCard(Request $r)
	{
		$request = new \Iyzipay\Request\CreatePaymentRequest();
		$request->setLocale(\Iyzipay\Model\Locale::TR);
		$request->setPrice("1");
		$request->setPaidPrice("1");
		$request->setCurrency(\Iyzipay\Model\Currency::TL);
		$request->setInstallment(1);
		$request->setPaymentChannel(\Iyzipay\Model\PaymentChannel::WEB);
		$request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
		$request->setCallbackUrl(route('subscribe.success'));

		$paymentCard = new \Iyzipay\Model\PaymentCard();
		$paymentCard->setCardHolderName($r->card_holder_name);
		$paymentCard->setCardNumber($r->card_number);
		$paymentCard->setExpireMonth($r->expire_month);
		$paymentCard->setExpireYear($r->expire_year);
		$paymentCard->setCvc($r->cvc);
		$paymentCard->setRegisterCard(1);
		$request->setPaymentCard($paymentCard);

		$buyer = new \Iyzipay\Model\Buyer();
		$buyer->setId($this->getUser()->id);
		$buyer->setName($this->getUser()->bill->name);
		$buyer->setSurname($this->getUser()->bill->surname);
		$buyer->setGsmNumber($this->getUser()->bill->phone);
		$buyer->setEmail($this->getUser()->email);
		$buyer->setIdentityNumber($this->getUser()->bill->identity_number);
		$buyer->setRegistrationAddress($this->getUser()->bill->address);
		$buyer->setIp($this->getUser()->bill->ip);
		$buyer->setCity($this->getUser()->bill->city);
		$buyer->setCountry("Turkey");
		$request->setBuyer($buyer);

		$billingAddress = new \Iyzipay\Model\Address();
		$billingAddress->setContactName($this->getUser()->member);
		$billingAddress->setCity($this->getUser()->city);
		$billingAddress->setCountry("Turkey");
		$billingAddress->setAddress($this->getUser()->address);
		$request->setBillingAddress($billingAddress);

		$basketItems = array();
		$firstBasketItem = new \Iyzipay\Model\BasketItem();
		$firstBasketItem->setId(session()->get('plan_id'));
		if(session()->has('plan') == 'monthly'){
			$firstBasketItem->setName("Aylık Premium");
		}else{
			$firstBasketItem->setName("Yıllık Premium");
		}
		$firstBasketItem->setCategory1('Premium');
		$firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::VIRTUAL);
		$firstBasketItem->setPrice("1");
		$basketItems[0] = $firstBasketItem;
		$request->setBasketItems($basketItems);
		$threedsInitialize = \Iyzipay\Model\ThreedsInitialize::create($request, $this->config());

		if($threedsInitialize->getStatus() == 'success')
		{
			return $threedsInitialize->getHtmlContent();
		}

	}

	public function getSubscribeSuccess(Request $r)
	{
		# create request class
		$request = new \Iyzipay\Request\CreateThreedsPaymentRequest();
		$request->setLocale(\Iyzipay\Model\Locale::TR);
		$request->setConversationId($r->consversationId);
		$request->setPaymentId($r->paymentId);
		$request->setConversationData($r->consversationData);
		# make request
		$card = \Iyzipay\Model\ThreedsPayment::create($request, $this->config());
		# print result
		if($card->getStatus() == 'success'){
			$workshop = Workshop::find($this->getUser()->id);
			$cardTable = new Card;
			$cardTable->card_user_key = $card->getCardUserKey();
			$cardTable->card_token = $card->getCardToken();
			$cardTable->bin_number = $card->getBinNumber();
			$cardTable->card_association = $card->getCardAssociation();
			$cardTable->card_family = $card->getCardFamily();
			$workshop->card()->save($cardTable);
			// 
			$workshop->type = 'premium';
			$workshop->save();
			//
			$subscribeTable = new Subscription;
			$subscribeTable->plan_id = session()->get('plan_id');
			$subscribeTable->start_date = Carbon::now();
			$subscribeTable->end_date = Carbon::now()->addMonth();
			$workshop->subscribe()->save($subscribeTable);
			return view($this->viewPath.'success');
		}else{
			return $card->getErrorMessage();
		}
	}
}
