<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Favorite;
use App\Models\Workshop;
use Auth;

class FavoritesController extends Controller
{
    public function addFavorite(Request $request)
    {
    	if($request->ajax()) {
            if(Auth::check()) {
                $isFavorited = Favorite::where('user_id', Auth::user()->id)->where('workshop_id', $request->id)->exists();
                if(!$isFavorited) {
                    Auth::user()->favorites()->attach($request->id);
                    Workshop::where('id', $request->id)->increment('like');

                    return response()->json([ 'success' => true, 'action' => 'add']);
                }else{
                    Auth::user()->favorites()->detach($request->id);
                    Workshop::where('id', $request->id)->decrement('like');

                    return response()->json([ 'success' => true, 'action' => 'remove' ]);
                }
            }else{
                Workshop::where('id', $request->id)->increment('like');
                return response()->json([ 'success' => true, 'action' => 'add' ]);
            }
    	}
    }

    public function unAuthFavorite(Request $request){
         Workshop::where('id', $request->id)->decrement('like');
         return response()->json([ 'succes' => true, 'action' => 'remove' ]);
    }

}
