<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Models\Slider;
use App\Models\Workshop;
use App\Models\Category;
use Auth;
use Socialite;

class HomeController extends Controller
{
    public function getIndex(Request $request)
    {
        if($request->has('q')) {
            $workshops =  Workshop::where('title', 'LIKE', "%$request->q%")->with(['images', 'videos', 'favorites'])->paginate(12);
            return view('_website.pages.search.list', compact('workshops'));
        }

		$sliders = Cache::remember('sliders', 60, function(){
        	return Slider::active()->get();
        });

        // $workshops = Cache::remember('workshops', 1, function(){
        // 	return Workshop::active()->inRandomOrder()->get();
        // });

        $queryOrder = "CASE WHEN type = 'premium' THEN 1 ";
        $queryOrder .= "WHEN type = 'gold' THEN 2 ";
        $queryOrder .= "ELSE 3 END";
        $workshops =  Workshop::active()
                                ->with(['latestImage', 'videos', 'favorites', 'images'])
                                ->orderByRaw($queryOrder)
                                ->orderBy('point', 'DESC')
                                ->paginate(12);
                   
        // $popularWorkshops = Workshop::active()
        //                     ->where('type', 'premium')
        //                     ->popular()
        //                     ->with(['latestImage', 'videos', 'favorites', 'images'])
        //                     ->inRandomOrder()
        //                     ->take(6)
        //                     ->get();
        return view('_website.home', compact('sliders', 'workshops'));
    }

    public function getAllWorkshop()
    {

        $categories = Category::with(['workshops.images', 'workshops.videos','workshops.favorites'])->take(12)->get();

        return view('_website.pages.workshops.list', compact('categories'));
    }

    public function getFacebookLogin()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        dd($user);

        // $user->token;
    }

    
}
