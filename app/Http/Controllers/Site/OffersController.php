<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Workshop;
use Carbon\Carbon;
use Auth;
use Mail;
class OffersController extends Controller
{
    public function addOffer(Request $request, $slug)
    {
    	$item = Workshop::where('slug', $slug)->first();
    	if($request->ajax()) {
    		if(!Auth::check()) {
                $user = User::where('email', $request->email)->first();
                if($user){
                    $user->offers()->attach($item->id, ['description' => $request->description, 'image_id' => $request->product]);
                    $data = array(
                        'member' => $item->member,
                        'name' => $user->name, 
                        'lname' => $user->lname, 
                        'phone' => $user->phone, 
                        'email' => $user->email, 
                        'date' => $user->date,
                        'description' => $request->description
                    );
                    Mail::send('emails.offers', $data , function($message) use($data, $item){
                        $message->to($item->email);
                        $message->subject('Düğün Atölyeleri | Yeni Teklif Talebi');
                    });
                    return response()->json([ 'success' => true ]);
                }else{
                    $table = new User;
                    $table->name = $request->firstname;
                    $table->lname = $request->lastname;
                    $table->phone = $request->phone;
                    $table->email = $request->email;
                    $table->date = Carbon::createFromFormat('d/m/Y', $request->date);
                    $table->password = \Hash::make('secret');
                    $table->save();
                    $table->offers()->attach($item->id, ['description' => $request->description, 'image_id' => $request->product]);
                    $data = array(
                        'member' => $item->member,
                        'name' => $table->name, 
                        'lname' => $table->lname, 
                        'phone' => $table->phone, 
                        'email' => $table->email, 
                        'date' => $table->date,
                        'image_id' => $table->image_id,
                        'description' => $request->description
                    );
                    Mail::send('emails.offers', $data , function($message) use($data, $item){
                        $message->to($item->email);
                        $message->subject('Düğün Atölyeleri | Yeni Teklif Talebi');
                    });
                    return response()->json([ 'success' => true ]);
                }
    			
    		}else{
	    		Auth::user()->offers()->attach($item->id, ['description' => $request->description, 'image_id' => $request->product]);
                $user = Auth::user();
                $data = array(
                    'member' => $item->member,
                    'name' => $user->name, 
                    'lname' => $user->lname, 
                    'phone' => $user->phone, 
                    'email' => $user->email, 
                    'date' => $user->date,
                    'description' => $request->description,
                );
                Mail::send('emails.offers', $data, function($message) use($data, $item){
                    $message->to($item->email);
                    $message->subject('Düğün Atölyeleri | Yeni Teklif Talebi');
                });
	    		return response()->json([ 'success' => true ]);
    		}
    	}
    }
}
