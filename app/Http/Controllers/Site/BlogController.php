<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory as Category;
use App\Models\BlogComment as Comment;
use Auth;


class BlogController extends Controller
{
    protected $viewPath = '_website.pages.blog.';

    public function getBlog(Request $request)
    {
        if($request->has('ara')) {
            $items =  Blog::where('title', 'LIKE', "%$request->ara%")->active()->with('comments')->get();
            return view($this->viewPath.'search', compact('items'));
        }
    	$items = Blog::active()->orderBy('created_at', 'DESC')->with(['comments' => function ($query) {
            $query->where('active', 1);
        }])->get();
    	return view($this->viewPath.'list', compact('items'));
    }

    public function getDetailBlog($slug, $id)
    {
    	$item = Blog::where('id', $id)->where('slug', $slug)->with(['comments' => function ($query) {
            $query->where('active', 1);
        }])->firstOrFail();
        $posts = Blog::active()->where('id', '!=', $item->id)->where('blog_category_id', $item->blog_category_id)->with('comments')->inRandomOrder()->take(3)->get();
        $item->addView();
    	return view($this->viewPath.'detail', compact('item', 'posts'));
    }

    public function getCategory($slug)
    {
        $item = Category::where('slug', $slug)->with(['blogs' => function ($query) {
            $query->where('active', 1)->with('comments');
        }])->firstOrFail();
        return view($this->viewPath.'category', compact('item'));
    }

    public function postBlogComment(Request $request, $id)
    {
        if($request->ajax()){
            $table = new Comment;
            $table->blog_id = $id;
            $table->user_id = Auth::check() ? Auth::user()->id : null;
            $table->name = $request->name;
            $table->lname = $request->lname;
            $table->email = $request->email;
            $table->title = $request->title;
            $table->content = $request->content;
            $table->save();
            return response()->json([ 'success' => true ]);
        }
    }

    public function postBlogSearch(Request $request)
    {
        if($request->ajax()){
            $blogs = Blog::active()->where('title', 'LIKE', '%'.$request->term.'%')->get();
            $items = [];

            foreach($blogs as $blog):
                $items[] = [ 'id' => $blog->id, 'title' => $blog->title, 'url' => route('get.blog.detail', ['slug' => $blog->slug, 'id' => $blog->id])];
            endforeach;

            return response()->json(['items' => $items]);
        }
    }
}
