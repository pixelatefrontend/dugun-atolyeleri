<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Moment;
use App\Models\Workshop;
use Auth;

class ListsController extends Controller
{
    public function addList(Request $request, $slug)
    {

        $workshop = Workshop::where('slug', $slug)->first();

    	$table = new Moment;
    	$table->user_id = Auth::user()->id;
    	$table->title = $request->list_title;
    	$table->description = $request->list_description;
    	$table->save();
        $table->workshops()->attach($workshop->id);
    	return redirect()->back();
    }

    public function addListContent($slug, Request $request)
    {

    	$table = Moment::where('id', $request->list_id)->first();
    	$workshop = Workshop::where('slug', $slug)->first();
    	
    	if($request->ajax())
    	{
    		$table->workshops()->attach($workshop->id);
    		return response()->json(['success' => true, 'message' => 'Eklendi' ]);
    	}
    }
}
