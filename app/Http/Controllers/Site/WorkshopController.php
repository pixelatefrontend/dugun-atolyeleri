<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workshop;
use Event;
use App\Models\Visitor;
use Carbon\Carbon;

class WorkshopController extends Controller
{
	protected $viewPath = '_website.pages.workshops.';

	public function getList()
	{
		$items = Workshop::active()->with('images', 'videos', 'latestImage', 'favorites')->get();
		return view($this->viewPath.'list', compact('items'));	
	}

    public function getWorkshop($slug)
    {
    	$item =  Workshop::where('slug', $slug)->active()->with('comments', 'images', 'latestImage', 'videos', 'favorites')->firstOrFail();
        $data = array_merge(['images' => $item->images], ['videos' => $item->videos]);

        // foreach($data as $image):
        //     echo $image."<br>";
        // endforeach;
    	if(!$item) abort(404);
    	$item->addView();
    	return view($this->viewPath.'single', compact('item', 'data'));
    }

    public function postWorkshopVisitor(Request $request)
    {
        if($request->ajax())
        {
            $visitor = Visitor::where('workshop_id', $request->workshop_id)->orderBy('id', 'DESC')->first();
            if($visitor && $visitor->visitor_date->format('d.m.Y') == Carbon::now()->format('d.m.Y'))
            {
                $visitor->increment('count');
            }else{
                $table = new Visitor;
                $table->workshop_id = $request->workshop_id;
                $table->count = 1;
                $table->visitor_date = Carbon::now();
                $table->save();
            }
        }
    }
}
