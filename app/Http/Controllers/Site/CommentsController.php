<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;
use Auth;
use App\Models\Workshop;

class CommentsController extends Controller
{
    protected $rules = [ 
        'title' => 'required',
        'female' => 'required|min:3',
        'male' => 'required|min:3',
        'content' => 'required'
    ];
    public function addComment($slug, Request $request)
    {
            $workshop_id = Workshop::where('slug', $slug)->first()->id;
            if($request->ajax()){
                $table = new Comment;
                $table->user_id = Auth::check() ? Auth::user()->id : null;
                $table->workshop_id = $workshop_id;
                $table->male = $request->male;
                $table->female = $request->female;
                $table->title = $request->title;
                $table->content = $request->content;
                $table->like = 0;
                $table->active = 0;
                $table->save();
                return response()->json([ 'success' => true, 'message' => 'Yorumunuz incelendikten sonra yayınlanacaktır!' ]);
            }
    }
}
