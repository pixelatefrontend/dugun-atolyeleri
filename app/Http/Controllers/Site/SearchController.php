<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Workshop;

class SearchController extends Controller
{
    public function getSearch(Request $request)
    {
    	if($request->ajax()) {
    		$itemsW = array();
    		$itemsC = array();

    		$workshops = Workshop::active()
    				->where('title', 'LIKE', '%'.$request->term.'%')
					->get();

			$categories = Category::active()
					->where('title', 'LIKE', '%'.$request->term.'%')
					->get();


			foreach ($workshops as $item)
			{
			    $itemsW[] = [ 'id' => $item->id, 'title' => $item->title, 'url' => route('workshop.single', $item->slug)];
			}

			foreach($categories as $item)
			{
				$itemsC[] = [ 'id' => $item->id, 'title' => $item->title, 'url' => route('category.single', $item->slug) ];
			}
			return response()->json(['workshops' => $itemsW, 'categories' => $itemsC]);
    	}
    }

    public function getSearchFilter(Request $request)
    {
    	if($request->has('q')) {
    		$workshops =  Workshop::active()->where('title', 'LIKE', "%$request->q%")->with(['images', 'latestImage', 'videos', 'favorites'])->get();
    		return view('_website.pages.search.list', compact('workshops'));
    	}
    }
}
