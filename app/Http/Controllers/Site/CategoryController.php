<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Workshop;

class CategoryController extends Controller
{
	// Kategori dosya yolu (resources/view/_website/pages/categories/)
	protected $viewPath = '_website.pages.categories.';

	// Kategori ile birlikte gelen kategoriye ait olan atölyeler..
    public function getSlug($slug)
    {
      $queryOrder = "CASE WHEN type = 'premium' THEN 1 ";
      $queryOrder .= "WHEN type = 'gold' THEN 2 ";
      $queryOrder .= "ELSE 3 END";
    	$item = Category::where('slug', $slug)->firstOrFail();
   //  	$popularWorkshops = Workshop::active()->popular()->with('images', 'latestImage', 'videos', 'categories')->whereHas('categories', function($query) use ($slug) {
			// 	$query->where('slug', '=', $slug);
			// })->inRandomOrder()->take(6)->get();

   //  	$workshops = \Cache::remember('workshops', 60, function() use ($slug) {
   //  		return Workshop::active()->inRandomOrder()->with('images', 'videos', 'categories')->whereHas('categories', function($query) use ($slug) {
			// 	$query->where('slug', '=', $slug);
			// })->paginate(12);
   //  	});
        $workshops = Workshop::active()->orderByRaw($queryOrder)->with('images', 'latestImage', 'videos', 'categories')->whereHas('categories', function($query) use ($slug) {
                        $query->where('slug', '=', $slug);
                    })->paginate(11);

    	return view($this->viewPath.'list', compact('item', 'workshops'));
    }

}
