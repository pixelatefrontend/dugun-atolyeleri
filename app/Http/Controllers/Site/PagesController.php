<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Support\Facades\Redis;


class PagesController extends Controller
{

	protected $viewPath = '_website.pages.static.';

    public function getSss()
    {
        return view($this->viewPath.'sss');
    }

    public function getSssWorkshop()
    {
        return view($this->viewPath.'ass');
    }

    public function getContact()
    {
        return view($this->viewPath.'contact');
    }

    public function getPrivacy()
    {
        return view($this->viewPath.'privacy');
    }

    public function getTerms()
    {
        return view($this->viewPath.'terms');
    }

    public function getWhoAreWe()
    {
        return view($this->viewPath.'whoarewe');
    }

    public function getBlog()
    {
        return view($this->viewPath.'blog');
    }

    public function postContact(Request $request)
    {
        if($request->ajax()){
            $table = new Contact;
            $table->name = $request->name;
            $table->email = $request->email;
            $table->message = $request->message;
            $table->save();
            return response()->json([ 'success' => true ]);
        }
    }

    public function getLog()
    {
        
    }
}
