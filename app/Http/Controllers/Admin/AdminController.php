<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Role;

class AdminController extends Controller
{

    protected $viewPath = '_admin.pages.admins.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Admin::with('role')->get();

        return view($this->viewPath.'list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get()->pluck('name', 'id');
        return view($this->viewPath.'create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Admin;
        $table->role_id = $request->role;
        $table->name = $request->name;
        $table->email = $request->email;
        $table->password = bcrypt($request->password);
        $table->job_title = $request->job_title;
        $table->save();
        return redirect(route('admins.edit', $table->id))->with('message', 'Kullanıcı ekleme işlemi başarılı!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Admin::findOrFail($id);
        $roles = Role::get()->pluck('name', 'id');
        return view($this->viewPath.'update', compact('item', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Admin::findOrFail($id);
        $table->role_id = $request->role;
        $table->name = $request->name;
        $table->email = $request->email;
        $table->password = bcrypt($request->password) ? bcrypt($request->password) : $table->password;
        $table->job_title = $request->job_title;
        $table->save();
        return back()->with('message', 'Kullanıcı düzenleme işlemi başarılı!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();
        sleep(1);
        return redirect()->back();
    }
}
