<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vinkla\Backup\Backup;

class BackupController extends Controller
{
    public function getBackup()
    {
    	$backup = new Backup;
		$backup->run();
    }
}
