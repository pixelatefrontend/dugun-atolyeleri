<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Campaign;

class CampaignsController extends Controller
{
    protected $viewPath = '_admin.pages.campaigns.';

    public function getIndex()
    {
    	$items = Campaign::with('workshop')->get();
    	return view($this->viewPath.'list', compact('items'));
    }

    public function getUpdate($id)
    {
    	$item = Campaign::findOrFail($id);
    	return view($this->viewPath.'update', compact('item'));
    }

    public function postUpdate(Request $request, $id)
    {
    	$table = Campaign::findOrFail($id);
    	$table->title = $request->title;
        $table->discount = $request->discount;
        $table->active = $request->active;
        $table->end_date = $request->end_date;
        $table->content = $request->content;
        $table->save();
        session()->flash('message', 'Kampanya başarıyla düzenlendi!');
        return back();
    }
}
