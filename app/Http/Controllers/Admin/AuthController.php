<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Admin;
use Auth;

class AuthController extends Controller
{
    protected $registerPath  = '_admin.auth.register';
	protected $loginPath 	= '_admin.auth.login';
    	
    public function getLogin()
    {
    	return view($this->loginPath);
    }

    public function getRegister()
    {
    	return view($this->registerPath);
    }

    public function postLogin(Request $r)
    {

        $credentials = [ 'email' => $r->email, 'password' => $r->password ];
        if(Auth::guard('admin')->attempt($credentials))
        {            
            return redirect()->intended(route('admin.dashboard'));
        }
        return back()->withInput($r->only('email', 'password'));
    }

    public function getLogout()
    {
        Auth::guard('admin')->logout();
        return redirect(route('admin.get.login'));
    }
}
