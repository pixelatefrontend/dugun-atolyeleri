<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workshop;

class CategoryController extends Controller
{
    protected $rules = [
        'title' => 'required',
        'slug' => 'required',
        'cover_picture' => 'required',
        'icon_image' => 'required',
        'description' => 'required',
    ];

    protected $viewURL = '_admin.pages.categories.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Category::orderBy('sort', 'ASC')->get();
        return view($this->viewURL.'list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workshops = Workshop::orderBy('title', 'ASC')->pluck('title', 'id');
        return view($this->viewURL.'create', compact('workshops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);

        $table = new Category;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->description = $request->description;
        $table->keywords = $request->keywords;
        $table->cover_picture = $request->cover_picture;
        $table->icon = $request->icon_image;
        $table->workshop_id = $request->workshop;
        $table->active = $request->active == "1" ? true : false;
        $table->save();

        \Session::flash('message', 'Kategori Başarıyla Eklendi');
        return redirect(route('categories.edit', $table->id));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        /*$item = Category::findOrFail($category);
        return view('_admin/pages/categories/update', compact($item));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $item = Category::findOrFail($category->id);
        $workshops = Workshop::orderBy('title', 'ASC')->pluck('title', 'id');
        return view($this->viewURL.'update', compact('item', 'workshops'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validate(request(), $this->rules);

        $table = Category::findOrFail($category->id);
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->description = $request->description;
        $table->keywords = $request->keywords;
        $table->cover_picture = $request->cover_picture;
        $table->icon = $request->icon_image;
        $table->workshop_id = $request->workshop;
        if($request->active == "1"){
            $table->active = true;
        }else{
            $table->active = false;
        }
        $table->save();

        \Session::flash('message', 'Kategori Başarıyla Düzenlendi');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        sleep(1);
        return redirect()->back();
    }

    public function postSortable(Request $request)
    {
        $updateRecordsArray = $request->order;
        $i = 1;
 
        foreach ($updateRecordsArray as $recordID) {
            Category::where('id', '=', $recordID)->update(array('sort' => $i));
            $i++;
        }
        return \Response::json('ok');
    }
}
