<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;

class SliderController extends Controller
{
    protected $rules = [
        'title' => 'required',
        'description' => 'required',
        'url' => 'required',
        'image' => 'required',
    ];

    protected $viewURL = '_admin.pages.sliders.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Slider::all();
        return view($this->viewURL.'list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewURL.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Slider;
        $table->image = $request->image;
        $table->title = $request->title;
        $table->description = $request->description;
        $table->button_text = $request->button_text;
        $table->url = $request->url;
        if($request->active == "1"){
            $table->active = true;
        }else{
            $table->active = false;
        }
        $table->save();

        \Session::flash('message', 'Slider Başarıyla Eklendi');
        return redirect(route('sliders.edit', $table->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider::findOrFail($id);
        return view($this->viewURL.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table  = Slider::findOrFail($id);
        $table->image = $request->image;
        $table->title = $request->title;
        $table->description = $request->description;
        $table->button_text = $request->button_text;
        $table->url = $request->url;
        if($request->active == "1"){
            $table->active = true;
        }else{
            $table->active = false;
        }
        $table->save();

        \Session::flash('message', 'Slider Başarıyla Düzenlendi');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::find($id)->delete();
        sleep(1);
        return redirect()->back();
    }
}
