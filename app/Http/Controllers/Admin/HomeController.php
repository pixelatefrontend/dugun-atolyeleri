<?php

namespace App\Http\Controllers\Admin;

use Analytics;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workshop;
use App\Models\User;
use App\Models\Offer;
use App\Models\Category;
use App\Models\Image as Gallery;
use Intervention\Image\ImageManagerStatic as Image;
use Spatie\Analytics\Period;

class HomeController extends Controller
{
    public function getIndex()
    {
        $analytics = Analytics::fetchMostVisitedPages(Period::days(30))->take(10);
        $activeUser = Analytics::getAnalyticsService()->data_realtime->get('ga:'.env('VIEW_ID'), 'rt:activeVisitors')->totalsForAllResults['rt:activeVisitors'];
    	$workshops = Workshop::all();
    	$users = User::all();
        $offers = Offer::all();
        $categories = Category::pluck('title', 'id');
    	return view('_admin.pages.index', compact('workshops', 'users', 'offers', 'categories', 'analytics', 'activeUser'));
    }

    public function postSetting(Request $r)
    {
    	
    }

    public function getFoo()
    {
        $items = Workshop::with('images')->get();
        foreach($items as $item):
            $image = Gallery::where('workshop_id', $item->id)->first();
            if($image){
               $img = Image::make(public_path('uploads/workshops/'.$item->id.'/'.$image->filename))
                    ->fit(263)->save(public_path('uploads/workshops/'.$item->id.'/'.'thumb-'.$image->filename), 60);
                $table = Workshop::findOrFail($item->id);
                $table->first_picture = '/uploads/workshops/'.$item->id.'/'.'thumb-'.$image->filename;
                $table->save();
            }
        endforeach;
    }

    public function postWorkshopByCategory(Request $request)
    {
        if($request->ajax())
        {
            $items = Category::where('id', $request->category_id)->with(['workshops' => function($query){
                $query->orderBy('like', 'DESC')->orderBy('view_count', 'DESC');
            }])->take(20)->get();
            return response()->json([ 'items' => $items ]);
        }
    }

    public function changeBoot()
    {
        $items = Workshop::all();
        $data = [];
        foreach($items as $item):
            if(\File::exists(public_path('uploads/workshops/'.$item->slug))) {
                rename('uploads/workshops/'.$item->slug, 'uploads/workshops/'.$item->id);
            }
        endforeach; 
    }
}
