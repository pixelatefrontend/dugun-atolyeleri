<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
	protected $viewPath = '_admin.pages.settings.index';

	public function getIndex()
	{
		$item = Setting::first();
		return view($this->viewPath, compact('item'));
	}
    public function postUpdate(Request $request)
    {
    	
    }
}
