<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    public function getList()
    {
    	$items = Contact::orderBy('id', 'DESC')->get();
    	return view('_admin.pages.contact.list', compact('items'));
    }

    public function getActive(Request $request, $id)
    {
    	$item = Contact::findOrFail($id);
    	$item->active = 1;
    	$item->save();
    }

    public function destroy($id)
    {
        Contact::findOrFail($id)->delete();
        sleep(1);
        return back();
    }
}
