<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;

class CommentsController extends Controller
{
    protected $viewPath = "_admin.pages.comments.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Comment::whereActive('0')->get();
        return view($this->viewPath.'list', compact('items'));  
    }

    public function getActive($id)
    {
        $item = Comment::findOrFail($id);
        $item->active = $item->active == 1 ? 0 : 1;
        $item->save();
        \Session::flash('message', 'Yorum düzenleme işlemi başarılı');
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Comment::findOrFail($id);
        return view($this->viewPath.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Comment::findOrFail($id);
        $table->female = $request->female;
        $table->male = $request->male;
        $table->title = $request->title;
        $table->active = $request->active;
        $table->content = $request->content;
        $table->save();
        \Session::flash('message', 'Yorum başarıyla düzenlendi!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Comment::findOrFail($id);
        $item->delete();
        sleep(1);
        return back();
    }

    public function getActiveComments()
    {
        $items = Comment::whereActive('1')->get();
        return view($this->viewPath.'list', compact('items'));
    }
}
