<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BlogCategory as Category;

class BlogCategoryController extends Controller
{

    protected $viewPath = '_admin.pages.blogs.categories.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Category::orderBy('sort', 'ASC')->get();
        return view($this->viewPath.'list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewPath.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Category;
        $table->cover = $request->cover;
        $table->icon = $request->icon;
        $table->title = $request->title;
        $table->slug = str_slug($request->title);
        $table->description = $request->description;
        $table->active = $request->active;
        $table->save();
        return redirect(route('blog-categories.edit', $table->id))->with('message', 'Blog Kategorisi Başarıyla Eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Category::findOrFail($id);
        return view($this->viewPath.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Category::findOrFail($id);
        $table->cover = $request->cover;
        $table->icon = $request->icon;
        $table->title = $request->title;
        $table->slug = str_slug($request->title);
        $table->description = $request->description;
        $table->active = $request->active;
        $table->save();
        return back()->with('message', 'Blog Kategorisi Başarıyla Düzenlendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Category::findOrFail($id);
        $item->delete();
        sleep(1);
        return back();
    }

    public function postSortable(Request $request)
    {
        $updateRecordsArray = $request->order;
        $i = 1;
 
        foreach ($updateRecordsArray as $recordID) {
            Category::where('id', '=', $recordID)->update(array('sort' => $i));
            $i++;
        }
        return \Response::json('ok');
    }
}
