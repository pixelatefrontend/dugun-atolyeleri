<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BlogComment as Comment;

class BlogCommentsController extends Controller
{

    protected $viewPath = '_admin.pages.blogs.comments.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Comment::whereActive('0')->get();
        return view($this->viewPath.'list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Comment::findOrFail($id);
        return view($this->viewPath.'update', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Comment::findOrFail($id);
        $table->name = $request->name;
        $table->lname = $request->lname;
        $table->email = $requset->email;
        $table->title = $request->title;
        $table->content = $table->content;
        $table->active = $request->active;
        $table->save();
        return back()->with('message', 'Yorum başarıyla düzenlendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table = Comment::findOrFail($id);
        $table->delete();
        sleep(1);
        return back();
    }

    public function getActiveComments()
    {
        $items = Comment::whereActive('1')->get();
        return view($this->viewPath.'list', compact('items'));
    }

    public function getActive($id)
    {
        $item = Comment::findOrFail($id);
        $item->active = $item->active == 1 ? 0 : 1;
        $item->save();
        return back()->with('message', 'Yorum düzenleme işlemi başarılı');
    }
}
