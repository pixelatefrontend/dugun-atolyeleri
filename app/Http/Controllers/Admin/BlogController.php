<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory as Category;
use App\Models\BlogGallery as Gallery;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\BlogComment as Comment;
use Auth;

class BlogController extends Controller
{

    protected $viewPath = '_admin.pages.blogs.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Blog::orderBy('created_at', 'DESC')->get();
        return view($this->viewPath.'list', compact('items', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title', 'id');
        return view($this->viewPath.'create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = new Blog;
        $table->cover = $request->cover;
        $table->blog_category_id = $request->category;
        $table->title = $request->title;
        $table->slug = str_slug($request->title);
        $table->description = $request->description;
        $table->content = $request->content;
        $table->active = $request->active;
        $table->save();
        if($request->imageCheck){
            if($request->hasFile('images')){
                foreach($request->images as $image):
                    $file = public_path('uploads/blog/');
                    \File::exists($file) or \File::makeDirectory($file);
                    $fileName = time().rand().'.'.$image->getClientOriginalExtension();
                    $blogFile = Image::make($image->getRealPath());
                    $blogFile->resize(500, null, function($constraint){
                        $constraint->aspectRatio();
                    })->save($file . $fileName);

                    $model = new Gallery;
                    $model->blog_id = $table->id;
                    $model->filename = $fileName;
                    $model->save();
                endforeach;
            }
        }
        return redirect(route('blogs.edit', $table->id))->with('message', 'Blog başarıyla eklendi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Blog::findOrFail($id);
        $categories = Category::pluck('title', 'id');
        return view($this->viewPath.'update', compact('item', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Blog::findOrFail($id);
        $table->cover = $request->cover;
        $table->blog_category_id = $request->category;
        $table->title = $request->title;
        $table->slug = str_slug($request->title);
        $table->description = $request->description;
        $table->content = $request->content;
        $table->active = $request->active;
        $table->save();
        if($request->imageCheck){
            if($request->images){
                foreach($request->images as $image):
                    $file = public_path('uploads/blog/');
                    \File::exists($file) or \File::makeDirectory($file);
                    $fileName = time().rand().'.'.$image->getClientOriginalExtension();
                    $blogFile = Image::make($image->getRealPath());
                    $blogFile->resize(500, null, function($constraint){
                        $constraint->aspectRatio();
                    })->save($file . $fileName);

                    $model = new Gallery;
                    $model->blog_id = $table->id;
                    $model->filename = $fileName;
                    $model->save();
                endforeach;
            }
        }
        return back()->with('message', 'Blog başarıyla düzenlendi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workshop = Blog::findOrFail($id);
        $workshop->delete();
        sleep(1);
        return back();
    }

    public function imageDestroy(Request $request)
    {
        if($request->ajax())
        {
            $table = Gallery::findOrFail($request->id);
            $table->delete();
            return response()->json([ 'success' => true ]);
        }
    }
}
