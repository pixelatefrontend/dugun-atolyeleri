<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Workshop;
use App\Models\Category;
use App\Models\City;
use App\Models\Image as Gallery;
use Faker\Factory as Faker;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Service;
use App\Models\Map;
use App\Models\Question;
use App\Models\Video;
use App\Models\Other;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class WorkshopController extends Controller
{

    protected $viewURL = '_admin.pages.workshops.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->viewURL.'list');
    }

    public function getData()
    {
        $item = Workshop::select(array('id','title', 'instagram_username', 'created_at'))->active()->orderBy('created_at', 'DESC');

        return Datatables::of($item)
        ->addColumn('operations',
            ' <td style="text-align: right">
                                            <a href="{{route(\'workshops.edit\', $id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ \'workshops.destroy\', $id]) !!}
                                        </td>' )
         ->editColumn('created_at', function ($item) {
                return $item->created_at ? with(new Carbon($item->created_at))->diffForHumans() : '';
            })
        ->rawColumns(['operations'])
        ->make(true);
    }

    public function getDisabledData()
    {
        $item = Workshop::select(array('id','title', 'instagram_username','active','created_at'))->where('active', '!=', 1)->orderBy('created_at', 'DESC');

        return Datatables::of($item)
        ->addColumn('operations',
            ' <td style="text-align: right">
                                            <a href="{{route(\'workshops.edit\', $id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ \'workshops.destroy\', $id]) !!}
                                        </td>' )
        ->editColumn('active', function($item){
            if($item->active != 0) 
            {
                return '<label class="label label-primary">Onay Bekleyen</label>';
            }else{
                return '<label class="label label-warning">Pasife Çekilmiş</label>';
            }
        })
         ->editColumn('created_at', function ($item) {
                return $item->created_at ? with(new Carbon($item->created_at))->diffForHumans() : '';
            })
        ->rawColumns(['operations', 'active'])
        ->make(true);
    }

    public function getDisabledWorkshops()
    {
        return view($this->viewURL.'disabledList');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $cities = City::all();
        return view($this->viewURL.'create', compact('categories','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        $faker = Faker::create();
        $table = new Workshop;
        $table->type = $request->type;
        $table->avatar = $request->avatar;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->instagram_username = $request->instagram_username;
        $table->description = $request->description;
        if($request->email == null)
        {
            $table->email = $faker->unique()->email;
        }else {
            $table->email = $request->email;
        }
        $table->password = \Hash::make('secret');
        $table->city = $request->city;
        $table->active = $request->active;
        $table->save();
        
        $table->categories()->sync($request->workshop_categories);


        \Session::flash('message', 'Atölye Başarıyla Eklendi!');
        return redirect(route('workshops.edit', $table->id));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Workshop::with(['categories', 'images', 'map', 'services', 'questions', 'videos'])->find($id);
        $categories = Category::pluck('title', 'id');
        $cities = City::pluck('city', 'city');
        $types = [ 'premium', 'gold', 'normal' ];
        return view($this->viewURL.'update', compact('item','categories', 'cities', 'types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $table = Workshop::findOrFail($id);
        $table->type = $request->type;
        $table->title = $request->title;
        $table->slug = $request->slug;
        $table->instagram_username = $request->instagram_username;
        $table->email = $request->email;
        $table->description = $request->description;
        $table->city = $request->city;
        $table->active = $request->active;
        $table->save();
    
        $table->categories()->sync($request->workshop_categories);

        \Session::flash('message', 'Atölye Başarıyla Düzenlendi!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workshop $workshop)
    {
        $workshop->delete();
        sleep(1);
        return back();
    }

    public function imageDestroy(Request $request, $id)
    {
        if($request->ajax()) {
            $item = Gallery::find($id)->delete();
            return response()->json([ 
                'success' => true,
             ]);
        }
    }

    public function imageConf(Request $request, $id)
    {
        if($request->ajax()) {
            $item = Workshop::findOrFail($id);
            $item->first_picture = $request->path;
            $item->save();
            return response()->json([
                'success' => true
            ]);
        }
    }
    
    public function postImages(Request $request, $id)
    {
        $workshop = Workshop::findOrFail($id);
        $filePath = public_path('uploads/workshops/'.$workshop->id.'/');
        \File::exists($filePath) or \File::makeDirectory($filePath);
        // AVatar
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $fileNameAvatar = time().rand().'.'.$avatar->getClientOriginalExtension();
            $avatarImage = Image::make($avatar->getRealPath());
            $avatarImage->resize(150, null, function($constraint){
                $constraint->aspectRatio();
            })->save($filePath . $fileNameAvatar);
            $workshop->avatar = '/uploads/workshops/'.$workshop->id.'/'.$fileNameAvatar;
            $workshop->save();
        }
        // Cover
        if($request->hasFile('cover')){
            $cover  = $request->file('cover');
            $fileNameCover = time().rand().'.'.$cover->getClientOriginalExtension();
            $coverImage  = Image::make($cover->getRealPath());
            $workshop->cover = $fileNameCover;
            $coverImage->save($filePath . $fileNameCover);
            $workshop->save();
        }

        \Session::flash('message', 'Görseller Başarıyla Düzenlendi!');
        return back();

    }

    public function postGallery(Request $request, $id)
    {
        $table = Workshop::findOrFail($id);
        $images = $request->file('file');
       
        $fileName = time().rand().'.'.$images->getClientOriginalExtension();
        $filePath = public_path('uploads/workshops/'.$table->id.'/');
        $image = Image::make($images->getRealPath());
        \File::exists($filePath) or \File::makeDirectory($filePath);
        $image->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
        if($image->height() > 800) {
            $image->crop(600, 800);
        }
        $image->save($filePath . $fileName);
        $data = array(
                'workshop_id' => $table->id,
                'filename' => $fileName,
            );
        Gallery::insert($data);

        \Session::flash('message', 'Resimler Başarıyla Düzenlendi!');
        return back();
    }

    public function imageSortable(Request $request)
    {
        $updateRecordsArray = $request->order;
        $i = 1;
 
        foreach ($updateRecordsArray as $recordID) {
            Gallery::where('id', '=', $recordID)->update(array('sort' => $i));
            $i++;
        }
        return \Response::json('ok');
    }

    public function postWorkshopDetail(Request $request, $id)
    {
        $workshop = Workshop::findOrFail($id);
        $workshop->member = $request->member;
        $workshop->address = $request->address;
        $workshop->phone = $request->phone;
        $workshop->save();
        if($workshop->services === null)
        {
            $services = new Service;
            $services->price = $request->price;
            $services->delivery_time = $request->delivery_time;
            $workshop->services()->save($services);
        }else{
            $workshop->services()->update([ 'price' => $request->price, 'delivery_time' => $request->delivery_time ]);
        }

        if($workshop->map === null)
        {
            $map = new Map;
            $map->x = $request->x;
            $map->y = $request->y;
            $workshop->map()->save($map);
        }else{
            $workshop->map()->update([ 'x' => $request->x, 'y' => $request->y ]);
        }

        \Session::flash('message', 'Detaylar Başarıyla Düzenlendi!');
        return back();
    }

    public function postQuestions(Request $request, $id)
    {
        $ids = explode(',',$id);
        foreach($ids as $id){
            $item = Question::where('workshop_id', $id);
            try {
                $item->delete();
            } catch (Exception $e) {
                return Misc::exceptionalJson($e);
            }
        }
        if($request->has('questions')){
            foreach ($request->questions as $key => $v) {
                $data = array(
                    'workshop_id' => $id,
                    'question' => $request->questions[$key],
                    'reply' => $request->replies[$key],
                    );
               Question::insert($data);
            }
        }

        \Session::flash('message', 'Atölye başarıyla düzenlendi!');
         return back();
    }

    public function postVideo(Request $request, $id)
    {
        $ids = explode(',',$id);
        foreach($ids as $id){
            $item = Video::where('workshop_id', $id);
            try {
                $item->delete();
            } catch (Exception $e) {
                return Misc::exceptionalJson($e);
            }
        }

        $item = Workshop::findOrFail($id);
        $url = $request->url;
        foreach($url as $key => $row):
            if($row != null){
            if($request->hasFile('videoCover')){
                $images = $request->file('videoCover');
                $fileName = time().rand().'.'.$images->getClientOriginalExtension();
                $filePath = public_path('uploads/workshops/'.$item->id.'/');
                $image = Image::make($images->getRealPath());
                \File::exists($filePath) or \File::makeDirectory($filePath);
                $image->resize(600, null, function ($constraint) {
                                $constraint->aspectRatio();
                            });
                if($image->height() > 800) {
                    $image->crop(600, 800);
                }
                $image->save($filePath . $fileName);
            }else{
                $fileName = '';
            }

            $data = array(
                    'workshop_id' => $item->id,
                    'url' => $url[$key],
                    'description' => $request->description[$key],
                    'site' => $request->site[$key],
                    'cover' => $fileName,
                );
            Video::insert($data);
            }
        endforeach;
        \Session::flash('message', 'Atölye başarıyla düzenlendi!');
         return back();
    }

    public function postOther(Request $request, $id)
    {
        $item = Workshop::findOrFail($id);

        if($item->other === null) {
            $other = new Other;
            $other->facebook = $request->facebook;
            $other->twitter = $request->twitter;
            $other->website = $request->website;
            $other->note = $request->note;
            $item->other()->save($other);
        }else{
            $item->ohter()->update([ 
                'facebook' => $request->facebook,
                'twitter' => $request->twitter,
                'website' => $request->website,
                'note' => $request->note
            ]);
        }
        \Session::flash('message', 'Atölye başarıyla düzenlendi!');
        return back();
    }

    public function getWorkshopList()
    {
        $items = Workshop::all();
        return view($this->viewURL.'list-call', compact('items'));
    }

    public function postCall(Request $request)
    {
        if($request->ajax()) {
            $item = Workshop::findOrFail($request->id);
            $item->call_active = $item->call_active == 1 ? 0 : 1;
            $item->save();
            return response()->json(['success' => true]);
        }
    }

    public function createAcc(Request $request, $id)
    {
        $table = Workshop::findOrFail($id);
        $table->username = $request->username;
        $table->password = $request->password ? bcrypt($request->password) : $table->password;
        $table->save();
        \Session::flash('message', 'Atölye başarıyla düzenlendi!');
        return back();
    }
}