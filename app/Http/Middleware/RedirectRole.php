<?php

namespace App\Http\Middleware;

use Closure;

class RedirectRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->user()->role->name != 'Blog Editör') {
            return $next($request);
        }else{
            return redirect('/')->with('message', 'Yetkiniz Yok');
        }
    }
}
