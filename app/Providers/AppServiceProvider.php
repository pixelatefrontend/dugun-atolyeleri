<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use App\Models\City;
use Carbon\Carbon;
use Auth;
use App\Models\BlogCategory;
use IyzipayLaravel;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Carbon::setLocale('tr');
        view()->composer(['_website.layouts.header', '_website.pages.blog.layouts.footer', '_website.home', '_website.layouts.footer', '_user.auth.register', '_user.auth.login', '_user.layouts.footer', '_user.auth.password.email', '_user.auth.password.reset', '_user.layouts.header', '_workshop.auth.register'], function ($view) {
            $data = \Cache::remember('categories', 60, function(){
                return Category::active()->orderBy('sort', 'ASC')->get();
            });
            // $data = Category::active()->orderBy('sort', 'ASC')->get();
            $cities = \Cache::remember('cities', 60, function(){
                return City::all();
            });
            $view->with([
                'all_categories' => $data,
                'cities' => $cities
            ]);
        });

        view()->composer('_website.pages.blog.layouts.header', function($view){
            $blogCategories = BlogCategory::active()->orderBy('sort', 'ASC')->get();
            $cities = \Cache::remember('cities', 60, function(){
                return City::all();
            });
            $view->with(['blogCategories' => $blogCategories, 'cities' => $cities]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
          // IyzipayLaravel::plan('aylik-platinum', 'Aylık Platinum')->trialDays(15)->price(40);
    }
}
