<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
	protected $casts = [ 'active' => 'boolean' ];
    protected $fillable = ['title', 'description', 'url','button_text', 'image', 'active'];

    public function scopeActive($query)
    {
    	return $query->where('active', 1);
    }
}
