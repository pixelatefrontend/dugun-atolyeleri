<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserList extends Model
{
    protected $table = 'lists';

    protected $fillable = [ 'workshop_id', 'title', 'description' ];

    public function workshops()
    {
    	return $this->belongsToMany(Workshop::class)->withPivot('workshop_id');
    }
}
