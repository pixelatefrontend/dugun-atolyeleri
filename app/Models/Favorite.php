<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public function isFavorited()
    {
    	return $this->favorites()->where('user_id', Auth::user()->id)->exists();
    }
}
