<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use App\Notifications\UserPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $dates = ['date'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
      $this->notify(new UserPasswordNotification($token));
    }

    public function getFullName()
    {
        return $this->name.' '.$this->lname;
    }

    public function noImageAvatar()
    {
        return mb_strtoupper($this->name[0].' '.$this->lname[0], 'UTF-8');
    }

    public function isFavorited()
    {
        return $this->favorites()->where('user_id', Auth::user()->id)->exists();
    }

    public function lists()
    {
        return $this->hasMany('App\Models\Moment');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Workshop::class, 'favorites')->withTimestamps();
    }

    public function offers()
    {
        return $this->belongsToMany(Workshop::class, 'offers')->withTimestamps()->withPivot('completed', 'description', 'created_at');
    }

}
