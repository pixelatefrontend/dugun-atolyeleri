<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

   public function getFullName()
   {
   		return $this->female.' & '.$this->male;
   }

   public function getShortName()
   {
   		return strtoupper($this->female[0].' & '. $this->male[0]);
   }

   public function getCreatedAt()
   {
   		return $this->created_at->diffForHumans();
   }

   public function scopeActive($query)
   {
      return $query->where('active', 1);
   }

   public function workshop()
   {
      return $this->belongsTo(Workshop::class, 'workshop_id');
   }
}
