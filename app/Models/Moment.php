<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Moment extends Model
{
    protected $fillable = ['workshop_id', 'title', 'description'];

    public function workshops()
    {
    	return $this->belongsToMany(Workshop::class);
    }

    public function users()
    {
    	return $this->belongsTo(User::class);
    }
}
