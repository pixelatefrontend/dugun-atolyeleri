<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Category;
use App\Models\Image;
use App\Models\Comment;
use Auth;

class Workshop extends Authenticatable 
{
    use SoftDeletes;

    protected $guard = 'workshop';
    protected $table = 'workshops';

	protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['created_at', 'updated_at' ,'deleted_at'];

	public function getFullName()
    {
        return $this->member;
    }

    public function getShortTitle($str = 19)
    {
        $title = $this->title;
        if (strlen($title) > $str)
        {
            if (function_exists("mb_substr")) $title = mb_substr($title, 0, $str, "UTF-8").'..';
            else $title = mb_substr($title, 0, $str, 'UTF-8').'..';
        }
        return $title;
    }

    public function noImageAvatar()
    {
        return mb_strtoupper($this->title[0], 'UTF-8');
    }

    function tr_strtoupper($text)
    {
        $search = array("ç","İ","ı","ğ","ö","ş","ü");
        $replace = array("C","I","i","G","O","S","u");
        $text = str_replace($search,$replace,$text);
        $text = mb_strtoupper($text);
        return $text;
    }

    public function getType()
    {
        $data = $this->type;
        switch($data) {
            case "premium":
                return "Premium paket";
                break;
            case "gold":
                return "Gold paket";
                break;
            default:
                return "Herhangi bir paketiniz bulunmamaktadır.";
        }
    }

    public function getDoping()
    {
        if($this->doping) {
            return "Doping paketiniz var";
        }else{
            return "Şu anda güncel bir dopinginiz bulunmamaktadır.";
        }
    }

    public function avatarPath()
    {
        return $this->avatar;
        // return '/uploads/workshops/'.$this->slug.'/'.$this->avatar;
    }

    public function coverPath()
    {
        return $this->cover;
    }
    
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopePopular($query)
    {
        return $query->orderBy('view_count', 'DESC');
    }

    public function addView()
    {
        if (\Session::has($this->id)) {
            
        }else{ 
            $this->increment('view_count');
            \Session::put($this->id, 'on');
        }
    }

   

    // Relationships
    public function categories()
    {
        return $this->belongsToMany(Category::class)->withPivot('category_id');
    }

    public function images()
    {
        return $this->hasMany(Image::class, 'workshop_id')->orderBy('sort', 'ASC');
    }

    public function latestImage()
    {
        return $this->hasOne(Image::class, 'workshop_id')->latest();
    }

    public function videos()
    {
        return $this->hasMany(Video::class);
    }

    public function lists()
    {
        return $this->belongsToMany('App\Models\Moment')->withPivot('workshop_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class)->whereActive('1');
    }

    public function disabledComments()
    {
        return $this->hasMany(Comment::class)->whereActive('0');
    }

    public function allComments()
    {
        return $this->hasMany(Comment::class)->orderBy('active', 'ASC')->orderBy('id', 'DESC');
    }

    public function offers()
    {
        return $this->belongsToMany(User::class, 'offers')->withTimestamps()->withPivot('id', 'description', 'completed', 'image_id');
    }
    
    public function favorites()
    {
        return $this->belongsToMany(User::class, 'favorites');
    }

    public function services()
    {
        return $this->hasOne(Service::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function map()
    {
        return $this->hasOne(Map::class);
    }

    public function other()
    {
        return $this->hasOne(Other::class);
    }

    public function isFavorited()
    {
        return $this->favorites()->where('user_id', Auth::user()->id)->exists();
    }

    public function bill()
    {
        return $this->hasOne(Bill::class);
    }

    public function card()
    {
        return $this->hasOne(Card::class);
    }

    public function subscribe()
    {
        return $this->hasOne(Subscription::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }

    public function visitors()
    {
        return $this->hasMany(Visitor::class);
    }
}
