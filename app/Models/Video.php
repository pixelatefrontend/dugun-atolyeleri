<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
	protected $fillable = [
        'url', 'description'
    ];

    public function strUrl()
    {
        if($this->site == 'youtube'){
            return str_replace('watch?v=','embed/', $this->url);
        }else{
            return str_replace('https://vimeo.com/','https://player.vimeo.com/video/', $this->url);
        }
    }

    public function coverPath($id)
    {
        return '/uploads/workshops/'.$id.'/'.$this->cover;
    }

    public function workshop()
    {
    	return $this->belongsTo(Workshop::class);
    }
}
