<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Workshop;

class Category extends Model
{
    protected $casts = [
    	'is_active' => 'boolean',
    ];
    public function workshops()
    {
    	return $this->belongsToMany(Workshop::class)->withPivot('workshop_id');
    }
    public function scopeActive($query)
    {
    	return $query->where('active', 1);
    }
    public function getWorkshopsAttribute()
    {
        return $this->workshops()->paginate(12);
    }

    public function adWorkshop()
    {
        return $this->belongsTo(Workshop::class, 'workshop_id');
    }
}
