<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $table = 'blog_categories';

    public function blogs()
    {
    	return $this->hasMany(Blog::class);
    }

    public function scopeActive($query)
    {
    	return $query->where('active', 1);
    }
}
