<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
	public function getFullName()
	{
		return $this->name.' & '.$this->lname;
	}

	public function getShortName()
	{
		return strtoupper($this->name[0].' & '. $this->lname[0]);
	}

	public function getCreatedAt()
	{
		return $this->created_at->diffForHumans();
	}

	public function scopeActive($query)
	{
		return $query->where('active', 1);
	}

	public function blog()
	{
	  	return $this->belongsTo(Blog::class, 'blog_id');
	}
}
