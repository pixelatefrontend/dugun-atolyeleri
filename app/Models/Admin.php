<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
	
	protected $guard = 'admin';
	protected $table = 'admins';
	
    //Mass assignable attributes
	protected $fillable = [
	  'name', 'email', 'password',
	];

	//hidden attributes
	protected $hidden = [
	   'password', 'remember_token',
	];

	public function isAdmin()
	{
		return $this->role->name == 'Admin';
	}

	public function isEditor()
	{
		return $this->role->name == 'Editör';
	}

	public function isBlogEditor()
	{
		return $this->role->name == 'Blog Editör';
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'role_id');
	}
}
