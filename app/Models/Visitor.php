<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
	protected $dates = [ 'visitor_date' ];
	public $timestamps = false;
    public function workshop()
    {
    	return $this->belongsTo(Workshop::class);
    }
}
