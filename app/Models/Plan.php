<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public function price()
    {
    	$price = $this->amount - ($this->amount * ($this->discount / 100));
    	return $price;
    }
}
