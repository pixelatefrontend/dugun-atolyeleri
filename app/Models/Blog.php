<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function scopeActive($query)
    {
    	return $query->where('active', 1);
    }

    public function addView()
    {
        $this->increment('view_count');
    }
    // Relations

    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'blog_category_id');
    }

    public function images()
    {
        return $this->hasMany(BlogGallery::class);
    }

    public function comments()
    {
        return $this->hasMany(BlogComment::class);
    }
}
