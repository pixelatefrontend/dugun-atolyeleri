<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{

	protected $fillable = [
        'completed', 'description'
    ];

	public function complete($query)
	{
		return $query->where('complete', 1);
	}

	public function nonComplete($query)
	{
		return $query->where('complete', 0);
	}

    public function workshop()
    {
    	return $this->belongsTo(Workshop::class, 'workshop_id');
    }

    public function user()
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function image()
    {
        return $this->belongsTo(Image::class);
    }
}
