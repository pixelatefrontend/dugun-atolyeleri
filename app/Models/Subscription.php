<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Subscription extends Model
{
	protected $dates = [ 'cancel_at' ];

    public function cancelled()
    {
    	return $this->cancel_at == null;
    }

    public function paymentTime()
    {
    	if(Carbon::now() > $this->end_date){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function plan()
    {
    	return $this->belongsTo(Plan::class);
    }
}
