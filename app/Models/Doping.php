<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doping extends Model
{
    protected $table = "dopings";
    protected $timestamps = true;

    public function workshops()
    {
    	return $this->belongsTo(Workshop::class, 'workshop_id')->withTimestamps();
    }
    
}
