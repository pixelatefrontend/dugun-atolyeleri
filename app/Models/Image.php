<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

	protected $fillable = [
        'workshop_id', 'filename', 'title'
    ];

    public function workshop()
    {
    	return $this->belongsTo(Workshop::class);
    }

    public function getFilename()
    {
    	return $this->filename;
    }

    public function path()
    {
        return '/uploads/workshops/'.$this->workshop()->slug.'/'.$this->filename;
    }
}
