function resizeSlider() {
    var e = $(".workshop-detail-page .slider-area .slider-item");
    e.css({height: e.width()});
    var t = $(".visible-sm .workshop-slider-item");
    t.css({height: t.width()});
    var i = $(".hidden-sm .workshop-slider-item");
    i.css({height: i.width()});
    var n = $(".week-design-list-item");
    n.css({height: n.width() + 40})
}

function firstLetter() {
    $('.workshop-pp-name').each(function() {
        var dataText = $(this).data('name');
        var arr = dataText.toString().substr(0,1);
        $(this).text(arr)
    });
    console.log('done')
}

setInterval(function () {
    resizeSlider()
    // $('.workshop-pp-name').each(function() {
    //     var dataText = $(this).data('name');
    //     var arr = dataText.substr(0,1);
    //     $(this).text(arr)
    // });
}, 200);

function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
        return data;
    }

    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
        return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if ( data.text.toLowerCase().indexOf(params.term) > -1 || data.text.indexOf(params.term) > -1) {
        var modifiedData = $.extend({}, data, true);
        // modifiedData.text += ' (matched)';
        modifiedData.text += '';

        // You can return modified objects from here
        // This includes matching the `children` how you want in nested data sets
        return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}

var mainFunction = {
    elements: {
        homePageSlider: ".home-page .home-page-slider",
        customSelect: ".custom-select",
        weekDesignList: ".week-design-list",
        gridItem: ".design-list-grid .design-list-grid-item",
        loadMoreButton: ".home-page .home-page-design-list .load-more",
        modalRegisterButton: ".sign-modal .register-btn",
        userSectionInner: "header .middle-nav-container .middle-nav .user-section .user-section-inner",
        userSectionMenu: "header .middle-nav-container .middle-nav .user-section .user-section-menu",
        workShopDetailSlider: ".workshop-detail-page .slider-area .slider",
        workShopDetailSliderThumbItem: ".workshop-detail-page .slider-area .thumbs li",
        mainSearchInput: ".search-section .search-section-inner input[type=text]",
        mobilSearchInput: ".search-wrap .search-section .search-section-inner input[type=text]",
        workshopItemHeartButton: ".home-page .week-design-list-item .footer .avatar-figure .hover-svg-inner a",
        worksshopDetailLikeButton: ".workshop-detail-page .detail-area .studio-name .studio-name-inner .like-counter",
        bidSuccessButton: "#bid-success-button",
        bidSuccessCompleteButton: "#bid-success-complete-button",
        allWorkShopCategorySlider: ".all-workshop-page .category-slider",
        premiumDetailAllPhotosButton: ".premium-detail-page .detail-section .workshop-slider-container .all-photos-button",
        weekDesignSlider: ".week-design-list-slider"
    }, MAIN_SEARCH_LIST_JSON_URL: "/search", init: function () {
        mainFunction.slickSettings(), mainFunction.signModalSettings(), mainFunction.hoverSettings(), mainFunction.workShopDetailSliderSettings(), mainFunction.datePickerSettings(), mainFunction.mainSearchSettings(), mainFunction.workShopItemSettings(), mainFunction.formControl(), mainFunction.workShopDetailLikeSettings(), mainFunction.bidModalSettings(), mainFunction.select2Settings(), mainFunction.inputMask(), mainFunction.accordionSettings(), mainFunction.mobilMenuSettings(), mainFunction.defaultSettings(), mainFunction.contactSettings(), mainFunction.premiumDetailSettings()
    }, defaultSettings: function () {
        $("html").hasClass("sign-success")
    }, contactSettings: function () {
        $('input[name="name"]').keyup(function () {
            $("div#name").hide()
        }), $('input[name="email"]').keyup(function () {
            $("div#email").hide()
        }), $('input[name="message"]').keyup(function () {
            $("div#message").hide()
        }), $("#contact-send-button").on("click", function (e) {
            e.preventDefault();
            var t = $(".sign-modal .contact-form > form > .row-inputs"),
                i = $(".sign-modal .contact-form > form > .row-success"),
                n = $("form#contact-form").serialize(),
                a = $('input[name="name"]').val();
                email = $('input[name="email"]').val(),
                message = $('textarea[name="message"]').val(),
                a ? email && function (e) {
                return /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i.test(e)
            }(email) ? message ? ($.ajax({
                method: "POST",
                data: n,
                url: "/iletisim"
            }), TweenMax.to(t, .5, {
                css: {scale: 0, opacity: 0},
                ease: Back.easeIn.config(1.7),
                onComplete: function () {
                    $(t).css("display", "none"), $(i).css("display", "flex"), $(i).css("display", "-webkit-flex"), TweenMax.to(i, .5, {
                        css: {
                            scale: 1,
                            opacity: 1
                        }, ease: Back.easeOut.config(1.7)
                    })
                }
            })) : $("div#message").show() : $("div#email").show() : $("div#name").show()
        })
    }, premiumDetailSettings: function () {
        $(".fancybox-thumb").fancybox({
            prevEffect: "none",
            nextEffect: "none",
            helpers: {title: {type: "outside"}, thumbs: {width: 50, height: 50}}
        }), $(mainFunction.elements.premiumDetailAllPhotosButton).on("click", function (e) {
            e.preventDefault(), $(".fancybox-container .fancybox-thumb:first-child").trigger("click")
        })
    }, formControl: function () {
        function e(e) {
            return -1 == e.search("X")
        }

        function t(e) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(e)
        }

        function i(e) {
            return /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g.test(e)
        }

        $(".form-control-button").on("click", function (n) {
            for (var a = $(this).parent().parent(), s = a.find("> div").length, o = 0, r = 0, l = 0; l < s; l++) {
                var d = a.find("> div:nth-child(" + (l + 1) + ")"), c = 0 != d.find(".input-control").length,
                    u = 0 != d.find(".input-container").length, m = 0 != d.find(".select-container").length,
                    h = (d.find(".textarea-control").length, 0 != d.find(".checkbox-container").length);
                if (c) if (r++, u) {
                    var f = d.find(".input-container input"), p = d.find(".textarea-control textarea");
                    "" != f.val() ? f.parent().hasClass("phone-control") ? e(f.val()) ? (o++, f.parent().removeClass("show-error")) : f.parent().addClass("show-error") : f.parent().hasClass("mail-control") ? t(f.val()) ? (o++, f.parent().removeClass("show-error")) : f.parent().addClass("show-error") : f.parent().hasClass("instagram-control") ? i(f.val()) ? (o++, f.parent().removeClass("show-error")) : f.parent().addClass("show-error") : p.parent().hasClass("textarea-control") ? p.val() ? (o++, p.parent().removeClass("show-error")) : p.parent().addClass("show-error") : (o++, f.parent().removeClass("show-error"), p.parent().removeClass("show-error")) : (f.parent().addClass("show-error"), p.parent().addClass("show-error"))
                } else if (m) {
                    var w = d.find(".select-container select");
                    null != w.select2("val") ? (o++, w.parent().removeClass("show-error")) : w.parent().addClass("show-error")
                } else if (h) {
                    var v = d.find(".checkbox-container input");
                    v.prop("checked") ? (o++, v.parent().removeClass("show-error")) : v.parent().addClass("show-error")
                }
            }
            o == r ? console.log("Success!") : (console.log("Error!"), n.preventDefault())
        })
    }, mobilMenuSettings: function () {
        $("#account-button").on("click", function (e) {
            e.preventDefault(), $("html").toggleClass("right-mobil-menu-open"), $("html").removeClass("left-mobil-menu-open"), $(".sign-in-wrp").show(), $(".sign-in-form-2").show(), $(".sign-up-form-mobile").hide()
        }), $("#hamburger-button").on("click", function (e) {
            e.preventDefault(), $("html").removeClass("right-mobil-menu-open"), $("html").toggleClass("left-mobil-menu-open"), $(".sign-in-wrp").show(), $(".sign-in-form-2").show(), $(".sign-up-form-mobile").hide()
        })
    }, accordionSettings: function () {
        $("#accordion").accordion({
            heightStyle: "content", collapsible: !0, beforeActivate: function (e, t) {
                if (t.newHeader[0]) n = (i = t.newHeader).next(".ui-accordion-content"); else var i = t.oldHeader,
                    n = i.next(".ui-accordion-content");
                var a = "true" == i.attr("aria-selected");
                return i.toggleClass("ui-corner-all", a).toggleClass("accordion-header-active ui-state-active ui-corner-top", !a).attr("aria-selected", (!a).toString()), i.children(".ui-icon").toggleClass("ui-icon-triangle-1-e", a).toggleClass("ui-icon-triangle-1-s", !a), n.toggleClass("accordion-content-active", !a), a ? n.slideUp() : n.slideDown(), !1
            }
        })
    }, inputMask: function () {
        $(".phone-mask").inputmask({
            // mask: "(+99) 999 999 99 99", // ex
            mask: "(+\\90) 999 999 99 99",
            placeholder: "(+90) XXX XXX XX XX",
            showMaskOnHover: !1
        })
    }, select2Settings: function () {
        $(".workshop-category-select").select2({
            placeholder: "Atöyle Kategorisi",
            minimumResultsForSearch: 1 / 0,
            maximumSelectionLength: 2,
            matcher: matchCustom,
            language: {
                noResults: function (params) {
                    return "Sonuç bulunamadı.";
                }
            }
        }), $(".country-category-select").select2({
            placeholder: "Ülke",
            minimumResultsForSearch: 1 / 0
        }), $(".city-category-select").select2({
            placeholder: "Şehir",
            // minimumResultsForSearch: 1 / 0,
            matcher: matchCustom,
            language: {
                noResults: function (params) {
                    return "Sonuç bulunamadı.";
                }
            }
        }), $(".day-category-select").select2({
            placeholder: "Gün",
            minimumResultsForSearch: 1 / 0
        }), $(".month-category-select").select2({
            placeholder: "Ay",
            minimumResultsForSearch: 1 / 0
        }), $(".year-category-select").select2({placeholder: "Yıl", minimumResultsForSearch: 1 / 0})
    }, bidModalSettings: function () {
        $(mainFunction.elements.bidSuccessButton).on("click", function () {
            $("#bid-modal").find(".close").trigger("click")
        }), $(mainFunction.elements.bidSuccessCompleteButton).on("click", function () {
            $("#bid-success-modal").removeClass("show")
        }), $("#add-list-button").on("click", function () {
            $("#fav-modal").removeClass("show"), $("#fav-modal").find(".close").trigger("click")
        })
    }, workShopDetailLikeSettings: function () {
        $(mainFunction.elements.worksshopDetailLikeButton).on("click", function () {
            $(this).hasClass("like-counter-selected") ? ($(this).removeClass("like-counter-selected"), $(this).find("span").text(Number($(this).find("span").text()) - 1)) : ($(this).addClass("like-counter-selected"), $(this).find("span").text(Number($(this).find("span").text()) + 1))
        })
    }, workShopItemSettings: function () {
        $(mainFunction.elements.workshopItemHeartButton).on("click", function (e) {
            e.preventDefault();
            var t = $(this).parent().parent().parent().parent(), i = t.find(".footer-inner a span"),
                n = t.find(".footer-inner a");
            $(this).parent().hasClass("selected") ? ($(this).parent().removeClass("selected"), $(this).parent().removeClass("selected"), i.text(Number(i.text()) - 1), n.removeClass("increase")) : ($(this).parent().addClass("selected"), $(this).parent().addClass("selected"), i.text(Number(i.text()) + 1), n.addClass("increase"))
        })
    }, mainSearchSettings: function () {
        var e = {
            url: mainFunction.MAIN_SEARCH_LIST_JSON_URL,
            categories: [{listLocation: "workshops", header: "Atölyeler"}, {
                listLocation: "categories",
                header: "Kategoriler"
            }],
            getValue: "title",
            template: {type: "links", fields: {link: "url"}},
            list: {
                match: {enabled: !0},
                showAnimation: {type: "fade", time: 400},
                hideAnimation: {type: "fade", time: 400}
            }
        };
        $(mainFunction.elements.mainSearchInput).easyAutocomplete(e), $(mainFunction.elements.mobilSearchInput).easyAutocomplete(e)
    }, datePickerSettings: function () {
        $(".datetimepicker").datepicker({
            monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            dayNamesMin: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt"],
            firstDay: 1,
            minDate: 0
        }), $(".datetimepicker").datepicker("option", "showAnim", "slideDown"), $(".datepicker").datepicker({
            monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            dayNamesMin: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt"],
            firstDay: 1,
            minDate: 0,
            dateFormat: "dd/mm/yy"
        }), $(".datepicker").datepicker("option", "showAnim", "slideDown")
    }, workShopDetailSliderSettings: function () {
        $(mainFunction.elements.workShopDetailSliderThumbItem).on("click", function () {
            $(mainFunction.elements.workShopDetailSliderThumbItem).removeClass("selected"), $(this).addClass("selected");
            var e = $(this).index();
            $(mainFunction.elements.workShopDetailSlider).find(".slick-dots li:nth-child(" + (e + 1) + ")").trigger("click")
        })
    }, hoverSettings: function () {
        !function (e, t) {
            $(e + "," + t).hover(function () {
                $(e).addClass("selected"), $(t).css("display", "block"), TweenMax.to($(t), .5, {opacity: 1})
            }, function () {
                $(e).removeClass("selected"), TweenMax.to($(t), .5, {
                    opacity: 0, onComplete: function () {
                        $(t).css("display", "none")
                    }
                })
            })
        }(mainFunction.elements.userSectionInner, mainFunction.elements.userSectionMenu)
    }, signModalSettings: function () {
        $(mainFunction.elements.modalRegisterButton).on("click", function () {
            $("#sign-in-modal").find(".close").trigger("click")
        })
    }, slickSettings: function () {
        $(mainFunction.elements.homePageSlider).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !1,
            dots: !1
        }), $(mainFunction.elements.workShopDetailSlider).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !1,
            prevArrow: $(".list-prev"),
            nextArrow: $(".list-next"),
            dots: !0,
            responsive: [{breakpoint: 767, settings: {slidesToShow: 1, dots: !1, arrows: !0}}]
        }), $(mainFunction.elements.workShopDetailSlider).on("afterChange", function (e, t, i) {
            $(mainFunction.elements.workShopDetailSliderThumbItem).removeClass("selected"), $(".workshop-detail-page .slider-area .thumbs li:nth-child(" + (i + 1) + ")").trigger("click")
        }), $(mainFunction.elements.allWorkShopCategorySlider).slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: !0,
            dots: !1,
            responsive: [{breakpoint: 991, settings: {slidesToShow: 3}}, {
                breakpoint: 767,
                settings: {slidesToShow: 2}
            }, {breakpoint: 450, settings: {slidesToShow: 1}}]
        }), $("#premium-detail-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !0,
            asNavFor: "#premium-detail-slider-thumb"
        }), $("#premium-detail-slider-thumb").slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: "#premium-detail-slider",
            arrows: !1,
            dots: !1,
            focusOnSelect: !0,
            variableWidth: !0
        }), $("#premium-detail-slider-mobil").slick({slidesToShow: 1, slidesToScroll: 1, arrows: !0})
    }, infiniteScroll: function () {
        function e() {
            var e = n;
            n >= i.length && (e = i.length);
            for (var t = 0; t < e; t++) mainFunction.blogGrid.append(i[t].item), mainFunction.blogGrid.isotope("layout");
            for (var a = 0; a < e; a++) i.shift();
            0 == i.length && $(mainFunction.elements.loadMoreButton).remove(), mainFunction.workShopItemSettings();
        }

        var t = $(mainFunction.elements.gridItem), i = [], n = 3;
        !function () {
            for (var e = 0; e < t.length; e++) {
                var n = $(".design-list-grid .design-list-grid-item:nth-child(" + (e + 1) + ")"), a = $(n);
                i.push({item: a})
            }
            t.remove()
        }(), e(), $(document).on("scroll", function () {
            $("body").scrollTop() >= $(".base-page").height() + $("header").height() - $(window).height() + 100 - 80 && e()
        }), $(mainFunction.elements.loadMoreButton).on("click", function (t) {
            t.preventDefault(), e()
        })
    }
};
$(function () {
    "use strict";
    $.fn.sliderFade = function (e) {
        function t(e, t) {
            var i = d.find(".single");
            e = e || i.length;
            var n = [], a = (t = t || $("<div/>", {class: "fade-outer"})).attr("class");
            for (i.parent("." + a).children().unwrap(); i.length > 0;) n.push(i.splice(0, e));
            return n.forEach(function (e) {
                $(e).wrapAll(t)
            }), i
        }

        function i(e) {
            return d.find(".single").css({width: 100 / e + "%"}), d.find(".fade-outer").first().addClass("active").fadeIn(), this
        }

        function n() {
            $(window).width() <= l.mobileScreen ? (t(l.mobileItem), i(l.mobileItem)) : $(window).width() > l.mobileScreen && $(window).width() <= l.bigMobileScreen ? (t(l.bigMobileItem), i(l.bigMobileItem)) : $(window).width() > l.bigMobileScreen && $(window).width() <= l.tabletScreen ? (t(l.tabletItem), i(l.tabletItem)) : $(window).width() > l.tabletScreen && $(window).width() <= 5e3 && (t(l.desktopItem), i(l.desktopItem))
        }

        function a() {
            d.find(".fade-outer.active").height();
            var e = $(".week-design-list-item");
            e.css({height: e.width() + 40}), d.find(".item-slider").css("height", e.width() + 40)
        }

        function s() {
            d.find(".fade-outer:last").hasClass("active") ? (d.find(".fade-outer.active").removeClass("active"), d.find(".fade-outer:first").addClass("active")) : d.find(".fade-outer.active").removeClass("active").next(".fade-outer").addClass("active"), d.find(".dots span:last").hasClass("active") ? (d.find(".dots span.active").removeClass("active"), d.find(".dots span:first").addClass("active")) : d.find(".dots span.active").removeClass("active").next(".dots span").addClass("active"), a(), clearInterval(m)
        }

        function o() {
            d.find(".fade-outer:first").hasClass("active") ? (d.find(".fade-outer.active").removeClass("active"), d.find(".fade-outer:last").addClass("active")) : d.find(".fade-outer.active").removeClass("active").prev(".fade-outer").addClass("active"), d.find(".dots span:first").hasClass("active") ? (d.find(".dots span.active").removeClass("active"), d.find(".dots span:last").addClass("active")) : d.find(".dots span.active").removeClass("active").prev(".dots span").addClass("active"), a(), clearInterval(m)
        }

        function r() {
            d.find(".dots").empty();
            d.find(".single");
            for (var e = d.find(".fade-outer").length, t = 0; t < e; t++) {
                var i = t + 1;
                d.find(".dots").append("<span>" + i + "</span>")
            }
            return d.find(".dots span:first").addClass("active"), this
        }

        var l = $.extend({
            desktopItem: 4,
            tabletItem: 4,
            bigMobileItem: 3,
            mobileItem: 2,
            tabletScreen: 1024,
            bigMobileScreen: 768,
            mobileScreen: 640,
            autoplayTime: 5e3,
            dots: !0,
            arrows: !0
        }, e), d = $(this);
        d.addClass("fadder");
        n();
        var c = $(window).width(), u = !1;
        $(window).resize(function () {
            $(window).width() != c && (c = $(window).width(), u && clearTimeout(u), u = setTimeout(function () {
                n()
            }, 200))
        }), setTimeout(function () {
            a()
        }, 50);
        var m;
        !function () {
            var e = $("<div class='dots'></div>");
            d.append(e), d.find(".dots").empty()
        }(), r();
        var h = d.find(".dots");
        1 == l.dots ? h.css({display: "block"}) : h.css({display: "none"});
        var f = !1, p = $(window).width();
        $(window).resize(function () {
            $(window).width() != p && (p = $(window).width(), f && clearTimeout(f), f = setTimeout(function () {
                r(), a(), d.find(".dots span").on("click", function (e) {
                    e.preventDefault();
                    var t = $(this).index();
                    $(this).addClass("active").siblings().removeClass("active"), d.find(".fade-outer").eq(t).addClass("active").siblings().removeClass("active"), clearInterval(m)
                })
            }, 500))
        }), d.find(".dots span").on("click", function (e) {
            e.preventDefault();
            var t = $(this).index();
            $(this).addClass("active").siblings().removeClass("active"), d.find(".fade-outer").eq(t).addClass("active").siblings().removeClass("active"), clearInterval(m)
        });
        var w = $("<div class='arrows'></div>"),
            v = $('<div class=\'fade-slider-arrow left-arrow\'><i class="fa fa-chevron-left" aria-hidden="true"></i></div>'),
            g = $('<div class=\'fade-slider-arrow right-arrow\'><i class="fa fa-chevron-right" aria-hidden="true"></i></div>');
        d.append(w), d.find(w).append(v), d.find(w).append(g);
        var F = d.find(".arrows");
        1 == l.arrows ? F.css({display: "block"}) : F.css({display: "none"}), d.find(".arrows .right-arrow").click(function () {
            s()
        }), d.find(".arrows .left-arrow").click(function () {
            o()
        });
        $(".fadder img").on("mousedown", function (e) {
            e.preventDefault()
        })
    }
}), $(document).on("ready", mainFunction.init), $(document).on("keyup change", "input", function () {
    $(this).parent(".input-control").removeClass("show-error")
}), $(document).on("keyup change", "textarea", function () {
    $(this).parent(".textarea-control").removeClass("show-error")
}), $(document).on("change", "select", function () {
    $(this).parent(".input-control").removeClass("show-error")
}), $(document).on("change", ".datepicker", function () {
    $(this).parent(".input-control").removeClass("show-error"), $(this).parent().removeClass("show-error")
}), $("#date").focus(function (e) {
    $(this).datepicker()
}), $(document).ready(function () {
    resizeSlider(),
        $(".workshop-pp-name").each(function () {
        var e = $(this).data("name").toString().substr(0, 1);
        $(this).text(e)
    }),
        $(".comment-pp-name").each(function () {
        var e = $(this).data("male"),
            t = $(this).data("female"),
            i = e.toString().substr(0, 1),
            n = t.toString().substr(0, 1);
        // console.log(e)
        $(this).text(i + " & " + n)
    }),
        $('.pp-username').each(function() {
            var dataText = $('.pp-username').data('username');
            // var dataText = $('.pp-username').data('username');
            var arr = dataText.toString().substr(0,1)
            $(this).text(arr)
    }),
        $('.pp-username-one').each(function() {
            var dataFname = $(this).data('fname');
            var dataLname = $(this).data('lname');
            var txtFname = dataFname.toString().substr(0,1);
            var txtLname = dataLname.toString().substr(0,1);
            $(this).text(txtFname + ' ' + txtLname)
    }),
        $(".week-design-list-slider").sliderFade({
        desktopItem: 3,
        tabletItem: 3,
        bigMobileItem: 2,
        mobileItem: 1,
        tabletScreen: 1024,
        bigMobileScreen: 768,
        mobileScreen: 576,
        autoplayTime: 1e3,
        arrows: !0,
        dots: !1
    }), $(".offer-fixed").click(function (e) {
        $("#offerForm").find(".datepicker").val($(".fixed-date").val())
    }), $(window).bind("scroll", function () {
        $(document).height();
        $(window).scrollTop() > 820 ? $(".workshop-stick").addClass("fixed") : $(".workshop-stick").removeClass("fixed");
    }), $(".user-nav-link.selected").click(function (e) {
        $(window).width() < 730 && (e.preventDefault(), $(".user-nav-link:not(.selected)").slideToggle(), $(this).parents("ul").toggleClass("active"), $(".user-hidden-arrow").toggleClass("active"))
    }), $(".btn-singup").click(function (e) {
        e.preventDefault(), $(".sign-in-wrp").hide(), $(".sign-in-form-2").hide(), $(".sign-up-form-mobile").fadeIn()
    }), $(".btn-singin").click(function (e) {
        e.preventDefault(), $(".sign-in-wrp").fadeIn(), $(".sign-in-form-2").fadeIn(), $(".sign-up-form-mobile").hide()
    }), $(".loginMessage").click(function () {
        $(".loginMessage").fadeOut()
    }), $(window).width() > 991 && ($(window).scrollTop() > 80 ? ($(".search-overlay").removeClass("show"), $(".home-page-artc").addClass("fade"), $(".index-header").addClass("fixed")) : ($(".index-header").removeClass("fixed"), $(".home-page-artc").removeClass("fade"), $(".home-container-content").hasClass("active") && $(".search-overlay").addClass("show")), $(window).scrollTop() > 250 ? ($(".home-page-search").addClass("fade"), $(".index-header").addClass("fix-full"), $(".header-search-wrp").addClass("fade-in")) : ($(".home-page-search").removeClass("fade"), $(".index-header").removeClass("fix-full"), $(".header-search-wrp").removeClass("fade-in")), $(window).scrollTop() > 450 ? ($(".category-wrp").addClass("fade-in"), $(".index-header").addClass("shadow")) : ($(".category-wrp").removeClass("fade-in"), $(".index-header").removeClass("shadow"))), $("#main-search-atf").on("focus", function (e) {
        $(".search-overlay").addClass("show"), $(".home-container-content").addClass("active")
    }).blur(function () {
        $(".search-overlay").removeClass("show"), $(".home-container-content").removeClass("active")
    }), $(window).bind("scroll", function () {
        $(window).scrollTop() > 80 ? $(".search-overlay").removeClass("show") : $(".home-container-content").hasClass("active") && $(".search-overlay").addClass("show"), $(window).width() > 991 && ($(window).scrollTop() > 80 ? ($(".search-overlay").removeClass("show"), $(".home-page-artc").addClass("fade"), $(".index-header").addClass("fixed")) : ($(".index-header").removeClass("fixed"), $(".home-page-artc").removeClass("fade"), $(".home-container-content").hasClass("active") && $(".search-overlay").addClass("show")), $(window).scrollTop() > 250 ? ($(".home-page-search").addClass("fade"), $(".index-header").addClass("fix-full"), $(".header-search-wrp").addClass("fade-in")) : ($(".home-page-search").removeClass("fade"), $(".index-header").removeClass("fix-full"), $(".header-search-wrp").removeClass("fade-in")), $(window).scrollTop() > 450 ? ($(".category-wrp").addClass("fade-in"), $(".index-header").addClass("shadow")) : ($(".category-wrp").removeClass("fade-in"), $(".index-header").removeClass("shadow")))
    })
}), $(window).resize(function () {
});