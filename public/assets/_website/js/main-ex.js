function resizeSlider() {
    var sliderItem = $('.workshop-detail-page .slider-area .slider-item');
    sliderItem.css({
        height: sliderItem.width()
    });
    var sliderItemPre = $('.visible-sm .workshop-slider-item');
    sliderItemPre.css({
        height: sliderItemPre.width()
    });

    var sliderItemPrePc = $('.hidden-sm .workshop-slider-item');
    sliderItemPrePc.css({
        height: sliderItemPrePc.width()
    });

    var listItem = $('.week-design-list-item');
    listItem.css({
        height: listItem.width() + 40
    });
    // console.log('test111')
}

setInterval(function(){
    resizeSlider();
    },200);

var mainFunction =
    {

        elements: {

            homePageSlider: ".home-page .home-page-slider",

            customSelect: ".custom-select",

            weekDesignList: ".week-design-list",

            gridItem: ".design-list-grid .design-list-grid-item",

            loadMoreButton: ".home-page .home-page-design-list .load-more",

            modalRegisterButton: ".sign-modal .register-btn",

            userSectionInner: "header .middle-nav-container .middle-nav .user-section .user-section-inner",

            userSectionMenu: "header .middle-nav-container .middle-nav .user-section .user-section-menu",

            workShopDetailSlider: ".workshop-detail-page .slider-area .slider",

            workShopDetailSliderThumbItem: ".workshop-detail-page .slider-area .thumbs li",

            mainSearchInput: ".search-section .search-section-inner input[type=text]",

            mobilSearchInput: ".search-wrap .search-section .search-section-inner input[type=text]",

            workshopItemHeartButton: ".home-page .week-design-list-item .footer .avatar-figure .hover-svg-inner a",

            worksshopDetailLikeButton: ".workshop-detail-page .detail-area .studio-name .studio-name-inner .like-counter",

            bidSuccessButton: "#bid-success-button",

            bidSuccessCompleteButton: "#bid-success-complete-button",

            allWorkShopCategorySlider: ".all-workshop-page .category-slider",

            premiumDetailAllPhotosButton: ".premium-detail-page .detail-section .workshop-slider-container .all-photos-button",

            weekDesignSlider: ".week-design-list-slider"

        },

        MAIN_SEARCH_LIST_JSON_URL: "/search",

        init: function () {

            mainFunction.slickSettings();

            mainFunction.signModalSettings();

            mainFunction.hoverSettings();

            mainFunction.workShopDetailSliderSettings();

            mainFunction.datePickerSettings();

            mainFunction.mainSearchSettings();

            mainFunction.workShopItemSettings();

            mainFunction.formControl();

            mainFunction.workShopDetailLikeSettings();

            mainFunction.bidModalSettings();

            mainFunction.select2Settings();

            mainFunction.inputMask();

            mainFunction.accordionSettings()

            mainFunction.mobilMenuSettings();

            mainFunction.defaultSettings();

            mainFunction.contactSettings();

            mainFunction.premiumDetailSettings();

        },

        defaultSettings: function () {

            if (!$("html").hasClass("sign-success")) {

                /*if( $(window).width() <= 991 ) {
                    $("#account-button").trigger("click");
                }
                else
                {
                    $("#sign-in-modal-button").trigger("click");
                }*/

            }

        },


        contactSettings: function () {

            $('input[name="name"]').keyup(function () {
                $('div#name').hide();
            });

            $('input[name="email"]').keyup(function () {
                $('div#email').hide();
            });

            $('input[name="message"]').keyup(function () {
                $('div#message').hide();
            });

            $("#contact-send-button").on("click", function (e) {

                e.preventDefault();

                function isValidEmailAddress(emailAddress) {
                    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
                    return pattern.test(emailAddress);
                };

                var inputs = $(".sign-modal .contact-form > form > .row-inputs");

                var success = $(".sign-modal .contact-form > form > .row-success");

                var data = $('form#contact-form').serialize();

                var name = $('input[name="name"]').val();
                email = $('input[name="email"]').val();
                message = $('textarea[name="message"]').val();

                if (!name) {
                    $('div#name').show();
                }
                else if (!email || !isValidEmailAddress(email)) {
                    $('div#email').show();
                }
                else if (!message) {
                    $('div#message').show();
                }
                else {
                    $.ajax({
                        method: 'POST',
                        data: data,
                        url: '/iletisim'
                    });
                    TweenMax.to(inputs, .5, {
                        css: {scale: 0, opacity: 0}, ease: Back.easeIn.config(1.7), onComplete: function () {

                            $(inputs).css("display", "none");

                            $(success).css("display", "flex");

                            $(success).css("display", "-webkit-flex");

                            TweenMax.to(success, .5, {css: {scale: 1, opacity: 1}, ease: Back.easeOut.config(1.7)});

                        }
                    });
                }

            });


        },

        premiumDetailSettings: function () {

            $(".fancybox-thumb").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
                helpers: {
                    title: {
                        type: 'outside'
                    },
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });

            $(mainFunction.elements.premiumDetailAllPhotosButton).on("click", function (e) {

                e.preventDefault();

                $(".fancybox-container .fancybox-thumb:first-child").trigger("click");

            });

        },

        formControl: function () {

            function controlPhone(phone) {
                var result = (phone.search("X") == -1) ? true : false;
                return result;
            }

            function controlMail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }

            function controlInstagram(name) {
                var re = /^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-]+$/g;
                return re.test(name);
            }

            $(".form-control-button").on("click", function (e) {


                // var formControl = $(this).parent().parent();
                var formControl = $(this).parent().parent();

                var length = formControl.find("> div").length;

                var controlCount = 0;

                var controlLength = 0;

                for (var i = 0; i < length; i++) {

                    var item = formControl.find("> div:nth-child(" + (i + 1) + ")");

                    var isControl = (item.find(".input-control").length == 0) ? false : true;

                    var isInput = (item.find(".input-container").length == 0) ? false : true;

                    var isSelect = (item.find(".select-container").length == 0) ? false : true;

                    var isTextarea = (item.find(".textarea-control").length == 0) ? false : true;

                    var isCheckBox = (item.find(".checkbox-container").length == 0) ? false : true;

                    if (isControl) {
                        controlLength++;
                        if (isInput) {
                            var input = item.find(".input-container input");
                            var textarea = item.find(".textarea-control textarea");
                            if (input.val() != "") {
                                if (input.parent().hasClass("phone-control")) {
                                    if (controlPhone(input.val())) {
                                        controlCount++;
                                        input.parent().removeClass("show-error");
                                    }
                                    else {
                                        input.parent().addClass("show-error");
                                    }
                                }
                                else if (input.parent().hasClass("mail-control")) {
                                    if (controlMail(input.val())) {
                                        controlCount++;
                                        input.parent().removeClass("show-error");
                                    }
                                    else {
                                        input.parent().addClass("show-error");
                                    }
                                }
                                else if (input.parent().hasClass("instagram-control")) {
                                    if (controlInstagram(input.val())) {
                                        controlCount++;
                                        input.parent().removeClass("show-error");
                                    }
                                    else {
                                        input.parent().addClass("show-error");
                                    }
                                }

                                else if (textarea.parent().hasClass("textarea-control")) {
                                    if (textarea.val()) {
                                        controlCount++;
                                        textarea.parent().removeClass("show-error");
                                    }
                                    else {
                                        textarea.parent().addClass("show-error");
                                    }
                                }

                                else {
                                    controlCount++;
                                    input.parent().removeClass("show-error");
                                    textarea.parent().removeClass("show-error");
                                }
                            }
                            else {
                                input.parent().addClass("show-error");
                                textarea.parent().addClass("show-error");
                                // $('.textare-control textarea').parent().addClass("show-error");

                            }

                        }
                        else if (isSelect) {
                            var select = item.find(".select-container select");
                            if (select.select2("val") != null) {
                                controlCount++;
                                select.parent().removeClass("show-error");
                            }
                            else {
                                select.parent().addClass("show-error");
                            }
                        }
                        else if (isCheckBox) {
                            var checkbox = item.find(".checkbox-container input");
                            if (checkbox.prop("checked")) {
                                controlCount++;
                                checkbox.parent().removeClass("show-error");
                            }
                            else {
                                checkbox.parent().addClass("show-error");
                            }
                        }
                    }
                }

                // if ($('.textarea-control').find('textarea').val()=="") {
                //     formControl.find('.textarea-control').addClass('show-error')
                // }

                if (controlCount == controlLength) {
                    console.log("Success!");
                }
                else {
                    console.log("Error!");
                    e.preventDefault();
                }

            });

        },

        mobilMenuSettings: function () {

            $("#account-button").on("click", function (e) {

                e.preventDefault();

                $("html").toggleClass("right-mobil-menu-open");
                $("html").removeClass("left-mobil-menu-open");
                $('.sign-in-wrp').show();
                $('.sign-in-form-2').show();
                $('.sign-up-form-mobile').hide()

            });

            $("#hamburger-button").on("click", function (e) {
                e.preventDefault();
                $("html").removeClass("right-mobil-menu-open");
                $("html").toggleClass("left-mobil-menu-open");
                $('.sign-in-wrp').show();
                $('.sign-in-form-2').show();
                $('.sign-up-form-mobile').hide()
            });

        },

        accordionSettings: function () {

            $("#accordion").accordion({
                heightStyle: "content",
                collapsible: true,
                beforeActivate: function (event, ui) {
                    if (ui.newHeader[0]) {
                        var currHeader = ui.newHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                    } else {
                        var currHeader = ui.oldHeader;
                        var currContent = currHeader.next('.ui-accordion-content');
                    }
                    var isPanelSelected = currHeader.attr('aria-selected') == 'true';
                    currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));
                    currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);
                    currContent.toggleClass('accordion-content-active', !isPanelSelected)
                    if (isPanelSelected) {
                        currContent.slideUp();
                    } else {
                        currContent.slideDown();
                    }

                    return false;
                }
            });

        },

        inputMask: function () {

            $(".phone-mask").inputmask({
                mask: '(+\\90) 999 999 99 99',
                placeholder: '(+90) XXX XXX XX XX',
                showMaskOnHover: false
            });

        },

        select2Settings: function () {

            $(".workshop-category-select").select2({
                placeholder: "Atöyle Kategorisi",
                minimumResultsForSearch: Infinity,
                maximumSelectionLength: 2
            });

            $(".country-category-select").select2({placeholder: "Ülke", minimumResultsForSearch: Infinity});

            $(".city-category-select").select2({placeholder: "Şehir", minimumResultsForSearch: Infinity});

            $(".day-category-select").select2({placeholder: "Gün", minimumResultsForSearch: Infinity});

            $(".month-category-select").select2({placeholder: "Ay", minimumResultsForSearch: Infinity});

            $(".year-category-select").select2({placeholder: "Yıl", minimumResultsForSearch: Infinity});

        },

        bidModalSettings: function () {

            $(mainFunction.elements.bidSuccessButton).on("click", function () {

                $("#bid-modal").find(".close").trigger("click");

            });

            $(mainFunction.elements.bidSuccessCompleteButton).on("click", function () {

                $("#bid-success-modal").removeClass("show");

            });

            $("#add-list-button").on("click", function () {

                $("#fav-modal").removeClass("show");

                $("#fav-modal").find(".close").trigger("click");

            });

        },

        workShopDetailLikeSettings: function () {

            $(mainFunction.elements.worksshopDetailLikeButton).on("click", function () {

                if ($(this).hasClass("like-counter-selected")) {
                    $(this).removeClass("like-counter-selected");

                    $(this).find("span").text(Number($(this).find("span").text()) - 1);
                }
                else {
                    $(this).addClass("like-counter-selected");

                    $(this).find("span").text(Number($(this).find("span").text()) + 1);
                }

            });

        },

        workShopItemSettings: function () {

            $(mainFunction.elements.workshopItemHeartButton).on("click", function (e) {

                e.preventDefault();

                var parent = $(this).parent().parent().parent().parent();

                var countItem = parent.find(".footer-inner a span");

                var countA = parent.find(".footer-inner a");

                if (!$(this).parent().hasClass("selected")) {
                    $(this).parent().addClass("selected");

                    $(this).parent().addClass("selected");

                    countItem.text(Number(countItem.text()) + 1);

                    countA.addClass("increase");
                }
                else {
                    $(this).parent().removeClass("selected");

                    $(this).parent().removeClass("selected");

                    countItem.text(Number(countItem.text()) - 1);

                    countA.removeClass("increase");
                }

            });

        },

        mainSearchSettings: function () {

            var options = {

                url: mainFunction.MAIN_SEARCH_LIST_JSON_URL,

                categories: [

                    {listLocation: "workshops", header: "Atölyeler"},

                    {listLocation: "categories", header: "Kategoriler"}

                ],

                getValue: "title",

                template: {

                    type: "links",

                    fields: {link: "url"}

                },

                list: {

                    match: {enabled: true},

                    showAnimation: {type: "fade", time: 400},

                    hideAnimation: {type: "fade", time: 400}

                }

            };

            $(mainFunction.elements.mainSearchInput).easyAutocomplete(options);

            $(mainFunction.elements.mobilSearchInput).easyAutocomplete(options);

        },

        datePickerSettings: function () {

            $(".datetimepicker").datepicker({

                monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],

                dayNamesMin: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt"],

                firstDay: 1,

                minDate: 0

            });

            $(".datetimepicker").datepicker("option", "showAnim", "slideDown");

            $(".datepicker").datepicker({

                monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],

                dayNamesMin: ["Paz", "Pzt", "Sal", "Çar", "Per", "Cum", "Cmt"],

                firstDay: 1,

                minDate: 0,
                dateFormat: 'dd/mm/yy'

            });

            $(".datepicker").datepicker("option", "showAnim", "slideDown");

        },

        workShopDetailSliderSettings: function () {

            $(mainFunction.elements.workShopDetailSliderThumbItem).on("click", function () {

                $(mainFunction.elements.workShopDetailSliderThumbItem).removeClass("selected");

                $(this).addClass("selected");

                var index = $(this).index();

                var slickThumb = $(mainFunction.elements.workShopDetailSlider).find(".slick-dots li:nth-child(" + (index + 1) + ")");

                slickThumb.trigger("click");

            });

        },

        hoverSettings: function () {

            setHover(mainFunction.elements.userSectionInner, mainFunction.elements.userSectionMenu);

            function setHover(item, hoverContent) {

                $(item + "," + hoverContent).hover(function () {

                    $(item).addClass("selected");

                    $(hoverContent).css("display", "block");

                    TweenMax.to($(hoverContent), .5, {opacity: 1});

                }, function () {

                    $(item).removeClass("selected");

                    TweenMax.to($(hoverContent), .5, {
                        opacity: 0, onComplete: function () {

                            $(hoverContent).css("display", "none");

                        }
                    });

                });

            }

        },

        signModalSettings: function () {

            $(mainFunction.elements.modalRegisterButton).on("click", function () {

                $("#sign-in-modal").find(".close").trigger("click");

            });

        },

        slickSettings: function () {

            $(mainFunction.elements.homePageSlider).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false
            });

            // $(mainFunction.elements.weekDesignSlider).slick({slidesToShow: 3, slidesToScroll: 3, speed: 500,fade:false, cssEase: 'linear', arrows: true, dots: false,  responsive: [ { breakpoint: 767, settings: { slidesToShow: 2, slidesToScroll: 2 } }, { breakpoint: 450, settings: { slidesToShow: 1, slidesToScroll: 1 }}]});

            $(mainFunction.elements.workShopDetailSlider).slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                prevArrow: $(".list-prev"),
                nextArrow: $(".list-next"),
                dots: true,
                responsive: [{breakpoint: 767, settings: {slidesToShow: 1, dots: false, arrows: true}}]
            });

            $(mainFunction.elements.workShopDetailSlider).on('afterChange', function (event, slick, currentSlide) {
                $(mainFunction.elements.workShopDetailSliderThumbItem).removeClass("selected");
                var slickThumb = $(".workshop-detail-page .slider-area .thumbs li:nth-child(" + (currentSlide + 1) + ")");
                slickThumb.trigger("click");
            });

            $(mainFunction.elements.allWorkShopCategorySlider).slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {slidesToShow: 3}
                    },
                    {
                        breakpoint: 767,
                        settings: {slidesToShow: 2}
                    },
                    {
                        breakpoint: 450,
                        settings: {slidesToShow: 1}
                    }
                ]
            });


            $('#premium-detail-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                asNavFor: '#premium-detail-slider-thumb'
            });

            $('#premium-detail-slider-thumb').slick({
                slidesToShow: 6,
                slidesToScroll: 1,
                asNavFor: '#premium-detail-slider',
                arrows: false,
                dots: false,
                focusOnSelect: true,
                variableWidth: true
            });

            $('#premium-detail-slider-mobil').slick({slidesToShow: 1, slidesToScroll: 1, arrows: true});

        },

        infiniteScroll: function () {

            var items = $(mainFunction.elements.gridItem);

            var collection = [];

            var maxShow = 3;

            removeAndPush();

            showNext();

            $(document).on("scroll", function () {

                var top = $("body").scrollTop();

                var gap = (($(".base-page").height() + $("header").height()) - $(window).height()) + 100;

                if (top >= (gap - 80)) {
                    showNext();
                }

            });

            $(mainFunction.elements.loadMoreButton).on("click", function (e) {

                e.preventDefault();

                showNext();

            });

            function removeAndPush() {

                for (var i = 0; i < items.length; i++) {

                    var item = $(".design-list-grid .design-list-grid-item:nth-child(" + (i + 1) + ")");

                    var realItem = $(item);

                    collection.push({item: realItem});

                }

                items.remove();

            }

            function showNext() {

                var newtMaxShow = maxShow;

                if (maxShow >= collection.length) {
                    newtMaxShow = collection.length;
                }

                for (var k = 0; k < newtMaxShow; k++) {

                    mainFunction.blogGrid.append(collection[k].item);

                    mainFunction.blogGrid.isotope('layout');

                }

                for (var l = 0; l < newtMaxShow; l++) {

                    collection.shift();

                }

                if (collection.length == 0) {

                    $(mainFunction.elements.loadMoreButton).remove();

                }

                mainFunction.workShopItemSettings();

            }

        },
    };

/* fade slider */

/*!
 * Fadder - jQuery Plugin
 * version: 1.0.0 (Sat, 18 Mar 2017)
 * @requires jQuery v1.9 or later
 *
 * Examples at ##
 * License: ##
 *
 * Copyright 2017
 *
 */

$(function () {

    'use strict';

    $.fn.sliderFade = function (options) {
        // Default settings
        var settings = $.extend({
            desktopItem: 4,
            tabletItem: 4,
            bigMobileItem: 3,
            mobileItem: 2,
            tabletScreen: 1024,
            bigMobileScreen: 768,
            mobileScreen: 640,
            autoplayTime: 5000,
            dots: true,
            arrows: true,
        }, options);

        var $this = $(this);

        var $outer_class = $this.addClass('fadder');

        function wrapEvery(num, wrapper) {

            var items = $this.find('.single');

            num = num || items.length;

            wrapper = wrapper || $('<div/>', {
                "class": "fade-outer"
            });
            var itemsToWrap = [];
            var parentClass = wrapper.attr('class');

            items.parent('.' + parentClass).children().unwrap();

            while (items.length > 0) {
                itemsToWrap.push(items.splice(0, num));
            }

            itemsToWrap.forEach(function (arr) {
                $(arr).wrapAll(wrapper);
            });

            return items;
        };

        function widthCount(perWidth) {
            $this.find('.single').css({
                "width": (100 / perWidth) + '%'
            });
            $this.find('.fade-outer').first().addClass('active').fadeIn();
            return this;
        }


        function wrapItems() {

            if ($(window).width() <= settings.mobileScreen) {
                wrapEvery(settings.mobileItem);
                widthCount(settings.mobileItem);
            }
            else if ($(window).width() > settings.mobileScreen && $(window).width() <= settings.bigMobileScreen) {
                wrapEvery(settings.bigMobileItem);
                widthCount(settings.bigMobileItem);
            }
            else if ($(window).width() > settings.bigMobileScreen && $(window).width() <= settings.tabletScreen) {
                wrapEvery(settings.tabletItem);
                widthCount(settings.tabletItem);
            }
            else if ($(window).width() > settings.tabletScreen && $(window).width() <= 5000) {
                wrapEvery(settings.desktopItem);
                widthCount(settings.desktopItem);
            }

        }

        wrapItems(); //fire on document ready

        var windowWidth = $(window).width();
        var lightbox_resize = false;
        $(window).resize(function () {
            if ($(window).width() != windowWidth) {

                // Update the window width for next time
                windowWidth = $(window).width();

                // Do stuff here
                if (lightbox_resize)
                    clearTimeout(lightbox_resize);
                lightbox_resize = setTimeout(function () {
                    wrapItems()
                }, 200);
            }

        });


        /*----------------------------------------------------------------*/

        function ul_height() {
            var my_height = $this.find('.fade-outer.active').height();
            var listItem = $('.week-design-list-item');
            listItem.css({
                height: listItem.width() + 40
            });
            // $this.find('.item-slider').css('height', my_height);
            $this.find('.item-slider').css('height', listItem.width() + 40);


        }

        setTimeout(function () {
            ul_height();
        }, 50)

        var timer;

        // function start_interval(){
        //     timer = setInterval(function(){
        //         next_move();
        //         ul_height();
        //     }, settings.autoplayTime);
        // }

        function next_move() {
            if ($this.find('.fade-outer:last').hasClass('active')) {
                $this.find('.fade-outer.active').removeClass('active');
                $this.find('.fade-outer:first').addClass('active');
            } else {
                $this.find('.fade-outer.active').removeClass('active').next('.fade-outer').addClass('active');
            }
            if ($this.find('.dots span:last').hasClass('active')) {
                $this.find('.dots span.active').removeClass('active');
                $this.find('.dots span:first').addClass('active');
            } else {
                $this.find('.dots span.active').removeClass('active').next('.dots span').addClass('active');
            }
            ul_height();
            clearInterval(timer);
            // start_interval()
        }

        function prev_move() {
            if ($this.find('.fade-outer:first').hasClass('active')) {
                $this.find('.fade-outer.active').removeClass('active');
                $this.find('.fade-outer:last').addClass('active');
            } else {
                $this.find('.fade-outer.active').removeClass('active').prev('.fade-outer').addClass('active');
            }
            if ($this.find('.dots span:first').hasClass('active')) {
                $this.find('.dots span.active').removeClass('active');
                $this.find('.dots span:last').addClass('active');
            } else {
                $this.find('.dots span.active').removeClass('active').prev('.dots span').addClass('active');
            }
            ul_height();
            clearInterval(timer);
            // start_interval()
        }

        // start_interval();

        function make_dots() {
            var $dots = $("<div class='dots'></div>");
            $this.append($dots);
            $this.find(".dots").empty();
        }

        make_dots();

        function sliderDots() {
            $this.find(".dots").empty();
            var items = $this.find('.single');
            var all_length = $this.find('.fade-outer').length;

            for (var dot_length = 0; dot_length < all_length; dot_length++) {
                var dot_length_new = dot_length + 1
                $this.find(".dots").append('<span>' + dot_length_new + '</span>');
            }

            $this.find(".dots span:first").addClass('active');

            return this;
        };
        sliderDots();

        var $dots_show = $this.find(".dots");
        if (settings.dots == true) {
            $dots_show.css({
                "display": 'block',
            });
        }
        else {
            $dots_show.css({
                "display": 'none',
            });
        }

        var lightbox_resize2 = false;
        var windowWidth2 = $(window).width();
        $(window).resize(function () {
            if ($(window).width() != windowWidth2) {

                windowWidth2 = $(window).width();

                if (lightbox_resize2)
                    clearTimeout(lightbox_resize2);
                lightbox_resize2 = setTimeout(function () {
                    sliderDots();
                    ul_height();
                    $this.find(".dots span").on('click', function (event) {
                        event.preventDefault();
                        var dot_num = $(this).index();
                        $(this).addClass('active').siblings().removeClass('active');
                        $this.find('.fade-outer').eq(dot_num).addClass('active').siblings().removeClass('active');

                        clearInterval(timer);
                        // start_interval();
                    });

                }, 500);
            }
        });

        $this.find(".dots span").on('click', function (event) {
            event.preventDefault();
            var dot_num = $(this).index();
            $(this).addClass('active').siblings().removeClass('active');
            $this.find('.fade-outer').eq(dot_num).addClass('active').siblings().removeClass('active');

            clearInterval(timer);
            // start_interval();
        });

        /*----------------------------------------------------------------*/

        var $arrows = $("<div class='arrows'></div>"),
            $left_arrow = $("<div class='fade-slider-arrow left-arrow'><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i></div>"),
            $right_arrow = $("<div class='fade-slider-arrow right-arrow'><i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i></div>");

        $this.append($arrows);
        $this.find($arrows).append($left_arrow)
        $this.find($arrows).append($right_arrow)

        var $arrow_show = $this.find(".arrows");
        if (settings.arrows == true) {
            $arrow_show.css({
                "display": 'block',
            });
        }
        else {
            $arrow_show.css({
                "display": 'none',
            });
        }

        $this.find('.arrows .right-arrow').click(function () {
            next_move()
        })
        $this.find('.arrows .left-arrow').click(function () {
            prev_move()
        });

        var count = 0;
        // var swipe_blocks = $this.find(".fade-outer");
        //
        // swipe_blocks.swipe( {
        //     swipeLeft:function(event, direction, distance, duration, fingerCount) {
        //         next_move()
        //     },
        //     swipeRight:function(event, direction, distance, duration, fingerCount) {
        //         prev_move()
        //     }
        // });

        $('.fadder img').on('mousedown', function (e) {
            e.preventDefault();
        });

    };

});

/* end fade slider */


$(document).on("ready", mainFunction.init);
$(document).on("keyup change", "input", function () {
    $(this).parent('.input-control').removeClass("show-error");
});
$(document).on("keyup change", "textarea", function () {
    $(this).parent('.textarea-control').removeClass("show-error");
});

// $(document).on("change", "input", function(){
//     $(this).parent('.input-control').removeClass("show-error");
// });


$(document).on("change", "select", function () {
    $(this).parent('.input-control').removeClass("show-error");
});

$(document).on("change", ".datepicker", function () {
    $(this).parent('.input-control').removeClass("show-error");
    $(this).parent().removeClass("show-error");
});



$('#date').focus(function (e) {
    $(this).datepicker();

});

$(document).ready(function () {
    // var sliderItem = $('.workshop-detail-page .slider-area .slider-item');
    // sliderItem.css({
    //     height: sliderItem.width()
    // });
    // var sliderItemPre = $('.visible-sm .workshop-slider-item');
    // sliderItemPre.css({
    //     height: sliderItemPre.width()
    // });
    //
    // var listItem = $('.week-design-list-item');
    // listItem.css({
    //     height: listItem.width() + 40
    // });

    resizeSlider();

    // var text = '';
    // var dataName= $('.workshop-pp-name');
    // var arrayData = [] ;
    $('.workshop-pp-name').each(function() {
        var dataText = $(this).data('name');
        var arr = dataText.substr(0,1)
        $(this).text(arr)
    });

    $('.comment-pp-name').each(function() {
        var dataMale = $(this).data('male');
        var dataFemale = $(this).data('female');
        var txtMale = dataMale.substr(0,1)
        var txtFemale = dataFemale.substr(0,1)
        $(this).text(txtMale + ' & ' + txtFemale)
    });

    /*$('.pp-username').each(function() {
        var txt = $(this).data('username');
        var txt = txt.substr(0,1);
        $(this).text(txt)
    });*/

    // var fname = $('.pp-username-one').data('fname');
    // var lname = $('.pp-username-one').data('lname');
    // var editfName = fname.substr(0,1)
    // var editlName = lname.substr(0,1)
    // $('.pp-username-one').text(editfName + editlName);




//     var arr = "Java Script Object Notation".split(' ');
// for(i=0;i<arr.length;i++) {
//     text += arr[i].substr(0,1)
// }


// alert(text);


    // $(mainFunction.elements.allWorkShopCategorySlider).sliderFade({
    //     // options...
    //     desktopItem: 4,  //Greater than 1024
    //     tabletItem: 3,  //1023 to 768
    //     bigMobileItem: 2, //768 to 420
    //     mobileItem: 1, //420 to 0
    //     tabletScreen: 1024,
    //     bigMobileScreen: 768,
    //     mobileScreen: 576,
    //     autoplayTime: 1000, //autoplay speed in milisecond
    //     arrows: true, //true or false
    //     dots: false
    // });

    // var listItem = $('.week-design-list-item');
    //
    // listItem.css({
    //     height: listItem.width() + 40
    // });



    $('.week-design-list-slider').sliderFade({
        // options...
        desktopItem: 3,  //Greater than 1024
        tabletItem: 3,  //1023 to 768
        bigMobileItem: 2, //768 to 420
        mobileItem: 1, //420 to 0
        tabletScreen: 1024,
        bigMobileScreen: 768,
        mobileScreen: 576,
        autoplayTime: 1000, //autoplay speed in milisecond
        arrows: true, //true or false
        dots: false
    });

    // listItem.css({
    //     height: listItem.width() + 40
    // });

    $('.offer-fixed').click(function (e) {
        $('#offerForm').find('.datepicker').val($('.fixed-date').val())
    });

    $(window).bind('scroll', function () {
        var docHeight = $(document).height();
        // alert(targetOffset)
        if ($(window).scrollTop() > 820) {
            $('.workshop-stick').addClass('fixed');
        } else {
            $('.workshop-stick').removeClass('fixed');
        }
    });


    $('.user-nav-link.selected').click(function (e) {
        if ($(window).width() < 730) {
            e.preventDefault();
            $('.user-nav-link:not(.selected)').slideToggle();
            $(this).parents('ul').toggleClass('active');
            $('.user-hidden-arrow').toggleClass('active')
        }
    });

    $('.btn-singup').click(function (e) {
        e.preventDefault();
        $('.sign-in-wrp').hide();
        $('.sign-in-form-2').hide();
        $('.sign-up-form-mobile').fadeIn()
    });

    $('.btn-singin').click(function (e) {
        e.preventDefault();
        $('.sign-in-wrp').fadeIn();
        $('.sign-in-form-2').fadeIn();
        $('.sign-up-form-mobile').hide()
    });

    $('.loginMessage').click(function () {
        $('.loginMessage').fadeOut();
    });


    if ($(window).width() > 991) {


        if ($(window).scrollTop() > 80) {
            $('.search-overlay').removeClass('show');
            $('.home-page-artc').addClass('fade');
            $('.index-header').addClass('fixed');
            // $('.logo-section').addClass('active');

        }

        else {
            $('.index-header').removeClass('fixed');
            $('.home-page-artc').removeClass('fade');
            // $('.logo-section').removeClass('active');
            // $('.home-page-search').removeClass('fade');
            // $('.header-search-wrp').removeClass('fade-in');

            if ($('.home-container-content').hasClass('active')) {
                $('.search-overlay').addClass('show');
            }
        }
        if ($(window).scrollTop() > 250) {
            $('.home-page-search').addClass('fade');
            $('.index-header').addClass('fix-full');
            $('.header-search-wrp').addClass('fade-in');

        } else {
            $('.home-page-search').removeClass('fade');
            $('.index-header').removeClass('fix-full');
            // $('.category-wrp').removeClass('fade-in');
            $('.header-search-wrp').removeClass('fade-in');//

        }

        if ($(window).scrollTop() > 450) {
            $('.category-wrp').addClass('fade-in');
            $('.index-header').addClass('shadow');


        }

        else {
            $('.category-wrp').removeClass('fade-in');
            $('.index-header').removeClass('shadow');
        }

    }

    $('#main-search-atf').on('focus', function (e) {
        $('.search-overlay').addClass('show');
        $('.home-container-content').addClass('active');
    }).blur(function () {
        $('.search-overlay').removeClass('show');
        $('.home-container-content').removeClass('active');
    });


    $(window).bind('scroll', function () {

        if ($(window).scrollTop() > 80) {
            $('.search-overlay').removeClass('show');
        }

        else {
            if ($('.home-container-content').hasClass('active')) {
                $('.search-overlay').addClass('show');
            }
        }

        if ($(window).width() > 991) {


            if ($(window).scrollTop() > 80) {
                $('.search-overlay').removeClass('show');
                $('.home-page-artc').addClass('fade');
                $('.index-header').addClass('fixed');
                // $('.logo-section').addClass('active');

            }

            else {
                $('.index-header').removeClass('fixed');
                $('.home-page-artc').removeClass('fade');
                // $('.logo-section').removeClass('active');
                // $('.home-page-search').removeClass('fade');
                // $('.header-search-wrp').removeClass('fade-in');

                if ($('.home-container-content').hasClass('active')) {
                    $('.search-overlay').addClass('show');
                }
            }
            if ($(window).scrollTop() > 250) {
                $('.home-page-search').addClass('fade');
                $('.index-header').addClass('fix-full');
                $('.header-search-wrp').addClass('fade-in');

            } else {
                $('.home-page-search').removeClass('fade');
                $('.index-header').removeClass('fix-full');
                // $('.category-wrp').removeClass('fade-in');
                $('.header-search-wrp').removeClass('fade-in');//

            }

            if ($(window).scrollTop() > 450) {
                $('.category-wrp').addClass('fade-in');
                $('.index-header').addClass('shadow');


            }

            else {
                $('.category-wrp').removeClass('fade-in');
                $('.index-header').removeClass('shadow');
            }

        }

    });
});


$(window).resize(function () {
    // var listItem = $('.week-design-list-item');
    //
    // listItem.css({
    //     height: listItem.width() + 40
    // });

    // var sliderItem = $('.workshop-detail-page .slider-area .slider-item');
    // sliderItem.css({
    //     height: sliderItem.width()
    // });
    //
    // var sliderItemPre = $('.visible-sm .workshop-slider-item');
    // sliderItemPre.css({
    //     height: sliderItemPre.width()
    // });

    // resizeSlider();

});










