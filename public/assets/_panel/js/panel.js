    $(document).ready(function(){

    "use strict";

    var values = [],
            labels = [],
            legends = true,
            legendsElement = $('#TicketByDepartmentLegends'),
        colors = ['#fcaf17','#aa85bc','#7cbe88','#e67157'];

        $("#TicketByDepartment tr").each(function () {
            values.push(parseInt($("td", this).text(), 10));
            labels.push($("th", this).text());
        });

        $("#TicketByDepartment").hide();
        var r = Raphael("DonutTicketsByDepartment", 200, 200);
            r.donutChart(100, 100, 88, 35, values, labels, colors, legends, legendsElement, colors);


        $(".bar-colours-1").peity("bar", {
          fill: ["red", "green", "blue"]
        })

        $(".bar-colours-2").peity("bar", {
          fill: function(value) {
            return value > 0 ? "green" : "red"
          }
        })

        $(".bar-colours-3").peity("bar", {
          fill: function(_, i, all) {
            var g = parseInt((i / all.length) * 255)
            return "rgb(255, " + g + ", 0)"
          }
        })

        $(".pie-colours-1").peity("pie", {
          fill: ["cyan", "magenta", "yellow", "black"]
        })

        $(".pie-colours-2").peity("pie", {
          fill: function(_, i, all) {
            var g = parseInt((i / all.length) * 255)
            return "rgb(255, " + g + ", 0)"
          }
        })

        $(".data-attributes span").peity("donut")

        $(".bars").peity("bar")

        $(".line").peity("line")

        $('.donut').peity('donut')

        $("span.pie").peity("pie")

        //*** Pie Charts ***//
        function sliceSize(dataNum, dataTotal) {
          return (dataNum / dataTotal) * 360;
        }
        function addSlice(sliceSize, pieElement, offset, sliceID, color) {
          $(pieElement).append("<div class='slice "+sliceID+"'><span></span></div>");
          var offset = offset - 1;
          var sizeRotation = -158 + sliceSize;
          $("."+sliceID).css({
            "transform": "rotate("+offset+"deg) translate3d(0,0,0)"
          });
          $("."+sliceID+" span").css({
            "transform"       : "rotate("+sizeRotation+"deg) translate3d(0,0,0)",
            "background-color": color
          });
        }
        function iterateSlices(sliceSize, pieElement, offset, dataCount, sliceCount, color) {
          var sliceID = "s"+dataCount+"-"+sliceCount;
          var maxSize = 158;
          if(sliceSize<=maxSize) {
            addSlice(sliceSize, pieElement, offset, sliceID, color);
          } else {
            addSlice(maxSize, pieElement, offset, sliceID, color);
            iterateSlices(sliceSize-maxSize, pieElement, offset+maxSize, dataCount, sliceCount+1, color);
          }
        }
        function createPie(dataElement, pieElement) {
          var listData = [];
          $(dataElement+" span").each(function() {
            listData.push(Number($(this).html()));
          });
          var listTotal = 0;
          for(var i=0; i<listData.length; i++) {
            listTotal += listData[i];
          }
          var offset = 0;
          var color = [
            "#bebebe", 
            "#ffb8b8", 
            "#ace9ff", 
            "#b8b8ff", 
            "#faa5ff", 
          ];
          for(var i=0; i<listData.length; i++) {
            var size = sliceSize(listData[i], listTotal);
            iterateSlices(size, pieElement, offset, i, 0, color[i]);
            $(dataElement+" li:nth-child("+(i+1)+")").css("border-color", color[i]);
            offset += size;
          }
        }
        createPie(".pieID.legend", ".pieID.pie");


        //*** Random Numbers ***//
        function generate() {
            $('#number').text(Math.floor(Math.random() * 100) + 1);
        }
        setInterval(generate, 1000);

        //*** Live Updating Chart ***//
        var updatingChart = $(".updating-chart").peity("line", { 
            width: ["100px"],
             fill: ["#b8b8ff"],
             stroke: ["#8181ff"],
             height: ["40px"]
            })
        setInterval(function() {
          var random = Math.round(Math.random() * 10)
          var values = updatingChart.text().split(",")
          values.shift()
          values.push(random)

          updatingChart
            .text(values.join(","))
            .change()
        }, 1000)

        //*** Server Chart ***//
        var data = [
            { y: '2005', a: 50, b: 90},
            { y: '2006', a: 65,  b: 75},
            { y: '2007', a: 50,  b: 50},
            { y: '2008', a: 75,  b: 60},
            { y: '2009', a: 80,  b: 65},
            { y: '2010', a: 90,  b: 70},
            { y: '2011', a: 100, b: 75},
            { y: '2012', a: 115, b: 75},
            { y: '2013', a: 120, b: 85},
            { y: '2014', a: 145, b: 85},
            { y: '2014', a: 160, b: 95}
          ],
          config = {
            data: data,
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Server 1', 'Server 2'],
            fillOpacity: 0.7,
            hideHover: 'auto',
            behaveLikeLine: true,
            resize: true,
            pointFillColors:['#ffffff'],
            pointStrokeColors: ['gray'],
            lineColors:['#b8b8ff','#ffb8b8']
        };
        config.element = 'area-chart';
        Morris.Area(config);

        //*** Activity Chart ***//
        function updateChartData(object, data) {
          for (var i = 0; i < data.length; i++) {
            object[i].value = data[i];
          }
        };

        var data = {
            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "Octuber", "Noverber", "December"],
            datasets: [
                {
                    label: "My First dataset",
                    fillColor: "rgba(255,162,162,0.2)",
                    strokeColor: "rgba(255,162,162,1)",
                    pointColor: "rgba(255,162,162,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: [65, 59, 120, 200, 130, 100, 180, 150, 120, 170, 210, 350]
                },
                {
                    label: "My Second dataset",
                    fillColor: "rgba(173,173,253,0.2)",
                    strokeColor: "rgba(173,173,253,1)",
                    pointColor: "rgba(173,173,253,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(173,173,253,1)",
                    data: [100, 200, 150, 300, 90, 180, 350, 100, 400, 200, 50, 400]
                }
            ]
        };

        var ctx = document.getElementById("chart").getContext("2d");
        var myLineChart = new Chart(ctx).Line(data, {
            multiTooltipTemplate: "<%= value %>%",
            legendTemplate: '',
            scaleShowLabels: false,
          scaleShowGridLines : true,
        pointDotRadius : 6,

            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth : 2,

            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius : 20,
            //String - Colour of the grid lines
            scaleGridLineColor : "rgba(0,0,0,.0)",


            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,

            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
        });


        //*** Piety Mini Charts ***//
        $(function() {
            $(".bar").peity("bar", {
              fill: ["#ff8484"],
              height: ["40px"],
              width: ["94px"]
            })

            $(".bar2").peity("bar", {
              fill: ["#9797ff"],
              height: ["40px"],
              width: ["94px"]
            })
        });

    });