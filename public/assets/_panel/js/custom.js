function deleteStuff(item){
    swal({
        title: 'Uyarı!',
        text: "Bu öğeyi silmek istediğinden emin misin?",
        type: 'warning',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sil',
        cancelButtonText: 'İptal',
        showCancelButton: true,
      }).then(function () {
        $(item).parent('form').submit();
        swal(
          'Silindi!',
          'Silme işlemi başarılı',
          'success',
        )
      });
}
function confirmComment(item) {
  swal({
        title: 'Uyarı!',
        text: "Bu yorumu onaylamak istediğinize emin misiniz?",
        type: 'warning',
        confirmButtonColor: '#66CC99',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Onayla',
        cancelButtonText: 'İptal',
        showCancelButton: true,
      }).then(function () {
        $(item).parent('form').submit();
        swal(
          'Onaylandı!',
          'Onaylama işlemi başarılı',
          'success',
        )
      });
}

function disabledComment(item) {
  swal({
        title: 'Uyarı!',
        text: "Bu yorumu yayından almak istediğinize emin misiniz?",
        type: 'warning',
        confirmButtonColor: '#66CC99',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yayından Al',
        cancelButtonText: 'İptal',
        showCancelButton: true,
      }).then(function () {
        $(item).parent('form').submit();
        swal(
          'Yayından Alındı!',
          'Yayından alma işlemi başarılı',
          'success',
        )
      });
}

$('#myCarousel').carousel({
                interval: 5000
        });
 
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click(function () {
        var id_selector = $(this).attr("id");
        try {
            var id = /-(\d+)$/.exec(id_selector)[1];
            console.log(id_selector, id);
            jQuery('#myCarousel').carousel(parseInt(id));
        } catch (e) {
            console.log('Regex failed!', e);
        }
    });
        // When the carousel slides, auto update the text
        $('#myCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });

$('li#thumb').hover(function(){
  $(this).find('div').animate({ opacity: 1 });
}, function(){
  $(this).find('div').animate({ opacity: 0.3 });
});

// Filemenager
$('#files').filemanager('file');
$('#cover').filemanager('file');
$('#icon').filemanager('file');
$('.blogImage').filemanager('file');
// Tinymce Config

var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 2,
        height : y * 2,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);

  // SEO url
  
  $('#slug').slugify('#title');

  // Selectbox


  $(".workshop-category-select").select2({
    placeholder: "Atölye Kategorisi",
    minimumResultsForSearch: Infinity,
    maximumSelectionLength: 2
  });

var mainFunction = 
{

  elements: {

    customAdminCategorySelect : "#admin-category-select",

    showImage : ".custom-admin-select .image-show"

  },

  init: function(){
    
    $(mainFunction.elements.customAdminCategorySelect).on("change", function(){

      var value = $(this).val();

      $(mainFunction.elements.showImage).find("svg").remove();

      $(mainFunction.elements.showImage).append( mainFunction.getCategorySVG(value) );

    });
    
  },

  getCategorySVG: function(value){

    var item = "<svg class='icon " + value + "'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='/files/svg/icons.svg#" + value +"'></use></svg>";

    return item;

  }

};

$(document).on("ready", mainFunction.init);