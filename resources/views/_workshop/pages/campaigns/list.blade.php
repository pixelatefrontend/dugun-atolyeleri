@extends('_workshop.master')
@section('content')
<div class="panel-content">
    <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                <a href="{{ route('campaigns.create') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-plus"></i></span>
                                <h3>Kampanya Ekle</h3>
                                <p>Yeni Kampanya Oluştur</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="table-area">
                    <div class="widget-title">
                        <h3>Kampanya Tablosu</h3>
                        <span>{{ $item->campaigns->count() }} Kampanya Listelendi</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Başlık</th>
                                    <th>Oluşturulma Tarihi</th>
                                    <th>Durum</th>
                                    <th>İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($item->campaigns as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->created_at->diffForHumans() }}</td>
                                    @if($item->active == 1)
                                    <td><span class="label label-success">Yayında</span></td>
                                    @else
                                    <td><span class="label label-warning">Beklemede</span></td>
                                    @endif
                                    <td>
                                        <a href="{{route('campaigns.edit', $item->id)}}" target="_blank">
                                            <button class="btn btn-edit">
                                                Düzenle <i class="fa fa-edit"></i>
                                            </button>
                                        </a>
                                        {!! stuff_destroy([ 'campaigns.destroy', $item->id]) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
@endsection