@extends('_workshop.master')
@section('content')
@include('_workshop.partials.section-header')
	<div class="panel-content">
		<div class="row">
			<div class="col-md-12">
				@if(Session::has('message'))
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
							<strong><i class="fa fa-check"></i></strong> {{ Session::get('message') }}
					</div>
				@endif
				@if(count($errors) > 0)
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
						<strong><i class="fa fa-remove"></i></strong> Boş alan bırakılamaz.
					</div>
				@endif
			</div>
		</div>
		<div class="widget">
					<div class="form-elements-sec">
						<div class="widget-title">
							<h3>Kampanya Ekle</h3>
						</div>
						{!! Form::open([ 'url' => route('campaigns.store'), 'class' => 'sec', 'role' => 'form' ]) !!}
						<div class="row">
							<div class="col-md-6 m-b">
								<div class="form-group">
									<label for="title">Kampanya Başlığı</label>
									<input type="text" placeholder="Başlık" class="form-control custom-input" name="title" id="title">
								</div>
							</div>
							<div class="col-md-6 m-b">
								<div class="form-group">
									<label for="discount">İndirim Oranı <small>(opsiyonel)</small></label>
									<input type="text" placeholder="%50" class="form-control custom-input" name="discount" id="discount">
								</div>
							</div>
							<div class="col-md-6 m-b">
								<div class="form-group">
									<label for="status">Kampanya Durumu</label>
									<select name="active" id="access" class="form-control custom-input" placeholder="Seçiniz">
										<option value="" disabled selected hidden>Durum</option>
										<option value="1">Aktif</option>
										<option value="0">Pasif</option>
									</select>
								</div>
							</div>
							<div class="col-md-6 m-b">
								<div class="form-group">
									<label for="end_date">Kampanya Bitiş Tarihi</label>
									<input type="date" name="end_date" class="form-control custom-input">
								</div>
							</div>
							<div class="col-md-12 m-b">
								<div class="form-group">
									<label for="content">Kampanya İçeriği</label>
									<textarea name="content" id="campaingContent" class="form-control custom-input" placeholder="Açıklama"></textarea>
								</div>
							</div>
							<hr>
							<div class="col-md-12">
								<div class="record-buttons" style="width: 270px;">
									<button class="btn btn-edit" type="submit">
										<i class="fa fa-plus"></i> Kampanya Ekle
									</button>
									<a href="{{ route('campaigns.index') }}">
										<button class="btn btn-trash" type="button">
											<i class="fa fa-remove"></i> İptal
										</button>
									</a>
								</div>
							</div>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
		<hr>
	</div>
@endsection
@push('script')
<script>
	tinymce.init({
		  selector: '#campaingContent',
		  height: 500,
		  theme: 'modern',
		  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
		  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
		  image_advtab: true,
		  templates: [
		    { title: 'Test template 1', content: 'Test 1' },
		    { title: 'Test template 2', content: 'Test 2' }
		  ],
		  content_css: [
		    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
		    '//www.tinymce.com/css/codepen.min.css'
		  ]
		 });
</script>
@endpush