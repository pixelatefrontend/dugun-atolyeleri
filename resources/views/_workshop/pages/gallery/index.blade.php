@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <svg class="hidden">
      <defs>
        <symbol id="icon-images" viewBox="0 0 36 32">
        <title>images</title>
        <path d="M34 4h-2v-2c0-1.1-0.9-2-2-2h-28c-1.1 0-2 0.9-2 2v24c0 1.1 0.9 2 2 2h2v2c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-24c0-1.1-0.9-2-2-2zM4 6v20h-1.996c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v1.996h-24c-1.1 0-2 0.9-2 2v0zM34 29.996c-0.001 0.001-0.002 0.003-0.004 0.004h-27.993c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v23.993z"></path>
        <path d="M30 11c0 1.657-1.343 3-3 3s-3-1.343-3-3 1.343-3 3-3 3 1.343 3 3z"></path>
        <path d="M32 28h-24v-4l7-12 8 10h2l7-6z"></path>
        </symbol>
      </defs>
    </svg>
    <div class="panel-content">
        <hr>
    <div class="gallery-sec">
        <div class="row">
           <div class="col-md-12">
               <form action="{{ route('workshop.gallery.update.post') }}" class="dropzone" id="my-awesome-dropzone" enctype="multipart/form-data">
                <div class="form-content">
                  <svg class="icon icon-images"><use xlink:href="#icon-images"></use></svg>
                </div>
                {{ csrf_field() }}
               </form>
           </div>
        </div>
    </div>
    <div class="gallery-sec">
    <div class="row" id="imageSortable">
        @foreach($item->images as $image)
        <div class="col-md-3 imageDes" id="{{ $image->id }}">
            <div class="gallery-box">

              @if($item->type != 'premium')
                <img src="/uploads/workshops/{{$item->id}}/{{ $image->filename }}" id="img" alt="" data-img-id="{{ $image->id }}" class="{{ $loop->iteration > 5 ? 'pasif' : '' }}">
                @else
                  <img src="/uploads/workshops/{{$item->id}}/{{ $image->filename }}" id="img" alt="" data-img-id="{{ $image->id }}">
                @endif
                <div class="gallery-inner">
                    <div class="gallery-padding">
                        <div class="gallery-info" style="margin-top: 10px; margin-right: 5px;">
                          <div class="dropdown">
                              <i class="fa fa-ellipsis-h dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color: #fff;font-size: 25px;cursor: pointer;width: 30px;height: 30px; text-align: center;"></i>
                            </button>
                            <ul class="dropdown-menu" style="left: -165px;" aria-labelledby="dropdownMenu1">
                              <li>
                                <a data-toggle="modal" data-target="#picModal{{$image->id}}" href="#"><i class="fa fa-pencil"></i> Düzenle</a>
                              </li>
                              <li role="separator" class="divider"></li>
                              <li><a href="javascript:;" onclick="firstPic(this)" data-path="/uploads/workshops/{{$item->id}}/{{ $image->filename }}"><i class="fa fa-desktop"></i> Kapak Fotoğrafı Yap</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="javascript:;" onclick="destroyImage(this)" value="{{ $image->id }}"><i class="fa fa-trash"></i> Sil</a></li>
                            </ul>
                          </div>
                        </div><!-- Gallery Info -->
                    </div>
                </div>
            </div><!-- Gallery Box -->
        </div>
        @endforeach
    </div>
</div><!-- gallery Section -->
<div class="custom-alert alert alert-success" id="notification">
   <i class="fa fa-check"></i>
   <span>Kaydedildi</span>
   <p> &nbsp; &nbsp;&nbsp; Son değişiklikler kaydedildi.</p>
</div>
@foreach($item->images as $image)
  <div id="picModal{{$image->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resmi Düzenle</h4>
      </div>
      {{ Form::open([ 'url' => route('workshop.image.detail'), 'id' => 'imageDetail' ]) }}
      <div class="modal-body">
        <input type="hidden" name="image_id" value="{{$image->id}}">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
                <label for="name">Ürün Başlığı</label>
                <input type="text" name="title" class="form-control custom-input" value="{{ $image->title }}">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label for="name">Ürün Kodu</label>
                <input type="text" 
                    name="code" 
                    class="form-control custom-input" 
                    value="{{$image->code or substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10) }}">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
                <label for="name">Ürün Açıklaması</label>
                <textarea name="description" class="form-control custom-input" id="description" rows="7" style="resize: none">{{ $image->description }}</textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-edit"><i class="fa fa-pencil"></i> Düzenle</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@endforeach
</div>
@endsection
@push('script')
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
  $('form#imageDetail').submit(function(e){
      e.preventDefault();
      var data = $(this).serialize();
      var image_id = $('input[name="image_id"]').val();
      $.ajax({
          method: 'POST',
          url: '{{ route("workshop.image.detail") }}',
          data: data,
          success: function(response){
            if(response.success){
              $('#picModal'+image_id).modal('hide');
              console.log('success');
            }else{
              console.log('error');
            }
          }
      });
  });
</script>
<script>
    $("#imageSortable").sortable({
        update: function( event, ui ) {
            var order = $(this).sortable('toArray');
            console.log(order);
            $.post('{{route('workshop.image.sortable')}}', { order: order })
            .done(function(){
              $('.custom-alert').fadeIn(1000).delay(3000).fadeOut('slow');
            });
        }
    });
  </script>
  <script type="text/javascript">
        Dropzone.options.myAwesomeDropzone = {
          maxFilesize: 20, // Size in MB
          dictDefaultMessage: "Dosya yüklemek için tıklayın veya sürükleyin",
          dictInvalidFileType: 'Sadece .jpg, .png, .jpeg dosyalarına izin verilmektedir',
          addRemoveLinks: true,
          renameFile: true,
          acceptedFiles: ".png,.jpg,.jpeg",
          dictRemoveFile: 'Resmi Sil',
          removedfile: function(file) { 
              var fileRef;
              return (fileRef = file.previewElement) != null ? 
              fileRef.parentNode.removeChild(file.previewElement) : void 0;
          },
      };
</script>
<script>
    destroyImage = function(item){
        var data = $(item).attr('value');

        $.ajax({
            type: "POST",
            url: "/image/destroy/"+data,
            data: {id: data},
            success: function(response){
                if(response.success)
                {
                    $(item).closest('.imageDes').fadeOut(300, function(){ $(item).remove() });
                }else{
                    console.log('HATA!');
                }
            }
        });
    }
    firstPic = function(item){
        var data = $(item).data('path');
        var id = {{ $item->id }};
        $.ajax({
            type: 'POST',
            url: '/image/conf/'+id,
            data: { id: id, path: data },
            success: function(response){
                if(response.success){
                    $('.custom-alert').fadeIn(1000).delay(3000).fadeOut('slow');
                }else{
                    console.log('HATA');
                }
            }
        });
        $(item).parents('.form-elements-item').find('.conf-inner').hide();
    }
</script>
@endpush