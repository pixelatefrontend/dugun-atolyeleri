@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="mini-stats-sec">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="red-skin"><i class="fa fa-eye"></i></span>
                                    <p>Ziyaretçi Sayısı</p>
                                    <h3>{{ Auth::guard('workshop')->user()->view_count }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="sky-skin"><i class="fa fa-code-fork"></i></span>
                                    <p>Cevap Bekleyen Çift</p>
                                    <h3>5 Çift</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="pink-skin"><i class="fa fa-shopping-cart"></i></span>
                                    <p>Düğünatölyeleri.com'dan <b>8 çift</b> ile anlaştınız</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Mini stats Sec -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-controls">
                        <span class="close-content"><i class="fa fa-trash-o"></i></span>
                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                    </div><!-- Widget Controls -->
                    <div class="activity-sec">
                        <div class="widget-title">
                            <h3>Activities</h3>
                            <span>Your last activity is posted 4 hours ago</span>
                        </div>
                        <div id="graph-wrapper">
                            <div class="graph-info">
                                <a href="" class="visitors"><span class="red-skin"></span>Visitors</a>
                                <a href="" class="returning"><span class="purple-skin"></span>Returning Visitors</a>
                            </div>
                            <div class="graph-container">
                                <canvas id="chart" width="969" height="232" style="width: 969px; height: 232px;"></canvas>
                            </div>
                            <div class="graph-details">
                                <ul>
                                    <li><span class="red-skin">+5.00%</span><strong>$5,450</strong><p>Average of $450.35 Per Day</p></li>
                                    <li><span class="purple-skin">+5.00%</span><strong>1,120</strong><p>Total Visitors : 2,58,256,00</p></li>
                                </ul>
                            </div>
                        </div>  
                    </div><!-- Activity Sec -->
                </div>
            </div>
        </div>
    </div>
@endsection
