@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
        {!! Form::open([ 'url' => route('workshop.services.post') ]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="mini-stats-sec">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div>
                                <div class="mini-stats c-title">
                                    <h4>Minimum Paket Fiyatı</h4>
                                    <select name="price" id="price" class="form-control custom-input">
                                        @foreach($prices as $price)
                                            @if($workshop->services)
                                                <option value="{{ $price }}" {{{ $workshop->services->price == $price ? 'selected' : '' }}}>
                                                    {{ $price }} TL
                                                </option>
                                            @else
                                                <option value="{{ $price }}">
                                                    {{ $price }} TL
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- End col-md --}}
                        <div class="col-md-4">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div>
                                <div class="mini-stats c-title">
                                    <h4>Teslimat Süresi</h4>
                                    <select name="delivery" id="delivery" class="form-control custom-input">
                                       @foreach($delivery as $row)
                                            @if($workshop->services)
                                                <option value="{{ $row }}" {{{ $workshop->services->delivery_time == $row ? 'selected' : '' }}}>
                                                    {{ $row }} 
                                                </option>
                                            @else
                                                <option value="{{ $row }}">
                                                    {{ $row }} 
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- End col-md --}}
                        <div class="col-md-4">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div>
                                <div class="mini-stats c-title">
                                    <h4>İletişim Süresi</h4>
                                    <select name="contact_time" id="contact_time" class="form-control custom-input">
                                       @foreach($contact as $row)
                                            @if($workshop->services)
                                                <option value="{{ $row }}" {{{ $workshop->services->contact_time == $row ? 'selected' : '' }}}>
                                                    {{ $row }} 
                                                </option>
                                            @else
                                                <option value="{{ $row }}">
                                                    {{ $row }} 
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style="padding-top: 20px;">
                                <button class="btn btn-atolye pull-right">Kaydet</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        {{-- End row --}}
        {!! Form::open([ 'url' => route('workshop.questions.post') ]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="mini-stats-sec">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div>
                                <div class="mini-stats c-title">
                                    <h4>Cevaplanmamış Sorular</h4>
                                    <div class="row">
                                        @foreach($questions as $question)
                                            <div class="col-md-4">
                                                <div class="question">
                                                    Soru 1
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <hr>
                                    <div class="row">
                                    {!! Form::open([ 'url' => route('workshop.questions.post'), 'class' => 'form-inline' ]) !!}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="for-atolye">Listeden Soru Seçin</label>
                                                <select name="question" id="question" class="form-control custom-input">
                                                    @foreach ($questions as $question)
                                                        <option value="{{ $question }}">{{ $question }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea name="answer" id="" rows="10" class="form-control custom-input"></textarea>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
