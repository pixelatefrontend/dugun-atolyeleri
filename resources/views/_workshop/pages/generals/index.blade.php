@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('message'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong><i class="fa fa-check"></i></strong> {{ Session::get('message') }}
                    </div>
                @endif
                @if(count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <strong><i class="fa fa-remove"></i></strong> Boş alan bırakılamaz.
                    </div>
                @endif
            </div>
        </div>
        <hr>

        {{--<div class="editor-toggle">--}}
            {{--<span class="editor-toggle__btn" data-toggle="list">Liste</span>--}}
            {{--<span class="dot"></span>--}}
            {{--<span class="editor-toggle__btn" data-toggle="online">Online Editor</span>--}}
        {{--</div>--}}

   {{--      <div class="editor-toggle">
            <div class="editor-toggle-label editor-toggle-label-off">Liste</div>
            <div class="editor-toggle-switch"></div>
            <div class="editor-toggle-label editor-toggle-label-on">Online Editor</div>
        </div>
 --}}
        <div class="custom-tab editor-tab active" id="editorList">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#general">Genel Bilgiler</a></li>
                        <li><a data-toggle="tab" href="#image">Atölye Görseli</a></li>
                        <li><a data-toggle="tab" href="#details">Atölye Detay</a></li>
                        <li><a data-toggle="tab" href="#questions">Sorular</a></li>
                    </ul>
                </div>
            </div>
            <div class="tab-content" style="margin-top: 20px;">
                <div id="general" class="tab-pane fade in active">
                    @include('_workshop.pages.generals.partials.general')
                </div>

                <div id="image" class="tab-pane fade">
                    @include('_workshop.pages.generals.partials.images')
                </div>

                <div id="details" class="tab-pane fade">
                    @include('_workshop.pages.generals.partials.details')
                </div>

                <div id="questions" class="tab-pane fade">
                    @include('_workshop.pages.generals.partials.questions')
                </div>
            </div>
        </div>

        <div class="editor-tab" id="editorOnline">
            <div class="premium-detail">

                <div class="js-quick-wrp">
                    <div class="cover-section" style="background-image: url({{ $item->coverPath() }})">
                        <a href="#" class="btn-quick-edit"  data-quick="background" style="top: 0;">
                            <svg class="icon icon-edit">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                            </svg>
                        </a>

                        <a href="#" class="js-quick-save">
                            <svg class="icon icon-edit">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                            </svg>
                        </a>

                        <a href="#" class="js-quick-cancel">
                            <svg class="icon icon-edit">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                            </svg>
                        </a>

                        <div class="container-workshop">

                            <div class="row">

                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                                    <div class="detail">

                                        <figure>
                                            @if ($item->avatar)

                                                <div class="js-quick-wrp">

                                                    <input type="file" class="js-quick-image-src hidden" accept="image/x-png,image/png,image/jpeg, image/jpg">
                                                    <img src="{{ $item->avatarPath() }}" alt="{{ $item->title }}" class="js-quick-image">

                                                    <a href="#" class="btn-quick-edit"  data-quick="image">
                                                        <svg class="icon icon-edit">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                        </svg>
                                                    </a>

                                                    <a href="#" class="js-quick-save">
                                                        <svg class="icon icon-edit">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                        </svg>
                                                    </a>

                                                    <a href="#" class="js-quick-cancel">
                                                        <svg class="icon icon-edit">
                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                        </svg>
                                                    </a>

                                                </div>
                                            @else
                                                <div class="no-image">
                                                    <span>{{ $item->noImageAvatar() }}</span>
                                                </div>
                                            @endif

                                        </figure>

                                        <article>

                                            <div class="workshop-name">

                                                <h3>

                                                    <div class="js-quick-wrp">
                                                        <input type="text" value="{{ $item->title }}" class="quick-edit" disabled="disabled">
                                                        <a href="#" class="btn-quick-edit" data-quick="text">
                                                            <svg class="icon icon-edit">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                            </svg>
                                                        </a>

                                                        <a href="#" class="js-quick-save">
                                                            <svg class="icon icon-edit">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                            </svg>
                                                        </a>

                                                        <a href="#" class="js-quick-cancel">
                                                            <svg class="icon icon-edit">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                            </svg>
                                                        </a>

                                                    </div>
                                                    {{--<div class="ribbon-badge"><div class="kurdele-inner"><svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg></div></div>--}}
                                                </h3>



                                            </div>

                                            <div class="counters section-disabled">
                                                @if(Auth::check())
                                                    <div class="like-counter {{{ $item->isFavorited() ? 'like-counter-selected' : '' }}}">
                                                        @else
                                                            <div class="like-counter">
                                                                @endif
                                                                <svg>
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use>
                                                                </svg>
                                                                <span id="likes">{{ $item->like }}</span>
                                                            </div>

                                                            <div class="view-counter">
                                                                <svg>
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use>
                                                                </svg>
                                                                <span>{{ $item->view_count }}</span>
                                                            </div>

                                                    </div>

                                                    <div class="buttons">
                                                        @if(Auth::check())
                                                            <a class="like-button section-disabled {{{ $item->isFavorited() ? 'liked' : '' }}}" href="javascript:void(0)"
                                                               value="{{ $item->id }}"
                                                               type="button"
                                                               disabled>
                                                                <svg>
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
                                                                </svg>
                                                                <span>BEĞEN</span>
                                                            </a>
                                                        @else
                                                            <a class="like-button section-disabled" href="javascript:void(0)"
                                                               value="{{ $item->id }}"
                                                               type="button"
                                                               disabled>
                                                                <svg>
                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
                                                                </svg>
                                                                <span>BEĞEN</span>
                                                            </a>
                                                        @endif

                                                        <a class="add-fav-button section-disabled"
                                                           href="#"
                                                           data-toggle="modal"
                                                           disabled
                                                           @if(Auth::check())
                                                           @if(count(Auth::user()->lists) > 0)
                                                           id="fav-modal"
                                                           data-target="#fav-modal"
                                                           @else
                                                           id="add-list-modal"
                                                           data-target="#add-list-modal"
                                                           @endif
                                                           @else
                                                           data-target="#register-modal"
                                                                @endif
                                                        >
                                                            <svg>
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/add-list-icon.svg#add-list-icon"></use>
                                                            </svg>
                                                            <span>LİSTEME EKLE</span>
                                                        </a>

                                                    </div>

                                                    <div class="single-button">

                                                        <a href="#" class="section-disabled" disabled id="offer-button" data-toggle="modal" data-target="#offer-modal">
                                                            <span>TEKLİF İSTE</span>
                                                            <svg>
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use>
                                                            </svg>
                                                        </a>

                                                    </div>
                                            </div>
                                        </article>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>

                <div class="detail-section">

                    <div class="container-workshop">

                        <div class="row">

                            <div class="col-md-6 col-lg-6">

                                <div class="workshop-detail">

                                    <h3>ATÖLYE HAKKINDA</h3>

                                    <ul class="tags">
                                        @foreach ($item->categories as $category)
                                            <li><a href="{{ route('category.single', $category->slug) }}">#{{ str_slug(strtolower($category->title)) }}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="js-quick-wrp">
                                        <p>
                                            <textarea name="" id="" cols="30" rows="10" class="quick-edit" disabled="disabled">{!! $item->description !!}</textarea>

                                        </p>
                                        <a href="#" class="btn-quick-edit"  data-quick="text">
                                            <svg class="icon icon-edit">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                            </svg>
                                        </a>

                                        <a href="#" class="js-quick-save">
                                            <svg class="icon icon-edit">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                            </svg>
                                        </a>

                                        <a href="#" class="js-quick-cancel">
                                            <svg class="icon icon-edit">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                            </svg>
                                        </a>

                                    </div>


                                </div>

                            </div>

                            <div class="col-md-6 col-lg-6">

                                <div class="workshop-slider-container">
                                    <div id="premium-detail-slider" class="workshop-slider">
                                        {{-- Popup içerisine videoyu çekmek için bu kodu kullanacaksın--}}
                                        @if(count($item->videos) > 0)
                                            <div class="workshop-slider-item">
                                                <div class="video-item-wrp">
                                                    {{--<iframe style="width: 100%; height: 100%;" src="{{ $item->videos->last()->strUrl().'?autoplay=0&showinfo=0&controls=0' }}" frameborder="0" allowfullscreen allowscriptaccess="always"></iframe>--}}
                                                    <img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">
                                                    <div class="overlay">
                                                        <a href="#" class="btn-play" data-video-id="video1"><i class="fa fa-play" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        @foreach ($item->images as $image)
                                            <div class="workshop-slider-item">
                                                <img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}" data-img-id="{{ $image->id }}">
                                            </div>
                                        @endforeach
                                    </div>

                                    <div id="premium-detail-slider-thumb" class="workshop-slider-thumb">
                                        @if(count($item->videos) > 0)
                                            <div class="workshop-slider-thumb-item">
                                                <img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">
                                                <div class="overlay">
                                                    <span class="play-thumb"><i class="fa fa-play" aria-hidden="true"></i></span>
                                                </div>


                                                {{--<div class="videoPlay">--}}

                                                {{--</div>--}}

                                            </div>
                                        @endif
                                        @foreach ($item->images->take(5) as $image)
                                            <div class="workshop-slider-thumb-item">
                                                <img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}">
                                            </div>
                                        @endforeach
                                    </div>

                                    <a class="all-photos-button" href="">Tüm <br>Fotoğraflar</a>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="features-secion">

                    <div class="container-workshop">

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                                <div class="features-container">

                                    <div class="features-item">

                                        <strong>KATEGORİ</strong>


                                        @foreach ($item->categories as $category)
                                            <span>{{ $category->title }}</span>
                                        @endforeach

                                    </div>

                                    <hr>

                                    <div class="features-item">

                                        <strong>MİN FİYAT</strong>

                                        @if(isset($item->services->price))
                                            <span>{{ $item->services->price }} TL</span>
                                        @else
                                            <span>-</span>
                                        @endif

                                    </div>

                                    <hr>

                                    <div class="features-item">

                                        <strong>TESLİM SÜRESİ</strong>
                                        @if(isset($item->services->delivery_time))
                                            <span>{{ $item->services->delivery_time }} GÜN</span>
                                        @else
                                            <span>-</span>
                                        @endif

                                    </div>

                                    <hr>

                                    <div class="features-item">

                                        <strong>ŞEHİR</strong>

                                        <span>{{ $item->city }}</span>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="features-detail-section">

                    <div class="container-workshop">

                        @foreach($item->questions->chunk(3) as $question)
                            <div class="row">
                                @foreach($question as $content)
                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 features-detail-item">
                                        <div class="js-quick-wrp">
                                            <h4>
                                                <input type="text" value="{{ $content->question }}" class="quick-edit" disabled="disabled">
                                            </h4>


                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                        <div class="js-quick-wrp">
                                            <p>
                                                <input type="text" value="{{ $content->reply }}" class="quick-edit" disabled="disabled"></p>
                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>


                                @endforeach
                            </div>
                        @endforeach

                    </div>

                </div>
                @include('_website.partials.workshops.map')
                <div class="hidden-xs hidden-sm visible-md visible-lg contact-section">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            {{--<label for="name">Harita</label>--}}
                            <fieldset class="gllpLatlonPicker">
                                <div class="gllpMap" id="mapGoogle">Google Maps</div>
                                <input type="hidden" class="gllpLatitude" name="x" value="{{ $item->map->x or '' }}"/>
                                <input type="hidden" class="gllpLongitude" name="y" value="{{ $item->map->y or '' }}"/>
                            </fieldset>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                            <div class="contact-detail">

                                <h3>İLETİŞİM</h3>

                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                        <h4>YETKİLİ KİŞİ</h4>

                                        <div class="js-quick-wrp">
                                        <span>
                                            <input type="text" value="{{ $item->member }}" class="quick-edit" disabled="disabled">
                                        </span>
                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                        <h4>TELEFON NUMARASI</h4>

                                        <div class="js-quick-wrp">
                                        <span class="phone">
                                            <input type="text" value="{{ $item->phone }}" class="quick-edit" disabled="disabled">
                                        </span>
                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                        <h4>ADRES</h4>

                                        <div class="js-quick-wrp">
                                        <span>
                                            <input type="text" value="{{ $item->address }}" class="quick-edit" disabled="disabled">
                                        </span>
                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                        <h4>E- POSTA ADRESİ</h4>

                                        <div class="js-quick-wrp">
                                        <span>
                                            <input type="text" value="{{ $item->email }}" class="quick-edit" disabled="disabled">
                                        </span>
                                            <a href="#" class="btn-quick-edit"  data-quick="text">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-edit"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-save">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-check"></use>
                                                </svg>
                                            </a>

                                            <a href="#" class="js-quick-cancel">
                                                <svg class="icon icon-edit">
                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-close"></use>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="visible-xs visible-sm hidden-md hidden-lg contact-section">

                    <div class="container-workshop">

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                <div class="contact-detail">

                                    <h3>İLETİŞİM</h3>

                                    <div class="row">

                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                            <h4>YETKİLİ KİŞİ</h4>

                                            <span>{{ $item->getFullName() }}</span>

                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                            <h4>TELEFON NUMARASI</h4>

                                            <span class="phone">{{ $item->phone }}</span>

                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                            <h4>ADRES</h4>

                                            <span> {{ $item->address }} </span>

                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

                                            <h4>E- POSTA ADRESİ</h4>

                                            <span> {{ $item->email }} </span>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

                                <div id="map-premium-mobil" class="google-map"></div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>



    </div>
@endsection
@push('script')
    <script>
        function initImageUpload(box) {
            let uploadField = box.querySelector('.image-upload');

            uploadField.addEventListener('change', getFile);

            function getFile(e) {
                let file = e.currentTarget.files[0];
                checkType(file);
            }

            function previewImage(file) {
                let thumb = box.querySelector('.js--image-preview'),
                    reader = new FileReader();

                reader.onload = function () {
                    thumb.style.backgroundImage = 'url(' + reader.result + ')';
                }
                reader.readAsDataURL(file);
                thumb.className += ' js--no-default';
            }

            function checkType(file) {
                let imageType = /image.*/;
                if (!file.type.match(imageType)) {
                    throw 'Datei ist kein Bild';
                } else if (!file) {
                    throw 'Kein Bild gewählt';
                } else {
                    previewImage(file);
                }
            }

        }

        // initialize box-scope
        var boxes = document.querySelectorAll('.box');

        for (let i = 0; i < boxes.length; i++) {
            let box = boxes[i];
            initDropEffect(box);
            initImageUpload(box);
        }


        /// drop-effect
        function initDropEffect(box) {
            let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;

            // get clickable area for drop effect
            area = box.querySelector('.js--image-preview');
            area.addEventListener('click', fireRipple);

            function fireRipple(e) {
                area = e.currentTarget
                // create drop
                if (!drop) {
                    drop = document.createElement('span');
                    drop.className = 'drop';
                    this.appendChild(drop);
                }
                // reset animate class
                drop.className = 'drop';

                // calculate dimensions of area (longest side)
                areaWidth = getComputedStyle(this, null).getPropertyValue("width");
                areaHeight = getComputedStyle(this, null).getPropertyValue("height");
                maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

                // set drop dimensions to fill area
                drop.style.width = maxDistance + 'px';
                drop.style.height = maxDistance + 'px';

                // calculate dimensions of drop
                dropWidth = getComputedStyle(this, null).getPropertyValue("width");
                dropHeight = getComputedStyle(this, null).getPropertyValue("height");

                // calculate relative coordinates of click
                // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
                x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10) / 2);
                y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10) / 2) - 30;

                // position drop and animate
                drop.style.top = y + 'px';
                drop.style.left = x + 'px';
                drop.className += ' animate';
                e.stopPropagation();

            }
        }
    </script>
    <script>
        $(function () {
            if ($('#itemCounter').val() == 0 || $('#itemCounter').val() == 1) {
                $('#destroy-clone').hide();
            } else if ($('#itemCounter').val() >= 6) {
                $('#addItem').prop('disabled', true);
            }
        });

        function remove(div) {
            $(div).parent().parent().parent().slideUp('slow', function () {
                $(this).remove();
            });
            $('#itemCounter').val(Number($('#itemCounter').val()) - 1);

            if ($('#itemCounter').val() == 1) {
                $('#destroy-clone').hide();
            }

            if ($('#itemCounter').val() >= 6) {
                $('#addItem').prop('disabled', true);
            } else {
                $('#addItem').prop('disabled', false);
            }
        }

        // Accepts an element and a function
        function childRecursive(element, func) {
            // Applies that function to the given element.
            func(element);
            var children = element.children();
            if (children.length > 0) {
                children.each(function () {
                    // Applies that function to all children recursively
                    childRecursive($(this), func);
                });
            }
        }

        // Expects format to be xxx-#[-xxxx] (e.g. item-1 or item-1-name)
        function getNewAttr(str, newNum) {
            // Split on -
            var arr = str.split('-');
            // Change the 1 to wherever the incremented value is in your id
            arr[1] = newNum;
            // Smash it back together and return
            return arr.join('-');
        }

        // Written with Twitter Bootstrap form field structure in mind
        // Checks for id, name, and for attributes.
        function setCloneAttr(element, value) {
            // Check to see if the element has an id attribute
            if (element.attr('id') !== undefined) {
                // If so, increment it
                element.attr('id', getNewAttr(element.attr('id'), value));
            } else { /*If for some reason you want to handle an else, here you go */
            }
            // Do the same with name...
            // And don't forget to show some love to your labels.
            if (element.attr('data-inputid') !== undefined) {
                element.attr('data-inputid', getNewAttr(element.attr('data-inputid'), value));
            } else {
            }
        }

        // Sets an element's value to ''
        function clearCloneValues(element) {
            if (element.attr('value') !== undefined) {
                element.val('');
            }
        }

        var say = 0;

        //$(document).ready(function(){
        $('#addItem').click(function () {
            $('#destroy-clone').show();
            //increment the value of our counter
            $('#itemCounter').val(Number($('#itemCounter').val()) + 1);
            if ($('#itemCounter').val() >= 6) {
                $('#addItem').prop('disabled', true);
            }
            //clone the first .item element
            var newItem = $('div.item').first().clone();
            //recursively set our id, name, and for attributes properly
            childRecursive(newItem,
                // Remember, the recursive function expects to be able to pass in
                // one parameter, the element.
                function (e) {
                    setCloneAttr(e, $('#itemCounter').val());
                });
            // Clear the values recursively
            childRecursive(newItem,
                function (e) {
                    clearCloneValues(e);
                });
            // Finally, add the new div.item to the end
            newItem.appendTo($('#items'));
            say + 1;

        });
        //});

        //*** sidebar active item ***//
        $("#premium-detail-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !0,
            asNavFor: "#premium-detail-slider-thumb"
        });
        $("#premium-detail-slider-thumb").slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            asNavFor: "#premium-detail-slider",
            arrows: !1,
            dots: !1,
            infinite: false,
            draggable: false,
            // focusOnSelect: !0,
            focusOnSelect: !0,
            variableWidth: !0
        });


    </script>
@endpush