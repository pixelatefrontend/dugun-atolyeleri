{{--<div class="row m-t">--}}
    {{--<div class="col-md-12">--}}
        {{--<h4>Atölye Detay</h4>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            {{--<div class="widget-controls">--}}
               {{--<span class="refresh-content"><i class="fa fa-refresh"></i></span>--}}
           {{--</div>--}}
           <div class="form-elements-sec workshop-form">
            {{ Form::open([ 'url' => route('workshop.detail.update.post')]) }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Yetkili Kişi</label>
                        <input type="text" name="member" class="form-control custom-input" value="{{ $item->member }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Telefon</label>
                        <input type="text" name="phone" class="form-control custom-input" value="{{ $item->phone }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Adres</label>
                        <textarea type="text" rows="5" name="address" class="form-control custom-input">{{ $item->address }}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Harita</label>
                       <fieldset class="gllpLatlonPicker">
                            <div class="gllpMap" id="mapGoogle">Google Maps</div>
                            <input type="hidden" class="gllpLatitude" name="x" value="{{ $item->map->x or '' }}"/>
                            <input type="hidden" class="gllpLongitude" name="y" value="{{ $item->map->y or '' }}"/>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Fiyat Aralığı (TL)</label>
                        <input type="text" name="price" class="form-control custom-input" value="{{ $item->services->price or '' }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Teslim Süresi</label>
                        {!! Form::select('delivery_time',
                                [ 
                                    '1-3' => '1-3 Gün',
                                    '3-5' => '3-5 Gün',
                                    '7-10' => '7-10 Gün',
                                    '10-15' => '10-15 Gün',
                                    '15-30' => '15-30 Gün',
                                ],
                                $item->services ? $item->services->delivery_time : null,
                                ['class' => 'form-control custom-input', 'placeholder' => 'Seçiniz']) !!}
                    </div>
                </div>
                <div class="col-md-12">
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-edit pull-right"><i class="fa fa-pencil"></i> Bilgileri Kaydet</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
</div>
@push('script')
    <script>
  $(document).ready(function() {
    // Copy the init code from "jquery-gmaps-latlon-picker.js" and extend it here
    $(".gllpLatlonPicker").each(function() {
      $obj = $(document).gMapsLatLonPicker();
      $obj.params.defLat = {{ $item->map->x or 41.0055005 }};
      $obj.params.defLng = {{ $item->map->y or 28.7319856 }};
      $obj.params.defZoom = 7;
      $obj.init( $(this) );
    });
  });
</script>
@endpush