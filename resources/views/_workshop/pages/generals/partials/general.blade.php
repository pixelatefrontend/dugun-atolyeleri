{{--<div class="row m-t">--}}
    {{--<div class="col-md-12">--}}
        {{--<h4>Genel Bilgiler</h4>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            {{--<div class="widget-controls">--}}
               {{--<span class="refresh-content"><i class="fa fa-refresh"></i></span>--}}
           {{--</div>--}}
           <div class="form-elements-sec workshop-form">
            {{ Form::open([ 'url' => route('workshop.panel.update.post')]) }}
            {{ method_field('PUT') }}
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Atölye Adı</label>
                        <input type="text" name="title" class="form-control custom-input" value="{{ $item->title }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Instagram Kullanıcı Adı</label>
                        <input type="text" name="instagram_username" class="form-control custom-input" value="{{ $item->instagram_username }}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Tanıtım Yazısı</label>
                        <textarea type="text" rows="5" name="description" class="form-control custom-input">{{ $item->description }}</textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">E-mail</label>
                        <input type="text" name="email" class="form-control custom-input" value="{{ $item->email }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Şehir</label>
                        {{ Form::select('city', $cities, $item->city, ['class' => 'form-control custom-input', 'placeholder' => 'Şehir']) }}
                    </div>
                </div>
                <div class="col-md-12">
                    <hr>
                    <div class="form-group">
                        <button type="submit" class="btn btn-edit pull-right"><i class="fa fa-pencil"></i> Bilgileri Kaydet</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
</div>