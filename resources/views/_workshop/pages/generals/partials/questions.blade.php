{{--<div class="row m-t">--}}
    {{--<div class="col-md-12">--}}
        {{--<h4>Atölye Soruları</h4>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-md-12">
        <div class="widget">

           <div class="form-elements-sec workshop-form">
            {{ Form::open([ 'url' => route('workshop.questions.update.post')]) }}
            {{ method_field('PUT') }}
                @if(count($item->questions) == 0)
            <div id="items">
                <input type="hidden" id="itemCounter" name="itemCounter" value="1">
                <div class="item">
                  <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="question">Soru</label>
                                <input type="text" name="questions[]" class="form-control custom-input" placeholder="Sorunuzu Yazın">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="answer">Cevap</label>
                                <input type="text" name="replies[]" class="form-control custom-input" placeholder="Cevabınızı Yazın">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="">&nbsp;</label>
                                <button type="button" id="destroy-clone" class="btn btn-trash form-control" onclick="remove(this)" style="margin-top: 3px;"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div id="items">
              <input type="hidden" id="itemCounter" name="itemCounter" value="{{ count($item->questions) }}">
              @for($i=0,$c=count($item->questions);$i<$c;$i++)
              <div class="item">
                  <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="question">Soru</label>
                                <input type="text" name="questions[]" class="form-control custom-input" value="{{ $item->questions[$i]->question }}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="answer">Cevap</label>
                                <input type="text" name="replies[]" class="form-control custom-input" value="{{ $item->questions[$i]->reply }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="">&nbsp;</label>
                                <button type="button" id="destroy-clone" class="btn btn-trash form-control" onclick="remove(this)" style="margin-top: 3px;"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        @endif
        <div class="col-md-12">
                    <hr>
                    <div class="form-group">
                            <button class="btn btn-edit add-question" id="addItem" type="button"><i class="fa fa-plus"></i> Yeni Soru Ekle</button>
                        <button type="submit" class="btn btn-edit pull-right"><i class="fa fa-pencil"></i> Bilgileri Kaydet</button>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
</div>