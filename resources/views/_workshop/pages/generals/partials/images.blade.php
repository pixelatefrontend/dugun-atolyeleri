{{--<div class="row m-t">--}}
    {{--<div class="col-md-12">--}}
        {{--<h4>Atölye Görseli</h4>--}}
    {{--</div>--}}
{{--</div>--}}
<div class="row">
    <div class="col-md-12">
        <div class="widget">
            {{--<div class="widget-controls">--}}
             {{--<span class="refresh-content"><i class="fa fa-refresh"></i></span>--}}
         {{--</div>--}}
         <div class="form-elements-sec workshop-form">
            {{ Form::open([ 'url' => route('workshop.image.update.post'), 'files' => true]) }}
            {{ method_field('PUT') }}
            <div class="wrapper">
                <div class="col-md-6">
                    <h3>Avatar</h3>
                    <div class="box">
                        <div class="js--image-preview" style="background-image: url({{ $item->avatarPath() }}) "></div>
                        <div class="upload-options">
                          <label>
                            <input type="file" class="image-upload" accept="image/*"  name="avatar"/>
                        </label>
                    </div>
                </div>
            </div>
            @if($item->type != 'normal')
            <div class="col-md-6">
                <h3>Kapak</h3>
                <div class="box">
                    <div class="js--image-preview" style="background-image: url({{ $item->coverPath() }}) "></div>
                    <div class="upload-options">
                      <label>
                        <input type="file" class="image-upload" accept="image/*" name="cover" />
                    </label>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="col-md-12">
        <hr>
        <div class="form-group">
            <button type="submit" class="btn btn-edit pull-right"><i class="fa fa-pencil"></i> Bilgileri Kaydet</button>
        </div>
    </div>
    {{ Form::close() }}
</div>
</div>
</div>
</div>