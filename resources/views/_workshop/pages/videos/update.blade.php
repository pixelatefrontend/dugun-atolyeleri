@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
        @if(Auth::guard('workshop')->user()->type == "premium")
        <div class="row">
            <div class="col-md-12">
                @include('_workshop.partials.notifications')
            </div>
            <div class="col-md-12">
                {!! Form::open([ 'url' => route('videos.store'), 'files' => true, 'class' => 'sec', 'role' => 'form' ]) !!}
                <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Video Düzenle</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-6 m-b">
                                <div class="form-group">
                                    <label for="site">Site</label>
                                    {!! Form::select('site',
                                            [ 'youtube' => 'Youtube', 'vimeo' => 'Vimeo' ],
                                            $item->site,
                                            ['class' => 'form-control custom-input']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="site">Kapak Fotoğrafı</label>
                                    <input type="file" name="videoCover" class="form-control custom-input">
                                </div>
                                <div class="form-group">
                                    <label for="url">Video Linki</label>
                                    <input type="text" placeholder="URL" class="form-control custom-input" name="url" value={{ $item->url }}>
                                </div>
                                <div class="form-group">
                                    <label for="site">Durumu</label>
                                    {!! Form::select('active',
                                            [ '1' => 'Aktif', '0' => 'Pasif' ],
                                            $item->active,
                                            ['class' => 'form-control custom-input']) !!}
                                </div>
                                <div class="form-group">
                                    <label for="description">Video Açıklaması</label>
                                    <textarea placeholder="Açıklama" class="form-control custom-input" name="description" rows="6">{{ $item->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6 m-b video-bg">
                                <div class="form-group">
                                    <label for="">Video Önizleme</label>
                                    <iframe style="width: 100%; height: 300px;" src="{{ $item->strUrl() }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="record-buttons" style="width: 260px;">
                                    <button class="btn btn-edit" type="submit">
                                        <i class="fa fa-plus"></i> Video Düzenle
                                    </button>
                                    <a href="{{ route('videos.index') }}">
                                        <button class="btn btn-trash" type="button">
                                            <i class="fa fa-remove"></i> İptal
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>               
            {!! Form::close() !!}                
            </div>
        </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <div class="widget no-color">
                        <div class="notify simple with-image">
                            <span><img src="images/resource/check.png" alt=""></span>
                            <div class="notify-content"><h3>:(</h3><p>Bu atölye premium değil</p>
                            <a title="" class="close">x</a></div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
@push('script')
    <script>
        function colDestroy(item){
          $(item).closest('#videoCol').slideUp('slow').remove();
        }
    </script>
@endpush