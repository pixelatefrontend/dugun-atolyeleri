<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="tr">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Premium Atölye | Düğün Atölyeleri</title>
	<meta name="Resource-type" content="Document" />
	<link rel="icon" type="image/png" href="http://pixelate.com.tr/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="http://pixelate.com.tr/favicons/favicon-16x16.png" sizes="16x16">
	 <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />
	{{ Html::style('assets/_panel/css/bootstrap.min.css') }}
	{{ Html::style('assets/_panel/css/premium.css') }}
	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	{{ Html::script('assets/_panel/js/scrolloverflow.min.js') }}
	{{ Html::script('assets/_panel/js/premium.js') }}

	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				lockAnchors: true,
				navigation: true,
				navigationPosition: 'right',
				showActiveTooltip: false,
				slidesNavigation: true,
				slidesNavPosition: 'bottom',
				sectionsColor: ['#1bbc9b', '#4BBFC3', '#2a2d32', 'whitesmoke', '#ccddff'],
				anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
				menu: '#menu',
				showActiveTooltip: true,
				slidesNavigation: true,
				slidesNavPosition: 'bottom',
				parallax: true,
				parallaxOptions: {type: 'reveal', percentage: 62, property: 'translate'},
				afterLoad: function(anchorLink, index){

					//section 2
					if(index == 2){
						
					}

					//section 3
					if(index == 3){
						//moving the image
						$('#section2').find('.intro').delay(500).animate({
							left: '0%'
						}, 1500, 'easeOutExpo');
					}
				}
			});

		});
	</script>
</head>
<body>
	<header>
		<div class="header-wrapper">
			<div class="col-md-6">
				<div class="logo">
					<a href="{{ config('app.baseURL') }}"><img src="/assets/_website/img/logo-pink-white.png" alt=""></a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="pull-right user-info">
					<a href="{{ route('workshop.dashboard') }}" class="return_workshop">
						@if(Auth::guard('workshop')->user()->avatarPath())
							<img src="{{ Auth::guard('workshop')->user()->avatarPath() }}" alt="">
						@else
							<img src="/assets/_panel/images/avatar.png" alt="">
						@endif
						<span>{{ Auth::guard('workshop')->user()->title }}</span>
					</a>
				</div>
			</div>
		</div>
	</header>
	<div id="fullpage">
		<div class="section active" id="section0">
			<div class="container">
				<div class="text-center pr-spot">
					<h1>Premium'a geç. Mutlu ol</h1>
					<h2>Premium avantajları ile sizde 400+ atölyenin içinde ön sıralarda yerinizi alın böylelikle daha çok insana ulaşabilirsiniz. </h2>
					<a href="#" class="btn btn-pre btn-lg">HEMEN PREMIUM'A GEÇ</a>
				</div>
			</div>
		</div>
		<div class="section" id="section1">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1>Düğün Atölyeleri Premium</h1>
						<p>Premium Atölye Özellikleri</p>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12 col-centered">
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-power"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-droplet"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-camera"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-music"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-rocket"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-database"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-power"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
						{{--  --}}
						<div class="col-md-6 feature">
							<div class="icon">
								<svg class="icon-svg icon-power"><use xlink:href="/assets/_panel/images/icons.svg#icon-droplet"></use></svg>
							</div>
							<div class="feature-info-flex">
								<div class="desc">
									<h3>Lorem ipsum dolor sit amet</h3>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row text-center">
					<button class="btn btn-premium btn-lg">Ücretsiz Denemeyi Hemen Başlat</button>
				</div>
			</div>
		</div>
		<div class="section" id="section2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1 class="text-center">Sık Sorulan Sorular</h1>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="sss-desc">
							<h3>Lorem ipsum dolor sit amet</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at nulla dapibus, lobortis massa vel, vehicula neque.</p>
						</div>
					</div>
					

				</div>
			</div>
		</div>
	</div>


</body>
</html>
