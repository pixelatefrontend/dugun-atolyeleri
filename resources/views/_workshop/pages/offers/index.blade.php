@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="table-area">
                    <div class="widget-title">
                        <h3>Teklifler Tablosu</h3>
                        <span>{{ $item->offers->count() }} Teklif Listelendi</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kullanıcı</th>
                                    <th>E-mail</th>
                                    <th>Tarih</th>
                                    <th>Teklif Gönderildi</th>
                                    <th>İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($item->offers as $offer)
                                <tr>
                                    <td>{{ $offer->pivot->id }}</td>
                                    <td>{{ $offer->getFullName() }}</td>
                                    <td>{{ $offer->email }}</td>
                                    <td>{{ $offer->pivot->created_at->diffForHumans() }}</td>
                                    <td>
                                        <label class="switch">
                                          <input type="checkbox" onclick="offerCheck(this)"
                                            data-id="{{ $offer->pivot->id }}"
                                            {{ $offer->pivot->completed == 1 ? 'checked' : '' }}>
                                          <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td>
                                        <button data-toggle="modal" data-target="#offerModal{{$loop->iteration}}" class="btn btn-edit"> <i class="fa fa-eye"></i> Teklif Detayları </button>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
@forelse($item->offers as $row)
    <div id="offerModal{{$loop->iteration}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Teklif Detayları</h4>
          </div>
          <div class="modal-body">
            <h5>Adı Soyadı</h5>
            <p>{{ $row->getFullName() }}</p>
            <h5>E-mail</h5>
            <p>{{ $row->email }}</p>
            <h5>Telefon</h5>
            <p>{{ $row->phone }}</p>
            <h5>Düğün Tarihi</h5>
            <p>{{ $row->date->format('d.m.Y') }}</p>
            <h5>Teklif Açıklaması</h5>
            <p>{{ $row->pivot->description }}</p>
            <?php $image = DB::table('images')->where('id', $row->pivot->image_id)->first();  ?>
            @if($image)
            <h5>Ürün</h5>
            <p><img src="/uploads/workshops/{{ Auth::guard('workshop')->user()->id }}/{{ $image->filename }}" class="img-responsive" alt=""></p>
            @endif
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          </div>
        </div>
      </div>
    </div>
@empty
@endforelse
@endsection
@push('script')
<script>
    function offerCheck(item){
        var id = $(item).attr('data-id');
        if($(item).is(':checked')){
            $.ajax({
                method: 'POST',
                url: '{{ route("workshop.offer.add") }}',
                data: { id: id },
                success: function(response){
                    if(response.success)
                    {
                        console.log('success');
                    }else{
                        console.log('error');
                    }
                }
            });
        }else{
            $.ajax({
                method: 'POST',
                url: '{{ route("workshop.offer.destroy") }}',
                data: { id: id },
                success: function(response){
                    if(response.success)
                    {
                        console.log('success');
                    }else{
                        console.log('error');
                    }
                }
            });
        }
    }
</script>
@endpush
