@extends('_workshop.master')
@section('content')
<div class="packets-section">
  <div class="as">
    <div class="row">
      <h2 class="packets-section-title">PAKETLER</h2>
      <p class="packets-section-txt">2018 yılı sonuna kadar ilk yılımıza özel fırsatlardan yararlanın!</p>
      <div class="packets-group">
        @foreach($plans as $plan)
        <div class="packets-item pre-packets">
          <div class="packets-item__top">
            <div class="packets-item__header">
              <div class="packets-item__icon">
                <img src="/assets/_website/img/packet-2.svg" alt="Premium">
              </div>
              <h4 class="packets-item__header-title">
                {{ $plan->name }} <br>
                <span>₺{{ $plan->amount }}</span>
                <span class="price">%{{ $plan->discount }} İNDİRİM</span>
              </h4>
              <div class="packets-item__title">
                <div><span>₺</span>{{ $plan->price() }}</div>
                <div>
                  <div class="subscription-txt">1 {{ $plan->slug == 'monthly' ? 'AYLIK' : 'YILLIK' }}<br></div>
                  <span class="kdv-txt">KDV Dahil</span>
                </div>
              </div>
            </div>
            <ul class="packets-item__list">
              <li>Premium Rozeti</li>
              <li>Atölye Adı</li>
              <li>Atölye Logosu</li>
              <li>Atölye Tanıtım Yazısı</li>
              <li>Sınırsız Fotoğraf</li>
              <li>Sınırsız Video</li>
              <li>Fiyat ve Teslimat Bilgisi</li>
              <li>Sıkça Sorulan Sorular</li>
              <li>İletişim Bilgileri</li>
              <li>Çift Yorumları</li>
              <li>1 Özel Röportaj</li>
              <li>Blog Bülten Yayınlama</li>
              <li>Üst Sırada Yer Alma</li>
            </ul>
          </div>
          <div class="packets-item__bottom">
            <span>İLK AY ÜCRETSİZ</span>
          </div>
          {{ Form::open(['url' => route('post.packages')]) }}
            <input type="hidden" name="plan" value="{{ $plan->slug }}">
            <input type="hidden" name="plan_id" value="{{ $plan->id }}">
            <input type="submit" value="Satın Al">
          {{ Form::close() }}
        </div>
        @endforeach
      </div>
      <div class="sss-bottom">
       <a href="http://locdugun.com/sss-atolye" class="sss-button">
         SIK SORULANLAR
       </a>
     </div>
   </div>
 </div>
</div>
@endsection
@push('script')

@endpush