@extends('_workshop.master')
@section('content')
@include('_workshop.partials.section-header')
<div class="row">
	<div class="col-md-12">
		<div class="widget">
			<div class="widget-controls">
				<span class="refresh-content"><i class="fa fa-refresh"></i></span>
			</div>
			<div class="form-elements-sec workshop-form">
				{{ Form::open([ 'url' => route('post.card')]) }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Kart Üzerindeki İsim</label>
							<input type="text" name="card_holder_name" class="form-control custom-input" placeholder="Ad Soyad">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="name">Kart Numarası</label>
							<input type="text" name="card_number" class="form-control custom-input" placeholder="•••• •••• •••• ••••">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="name">Son Kullanma Tarihi (AY)</label>
							<input type="text" name="expire_month" class="form-control custom-input" placeholder="02">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="name">Son Kullanma Tarihi (YIL)</label>
							<input type="text" name="expire_year" class="form-control custom-input" placeholder="2018">
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label for="name">CVC</label>
							<input type="text" name="cvc" class="form-control custom-input" placeholder="•••">
						</div>
					</div>
					<div class="col-md-12">
						<hr>
						<div class="form-group">
							<button type="submit" class="btn btn-edit pull-right"><i class="fa fa-pencil"></i> Aboneliği Başlat</button>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>
@endsection