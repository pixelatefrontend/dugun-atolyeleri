@extends('_workshop.master')
@section('content')
<div class="row">
  <div class="col-md-12">
      <div class="widget no-color">
          <div class="notify simple with-image">
              <span><img src="/assets/_panel/images/resource/check.png" alt=""></span>
              <div class="notify-content"><h3>Thank You</h3><p>Your Order has been recieved..</p>
          </div>
      </div>
  </div>
</div>
@endsection
@push('script')

@endpush