@extends('_workshop.master')
@section('content')
@include('_workshop.partials.section-header')
<div class="panel-content">
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="table-area">
                    <div class="widget-title">
                        <h3>Yorumlar Tablosu</h3>
                        <span>{{ $item->allComments->count() }} Yorum Listelendi</span>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Gelin</th>
                                    <th>Damat</th>
                                    <th>Tarih</th>
                                    <th>Durumu</th>
                                    <th>İşlemler</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($item->allComments as $comment)
                                <tr>
                                    <td>{{ $comment->id }}</td>
                                    <td>{{ $comment->female }}</td>
                                    <td>{{ $comment->male }}</td>
                                    <td>{{ $comment->created_at->diffForHumans() }}</td>
                                    @if($comment->active == 1)
                                    <td><span class="label label-success">Yayında</span></td>
                                    @else
                                    <td><span class="label label-warning">Beklemede</span></td>
                                    @endif
                                    <td>
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#commentModal{{$loop->iteration}}"> <i class="fa fa-eye"></i> Yorumu Gör </button>
                                        @if($comment->active == 0)
                                            {!! confirmComment([ 'workshop.comment.confirm', $comment->id ]) !!}
                                        @else
                                            {!! disabledComment([ 'workshop.comment.disabled', $comment->id ]) !!}
                                        @endif
                                        {!! stuff_destroy([ 'workshop.comment.destroy', $comment->id]) !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
@forelse($item->allComments as $item)
    <div id="commentModal{{$loop->iteration}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Teklif Detayları</h4>
          </div>
          <div class="modal-body">
            <h5>Yorum Başlığı</h5>
            <p>{{ $item->title }}</p>
            <h5>Yorum İçeriği</h5>
            <p>{{ $item->content }}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@empty
@endforelse
@endsection
