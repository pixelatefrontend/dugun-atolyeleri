@extends('_workshop.master')
@section('content')
    @include('_workshop.partials.section-header')
    <div class="panel-content">
        <div class="row">
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-controls">
                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                    </div>
                    <div class="mini-stats c-title">
                        <h4>Paket</h4>
                        <p>{{ $workshop->getType() }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget">
                    <div class="widget-controls">
                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                    </div>
                    <div class="mini-stats c-title">
                        <h4>Dopingler</h4>
                        <p>{{ $workshop->getDoping() }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="mini-stats-sec">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="red-skin"><i class="fa fa-eye"></i></span>
                                    <p>Ziyaretçi Sayısı</p>
                                    <h3>{{ Auth::guard('workshop')->user()->view_count }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="sky-skin"><i class="fa fa-envelope"></i></span>
                                    <p>Teklif Sayısı</p>
                                    <h3>{{ $workshop->offers->count() }}</h3>

                                    <a href="#" class="widget-notify sky-skin" data-count="5">
                                        <span class="btn-txt">
                                            <strong>5</strong> yeni teklifin var!
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="pink-skin"><i class="fa fa-thumbs-up"></i></span>
                                    <p>Beğeni Sayısı</p>
                                    <h3>{{ $workshop->like }}</h3>
                                    <a href="#" class="widget-notify pink-skin" data-count="35">
                                        <span class="btn-txt">
                                            <strong>35</strong> yeni beğeni!
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="orange-skin"><i class="fa fa-comment"></i></span>
                                    <p>Yorum Sayısı</p>
                                    <h3>{{ $workshop->comments->count() }}</h3>
                                    <a href="#" class="widget-notify orange-skin" data-count="12">
                                        <span class="btn-txt">
                                            <strong>12</strong> yeni yorum var!
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                </div><!-- Mini stats Sec -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="widget-controls">
                        <span class="close-content"><i class="fa fa-trash-o"></i></span>
                        <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                    </div><!-- Widget Controls -->
                    <div class="activity-sec">
                        <div class="widget-title">
                            <h3>Ziyaretçiler</h3>
                            <span>Atölyeniz kaç kere ziyaret edildi</span>
                            <div class="pull-right">
                                <select name="filterData" id="filterData" class="form-control custom-input"
                                 onchange="setData(this.options[this.selectedIndex].value);">
                                    <option value="daily" selected>Günlük</option>
                                    <option value="weekly">Haftalık</option>
                                    <option value="monthly">Aylık</option>
                                    <option value="yerly">Yıllık</option>
                                </select>
                            </div>
                        </div>
                        <div id="graph-wrapper">
                            <div class="graph-container">
                                <div class="chartcontainer">
                                  <div class="charttools">
                                    Group by:
                                    <input type="button" value="Günlük" onclick="setGrouping('DD', this);" class="selected" />
                                    <input type="button" value="Haftalık" onclick="setGrouping('WW', this);" />
                                    <input type="button" value="Aylık" onclick="setGrouping('MM', this);" />
                                  </div>
                                   <div id="visitor-chart" style="width: 100%; height: 500px;"></div>
                                </div>
                            </div>
                        </div>  
                    </div><!-- Activity Sec -->
                </div><!-- Widget -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Listeye Eklenme</h3>
                            <span>Atölyenizi Kimler Listeye Ekledi (Bugüne Kadar <b>{{ $workshop->lists->count() }}</b> Kullanıcı Ekledi)</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Ad Soyad</th>
                                        <th>Eklenme Tarihi</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                    @foreach($data_users as $user)
                                        <tr>
                                            <td>{{ $user->getFullName() }}</td>
                                            <td>{{ $user->created_at->format('d.m.Y') }} ({{ $user->created_at->diffForHumans() }})</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/amstock.js"></script>
<script src="https://www.amcharts.com/lib/3/lang/tr.js"></script>

<script>
    var chartData = <?php echo json_encode($visitors); ?>;
    var chart = AmCharts.makeChart("visitor-chart", {
      "type": "stock",
      "theme": "light",
      "dataSets": [{
        "fieldMappings": [{
          "fromField": "value",
          "toField": "value"
        }],
        "dataProvider": chartData,
        "categoryField": "date"
      }],

      "panels": [{
          "showCategoryAxis": false,
          "title": "Value",
          "percentHeight": 70,
          "stockGraphs": [{
            "id": "g1",
            "valueField": "value",
            "balloonText": "Ziyaretçi: <b>[[value]]</b>"
          }],

          "stockLegend": {
            "periodValueTextRegular": "[[value.close]]"
          }
        },

        {
          "title": "Volume",
          "percentHeight": 30,
          "stockGraphs": [{
            "valueField": "volume",
            "type": "column",
            "showBalloon": false,
            "fillAlphas": 1
          }],

          "stockLegend": {
            "periodValueTextRegular": "[[value.close]]"
          }
        }
      ],

      "categoryAxesSettings": {
        "maxSeries": 1,
        "groupToPeriods": ["DD"]
      },

      "chartScrollbarSettings": {
        "graph": "g1"
      },

      "chartCursorSettings": {
        "valueBalloonsEnabled": true,
        "fullWidth": true,
        "cursorAlpha": 0.1,
        "valueLineBalloonEnabled": true,
        "valueLineEnabled": true,
        "valueLineAlpha": 0.5
      },

      "periodSelector": {
        "position": "top",
        "inputFieldsEnabled": false,
        "periods": [{
          "period": "MM",
          "count": 1,
          "label": "1 month"
        }, {
          "period": "YYYY",
          "selected": true,
          "count": 1,
          "label": "1 year"
        }, {
          "period": "YTD",
          "label": "YTD"
        }, {
          "period": "MAX",
          "label": "MAX"
        }]
      }
    });

    function setGrouping(groupTo, button) {
      // set chart grouping
      chart.categoryAxesSettings.groupToPeriods = [groupTo];
      chart.validateData();

      // set selected style on the button
      var buttons = button.parentNode.getElementsByTagName("input");
      for (var i = 0; i < buttons.length; i++) {
        buttons[i].className = "";
      }
      button.className = "selected";
    }
</script>

@endpush
