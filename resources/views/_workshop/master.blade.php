<!DOCTYPE html>
<html lang="tr">
<head>
    <!-- Meta-Information -->
    <title>Admin Panel | Düğün Atölyeleri</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="svg/xml" />
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="http://pixelate.com.tr/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="http://pixelate.com.tr/favicons/favicon-16x16.png" sizes="16x16">
	 <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />

    {!! Html::style('assets/_panel/css/bootstrap.min.css') !!}
    {!! Html::style('assets/_panel/css/select2.min.css') !!}
    {!! Html::style('assets/_panel/css/sweetalert2.min.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/main.css') !!}
    {!! Html::style('assets/_panel/css/panel.css') !!}
    {!! Html::style('assets/_panel/css/icons.css') !!}
    {!! Html::style('assets/_panel/css/dropzone.css') !!}
    {!! Html::style('assets/_panel/css/jquery-gmaps-latlon-picker.css') !!}
    {!! Html::style('assets/_panel/css/custom.css') !!}

</head>
<body>
	@include('_workshop.layouts.header')
	@include('_workshop.layouts.sidebar')
	<div class="main-content">
			@yield('content')
	</div>
    <a href="{{ route('workshop.single', Auth::guard('workshop')->user()->slug) }}" target="_blank" data-toggle="tooltip" title="Atölyeyi Görüntüle">
        <div class="pageView">
            <i class="fa fa-eye"></i>
        </div>
    </a>

	{{-- <script src="https://maps.google.com/maps/api/js?sensor=false"></script> --}}
	{!! Html::script('assets/_panel/js/jquery-2.1.3.js') !!}
    {!! Html::script('assets/_panel/js/jquery-ui.min.js') !!}
    
	{!! Html::script('assets/_panel/js/bootstrap.min.js') !!}
	{!! Html::script('assets/_panel/js/app.js') !!}
	{!! Html::script('assets/_panel/js/common.js') !!}
	{!! Html::script('assets/_panel/js/slick.min.js') !!}
	{!! Html::script('assets/_panel/js/main.js') !!}
  	{!! Html::script('assets/_panel/js/sweetalert2.min.js') !!}
  	{!! Html::script('assets/_panel/js/select2.min.js') !!}
	{!! Html::script('assets/_panel/js/slugify.js') !!}
    {!! Html::script('assets/_panel/js/dropzone.js') !!}
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDBuKc38Izo0RQwG9Vic4L01iMiIKCsDoU&c"></script>
    <script>
        $.gMapsLatLonPickerNoAutoInit = 1;
      </script>
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    {!! Html::script('assets/_panel/js/custom.js') !!}
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
	@stack('script')
    {!! Html::script('assets/_panel/js/panel.js') !!}

    <script>
        $('#clickPre').click(function(e){
            var location = "{{ route('get.premium') }}";
            window.location.href = location;
        });
    </script>
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
    {!! Html::script('assets/_panel/js/jquery-gmaps-latlon-picker.js') !!}
    


	<!--[if lte IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>      
	<![endif]-->
</body>
</html>