<!DOCTYPE html>
<html>
<head>
    <!-- Meta-Information -->
    <title>Atölye Giriş | Düğün Atölyeleri</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />
    <meta name="robots" content="noindex">
    {!! Html::style('assets/_panel/css/bootstrap.min.css') !!}
    {!! Html::style('assets/_panel/css/icons.css') !!}
    {!! Html::style('assets/_panel/css/main.css') !!}
    {!! Html::style('assets/_panel/css/panel.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/custom.css') !!}

</head>
<body>
    <div class="main-content">
        <div class="account-user-sec">
            <div class="account-sec">

            <div class="acount-sec2">


                <section class="base-page sign-in-up-page workshop-login sign-modal">
                    <div class="container">
                        <div class="col-sm-5 col-md-5 col-lg-5 workshop-login__wrp">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-12">
                                    @if(Session::has('loginMessage'))
                                        <div class="error-attempt">
                                            <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong>Üye Girişi</strong><br><br> {{ Session::get('loginMessage') }}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="sign-in-form sign-in-form-2">
                                        <h3>GİRİŞ YAP</h3>
                                        {!! Form::open([ 'url' => route('workshop.login.post')]) !!}
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="input__group {{-- show-error --}}">
                                                    <label>E - POSTA</label>
                                                    <input type="text" placeholder="örnekmail@örnek.com" name="username">
                                                    {{--<div class="error-message">Mail adresi hatalı.</div>--}}
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="input__group">
                                                    <label>ŞİFRE</label>
                                                    <input type="password" placeholder="Şifreniz" name="password">
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="remember-wrap">
                                            <span>
                                                <input type="checkbox" id="registerCheck" name="formcheck_register">
                                                <label for="registerCheck">Oturumu açık tut</label>
                                            </span>
                                                    <a href="{{ route('get.password.reset') }}">Şifremi Unuttum</a>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="input__group submit__group">
                                                    <button type="submit" name="register_ajax" class="button button--large button--block button--pink">GİRİŞ YAP<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></button>
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div><!-- Account Sec -->
    </div>
</div><!-- Main Content -->

<!-- Vendor: Javascripts -->
{!! Html::script('assets/_panel/js/jquery-2.1.3.js') !!}
{!! Html::script('assets/_panel/js/bootstrap.min.js') !!}
<!-- Our Website Javascripts -->
</body>
</html>