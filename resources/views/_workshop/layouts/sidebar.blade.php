<header class="side-header dark-skin opened-menu">
    <div class="admin-details">
        <span><img src="{{ Auth::guard('workshop')->user()->avatarPath() }}" alt="" /></span>
        <a href="{{ route('workshop.dashboard') }}">
            <h3>{{ Auth::guard('workshop')->user()->title }}</h3>
        </a>
        <i>{{ Auth::guard('workshop')->user()->categories->last()->title }}</i>
    </div>
    <div class="menu-scroll">
        <div class="side-menus">
            <nav>
                <ul>
                    <li><a href="{{ route('workshop.dashboard') }}" class="selected"><i class="fa fa-dashboard"></i> <span>Genel Bilgiler</span></a></li>
                    <li><a href="{{ route('workshop.panel') }}"><i class="fa fa-wrench"></i> <span>Atölye Paneli</span></a></li>
                    <li><a href="{{ route('workshop.gallery') }}" title=""><i class="fa fa-picture-o"></i> <span>Resim Galeri</span></a></li>
                    <li><a href="{{ route('videos.index') }}" title=""><i class="fa fa-youtube-play"></i> <span>Video Galeri</span></a></li>
                    <li><a href="{{ route('workshop.offers') }}" title=""><i class="fa fa-list"></i> <span>Teklifler</span></a></li>
                    <li><a href="{{ route('workshop.comments') }}" title=""><i class="fa fa-comments"></i> <span>Yorumlar</span></a></li>
                    <li><a href="{{ route('campaigns.index') }}" title=""><i class="fa fa-bell"></i> <span>Kampanyalar</span></a></li>
                    {{-- <li><a href="{{ route('workshop.analytics') }}" title=""><i class="fa fa-line-chart"></i> <span>İstatistikler</a></li> --}}
                    <li><a href="{{ route('workshop.logout')}}" title=""><i class="fa fa-power-off"></i> <span>Çıkış Yap</span></a></li>
                </ul>
            </nav>
        </div>
    </div><!-- Menu Scroll -->
</header>