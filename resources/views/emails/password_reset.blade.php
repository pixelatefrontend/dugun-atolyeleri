<!DOCTYPE html>

<html>

	<head>

		<title>Düğün Atölyeleri Mailing</title>

		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	</head>

	<body style="margin: 0px; padding: 0px;">

		<style type="text/css">

			body { margin: 0px; padding: 0px; }

			@media screen and (max-width: 625px) {

				.wrapper { width: auto !important; margin-left: 30px !important; margin-right: 30px !important; }

				.container { width: auto !important; }

			}

			@media screen and (max-width: 465px) {

				.logo { width: 100% !important; text-align: center !important; }

				.social { width: 100% !important; text-align: center !important; margin-top: 20px !important; }

			}

			@media screen and (max-width: 445px) {

				.contact-item { width: 100% !important; margin-bottom: 10px !important; }

			}

			@media screen and (max-width: 438px) {

				.cta-button { width: 100% !important; }

			}

		</style>

		<div class="main" style="display: inline-block; width: 100%; background-color: #f0f1f4; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-family: Helvetica,Calibri, Arial, sans-serif;">

			<div class="wrapper" style="width: 625px; display: block; margin-left: auto; margin-right: auto;">

				<div class="container" style="width: 625px; margin-left: auto; margin-right: auto;">

					<table style="width: 100%; border-collapse: collapse; margin-top:30px;">

						<thead>

							<tr>

								<td>

									<a href="#" style="display: inline-block; float: left;" class="logo">

										<img src="https://dugunatolyeleri.com/assets/_website/img/logo-pink-black-2x.png" height="32" width="195" alt="">

									</a>

									<ul style="display: inline-block; float: right; margin:0px;" class="social">

										<li style="list-style-type: none; display: inline-block; margin-right: 10px;"><a href="https://www.facebook.com/dugunatolyeleri/"><img src="https://dugunatolyeleri.com/assets/_email/img/facebook-icon.png" height="30" width="30"></a></li>

										<li style="list-style-type: none; display: inline-block; margin-right: 10px;"><a href="https://twitter.com/dugunatolyeleri"><img src="https://dugunatolyeleri.com/assets/_email/img/twitter-icon.png" height="30" width="30"></a></li>


										<li style="list-style-type: none; display: inline-block;"><a href="https://www.instagram.com/dugunatolyeleri/"><img src="https://dugunatolyeleri.com/assets/_email/img/instagram-icon.png" height="30" width="30"></a></li>

									</ul>

								</td>

							</tr>

						</thead>

						<tbody>

							<tr>

								<td colspan="2">

									<div style="background-color: white; padding: 50px 40px; border-radius: 12px; margin-top: 20px;">

										<h3 style="font-size: 24px; color: #ec689e; font-weight: bold; text-transform: capitalize; text-align: left; border-bottom: 2px solid #f4f6f7; padding-bottom: 40px; margin-bottom: 40px;">Şifre Sıfırlama</h3>

										<p style="font-size: 16px; color: #9fa3a7; line-height: 24px;">Merhaba,{{-- {{ $name }} {{ $lname }} --}}<br>Yeni şifre belirlemek için linke tıklayınız.</p>
                                        
                                        <a href=" {{ route('get.password.token', $token) }} " style="font-size: 16px;color: white;text-decoration: none;padding: 15px 40px;border: 2px solid #ec689e;border-radius: 25px;margin-top: 10px;display: inline-block;background-color: #ec689e;" class="orders-cta">ŞİFREMİ SIFIRLA</a>
                                        
                                        <br><p style="font-size: 16px; color: #9fa3a7; line-height: 24px;">Şifre sıfırlama talebinde bulunmadıysanız bu maili dikkate almayın.<br><br>Yardıma ihtiyacınız olduğunda her zaman yanınızdayız. Daha fazla bilgi için <strong style="font-weight: bold; color: #2e538d;"><a href="{{ route('get.sss') }}">Sık Sorulanlar</a></strong> sayfamızı ziyaret edin veya <strong style="font-weight: bold; color: #2e538d;"> <a href="{{ route('get.contact') }}"> bizimle iletişime geçin.</a> </strong></p>
                                        

										

									</div>

								</td>

							</tr>

						</tbody>

						<tfoot>

							<tr>

								<td colspan="2">

									

									
									<br><br>
									<div style="text-align: center; margin-bottom: 10px;">

										<strong style="font-size: 12px; color: #505966; text-align: center;">© 2017 düğünatölyeleri.com Bu E-Mail Düğün Atölyeleri tarafından gönderilmiştir.</strong> <a href="mailto:bilgi@dugunatolyeleri.com" style="font-size: 12px; color: #505966;">E-Mail Destek</a>

									</div>
                                    <br><br>

								</td>

							</tr>

						</tfoot>

					</table>

				</div>

			</div>

		</div>
		
	</body>

</html>