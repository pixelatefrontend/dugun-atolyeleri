@component('mail::message')
# İletişim Formu
<p> <b>Ad:</b> {{ $data['name'] }}</p>
<p> <b>Email:</b> {{ $data['email'] }}</p>
<p> <b>Mesaj:</b> {{ $data['message'] }}</p>
@endcomponent
