<div class="profil-head">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="profil-wrap">

						<div class="avatar-wrap">

							<figure>
								@if(!Auth::user()->image)
									<div class="no-image">
										<span class="pp-username-one" data-fname="{{ Auth::user()->name }}" data-lname="{{ Auth::user()->lname }}"></span>
									</div>
								@else
									<img src="{{ Auth::user()->image }}">
								@endif

							</figure>

							<h4>{{ Auth::user()->getFullName() }}</h4>
							
						</div>

						<div class="detail-wrap">

							<ul>

								<li>

									<strong>E-Mail Adresi</strong>

									<span>{{ Auth::user()->email }}</span>

								</li>

								<li>

									<strong>Telefon Numarası</strong>

									<span>{{ Auth::user()->phone }}</span>
									
								</li>

								<li>

									<strong>Düğün Tarihi</strong>

									<span>{{ Auth::user()->date->format('d/m/Y') }}</span>
									
								</li>

								<li>

									<strong>Şehir</strong>

									<span>{{ Auth::user()->city }}</span>
									
								</li>

								<li>

									<a href="{{ route('user.dashboard') }}" class="edit-button">DÜZENLE</a>

								</li>

							</ul>

						</div>
						
					</div>
					
				</div>

			</div>

		</div>
		
	</div>

	<div class="profil-nav">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@include('_user.partials.navigation')
				</div>

			</div>

		</div>

	</div>