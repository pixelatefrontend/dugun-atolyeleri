<nav>

	<ul>

		<li class="user-nav-link {{ setActive('tekliflerim', 'selected') }}"><a class="{{ setActive('tekliflerim', 'selected') }}" href="{{ route('user.offers') }}">TEKLİFLERİM</a></li>

		<li class="user-nav-link {{ setActive('listelerim', 'selected') }}"><a class="{{ setActive('listelerim', 'selected') }}" href="{{ route('user.lists') }}">LİSTELERİM</a></li>

		<li class="user-nav-link {{ setActive('favorilerim', 'selected') }}"><a class="{{ setActive('favorilerim', 'selected') }}" href="{{ route('user.favorites') }}">BEĞENDİKLERİM</a></li>

		<li class="user-nav-link {{ setActive('yorumlarim', 'selected')  }}"><a class="{{ setActive('yorumlarim', 'selected')  }}" href="{{ route('user.comments') }}">YORUMLARIM</a></li>

		<li class="user-nav-link {{ setActive('/', 'selected') }}"><a class="{{ setActive('/', 'selected') }}" href="{{ route('user.dashboard') }}">AYARLARIM</a></li>

		<li class="user-hidden-arrow">
			<i class="fa fa-chevron-down" aria-hidden="true"></i>
		</li>

	</ul>

</nav>