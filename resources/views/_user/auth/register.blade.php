<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="p:domain_verify" content="07e75480d9124bc7b806a6f3612b1e23"/>

	<title>Kullanıcı Kayıt | Düğün Atölyeleri</title>
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="{{ config('app.twitter') }}">
	<meta name="twitter:title" content="{{ $item->title or config('app.title') }}">
	<meta name="twitter:description" content="{{ $item->description or config('app.description') }}">
	<meta name="twitter:image" content="{{ $item->image or config('app.image') }}">
	<meta name="twitter:url" content="{{ URL::current() }}">
	<!-- Twitter Card Data End -->
	<!-- Open Graph Data -->
	<meta property="og:title" content="{{ $item->title or config('app.title') }}" />
	<meta property="og:type" content="{{ config('app.type') }}" />
	<meta property="og:url" content="{{ URL::current() }}" />
	<meta property="og:image" content="{{ $item->image or config('app.image') }}" />
	<meta property="og:description" content="{{ $item->description or config('app.description') }}" /> 
	<meta property="og:site_name" content="{{ config('app.title') }}" />
	<!-- Open Graph Data End -->
	<!-- Meta Tag Data Start -->
	<meta name="title" content="{{ $item->title or config('app.title') }}" />
	<meta name="description" content="{{ $item->description or config('app.description') }}" />
	<meta name="keywords" content="{{ config('app.keywords') }}" />
	<meta name="copyright" content="{{ config('app.copyright') }}" />
	<!-- Icon -->

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />

<!-- Icon End -->



<!-- Styles -->

<link rel="stylesheet" type="text/css" href="assets/_website/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/select2.min.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/easy-autocomplete.min.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/easy-autocomplete.themes.min.css">

<link rel="stylesheet" type="text/css" href="assets/_website/css/main.css">

<!-- Styles End -->
</head>
<body>
	@include('_website.partials.modals.terms-modal')
	<header class="login-success">
		<div class="row middle-nav-container">

			<div class="container">

				<nav class="middle-nav">

					<div class="row">

						<div class="row-inner col-md-4 col-lg-3 col-sm-4 col-xs-6">

							<div class="logo-section">

								<h1><a href="{{ env('APP_URL') }}"><img alt="Düğün Atölyeleri" src="assets/_website/img/logo-pink-black.png" srcset="assets/_website/img/logo-pink-black-2x.png 2x"></a></h1>

							</div>

						</div>

						<div class="row-inner col-md-4 col-lg-6 col-sm-4 hidden-xs">

						</div>

						<div class="row-inner col-md-3 col-lg-3 col-sm-4 col-xs-6">

							<a href="tel:+90 216 504 22 62" class="call-section">

								<div class="call-section-inner">

									<figure>

										<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/phone-icon.svg#phone-icon"></use></svg>

									</figure>

									<div class="text">

										<strong>BİZİ ARAYIN</strong>

										<strong><span>0 (216)</span> 504 22 62</strong>

									</div>

								</div>

							</a>

						</div>

					</div>

				</nav>

			</div>

		</div>

		<div class="row big-nav-container">

			<div class="container">

			</div>

		</div>

	</header>
	<section class="base-page sign-in-up-page sign-modal">
	<div class="container">
		<div class="row">
			<div class="col-lg-10 col-lg-offset-1">
				<div class="row">
					<div class="col-sm-6 col-md-6 col-lg-7">
						<div class="sign-in-form sign-up-form">
							@if ($errors->any())
							    <div class="alert alert-danger">
							    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
							<h3>KAYIT OL</h3>
							{!! Form::open([ 'url' => route('user.register.post') ]) !!}
								<div class="row">
									<div class="col-lg-12">
										<h4>KİŞİSEL BİLGİLERİNİZ</h4>
									</div>
									<div class="col-lg-6">
										<div class="input__group input-container input-control">
											<input type="text" placeholder="Adınız" name="name">
											<div class="error-message">Adınız hatalı.</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input__group input-container input-control">
											<input type="text" placeholder="Soyadınız" name="lname">
											<div class="error-message">Soyadınız hatalı.</div>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input__group input__phone__group input-container input-control phone-control">
											<input type="text" placeholder="(+90) XXX XXX XX XX" name="phone" class="phone-mask">
											<span>( Teklif geri dönüşleri için )</span>
											<div class="error-message">Telefon hatalı.</div>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input__group input-container input-control mail-control">
											<input type="email" placeholder="E-mail" name="email">
											<div class="error-message">Mail hatalı.</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input__group input-container input-control">
											<input type="password" placeholder="Şifreniz" name="password">
											<div class="error-message">Şifre hatalı.</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input__group input-container input-control">
											<input type="password" placeholder="Şifre Tekrarı" name="confirm-password">
											<div class="error-message">Şifre hatalı.</div>
										</div>
									</div>
									<div class="col-lg-12">
										<h4>ORGANİZASYON BİLGİLERİNİZ</h4>
									</div>
									<div class="col-lg-6">
										<div class="input__group input__date__group input-container input-control">
											<input class="input__control datepicker" name="date" id="date" placeholder="Tarih" required="" data-date-format="dd/mm/yyyy">
											<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>
											<div class="error-message">Tarih hatalı.</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="input__group custom-select select-container input-control">
											<select placeholder="Şehir" class="city-category-select" name="city">
												<option value="" disabled="" selected="">Şehir</option>
				                               @foreach ($cities as $city)
				                               		<option value="{{$city->city}}">{{ $city->city }}</option>
				                               @endforeach
				                            </select>
				                            <div class="error-message">Şehir hatalı.</div>
			                            </div>
									</div>
									<div class="col-lg-12">
										<div class="checkbox-container input-control">
											<input type="checkbox" id="registerCheck" name="formcheck_register">
											<label for="registerCheck"><a href="#" data-toggle="modal" data-target="#termsModal">Üyelik Sözleşmesini ve Gizlilik Politikasını</a> kabul ediyorum</label>
											<div class="error-message" id="termsCheck">Sözleşme şartlarını kabul etmelisiniz.</div>
										</div>
									</div>
									<div class="col-lg-12">
										<button type="submit"  style="margin-top: 25px;" class="button button--large button--block button--pink form-control-button">KAYIT OL<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></button>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-sm-6 col-md-6 col-lg-5">
						<div class="sign-up-wrap sign-in-wrap sign-up-wrap-right">
							<svg class="icon-3"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/gain-icon.svg#gain-icon"></use></svg>
							<h4>Fırsatları Takip Et</h4>
							<p>düğünatölyeleri.com aracılığı ile düğün alışverişlerine yönelik güncel modayı takip edebilir, atölyelerimizin fırsatlarından haberdar olabilir ve faydalanabilirsiniz.</p>
							
                            <svg class="icon-1"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/company-presentation.svg#company-presentation"></use></svg>
							<h4>Favori Listeleri Oluştur</h4>
							<p>Beğendiğiniz atölyeler ile favori listeleri oluşturabilirsiniz. Favori listeleriniz sayesinde kaydettiğiniz atölye ve ürünlere kolayca erişebilirsiniz.</p>
							<svg class="icon-2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/get-offer-icon.svg#get-offer-icon"></use></svg>
							<h4>Teklif İste</h4>
							<p>düğünatölyeleri.com aracılığı ile atölyelerimizle iletişime geçebilirsiniz. Atölyelerin ürün ve hizmetleri için teklif isteyebilir, detaylı bilgi alabilirsiniz. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	<footer>

		<div class="row big-nav-container">

			<div class="container">

				<div class="big-nav">

					<div class="row">

						<div class="row-inner col-md-3 col-lg-3 col-sm-6 col-xs-12">

							<div class="logo-section-container">

								<div class="logo-section">

									<figure><a href=""><img src="/assets/_website/img/logo-pink-white.png" srcset="/assets/_website/img/logo-pink-white-2x.png 2x"></a></figure>

								</div>

								<div class="call-section-wrp">
									<a href="tel:908504458877" class="call-section">

										<div class="call-section-inner">

											<figure>

												<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/phone-icon.svg#phone-icon"></use></svg>

											</figure>

											<div class="text">

												<strong>BİZİ ARAYIN</strong>

												<strong><span>0 (850)</span> 214 25 99</strong>

											</div>

										</div>

									</a>
								</div>

								<nav class="logo-under-menu">

									<ul>

										{{-- <li><a href="{{ route('get.blog') }}">BLOG</a></li> --}}
										<li><a href="{{ route('get.whoarewe') }}">BİZ KİMİZ</a></li>
										<li><a href="{{ route('get.sss') }}">SIK SORULANLAR</a></li>
										<li><a href="{{ route('get.contact') }}">İLETİŞİM</a></li>

									</ul>

								</nav>

							</div>

						</div>

						<div class="row-inner col-md-6 col-sm-6 col-lg-6 hidden-xs">



							<div class="workshop-section-container">

								<h3>ATÖLYELER</h3>

								<nav class="workshop-menu">

									<ul>

										@foreach($all_categories as $category)
											<li><a href="{{ route('category.single', $category->slug) }}">{{ $category->title }}</a></li>
										@endforeach

									</ul>

								</nav>

							</div>

						</div>

						<div class="row-inner col-md-3 col-sm-12 col-lg-3 col-xs-12">



							<div class="social-section-container">



								<div class="workshop-register">

									<a href="{{ route('workshop.register') }}" class="workshop-button" href=""><span><svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg><strong>ATÖLYE KAYIT</strong></span></a>

								</div>

								<div class="social-follow">

									<h3>BİZİ TAKİP EDİN</h3>

									<a href="https://www.facebook.com/dugunatolyeleri" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>

									<a href="https://twitter.com/dugunatolyeleri" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>

									<a href="https://www.instagram.com/dugunatolyeleri/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>

									<a href="http://pinterest.com/dugunatolyeleri" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>

								</div>

							</div>

						</div>

						<div class="footer-bottom">
							<ul>

								<li><a href="{{ route('get.privacy') }}">Gizlilik Politikası</a></li>

								<li><a href="{{ route('get.terms') }}">Kullanıcı Sözleşmesi</a></li>

							</ul>
						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="row small-nav-container">

			<div class="container">

				<div class="small-nav">

					<span>© 2017 <strong>düğünatölyeleri.com</strong> Tüm hakları saklıdır. </span>

					<ul>

						<li><a href="{{ route('get.privacy') }}">Gizlilik Politikası</a></li>

						<li><a href="{{ route('get.terms') }}">Kullanıcı Sözleşmesi</a></li>

					</ul>

				</div>

			</div>

		</div>

	</footer>

	<!-- Scripts -->

<script type="text/javascript" src="assets/_website/js/jquery.min.js"></script>

<script type="text/javascript" src="assets/_website/js/jquery-ui.js"></script>

<script type="text/javascript" src="assets/_website/js/bootstrap.min.js"></script>

<script type="text/javascript" src="assets/_website/js/isotope.pkgd.min.js"></script>

<script type="text/javascript" src="assets/_website/js/slick.min.js"></script>

<script type="text/javascript" src="assets/_website/js/select2.min.js"></script>

<script type="text/javascript" src="assets/_website/js/TweenMax.min.js"></script>

<script type="text/javascript" src="assets/_website/js/jquery.easy-autocomplete.min.js"></script>

<script type="text/javascript" src="assets/_website/js/jquery.inputmask.bundle.min.js"></script>

<script type="text/javascript" src="assets/_website/js/main.js"></script>

<!-- Scripts End -->
</body>
</html>