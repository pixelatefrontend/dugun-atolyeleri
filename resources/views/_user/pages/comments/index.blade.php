@extends('_user.layouts.master')
@section('content')
	<div class="likes-section comments-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="title">
						<div class="list-length">
							<svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-search-list"></use></svg>
							<span><strong>{{ $user->comments()->count() }}</strong> YORUM LİSTELENDİ</span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<ul>
						@foreach($user->comments as $comment)
							<li>
								<div class="message">
									<h4>{{ $comment->title }}</h4>
									<p>{{ $comment->content }}</p>
									<span><a href="{{ route('workshop.single', $comment->workshop->slug) }}">{{ $comment->workshop->title }}</a> adlı atölyeye yazıldı</span><br>
									<div class="edit-button">
										{{ Form::open([ 'url' => route('user.comment.destroy', $comment->id), 'method' => 'DELETE' ]) }}
											<button class="delete-button" type="submit"><i class="fa fa-trash"></i> SİL</button>
										{{ Form::close() }}
									</div>
								</div>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
@endsection