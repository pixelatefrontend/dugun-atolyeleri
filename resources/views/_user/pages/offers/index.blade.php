@extends('_user.layouts.master')
@section('content')
	<div class="likes-section proposal-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="title">
						<div class="list-length">
							<svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-search-list"></use></svg>
							<span><strong>{{ $offers->count() }}</strong> TEKLİF LİSTELENDİ</span>
						</div>
					</div>
					<div class="proposal-list">
						<ul>
							@foreach($offers as $offer)
							<li>
								<div class="date">
									<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>
									<span>{{ $offer->pivot->created_at->diffForHumans() }}</span>
								</div>
								<div class="category">
									<svg class="icon {{$offer->categories->last()->icon}}">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/files/svg/icons.svg#{{ $offer->categories->last()->icon }}"></use>
									</svg>
									<span>{{ $offer->categories->last()->title }}</span>
								</div>
								<div class="profil">
									<svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>
									<figure>
									@if(!$offer->avatar)
										<div class="no-image">
											<span>{{ $offer->noImageAvatar() }}</span>
										</div>
									@else
										<img src="{{ $offer->avatarPath() }}">
									@endif
									</figure>
									<span><a href="{{ route('workshop.single', $offer->slug) }}">{{ $offer->getShortTitle(14) }}</a></span>
								</div>
								<div class="status">
									@if($offer->pivot->completed == 0)
										<span class="wait">Bekliyor</span>
									@else
										<span class="ready">Teslim Edildi</span>
									@endif
								</div>
								<div class="button-wrap">
									<a href="#" data-toggle="modal" data-target="#offerModal{{$loop->iteration}}" class="buton buton--pink">Teklif Detayları</a>
								</div>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	@foreach($offers as $item)
	    <div id="offerModal{{$loop->iteration}}" class="modal fade offer-modal" role="dialog">
	      <div class="modal-dialog">
	        <!-- Modal content-->
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h2 class="modal-title">Teklif Detayları</h2>
	          </div>
	          <div class="modal-body">
				  <div class="offer-modal-item">
					  <div class="offer-modal-el">
						  <h5>Adı Soyadı</h5>
						  <p>{{ Auth::user()->getFullName() }}</p>
					  </div>
					  <div class="offer-modal-el">
						  <h5>Düğün Tarihi</h5>
						  <p>{{ Auth::user()->date->format('d.m.Y') }}</p>
					  </div>
				  </div>



					<div class="offer-modal-title">ATÖLYE BİLGİLERİ</div>
				  <div class="offer-modal-item">
					  <div class="profil offer-modal-el">
						  <h5>Atölye Adı</h5>
						  {{--<svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>--}}

						  <p><a href="{{ route('workshop.single', $item->slug) }}">{{ $item->getShortTitle(14) }}</a></p>
					  </div>

					  <div class="category offer-modal-el">
						  <h5>Atölye Kategorisi</h5>
						  {{--<svg class="icon {{$offer->categories->last()->icon}}">--}}
							  {{--<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/files/svg/icons.svg#{{ $item->categories->last()->icon }}"></use>--}}
						  {{--</svg>--}}
						  <p>{{ $item->categories->last()->title }}</p>
					  </div>

				  </div>

				  <div class="offer-modal-item">
					  <div class="date offer-modal-el">
						  <h5>Teklif Tarihi</h5>
						  {{--<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>--}}
						  <p>{{ $item->pivot->created_at->diffForHumans() }}</p>
					  </div>

					  <div class="status offer-modal-el">
						  <h5>Teklif Durumu</h5>
						  @if($item->pivot->completed == 0)
							  <p class="wait">Bekliyor</p>
						  @else
							  <p class="ready">Teslim Edildi</p>
						  @endif
					  </div>


				  </div>

				  <div class="offer-modal-item">
					  <div class="offer-modal-el fw">
						  <h5>Teklif Açıklaması</h5>
						  <p>{{ $item->pivot->description }}</p>
					  </div>
				  </div>


	          </div>
	          {{--<div class="modal-footer">--}}
	            {{--<button type="button" class="btn btn-default" data-dismiss="modal">KAPAT</button>--}}
	          {{--</div>--}}
	        </div>
	      </div>
	    </div>
	@endforeach
@endsection