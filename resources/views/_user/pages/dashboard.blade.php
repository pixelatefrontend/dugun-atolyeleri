@extends('_user.layouts.master')
@section('content')
	<div class="settings-section">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-lg-6 col-md-offset-2 col-lg-offset-3">
					@if(Session::has('message'))
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  <strong>Başarılı! </strong> {{ Session::get('message') }}
						</div>
					@endif
					{!! Form::open([ 'url' => route('user.update.post', Auth::user()->id), 'method' => 'PUT' ]) !!}
						<div class="row sign-modal">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h4>KİŞİSEL BİLGİLERİNİZ</h4>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="input__group">
									<input type="text" placeholder="Adınız" name="name" value="{{ Auth::user()->name }}">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="input__group">
									<input type="text" placeholder="Soyadınız" name="lname" value="{{ Auth::user()->lname }}">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="input__group input__phone__group">
									<input type="text" placeholder="(+90) XXX XXX XX XX" name="phone" class="phone-mask" value="{{ Auth::user()->phone }}">
									<span>( Teklif geri dönüşleri için )</span>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h4>GİRİŞ BİLGİLERİNİZ</h4>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="input__group">
									<input type="password" placeholder="Şifreniz" name="password">
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="input__group">
									<input type="password" placeholder="Şifre Tekrarı" name="password2">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h4>ORGANİZASYON BİLGİLERİNİZ</h4>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="input__group input__date__group">
									<input class="input__control datepicker"
										type="text"
										placeholder="{{ Auth::user()->date->format('d/m/Y') }}"
										name="date" value="{{ Auth::user()->date->format('d/m/Y') }}">
									<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ">
								<div class="input__group">
									<div class="custom-select">
			                            {{ Form::select('city', $cities, Auth::user()->city, ['class' => 'city-category-select']) }}
		                            </div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="input__group submit__group">
									<button type="submit" class="button button--block button--pink edit--button">DÜZENLE</button>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection