@extends('_user.layouts.master')
@section('content')

	<div class="title list-title">
		<div class="list-length">
			<svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-search-list"></use></svg>
			<span><strong>{{ $user->lists()->count() }}</strong> LİSTE</span>
		</div>
	</div>
	@foreach($user->lists as $list)
	<div class="home-page-design-list lists-section">
		<div class="container">
			<div class="row design-list">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title">

							<div class="title-inner">

								<h3>{{ $list->title }}</h3>

								<div class="date">

									<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>

									<span>{{ $list->created_at->diffForHumans() }}</span>

								</div>

								<div class="count">

									<strong>{{ count($list->workshops) }}</strong>

									<span>ATÖLYE LİSTELENDİ {!! count($list->workshops) == 0 ? '<small>(Henüz bu listeye atölye eklenmemiş)</small>' : '' !!}</span>

								</div>

							</div>

							{{-- <a href="">KATEGORİYE GİT</a> --}}
							
						</div>

					</div>

					<div class="row category-slider">

						@foreach($list->workshops as $workshop)

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

							<div class="week-design-list-item">

								<div class="ribbon-badge">

									<div class="kurdele-inner">

										<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg>

									</div>

								</div>

								<div class="info-badge">

									<div class="info-badge-item">

										<div class="info-badge-item-inner">

											<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-video"></use></svg>

											<span>{{ count($workshop->videos) }}</span>

										</div>

									</div>

									<div class="info-badge-item">

										<div class="info-badge-item-inner">

											<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-camera"></use></svg>

											<span>{{ count($workshop->images) }}</span>

										</div>

									</div>

								</div>

								<figure class="main-figure">

									<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">

										<div class="click-detail-inner">

											<span>ATÖLYEYE GİT</span>

										</div>

									</a>
									@if($workshop->first_picture)
										<img src="{{ $workshop->first_picture }}" alt="">
									@elseif($workshop->latestImage)
										<img src="/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->filename }}" alt="">
									@else
										<img src="/assets/_website/img/week-design-image-1.jpg" alt="">
									@endif

								</figure>

								<div class="footer">

									<figure class="avatar-figure">

										<div class="hover-svg">

											<div class="hover-svg-inner">

												<button 
													value="{{ $workshop->id }}" 
													class="no-button" 
													type="button" 
													onclick="addFavorite(this)">
												<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
												</button>

											</div>

										</div>

										@if($workshop->avatar)
											<img src="/{{ $workshop->avatarPath() }}">
										@else
											<div class="no-image">
												<span>{{ $workshop->noImageAvatar() }}</span>
											</div>
										@endif

									</figure>

									<div class="footer-inner">

										<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->title }}</h4></a>

										@if(Auth::check())
											<a href="javascript:void(0)" id="favorite_{{$workshop->id}}" class={{{ $workshop->isFavorited() ? 'increase' : '' }}}>
										@else
											<a href="javascript:void(0)" id="favorite_{{$workshop->id}}">
										@endif

											<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-like"></use></svg>

											<span>{{ $workshop->like }}</span>

										</a>

									</div>

								</div>

							</div>

						</div>
						@endforeach

					</div>
					
				</div>
			</div>
		</div>
	</div>
@endforeach
@endsection
@push('scripts')
	@if(Session::has('workshop_message'))
		<script>
			$('#welcome_modal').modal('show');
		</script>
	@endif
	{!! Html::script('/assets/_website/js/infinite.min.js') !!}
	<script>
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>
<script type="text/javascript">
	// Beğenme işlemleri
	function addFavorite(item) {
			var w_id = $(item).attr('value');
			var data = { id: w_id };
			@if(Auth::check())
				
				$.ajax({
					type: 'POST',
					url: "{{ route('add.user.favorite') }}",
					data: data,
					success: function(response){
						if(response.action == "add"){
							$('#favorite_'+w_id).addClass('increase');
							$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
						}else{
							$('#favorite_'+w_id).removeClass('increase');
							$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
						}
					}
				});
			@else
				if($('#favorite_'+w_id).hasClass('increase')) {
					$('#favorite_'+w_id).removeClass('increase');
					$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
					$.ajax({
						type: 'POST',
						url: "{{ route('favorite.destroy') }}",
						data: data,
					});
				}else{
					$('#favorite_'+w_id).addClass('increase');
					$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
					$.ajax({
						type: 'POST',
						url: "{{ route('favorite.add') }}",
						data: data,
					});
				}
			@endif
	}

	// Infinity Scroll **
	var $container = $('#articles').infiniteScroll({
	  path: '.pagination li.active + li a',
	  append: '#article',
	  hideNav: 'ul.pagination',
	  status: '.load-more',
	  button: '.view-more-button',
	  history: false,
	});

	var $viewMoreButton = $('.view-more-button');
	var infScroll = $container.data('infiniteScroll');
	$container.on( 'load.infiniteScroll', onPageLoad );
	function onPageLoad() {
	  if ( infScroll.loadCount == 2 ) {
	    $container.infiniteScroll( 'option', {
	      loadOnScroll: false,
	    });
	    $viewMoreButton.show();
	    $container.off( 'load.infiniteScroll', onPageLoad );
	  }
	}
</script>
@endpush