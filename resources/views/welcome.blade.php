<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Welcome</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<select name="" id="">
		@foreach($data as $row)
			<option value="{{ $row }}" {{{ $item->services->price == $row ? 'selected' : '' }}}>{{ $row }}</option>
		@endforeach
	</select>
</body>
</html>