
<script type="text/javascript">
      var map;
      var map2;
      function initMap() {

		map = new google.maps.Map(document.getElementById('map-premium'), {
			center: {lat: {{ $item->map->x or 41.0055005 }}, lng: {{ $item->map->y or 28.7319856 }} },
			scrollwheel: true,
			panControl: true,
			zoomControl: true,
			mapTypeControl: false,
			streetViewControl: true,
			zoom: 8,
			styles : [
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "visibility": "on"
					            },
					            {
					                "hue": "#ff0000"
					            }
					        ]
					    },
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "labels.text.fill",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "labels.text.stroke",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.man_made",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f7f1df"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.natural",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#d0e3b4"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.natural.terrain",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "elementType": "labels",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.business",
					        "elementType": "all",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.medical",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#fbd3da"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.park",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#bde6ab"
					            }
					        ]
					    },
					    {
					        "featureType": "road",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "road",
					        "elementType": "labels",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffe15f"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#efd151"
					            }
					        ]
					    },
					    {
					        "featureType": "road.arterial",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "black"
					            }
					        ]
					    },
					    {
					        "featureType": "transit.station.airport",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#cfb2db"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#a2daf2"
					            }
					        ]
					    }
					]
		});

		map2 = new google.maps.Map(document.getElementById('map-premium-mobil'), {
			center: {lat: {{ $item->map->x or 41.0055005 }}, lng: {{ $item->map->y or 28.7319856 }} },
			scrollwheel: true,
			panControl: false,
			zoomControl: false,
			mapTypeControl: false,
			streetViewControl: false,
			zoom: 7,
			styles : [
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "visibility": "on"
					            },
					            {
					                "hue": "#ff0000"
					            }
					        ]
					    },
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "labels.text.fill",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "administrative.neighborhood",
					        "elementType": "labels.text.stroke",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.man_made",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#f7f1df"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.natural",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#d0e3b4"
					            }
					        ]
					    },
					    {
					        "featureType": "landscape.natural.terrain",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi",
					        "elementType": "labels",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.business",
					        "elementType": "all",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.medical",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#fbd3da"
					            }
					        ]
					    },
					    {
					        "featureType": "poi.park",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#bde6ab"
					            }
					        ]
					    },
					    {
					        "featureType": "road",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "road",
					        "elementType": "labels",
					        "stylers": [
					            {
					                "visibility": "off"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffe15f"
					            }
					        ]
					    },
					    {
					        "featureType": "road.highway",
					        "elementType": "geometry.stroke",
					        "stylers": [
					            {
					                "color": "#efd151"
					            }
					        ]
					    },
					    {
					        "featureType": "road.arterial",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#ffffff"
					            }
					        ]
					    },
					    {
					        "featureType": "road.local",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "black"
					            }
					        ]
					    },
					    {
					        "featureType": "transit.station.airport",
					        "elementType": "geometry.fill",
					        "stylers": [
					            {
					                "color": "#cfb2db"
					            }
					        ]
					    },
					    {
					        "featureType": "water",
					        "elementType": "geometry",
					        "stylers": [
					            {
					                "color": "#a2daf2"
					            }
					        ]
					    }
					]
		});
		
		marker = new google.maps.Marker({
			map:map,
			position: new google.maps.LatLng({{$item->map->x or 41.0055005}}, {{$item->map->y or 28.7319856}}),
			icon: '/assets/_website/img/pin.png' // null = default icon
		});

		marker2 = new google.maps.Marker({
			map:map2,
			position: new google.maps.LatLng(39.913306, 32.755117),
			icon: '/assets/_website/img/pin.png' // null = default icon
		});

      }

    </script>

    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxgXwDBjnw6KXiowacUb-uH9lOTx53E14&callback=initMap" async defer></script>