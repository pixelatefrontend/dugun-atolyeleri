	<div class="hidden-xs hidden-sm visible-md visible-lg cover-section" style="background-image: url({{ $item->coverPath() }})">
		
		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="premimum-breadcrumb">

						<ul>

							<li><a href="{{ route('website.home') }}">Anasayfa / </a></li>

							<li><a href="{{ route('workshop.all') }}">Atölyeler / </a></li>

							<li>{{ $item->title }}</li>

						</ul>

					</div>

					<div class="detail">

						<figure>
							@if ($item->avatar)
								<img src="{{ $item->avatarPath() }}" alt="{{ $item->title }}">
							@else
								<div class="no-image">
									<span>{{ $item->noImageAvatar() }}</span>
								</div>
							@endif

						</figure>

						<article>

							<div class="workshop-name">
									
								<h3>{{ $item->title }}
									<div class="ribbon-badge"><div class="kurdele-inner"><svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg></div></div>
								</h3>



							</div>

							<div class="counters">
								@if(Auth::check())
									<div class="like-counter {{{ $item->isFavorited() ? 'like-counter-selected' : '' }}}">
								@else
									<div class="like-counter">
								@endif
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use>
									</svg>
									<span id="likes">{{ $item->like }}</span>
								</div>

								<div class="view-counter">
								<svg>
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use>
								</svg>
								<span>{{ $item->view_count }}</span>
								</div>

							</div>

							<div class="buttons">
								@if(Auth::check())
								<a class="like-button {{{ $item->isFavorited() ? 'liked' : '' }}}" href="javascript:void(0)"
								    value="{{ $item->id }}"  
									type="button" 
									onclick="addFavorite(this)">
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
									</svg>
									<span>BEĞEN</span>
								</a>
								@else
									<a class="like-button" href="javascript:void(0)"
									   value="{{ $item->id }}"
									   type="button"
									   onclick="addFavorite(this)">
										<svg>
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
										</svg>
										<span>BEĞEN</span>
									</a>
								@endif

								<a class="add-fav-button" 
									href="#" 
									data-toggle="modal"
									@if(Auth::check())
										@if(count(Auth::user()->lists) > 0)
											id="fav-modal"
											data-target="#fav-modal"
										@else
											id="add-list-modal"
											data-target="#add-list-modal"
										@endif
									@else
										data-target="#register-modal"
									@endif
									>
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/add-list-icon.svg#add-list-icon"></use>
									</svg>
									<span>LİSTEME EKLE</span>
								</a>
								
							</div>

							<div class="single-button">

								<a href="#" id="offer-button" data-toggle="modal" data-target="#offer-modal">
									<span>TEKLİF İSTE</span>
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use>
									</svg>
								</a>
								
							</div>
							</div>
						</article>

					</div>
					
				</div>

			</div>

		</div>

	</div>
	<div class="hidden-xs hidden-sm visible-md visible-lg detail-section">

		<div class="container">

			<div class="row">

				<div class="col-md-6 col-lg-6">

					<div class="workshop-detail">

						<h3>ATÖLYE HAKKINDA</h3>

						<ul class="tags">
							@foreach ($item->categories as $category)
								<li><a href="{{ route('category.single', $category->slug) }}">#{{ str_slug(strtolower($category->title)) }}</a></li>
							@endforeach
						</ul>

						<p> {!! $item->description !!} </p>

						
					</div>

				</div>

				<div class="col-md-6 col-lg-6">

					<div class="workshop-slider-container">
						<div id="premium-detail-slider" class="workshop-slider">
							{{-- Popup içerisine videoyu çekmek için bu kodu kullanacaksın--}}
							@if(count($item->videos) > 0)
								 <div class="workshop-slider-item">
									<div class="video-item-wrp">
										{{--<iframe style="width: 100%; height: 100%;" src="{{ $item->videos->last()->strUrl().'?autoplay=0&showinfo=0&controls=0' }}" frameborder="0" allowfullscreen allowscriptaccess="always"></iframe>--}}
										<img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">
										<div class="overlay">
											<a href="#" class="btn-play" data-video-id="video1"><i class="fa fa-play" aria-hidden="true"></i></a>
										</div>
									</div>
								</div>
							@endif
							@foreach ($item->images as $image)
								<div class="workshop-slider-item">
									<img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}" data-img-id="{{ $image->id }}">
									{{-- BURAYA RESMİN AÇIKLAMASI GELECEK --}}
									{{-- <div class="productDetail">
										{{ $item->title }}
										{{ $item->description }}
										{{ $item->code }}
										{{ $item->price }}
									</div> --}}
								</div>
							@endforeach
						</div>
						<div id="product_details" class="product_info">
							@if(count($item->videos) > 0)
								 
							@endif
							@foreach ($item->images->take(5) as $image)
								<div class="workshop-slider-thumb-item">
									{{ $image->code }}
								</div>
							@endforeach
						</div>
						<div id="premium-detail-slider-thumb" class="workshop-slider-thumb">
							@if(count($item->videos) > 0)
								 <div class="workshop-slider-thumb-item">
									 <img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">
									 <div class="overlay">
										 <span class="play-thumb"><i class="fa fa-play" aria-hidden="true"></i></span>
									 </div>
								</div>
							@endif
							@foreach ($item->images->take(5) as $image)
									<div class="workshop-slider-thumb-item">
										<img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}">
									</div>
							@endforeach
						</div>

						<a class="all-photos-button" href="">Tüm <br>Fotoğraflar</a>
						
					</div>
					
				</div>

			</div>

		</div>

	</div>

	<div class="visible-xs visible-sm hidden-md hidden-lg detail-section">

		<div class="container">

			<div class="row">

				<div class="col-md-6 col-lg-6">

					<div class="workshop-slider-container">

						<div id="premium-detail-slider-mobil" class="workshop-slider">
							@if(count($item->videos) > 0)
								 <div class="workshop-slider-item">
									<div class="video-item-wrp">
										{{--<iframe style="width: 100%; height: 100%;" src="{{ $item->videos->last()->strUrl().'?autoplay=0&showinfo=0&controls=0' }}" frameborder="0" allowfullscreen allowscriptaccess="always"></iframe>--}}
										<img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">
										<div class="overlay">
											<a href="#" class="mobile-btn-play" data-video-id="video1"><i class="fa fa-play" aria-hidden="true"></i></a>

											<div class="mobile-video">
												{{--{{ $item->videos->last()->strUrl().'?autoplay=1&showinfo=0&controls=0' }}--}}
												<iframe class="mobile-iframe" style="width: 100%; height: 100%;" src="{{ $item->videos->last()->strUrl().'?showinfo=0&controls=0' }}" frameborder="0" allowfullscreen allowscriptaccess="always"></iframe>

											</div>
										</div>
									</div>
								</div>
							@endif
							@foreach ($item->images as $image)
								<div class="workshop-slider-item"><img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}"></div>
							@endforeach
						</div>

						<a class="all-photos-button" href="">Tüm <br>Fotoğraflar</a>
						
					</div>
					
				</div>

			</div>

		</div>

	</div>

	<div class="visible-xs visible-sm hidden-md hidden-lg cover-section">
		
		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="premimum-breadcrumb">

						<ul>

						{{-- 	<li><a href="">Anasayfa / </a></li>

							<li><a href="">Atölyeler / </a></li>

							<li><a href="">Davetiye Tasarımı Atölyeler / </a></li>

							<li>selvafilms</li> --}}

						</ul>

					</div>

					<div class="detail">
						<div class="ribbon-badge"><div class="kurdele-inner"><svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg></div></div>

						<figure>
							@if ($item->avatar)
								<img src="{{ $item->avatarPath() }}" alt="{{ $item->title }}">
							@else
								<div class="no-image">
									<span>{{ $item->noImageAvatar() }}</span>
								</div>
							@endif

						</figure>

						<article>

							<div class="workshop-name">
									
								<h3>{{ $item->title }}
								</h3>



							</div>

							<div class="counters">
								@if(Auth::check())
									<div class="like-counter {{{ $item->isFavorited() ? 'like-counter-selected' : '' }}}">
								@else
									<div class="like-counter">
								@endif
									<svg>
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use>
									</svg>
									<span>{{ $item->like }}</span>
								</div>

								<div class="view-counter">
								<svg>
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use>
								</svg>
								<span>{{ $item->view_count }}</span>
								</div>

							</div>

									<div class="buttons">

										@if(Auth::check())
										<a class="like-button {{{ $item->isFavorited() ? 'liked' : '' }}}" href="javascript:void(0)"
										    value="{{ $item->id }}"  
											type="button" 
											onclick="addFavorite(this)">
											<svg>
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
											</svg>
											<span>BEĞEN</span>
										</a>
										@else
											<a class="like-button" href="javascript:void(0)"
											   value="{{ $item->id }}"
											   type="button"
											   onclick="addFavorite(this)">
												<svg>
													<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/heart-empty-icon.svg#heart-empty-icon"></use>
												</svg>
												<span>BEĞEN</span>
											</a>
										@endif

										<a class="add-fav-button"
										   href="#"
										   data-toggle="modal"
										   @if(Auth::check())
										   @if(count(Auth::user()->lists) > 0)
										   id="fav-modal"
										   data-target="#fav-modal"
										   @else
										   id="add-list-modal"
										   data-target="#add-list-modal"
										   @endif
										   @else
										   data-target="#register-modal"
												@endif
										>
											<svg>
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/add-list-icon.svg#add-list-icon"></use>
											</svg>
											<span>LİSTEME EKLE</span>
										</a>

									</div>

									<div class="single-button">

										<a href="#" id="offer-button" data-toggle="modal" data-target="#offer-modal">
											<span>TEKLİF İSTE</span>
											<svg>
												<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use>
											</svg>
										</a>

									</div>
							</div>
							
						</article>

					</div>
					
				</div>

			</div>

		</div>

	</div>

	<div class="workshop-stick">
		<div class="date">
			<i class="fa fa-calendar" aria-hidden="true"></i>
			<input type="text" placeholder="Düğün Tarihi" class="datepicker fixed-date" name="date" readonly="readonly">
		</div>
		<a href="#" id="offer-button" data-toggle="modal" data-target="#offer-modal" class="offer-fixed">
			<span>TEKLİF İSTE</span>
			<svg>
				<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use>
			</svg>
		</a>
	</div>

	<div class="visible-xs visible-sm hidden-md hidden-lg detail-section">

		<div class="container">

			<div class="row">

				<div class="col-md-6 col-lg-6">

					<div class="workshop-detail">

						<h3>ATÖLYE HAKKINDA</h3>

						<ul class="tags">

							@foreach ($item->categories as $category)
								<li><a href="{{ route('category.single', $category->slug) }}">#{{ str_slug(strtolower($category->title)) }}</a></li>
							@endforeach

						</ul>

						<p>{!! $item->description !!}</p>
						
					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="features-secion">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

					<div class="features-container">

						<div class="features-item">

							<strong>KATEGORİ</strong>

							@foreach ($item->categories as $category)
								<span>{{ $category->title }}</span>
							@endforeach

						</div>

						<hr>

						<div class="features-item">

							<strong>MİN FİYAT</strong>

							@if(isset($item->services->price))
								<span>{{ $item->services->price }} TL</span>
							@else
								<span>-</span>
							@endif
							
						</div>

						<hr>

						<div class="features-item">

							<strong>TESLİM SÜRESİ</strong>
							@if(isset($item->services->delivery_time))
								<span>{{ $item->services->delivery_time }} GÜN</span>
							@else
								<span>-</span>
							@endif
							
						</div>

						<hr>

						<div class="features-item">

							<strong>ŞEHİR</strong>

							<span>{{ $item->city }}</span>
							
						</div>
						
					</div>

				</div>

			</div>

		</div>

	</div>

<div class="features-detail-section">

		<div class="container">

			@foreach($item->questions->chunk(3) as $question)
				<div class="row">
					@foreach($question as $content)
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 features-detail-item">
							<h4>{{ $content->question }}</h4>
							<p>{{ $content->reply }}</p>
						</div>
					@endforeach
				</div>
			@endforeach

		</div>

	</div> 
	@include('_website.partials.workshops.map')
	<div class="hidden-xs hidden-sm visible-md visible-lg contact-section">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div id="map-premium" class="google-map"></div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
				<div class="contact-detail">
					<h3>İLETİŞİM</h3>
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">
							<h4>YETKİLİ KİŞİ</h4>
							<span>{{ $item->member }}</span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">
							<h4>TELEFON NUMARASI</h4>
							<span class="phone">{{ $item->phone }}</span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">
							<h4>ADRES</h4>
							<span> {{ $item->address }} </span>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">
							<h4>E- POSTA ADRESİ</h4>
							<span> {{ $item->email }} </span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="visible-xs visible-sm hidden-md hidden-lg contact-section">

		<div class="container">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

				<div class="contact-detail">

					<h3>İLETİŞİM</h3>

					<div class="row">

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

							<h4>YETKİLİ KİŞİ</h4>

							<span>{{ $item->getFullName() }}</span>
							
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

							<h4>TELEFON NUMARASI</h4>

							<span class="phone">{{ $item->phone }}</span>
							
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

							<h4>ADRES</h4>

							<span> {{ $item->address }} </span>
							
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 contact-detail-item">

							<h4>E- POSTA ADRESİ</h4>

							<span> {{ $item->email }} </span>
							
						</div>

					</div>

				</div>

			</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

					<div id="map-premium-mobil" class="google-map"></div>

				</div>

			</div>
			
		</div>
		
	</div>
	@if($item->campaigns->count() > 0)
		<div class="campaigns-section">
			{{-- Code Base --}}
		</div>
	@endif
	<div class="fancybox-container" style="    display: block;">

		<div class="modal-slider-wrp">
			<div class="overlay"></div>
			<div class="modal-slider modal-slider-for">

				@if(count($item->videos) > 0)

					<div class="modal-slider-item item-video">
						<div class="item-inner">
							<button class="modal-slider-close">
								<i class="fa fa-times" aria-hidden="true"></i>
							</button>
							{{--<img src="/uploads/workshops/{{ $item->id }}/{{ $item->videos->last()->cover }}">--}}
							<iframe src="{{ $item->videos->last()->strUrl().'?&showinfo=0&controls=0' }}"
									frameborder="0">

							</iframe>
						</div>
					</div>

				@endif

                @foreach($item->images as $image)
                    <div class="modal-slider-item">
                        <div class="item-inner">
							<button class="modal-slider-close">
								<i class="fa fa-times" aria-hidden="true"></i>
							</button>
							<img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}">
						</div>
                    </div>
                @endforeach




			</div>

			<div class="modal-slider modal-slider-nav">
                @foreach($item->images as $image)
                    <div class="modal-slider-nav-item">
                        <img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}">
                    </div>
                @endforeach
			</div>
		</div>

	</div>

	@push('scripts')
	
		<script>
            $(document).ready(function () {
                $('.modal-slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    fade: true,
                    asNavFor: '.modal-slider-nav',
                    nextArrow: '<button class="slick-next"><i class="fa fa-chevron-right"></i></button>',
                    prevArrow: '<button class="slick-prev"><i class="fa fa-chevron-left"></i></button>',
                    onAfterChange : function() {
                        // alert('test')
                    }
                });

                var videoSrc = $('.item-video iframe').attr('src');

                $('.modal-slider-for').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                    $('.item-video iframe').attr('src', videoSrc);
                });

                $('.modal-slider-for').on('afterChange', function(event, slick, currentSlide, nextSlide){
                    if ($('.modal-slider-item.slick-current').hasClass('item-video')) {
                        var videoTarget =  videoSrc + "&autoplay=1";
                        $(this).find('iframe').attr('src', videoTarget);
					}
                });

                $('.modal-slider-nav').slick({
                    slidesToShow: 7, // 3,
                    slidesToScroll: 7,
                    asNavFor: '.modal-slider-for',
                    dots: false,
                    arrows: false,
                    centerMode: true,
                    focusOnSelect: true,
                    variableWidth: true,
                    // responsive: [
                    //     {
                    //         breakpoint: 1024,
                    //         settings: {
                    //             slidesToShow: 3,
                    //             slidesToScroll: 3,
                    //             infinite: true,
                    //             dots: false
                    //         }
                    //     }
                    //
                    // ]
                });

                $(".modal-slider-for").on("afterChange", function (){
					if($('.slick-active').hasClass('item-video')) {
					    // alert('video')
					}
                });
            });
		</script>
	@endpush

