	<div class="container">
		<div class="row">
			<div class="col-lg-12 breadcrump">
				<span><a href="{{ route('website.home') }}">Anasayfa</a> / <a href="{{ route('workshop.all') }}">Atölyeler</a> / {{ $item->title }}</span>
			</div>
		</div>
	</div>

	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-6 col-md-7 col-lg-6">

				<div class="slider-area">

					<div class="thumbs">

						<ul>

							@foreach($item->images->take(5) as $image)
								<li {{ $loop->first ? 'class=selected' : ''  }} ><img src="/uploads/workshops/{{$item->id}}/{{ $image->filename }}" data-img-id="{{ $image->id }}"></li>
							@endforeach

						</ul>

					</div>

					<div class="slider">

						@foreach ($item->images->take(5) as $image)
							<div class="slider-item"><img src="/uploads/workshops/{{$item->id}}/{{ $image->filename }}"></div>
						@endforeach

					</div>
					<div class="slider-controls">
						<button class="slider-controls-item list-prev">
							<i class="fa fa-chevron-left" aria-hidden="true"></i>
						</button>
						<button class="slider-controls-item list-next">
							<i class="fa fa-chevron-right" aria-hidden="true"></i>
						</button>
					</div>

				</div>

			</div>

			<div class="col-xs-12 col-sm-6 col-md-5 col-lg-6">

				<div class="detail-area">

					<div class="studio-name">
						<figure>
							@if($item->avatar)
								<img src="{{ $item->avatarPath() }}">
							@else
								<div class="no-image">
									<span>{{ strtoupper($item->title[0]) }}</span>
								</div>
							@endif
						</figure>
						<div class="studio-name-inner">

							<h4>{{ $item->title }}</h4>

							<div class="like like-visible">
								<div class="like-inner">
									@if(Auth::check())
									<button
											value="{{ $item->id }}"
											class="no-button like-button {{{ $item->isFavorited() ? 'liked' : '' }}}"
											type="button"
											onclick="addFavorite(this)">
										<svg>
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#empty-heart"></use>
										</svg>
										BEĞEN
									</button>
									@else
									<button
											value="{{ $item->id }}"
											class="no-button like-button"
											type="button"
											onclick="addFavorite(this)">
										<svg>
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#empty-heart"></use>
										</svg>
										BEĞEN
									</button>
									@endif
								</div>
							</div>

							@if(Auth::check())
								<div class="like-counter {{{ $item->isFavorited() ? 'like-counter-selected' : '' }}}">
							@else
								<div class="like-counter">
							@endif

							<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
							<span>{{ $item->like }}</span>
							</div>


							<div class="view-counter"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use></svg><span>{{ $item->view_count }}</span></div>

						</div>

					</div>

					<div class="studio-detail">

						<h5>ATÖLYE HAKKINDA</h5>

						<ul class="tags">
							@foreach ($item->categories as $category)
								<li><a href="{{ route('category.single', $category->slug) }}">#{{ str_slug(strtolower($category->title)) }}</a></li>
							@endforeach
						</ul>

						<p>{!! nl2br(e($item->description)) !!}</p>




						<div class="button-container">

							<div class="like like-hidden">
								<div class="like-inner">
									@if(Auth::check())
									<button
											value="{{ $item->id }}"
											class="no-button like-button {{{ $item->isFavorited() ? 'liked' : '' }}}"
											type="button"
											onclick="addFavorite(this)">
										<svg>
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#empty-heart"></use>
										</svg>
										<span>BEĞEN</span>
									</button>
									@else
									<button
											value="{{ $item->id }}"
											class="no-button like-button"
											type="button"
											onclick="addFavorite(this)">
										<svg>
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#empty-heart"></use>
										</svg>
										<span>BEĞEN</span>
									</button>
									@endif
								</div>
							</div>
							<a href="" id="offer-button" data-toggle="modal" data-target="#offer-modal">

								<div class="button-inner">

									<span>TEKLİF İSTE</span>

									<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg>

								</div>

							</a>

							<a href="#" 
									data-toggle="modal"
									@if(Auth::check())
										@if(count(Auth::user()->lists) > 0)
											id="fav-modal"
											data-target="#fav-modal"
										@else
											id="add-list-modal"
											data-target="#add-list-modal"
										@endif
									@else
										data-target="#register-modal"
									@endif
									>

								<div class="button-inner">

									<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/add-list-icon.svg#add-list-icon"></use></svg>

									<span>LİSTEME EKLE</span>

								</div>

							</a>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>
