<div id="add-list-modal" class="modal fade sign-modal bid-modal fav-modal comment-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>YENİ LİSTE OLUŞTUR</h2>
            </div>
            <div class="modal-body">
				{!! Form::open([ 'url' => route('list.add', $item->slug), 'class' => 'add-list-form', 'id' => 'list-form' ]) !!}
					<div class="row">
						<div class="col-lg-12">
							<label>LİSTE ADI</label>
							<div class="input__group input__phone__group">
								<input type="text" placeholder="(Ör: Düğün Pastası)" name="list_title">
							</div>
						</div>
						<div class="col-lg-12">
							<label>LİSTE AÇIKLAMASI</label>
							<div class="input__group input__phone__group input__textarea__group">
								<textarea name="list_description"></textarea>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="input__group submit__group">
								<button type="submit" class="button button--large button--block button--pink">YENİ LİSTEYİ OLUŞTUR</button>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
</div>