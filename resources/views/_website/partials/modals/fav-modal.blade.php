<div id="fav-modal" class="modal fade sign-modal bid-modal fav-modal comment-modal" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h2>LİSTEME EKLE</h2>

            </div>

            <div class="modal-body">

				<ul class="list">
					@foreach(Auth::user()->lists as $list)
						<li>

							<div class="content">

								<h4>{{ $list->title }}</h4>

								<div class="date">

									<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>

									<span>{{ $list->created_at->diffForHumans() }}</span>

								</div>

								<div class="count">

									<strong id="count_{{ $list->id }}">{{ count($list->workshops) }}</strong>

									<span>ATÖLYE</span>

								</div>

							</div>

							<div class="button-content">

								<button 
									style="border:none;" 
									type="button" class="add-button" 
									onclick="itemAdd(this)" 
									value="{{ $list->id }}">
									EKLE
								</button>

							</div>

						</li>
					@endforeach
				</ul>

            </div>

            <div class="add-list">

				<a href="#" class="add-list-button" id="add-list-button" data-toggle="modal" data-target="#add-list-modal">

					<svg class="new-list-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/new-list-icon.svg#new-list-icon"></use></svg>

					<span>YENİ LİSTE OLUŞTUR</span>

				</a>

			</div>

        </div>

    </div>
</div>