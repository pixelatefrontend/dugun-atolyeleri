<div id="register-modal" class="modal fade sign-modal bid-modal fav-modal comment-modal" role="dialog">
	<div class="modal-dialog">
	    <div class="modal-content">

	        <div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
	            <div class="row">
	            	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="sign-up-wrap">
							<svg class="not-remember-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/not-remember-icon.svg#not-remember-icon"></use></svg>
							<h3>Üye değil misiniz?</h3>
							<p>Üyelerimize özel ayrıcalıklardan faydalanabilmek için üye olun.</p>
							<a href="{{ route('user.register') }}" class="register-button button button--pink">KAYIT OL<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></a>
						</div>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
</div>