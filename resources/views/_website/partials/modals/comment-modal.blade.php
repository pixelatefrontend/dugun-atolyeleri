<div id="comment-modal" class="modal fade sign-modal bid-modal fav-modal comment-modal" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">
        
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>Yorum Yap</h2>
            </div>

            <div class="modal-body">

				{!! Form::open([ 'url' => route('add.comment', $item->slug), 'id' => 'commentForm' ] ) !!}

				<div class="comments-icon-wrap">

					<h4>BU FİRMAYI NASIL BULDUNUZ?</h4>

					<ul>

						<li>
							<label class="custom-check">
								<input type="radio" name="rate" value="1" class="input-radio">
								<span class="check-box">
									<svg class="smile-1 icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/smile-1.svg#smile-1"></use></svg>
									BAŞARISIZ
								</span>
							</label>
						</li>

						<li>
							<label class="custom-check">
								<input type="radio" name="rate" value="2" class="input-radio">
								<span class="check-box">
									<svg class="smile-2 icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/smile-2.svg#smile-2"></use></svg>
									KÖTÜ
								</span>
							</label>
						</li>

						<li>
							<label class="custom-check">
								<input type="radio" name="rate" value="3" class="input-radio">
								<span class="check-box">
									<svg class="smile-3 icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/smile-3.svg#smile-3"></use></svg>
									İDARE EDER
								</span>
							</label>
						</li>

						<li>
							<label class="custom-check">
								<input type="radio" name="rate" value="4" class="input-radio">
								<span class="check-box">
									<svg class="smile-4 icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/smile-4.svg#smile-4"></use></svg>
									İYİ
								</span>
							</label>
						</li>

						<li>
							<label class="custom-check">
								<input type="radio" name="rate" value="5" class="input-radio">
								<span class="check-box">
									<svg class="smile-5 icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/smile-5.svg#smile-5"></use></svg>
									BAYILDIM
								</span>
							</label>
						</li>

					</ul>

				</div>
				<div class="comments-add-wrap">
					<div class="row">
						<div class="col-md-6">
							{{--<input type="text" name="female"  placeholder="Gelin">--}}

							<div class="input__group input-container input-control">
								<input type="text" name="female"  placeholder="Gelin">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="col-md-6">
							{{--<input type="text" name="male"  placeholder="Damat">--}}
							<div class="input__group input-container input-control">
								<input type="text" name="male"  placeholder="Damat">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="col-md-12">
							{{--<input type="text" name="title"  placeholder="Yorum Başlığı">--}}

							<div class="input__group input-container input-control">
								<input type="text" name="title"  placeholder="Yorum Başlığı">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="input__group input-container input-control textarea-control">
								<textarea name="content" placeholder="Mesajınızı buraya giriniz."></textarea>
								<div class="error-message">Burayı boş bırakamazsınız.</div>
							</div>
						</div>

						<div class="foot col-md-12">

							<span>Mesajınız sistemimize iletildikten sonra yüklenecektir</span>

							<button type="submit" class="no-button form-control-button" id="commentButton">GÖNDER </button>
							<div class="loading" id="loadStatus">
								<img src="/assets/_website/img/load.gif" alt="">
							</div>

						</div>

					</div>

						{{--<textarea name="content" placeholder="Mesajınızı buraya giriniz."></textarea>--}}



				</div>
				{!! Form::close() !!}

            </div>

        </div>

    </div>

</div>