<div id="comment-success-modal" class="modal fade sign-modal bid-modal bid-success-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>YORUM GÖNDERİLDİ</h2>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="success-icon">
                        <span><svg class="success-icon-pink"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/success-icon.svg#success-icon"></use></svg></span>
                    </div>
                    <h4>Yorumunuz incelendikten sonra yayınlanacaktır!</h4>
                    <span style="font-size: 17px; color: #999eab">Teşekkür Ederiz</span>
				</div>
            </div>
        </div>
    </div>
</div>