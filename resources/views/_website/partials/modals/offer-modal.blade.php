<div id="offer-modal" class="modal fade sign-modal bid-modal" role="dialog">

	<div class="modal-dialog">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">&times;</button>

				<h2>TEKLİF AL</h2>

			</div>

			<form method="POST" id="offerForm">

				<div class="modal-body">

					<div class="row">
						{{--<div class="col-lg-12">--}}
							{{--<h4>KİŞİSEL BİLGİLERİNİZ</h4>--}}
						{{--</div>--}}
						<div class="col-lg-6">
							<div class="input__group input-container input-control">
                                <input type="text" {{ Auth::check() ? 'value='.Auth::user()->name : ''  }} class="input__control" placeholder="Adınız" name="firstname">

                                <div class="error-message">Adınız hatalı.</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="input__group input-container input-control">
                                <input type="text" {{ Auth::check() ? 'value='.Auth::user()->lname : ''  }} class="input__control" placeholder="Soyadınız" name="lastname">

                                <div class="error-message">Soyadınız hatalı.</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="input__group input__phone__group input-container input-control phone-control">
                                @if(Auth::check())
                                    <input type="text" value="{{ Auth::user()->phone }}" class="input__control" placeholder="Telefon Numaranız" name="phone">
                                @else
                                    <input type="text" class="input__control phone-mask" placeholder="(+90) XXX XXX XX XX" name="phone">
                                @endif
								<div class="error-message">Telefon hatalı.</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="input__group input-container input-control mail-control">
                                <input type="text" {{ Auth::check() ? 'value='.Auth::user()->email : ''  }} class="input__control" placeholder="E-mail Adresiniz" name="email">
								<div class="error-message">Mail hatalı.</div>
							</div>
						</div>
						<style>
                    		label#prod > input[type='radio']{ /* HIDE RADIO */
							  visibility: hidden; /* Makes input not-clickable */
							  position: absolute; /* Remove input from document flow */
							}
							label#prod > input[type='radio'] + img{ /* IMAGE STYLES */
							  cursor:pointer;
							  border:2px solid #ff89ba;
							  border-radius: 3px;
							  width: 150px;

							}
							label#prod > input[type='radio']:checked + img{ /* (RADIO CHECKED) IMAGE STYLES */
							  border: 2px solid #f00;
							}
                        </style>
                        <div class="col-lg-12">
							<label for="product">Hangi Ürüne Teklif Gönderilsin?</label>
							<div class="product-slide" style=" overflow: auto; white-space: nowrap;">
								@foreach($item->images as $image)
									<label id="prod">
										<input type="radio" name="product" value="{{ $image->id }}" />
  										<img src="/uploads/workshops/{{ $item->id }}/{{ $image->filename }}">
									</label>
								@endforeach
							</div>
						</div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                            <div class="input__group input__date__group input-container input-control">

							<span class="pos-absolute pad-20 width-full">

								<i class="fa fa-calendar" aria-hidden="true"></i>

                                @if(Auth::check())
                                    <input type="text" readonly="readonly" value="{{ Auth::user()->date->format('d/m/Y') }}" placeholder="{{ Auth::user()->date->format('d/m/Y') }}" name="date" class="datepicker date-picker pad-5 width-full">
                                @else
                                    <input type="text" placeholder="Düğün Tarihi" class="datepicker date-picker pad-5 width-full" name="date">
                                @endif
                                <div class="error-message">Tarih hatalı.</div>

                                <input type="date" placeholder="Düğün Tarihi" class="pad-5 width-full hidden">

							</span>



                                <div class="input__group input-control textarea-control">
                                    <textarea name="description" class="input__control" style="height:135px;padding-top:65px;" placeholder="Talepleriniz ile ilgili bilgileri paylaşın"></textarea>

                                    <div class="error-message">Bu alanı boş bırakamazsınız.</div>
                                </div>
                            </div>

                        </div>

						<hr>

                        <div class="row offer-modal-bottom-txt">

                            <div class="col-xs-12">

                                <p>* Teklifinizi İlgili Atölyeye ileteceğiz, lütfen teklifinizin takibini yapmayı unutmayınız.</p>

                                <p>Geri Dönüşler <strong>e-mail adresiniz</strong> ve <strong>Telefon numaranız</strong> üzerinden yapılacaktır.</p>

                            </div>

                        </div>

						<div class="col-lg-12">
                            <div class="loading" id="loadStatus">
                                <img src="/assets/_website/img/load.gif" alt="">
                            </div>
                            <input type="submit" name="submit_teklif" id="offerSend" class="button button--large button--pink form-control-button" value="Teklif İste">
                            <a href="" class="hide" id="bid-success-button" data-toggle="modal" data-target="#bid-success-modal">TEKLİF İSTE2<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></a>
						</div>
					</div>

				</div>
			</form>

		</div>

	</div>

</div>