<div id="workshop-register-success" class="modal fade sign-modal bid-modal bid-success-modal" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h2>KAYDINIZ ALINDI</h2>

            </div>

            <div class="modal-body">

				<div class="row">

					<div class="success-icon">

                        <span><svg class="success-icon-pink"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/success-icon.svg#success-icon"></use></svg></span>

                    </div>

                    <h4>Kaydınız Başarı ile Gerçekleşmiştir</h4>

                    <span><span class="pink-color">*</span> Kaydınız editörlerimiz tarafından incelenip tekrar tarafanıza dönüş yapılacaktır.</span>

                    <span>Geri Dönüşler e-mail adresiniz ve Telefon numaranız üzerinden yapılacaktır.</span>

				</div>

            </div>

        </div>

    </div>

</div>