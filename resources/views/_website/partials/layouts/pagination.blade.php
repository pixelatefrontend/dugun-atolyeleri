@if ($paginator->lastPage() > 1)
<div class="pagination-container">
		<div class="container">
			<div class="pagination">
				<div class="prev {{ ($paginator->currentPage() == 1) ? 'disabled' : '' }} " >
					<a href="{{ $paginator->url(1) }}">
						<svg class="icon">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-prev"></use>
						</svg>
					</a>
				</div>
				<div class="numbers">
					<ul class="list list--vertical">
						@for ($i = 1; $i <= $paginator->lastPage(); $i++)
					        <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
					            <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
					        </li>
					    @endfor
					</ul>
				</div>
				<div class="next {{ ($paginator->currentPage() == $paginator->lastPage()) ? 'disabled' : '' }}">
					<a href="{{ $paginator->url($paginator->currentPage()+1) }}">
						<svg class="icon">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-next"></use>
						</svg>
					</a>
				</div>
			</div>
		</div>
	</div>
@endif