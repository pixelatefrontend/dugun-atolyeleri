<div class="comments-section" {{ $item->type == 'normal' ? 'style=background-color:#fafafa;margin-top:40px;padding-top:25px;' : '' }}>
		<div class="container">
			<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						@if($item->comments->count() > 0)
						<div class="title">
							<h3>YORUMLAR</h3>
							<span>( {{ count($item->comments) }} YORUM )</span>
						</div>
						<ul class="comment-list">
							@foreach($item->comments as $comment)
							<li>
								<div class="profil-area">
									<figure>
										<!-- <img src="/assets/_website/img/avatar.jpg"> -->
										{{-- <span>{{ $comment->getShortName() }}</span> --}}
										<span class="comment-pp-name" style="text-transform: uppercase;" data-female="{{ $comment->female }}" data-male="{{ $comment->male }}"></span>
										{{--<span class="comment-pp-name" style="text-transform: uppercase;" data-female="22" data-male="aa"></span>--}}

									</figure>
									<strong>{{ $comment->getFullName() }}</strong>
								</div>
								<div class="comment-area">
									<strong>{{ $comment->title }}</strong>
									<p>{{ $comment->content }}</p>
									<div class="comment-bottom">
										<span>{{ $comment->getCreatedAt() }}</span>
									</div>

								</div>
							</li>
							@endforeach
						</ul>
						<div class="buttons">
							<a href="#" id="comment-button" data-toggle="modal" data-target="#comment-modal" class="add-comment"> <i class="fa fa-plus" aria-hidden="true"></i> YORUM YAP</a>
						</div>
						@else
						{{-- Eğer yorum yoksa buradan başlayacak --}}

							<div class="title">
								<h3>YORUMLAR</h3>
							</div>
							<h4>Buraya henüz yorum yapılmadı. İlk Yorumu Sen Yap!</h4>

							<br>
						<div class="buttons no-comment">
							<a href="#" id="comment-button" data-toggle="modal" data-target="#comment-modal" class="add-comment"> <i class="fa fa-plus" aria-hidden="true"></i> YORUM YAP</a>
						</div>


						@endif
					</div>
			</div>
		</div>
	</div>


