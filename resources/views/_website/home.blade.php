@extends('_website.layouts.master')
@section('title', 'Düğün Atölyeleri | Özel Günleriniz İçin Butik Tasarım Atölyeleri')
@section('content')
@include('_website.partials.modals.register-modal')
@include('_website.partials.modals.workshop-register-success')
<section class="base-page home-page">
    <div class="home-container">
        <div class="home-page-slider">
            @foreach($sliders as $slider)
                <div class="home-page-slider-item">
                    <figure><img src="{{ $slider->image }}"></figure>
                    {{--<article>--}}
                    {{--<div class="container article-container">--}}
                    {{--<div class="article-inner">--}}
                    {{--<div class="article-section">--}}
                    {{--<h4>{{ $slider->title }}</h4>--}}
                    {{--<p>{{ $slider->description }}</p>--}}
                    {{--<a class="button button-full" href="{{ $slider->url }}">{{ $slider->button_text }}</a>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</article>--}}
                </div>
            @endforeach
        </div>

		<div class="home-container-content">
			<div class="home-page-artc">
				<article>
					<div class="container article-container">
						<div class="article-inner">
							<div class="article-section">
								<h4>{{ $slider->title }}</h4>
								<p>{{ $slider->description }}</p>
							</div>
						</div>
					</div>
				</article>
			</div>
			<div class="search-overlay"></div>
			<div class="home-page-search">
				<div class="search-section">


					<div class="search-section-inner">


						{!! Form::open([ 'url' => route('website.home'), 'method' => 'GET' ]) !!}

						<input id="main-search-atf" type="text" name="q" placeholder="Davetiye, Nikah Şekeri, Pasta, Düğün Fotoğrafı...">
						<svg class="search-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#search-icon"></use></svg>

						<button class="no-button search-button" type="submit">

							<span>ARA</span>

							<svg class="right-long-arrow-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/right-long-arrow.svg#right-long-arrow"></use></svg>
							<svg class="search-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#search-icon"></use></svg>

						</button>
						{!! Form::close() !!}


					</div>

				</div>
			</div>
		</div>

    </div>


    <div class="category-home">
        <div class="row-dg big-nav-container">

            <div class="dg-container">

                <nav class="big-nav">
                    <ul>
                        @foreach($all_categories as $category)
                            <li class="{{ setActive('kategori/'.$category->slug, 'selected') }}">
                                <a href="{{route('category.single', $category->slug)}}">
                                    <svg class="icon {{$category->icon}}">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/files/svg/icons.svg#{{ $category->icon }}"></use>
                                    </svg>
                                    <span>{{ $category->title }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>

            </div>

        </div>
    </div>

	<div class="about-home">
		<div class="container">
			<div class="row">
				{{--<div class="about-wrp">--}}
				{{--<div class="about-wrp-title">--}}
				{{--NEDEN <strong> DÜĞÜNATÖLYELERİ.COM</strong>?--}}
				{{--</div>--}}

				{{--<div class="about-wrp-groups">--}}
				{{--<div class="about-wrp-item">--}}
				{{--<div class="about-wrp-item-title">--}}
				{{--<div class="about-wrp-item-icon">--}}
				{{--<svg class="icon icon-ring"><use xlink:href="/assets/_website/img/icons.svg#icon-ring"></use></svg>--}}
				{{--</div>--}}
				{{--<div class="about-wrp-item-txt">ÇİFTLER İÇİN <strong>NE İFADE EDİYORUZ?</strong></div>--}}
				{{--</div>--}}

				{{--<div class="about-wrp-item-content">Aradığınız her şeyi bulabileceğiniz keyifli bir platform. Instagram'da--}}
				{{--sayfalar dolusu, yüzlerce profili gezmek yerine, hepsine tek adresten ulaşabilirsiniz.. Instagram'da--}}
				{{--öne çıkan, onlarca çifte özel ürün tasarlamış yüzlerce düğün atölyesi burada!</div>--}}

				{{--<a href="#" class="about-wrp-item-btn">TÜM ATÖLYELER</a>--}}
				{{--</div>--}}

				{{--<div class="about-wrp-item">--}}
				{{--<div class="about-wrp-item-title">--}}
				{{--<div class="about-wrp-item-icon">--}}
				{{--<svg class="icon icon-ring"><use xlink:href="/assets/_website/img/icons.svg#icon-ring"></use></svg>--}}
				{{--</div>--}}
				{{--<div class="about-wrp-item-txt">ÇİFTLER İÇİN <strong>NE İFADE EDİYORUZ?</strong></div>--}}
				{{--</div>--}}

				{{--<div class="about-wrp-item-content">Aradığınız her şeyi bulabileceğiniz keyifli bir platform. Instagram'da--}}
				{{--sayfalar dolusu, yüzlerce profili gezmek yerine, hepsine tek adresten ulaşabilirsiniz.. Instagram'da--}}
				{{--öne çıkan, onlarca çifte özel ürün tasarlamış yüzlerce düğün atölyesi burada!</div>--}}

				{{--<a href="#" class="about-wrp-item-btn">TÜM ATÖLYELER</a>--}}
				{{--</div>--}}
				{{--</div>--}}


				{{--</div>--}}

				<div class="about-wrp">

					<div class="about-wrp-title">
						NEDEN <strong> DÜĞÜNATÖLYELERİ.COM</strong>?
					</div>

					<div class="about-foot-inner">
						<div class="left about-wrp-item">

							<div class="about-foot-inner-title">
								<svg class="wedding-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/wedding-icon.svg#wedding-icon"></use></svg>
								<h4>ÇİFTLER İÇİN <br><span>NE İFADE EDİYORUZ?</span></h4>
							</div>


							<p>
								Aradığınız her şeyi bulabileceğiniz keyifli bir platform. Instagram'da sayfalar dolusu, yüzlerce profili gezmek yerine, hepsine tek adresten ulaşabilirsiniz.. Instagram'da öne çıkan, onlarca çifte özel ürün tasarlamış yüzlerce düğün atölyesi burada!
							</p>
							<div class="form-group">
								<a href="{{ route('workshop.all') }}" class="btn all-workshop-button">
									TÜM ATÖLYELER
								</a>
							</div>
						</div>
						{{--<div class="middle">--}}
						{{--<hr>--}}
						{{--</div>--}}
						<div class="right about-wrp-item">
							<div class="about-foot-inner-title">
								<svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>
								<h4>ATÖLYELER İÇİN <br><span>NE İFADE EDİYORUZ?</span></h4>
							</div>

							<p>
								Çiftlerin hayallerini gerçeğe dönüştürme gücüne ve yeteneğine sahipsiniz. Ürettiğiniz özgün tasarımlar ve hizmetlerle Instagram hesabınızdan bugüne kadar onlarca çifte ulaştınız. düğünatölyeleri.com ile artık daha fazla çifte ulaşabilirsiniz.
							</p>
							<div class="form-group">
								<a href="{{ route('workshop.register') }}" class="btn all-workshop-button">
									ATÖLYE KAYIT
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>




	{{--<div class="home-page-week-design-list">--}}
		{{--<div class="container">--}}
			{{--<div class="row week-design-container">--}}
				{{--<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">--}}
					{{--<div class="title-container">--}}
						{{--<h3>Öne Çıkan Atölyeler</h3>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{--<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">--}}
					{{--<div class="week-design-list">--}}
						{{--<div class="week-design-list-inner">--}}
							{{--<div class="row">--}}
								{{--<div class="week-design-list-slider">--}}
									{{--<div class="item-slider">--}}
										{{--@foreach($popularWorkshops->chunk(3) as $items)--}}
											{{--@foreach($items as $workshop)--}}
												{{--<div class="{{{ $workshop->doping == 1 ? 'col-sm-12 col-md-8 col-lg-8' : ' col-md-4 col-lg-4 col-sm-4 col-xs-4 single' }}}">--}}
													{{--<div class="week-design-list-item">--}}
														{{--@if($workshop->type != 'normal')--}}
															{{--<div class="ribbon-badge">--}}
																{{--<div class="kurdele-inner">--}}
																	{{--<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/icons.svg#icon-kurdale"></use></svg>--}}
																{{--</div>--}}
															{{--</div>--}}
															{{--<div class="info-badge">--}}
																{{--<div class="info-badge-item">--}}
																	{{--<div class="info-badge-item-inner">--}}
																		{{--<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/icons.svg#icon-video"></use></svg>--}}
																		{{--<span>{{ count($workshop->videos) }}</span>--}}
																	{{--</div>--}}
																{{--</div>--}}
																{{--<div class="info-badge-item">--}}
																	{{--<div class="info-badge-item-inner">--}}
																		{{--<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="assets/_website/img/icons.svg#icon-camera"></use></svg>--}}
																		{{--<span>{{ count($workshop->images) }}</span>--}}
																	{{--</div>--}}
																{{--</div>--}}
															{{--</div>--}}
														{{--@endif--}}
														{{--<figure class="main-figure">--}}
															{{--<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">--}}
																{{--<div class="click-detail-inner">--}}
																	{{--<span>ATÖLYEYE GİT</span>--}}
																{{--</div>--}}
															{{--</a>--}}
															{{--@if($workshop->latestImage)--}}
																{{--<img src="           rename('uploads/workshops/'.$item->slug, 'uploads/workshops/'.$item->id);
/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->filename }}">--}}
															{{--@else--}}
																{{--<img src="assets/_website/img/week-design-image-1.jpg" alt="">--}}
															{{--@endif--}}
														{{--</figure>--}}
														{{--<div class="footer">--}}
															{{--<figure class="avatar-figure">--}}
																{{--<div class="hover-svg">--}}
																	{{--<div class="hover-svg-inner">--}}
																		{{--<button--}}
																				{{--value="{{ $workshop->id }}"--}}
																				{{--class="no-button"--}}
																				{{--type="button"--}}
																				{{--onclick="addFavorite(this)">--}}
																			{{--<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>--}}
																		{{--</button>--}}
																	{{--</div>--}}
																{{--</div>--}}
																{{--@if($workshop->avatar)--}}
																	{{--<img src="{{ $workshop->avatarPath() }}">--}}
																{{--@else--}}
																	{{--<div class="no-image">--}}
																		{{--<span>{{ strtoupper($workshop->title[0]) }}</span>--}}
																	{{--</div>--}}
																{{--@endif--}}
															{{--</figure>--}}
															{{--<div class="footer-inner">--}}
																{{--<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->getShortTitle() }}</h4></a>--}}
																{{--@if(Auth::check())--}}
																	{{--<a href="javascript:void(0)" id="favorite_{{$workshop->id}}" class={{{ $workshop->isFavorited() ? 'increase' : '' }}}>--}}
																		{{--@else--}}
																			{{--<a href="javascript:void(0)" id="favorite_{{$workshop->id}}">--}}
																				{{--@endif--}}
																				{{--<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-like"></use></svg>--}}
																				{{--<span>{{ $workshop->like }}</span>--}}
																			{{--</a>--}}

																	{{--</a>--}}
															{{--</div>--}}
														{{--</div>--}}
													{{--</div>--}}
												{{--</div>--}}
											{{--@endforeach--}}
										{{--@endforeach--}}
									{{--</div>--}}
								{{--</div>--}}
							{{--</div>--}}
						{{--</div>--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}

	{{--</div>--}}
 <div class="home-page-design-list">
		<div class="container">
			<div class="row design-list design-list-grid" id="articles">
				@foreach($workshops as $workshop)
					<div class="col-xs-6 col-sm-6 {{{ $workshop->doping == 1 ? 'col-md-6 col-lg-6' : 'col-md-3 col-sm-4 col-lg-3' }}} design-list-grid-item" id="article">
					<div class="week-design-list-item">
						@if($workshop->type != "normal")
						<div class="ribbon-badge">
							<div class="kurdele-inner">
								<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-kurdale"></use></svg>
							</div>
						</div>
						<div class="info-badge">
							<div class="info-badge-item">
								<div class="info-badge-item-inner">
									<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-video"></use></svg>
									<span>{{ count($workshop->videos) }}</span>
								</div>
							</div>
							<div class="info-badge-item">
								<div class="info-badge-item-inner">
									<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-camera"></use></svg>
									<span>{{ count($workshop->images) }}</span>
								</div>
							</div>
						</div>
						@endif
						<figure class="main-figure">
							<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">
								<div class="click-detail-inner">
									<span>ATÖLYEYE GİT</span>
								</div>
							</a>
							@if($workshop->first_picture)
								<img src="{{ $workshop->first_picture }}" alt="">
							@elseif($workshop->latestImage)
								<img src="/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->getFileName() }}" alt="">
							@else
								<img src="/assets/_website/img/week-design-image-1.jpg" alt="">
							@endif
						</figure>
						<div class="footer">
							<figure class="avatar-figure">
								<div class="hover-svg">
									<div class="hover-svg-inner">
										<button 
											value="{{ $workshop->id }}" 
											class="no-button" 
											type="button" 
											onclick="addFavorite(this)">
										<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-heart"></use></svg></button>
									</div>
								</div>
								@if($workshop->avatar)
									<img src="{{ $workshop->avatarPath() }}">
								@else
									<div class="no-image">
										<span class="workshop-pp-name" style="text-transform: uppercase;" data-name="{{ $workshop->title }}"></span>
									</div>
								@endif
							</figure>
							<div class="footer-inner">
								<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->getShortTitle() }}</h4></a>
								@if(Auth::check())
									<a href="javascript:void(0)" id="favorite_{{$workshop->id}}" class={{{ $workshop->isFavorited() ? 'increase' : '' }}}>
								@else
									<a href="javascript:void(0)" id="favorite_{{$workshop->id}}">
								@endif
									<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-heart"></use></svg>
									<span>{{ $workshop->like }}</span>
								</a>
									</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				{{ $workshops->links() }}
			</div>
		</div>
		<!-- ".load-more-button-mode" Class'ı eklenince, hover'lı oluyor. -->

		<div class="row">
			<div class="col-md-2 col-md-offset-5">
					<div class="form-group">
						<button class="btn view-more-button">DAHA FAZLA GÖSTER</button>
					</div>
			</div>
		</div>
	
		<a href="#" class="load-more">
			<div class="load-dot-container">
				<div class="load-dot-1"></div>
				<div class="load-dot-2"></div>
				<div class="load-dot-3"></div>
			</div>
		</a>

	</div>

</section>

<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="hidden">
	<defs>
		<symbol id="icon-video" viewBox="0 0 21 16">
			<path d="M91.36,3.37L88,5.3V2a2,2,0,0,0-2-2H73a2,2,0,0,0-2,2V14a2,2,0,0,0,2,2H86a2,2,0,0,0,2-2V10.7l3.31,1.9A0.46,0.46,0,0,0,92,12.2V3.74A0.43,0.43,0,0,0,91.36,3.37ZM73,14V2H86V14H73ZM90,9.6L88,8.5V7.6l2-1.1V9.6Z" transform="translate(-71 0)"/>
		</symbol>

		<symbol id="icon-kurdale" viewBox="0 0 34.69 34.84">
			<path class="cls-1" d="M490.53,155.36a1.2,1.2,0,0,0-.07-0.41,0.74,0.74,0,0,0-.77-0.55,37.62,37.62,0,0,1-12-1.32,29.27,29.27,0,0,0,6.11-2.94,2.75,2.75,0,0,0,1.33-2.34,1.66,1.66,0,0,0-.06-0.53,3.68,3.68,0,0,0-2.52-2.85l-0.77-.26a2.21,2.21,0,0,1-1.33-1.21l-0.83-1.79a3.64,3.64,0,0,0-1.59-1.72,3.13,3.13,0,0,0-4.06.8c-1.74,2.13-3.46,7.25-4,8.84a4.72,4.72,0,0,0-2.1,1.2,4.29,4.29,0,0,0-1.24,2.17c-1.66.52-6.75,2.25-8.8,4a3.12,3.12,0,0,0-.8,4.06,3.63,3.63,0,0,0,1.72,1.59l1.79,0.83a2.21,2.21,0,0,1,1.21,1.33L462,165a3.68,3.68,0,0,0,2.85,2.51,2.87,2.87,0,0,0,2.87-1.26,28.56,28.56,0,0,0,2.88-6,37.65,37.65,0,0,1,1.31,12,0.76,0.76,0,0,0,.55.76,0.82,0.82,0,0,0,.9-0.31l0.09-.13c2-2.93,3.06-4.65,3.63-5.6,0.62,0.27,2.43,1.06,5.27,2.45a0.81,0.81,0,0,0,.91-0.15,0.7,0.7,0,0,0,.23-0.54v-0.32h-0.06c-0.07-.12-0.27-0.47-0.49-0.86-0.69-1.23-1.92-3.41-3.17-5.31,2.45,1.61,5.64,3.42,6.15,3.65h0a0.71,0.71,0,0,0,.86-0.08,0.81,0.81,0,0,0,.15-0.91l-1.28-2.75c-0.54-1.14-.93-2-1.17-2.53,2.2-1.33,5.61-3.59,5.75-3.74A0.93,0.93,0,0,0,490.53,155.36Zm-15.19-3.79a9.6,9.6,0,0,0-.78-1.35,4.68,4.68,0,0,0-2.86-1.3c1.37-4,2.56-6.57,3.51-7.74a1.59,1.59,0,0,1,2.14-.42,2.51,2.51,0,0,1,.92,1l0.83,1.79a3.62,3.62,0,0,0,2.25,2.06l0.76,0.25a2.26,2.26,0,0,1,1.52,1.68v0.26a1.43,1.43,0,0,1-.72,1.12c-1.52,1.17-5.16,2.53-7.27,3.27,0-.09-0.09-0.19-0.13-0.29S475.4,151.68,475.35,151.57Zm-4.69-1a3.12,3.12,0,0,1,2.7.86,3.2,3.2,0,0,1,.34.41,3.1,3.1,0,0,1,.52,1.48,0.62,0.62,0,0,0,.06.23s0,0.06,0,.09a3.1,3.1,0,0,1-.77,2l-0.08,0a0.67,0.67,0,0,0-.19.23,3.12,3.12,0,0,1-2.56.78,3.41,3.41,0,0,1-1.69-.87,3.12,3.12,0,0,1-.87-2.69,0.62,0.62,0,0,0,0-.32,3.66,3.66,0,0,1,.79-1.42,3,3,0,0,1,1.53-.85A0.45,0.45,0,0,0,470.66,150.55Zm3,6.91a31.62,31.62,0,0,1,4.33,5c1.07,1.57,2.17,3.44,2.88,4.71-2.33-1.1-3.8-1.75-3.82-1.76a0.75,0.75,0,0,0-1,.31c-0.59,1.12-1.68,2.79-2.52,4.07a42.09,42.09,0,0,0-1.94-11.62A4.61,4.61,0,0,0,473.69,157.46Zm2.07-3.29a39.29,39.29,0,0,0,11.41,1.89l-0.17.11c-1.32.84-2.82,1.79-3.89,2.42a0.82,0.82,0,0,0-.3,1h0c0.09,0.26.68,1.54,1.75,3.82a48.3,48.3,0,0,1-4.71-2.88,28.78,28.78,0,0,1-4.92-4.23A4.4,4.4,0,0,0,475.76,154.17Zm-9.13.48a4.52,4.52,0,0,0,1.23,2.34,4.33,4.33,0,0,0,1.91,1.15,30.64,30.64,0,0,1-3.26,7.26,1.28,1.28,0,0,1-1.35.66,2.15,2.15,0,0,1-1.69-1.51l-0.26-.83a3.82,3.82,0,0,0-2-2.25l-1.8-.84a1.91,1.91,0,0,1-1-.91,1.67,1.67,0,0,1-.24-0.9,1.37,1.37,0,0,1,.72-1.18c1.17-1,3.77-2.13,7.72-3.5A4.52,4.52,0,0,0,466.62,154.65Z" transform="translate(-456.21 -138.64)"/><path class="cls-1" d="M472.7,173.48a1.63,1.63,0,0,1-.31,0,1.14,1.14,0,0,1-.85-1.13,36.66,36.66,0,0,0-1-10.81,24.57,24.57,0,0,1-2.5,5,3.27,3.27,0,0,1-3.24,1.41,4,4,0,0,1-3.14-2.76l-0.26-.77a1.84,1.84,0,0,0-1-1.11l-1.8-.84a4,4,0,0,1-1.9-1.77,3.48,3.48,0,0,1,.89-4.51c2.19-1.83,7.5-3.58,8.73-4a4.64,4.64,0,0,1,1.29-2.15,5.05,5.05,0,0,1,2.06-1.23c0.56-1.74,2.25-6.65,4-8.77a3.49,3.49,0,0,1,4.53-.89,4,4,0,0,1,1.75,1.9l0.83,1.79a1.83,1.83,0,0,0,1.1,1l0.78,0.26a4.06,4.06,0,0,1,2.77,3.15,1.81,1.81,0,0,1,.07.59,3.13,3.13,0,0,1-1.49,2.65,25,25,0,0,1-5.1,2.56,36.53,36.53,0,0,0,10.79,1h0a1.11,1.11,0,0,1,1.16.85,1.42,1.42,0,0,1,.07.48,1.28,1.28,0,0,1-.4.87c-0.2.2-3.48,2.36-5.54,3.62,0.24,0.53.59,1.27,1,2.22l0.1,0.22,1.18,2.53a1.18,1.18,0,0,1-.23,1.33,1.09,1.09,0,0,1-1.33.13c-0.54-.26-2.67-1.46-4.73-2.74,0.93,1.51,1.77,3,2.27,3.9l0.43,0.75a0.36,0.36,0,0,1,.17.29v0.32a1.08,1.08,0,0,1-.34.8,1.17,1.17,0,0,1-1.35.22c-2.45-1.19-4.13-2-5-2.31-0.6,1-1.66,2.66-3.47,5.34l-0.09.13A1.17,1.17,0,0,1,472.7,173.48Zm-2.1-13.57h0a0.38,0.38,0,0,1,.34.28,38,38,0,0,1,1.32,12.15,0.38,0.38,0,0,0,.27.37,0.44,0.44,0,0,0,.51-0.17l0.08-.12c2-2.94,3.06-4.66,3.62-5.58a0.37,0.37,0,0,1,.47-0.15c0.65,0.28,2.47,1.08,5.28,2.46a0.44,0.44,0,0,0,.48-0.08,0.33,0.33,0,0,0,.12-0.27v-0.11h0c-0.07-.11-0.25-0.43-0.46-0.81-0.7-1.25-1.94-3.45-3.19-5.36A0.38,0.38,0,0,1,480,162c2.39,1.57,5.59,3.39,6.1,3.62l0.08,0a0.34,0.34,0,0,0,.36,0,0.43,0.43,0,0,0,.08-0.48l-1.18-2.53-0.1-.22c-0.54-1.14-.93-2-1.17-2.54a0.38,0.38,0,0,1,.15-0.47c2.21-1.33,5.47-3.51,5.69-3.69a0.56,0.56,0,0,0,.17-0.32,0.82,0.82,0,0,0,0-.29,0.38,0.38,0,0,0-.42-0.29,37.94,37.94,0,0,1-12.14-1.33,0.38,0.38,0,0,1,0-.71,29.7,29.7,0,0,0,6-2.89,2.38,2.38,0,0,0,1.18-2,1.34,1.34,0,0,0,0-.41,3.35,3.35,0,0,0-2.29-2.62l-0.77-.25a2.59,2.59,0,0,1-1.55-1.42l-0.83-1.79a3.25,3.25,0,0,0-1.42-1.54,2.74,2.74,0,0,0-3.61.71c-1.71,2.09-3.41,7.2-3.9,8.71a0.37,0.37,0,0,1-.26.25,4.36,4.36,0,0,0-1.94,1.1,3.94,3.94,0,0,0-1.14,2,0.38,0.38,0,0,1-.25.27c-2.37.73-6.86,2.38-8.67,3.9a2.76,2.76,0,0,0-.71,3.59,3.25,3.25,0,0,0,1.55,1.43l1.8,0.83a2.6,2.6,0,0,1,1.41,1.57l0.25,0.76a3.3,3.3,0,0,0,2.55,2.26,2.49,2.49,0,0,0,2.52-1.13,29.15,29.15,0,0,0,2.82-5.86A0.37,0.37,0,0,1,470.61,159.91Zm3,10.26h-0.1a0.38,0.38,0,0,1-.27-0.35,42,42,0,0,0-1.92-11.5,0.38,0.38,0,0,1,.31-0.49,4.21,4.21,0,0,0,1.89-.66,0.37,0.37,0,0,1,.46,0,32.21,32.21,0,0,1,4.38,5.07c1.34,2,2.56,4.13,2.9,4.74a0.37,0.37,0,0,1-.49.52c-2.33-1.1-3.79-1.75-3.8-1.75a0.38,0.38,0,0,0-.52.16c-0.61,1.16-1.74,2.88-2.53,4.08A0.38,0.38,0,0,1,473.57,170.18Zm-1.45-11.69a42.61,42.61,0,0,1,1.78,10.12c0.67-1,1.41-2.2,1.86-3.06a1.13,1.13,0,0,1,1.46-.49s1,0.45,2.77,1.26c-0.55-1-1.39-2.34-2.28-3.65a31.72,31.72,0,0,0-4.06-4.75A4.9,4.9,0,0,1,472.12,158.49Zm-6.72,8a1.55,1.55,0,0,1-.34,0,2.52,2.52,0,0,1-2-1.77l-0.26-.83a3.44,3.44,0,0,0-1.85-2l-1.8-.84a2.25,2.25,0,0,1-1.2-1.08,2,2,0,0,1-.28-1.06,1.74,1.74,0,0,1,.9-1.5c1.16-1,3.8-2.15,7.8-3.54a0.38,0.38,0,0,1,.5.31,4.19,4.19,0,0,0,.08.47v0a4.13,4.13,0,0,0,1.13,2.14,4,4,0,0,0,1.75,1.05,0.37,0.37,0,0,1,.25.48,30.93,30.93,0,0,1-3.32,7.37A1.82,1.82,0,0,1,465.4,166.45Zm0.83-11.82c-3.68,1.29-6.1,2.4-7.19,3.29a1,1,0,0,0-.58.89,1.29,1.29,0,0,0,.18.7,1.55,1.55,0,0,0,.83.75l1.83,0.85a4.19,4.19,0,0,1,2.25,2.47l0.26,0.84a1.77,1.77,0,0,0,1.39,1.25,0.91,0.91,0,0,0,1-.52,29.12,29.12,0,0,0,3.08-6.79,4.78,4.78,0,0,1-1.7-1.12,4.9,4.9,0,0,1-1.33-2.52Zm18.33,9.13a0.38,0.38,0,0,1-.18,0,47.37,47.37,0,0,1-4.75-2.9,29.18,29.18,0,0,1-5-4.28,0.38,0.38,0,0,1,0-.48,4,4,0,0,0,.75-1.93,0.37,0.37,0,0,1,.17-0.27,0.37,0.37,0,0,1,.32,0,39.21,39.21,0,0,0,11.3,1.87,0.37,0.37,0,0,1,.2.69l-0.17.11c-1.32.84-2.82,1.79-3.91,2.43a0.43,0.43,0,0,0-.15.48l0,0.05c0,0.14.33,0.82,1.74,3.78A0.38,0.38,0,0,1,484.56,163.76Zm-9.15-7.52a28.54,28.54,0,0,0,4.65,4,40.79,40.79,0,0,0,3.66,2.29c-1-2.12-1.2-2.6-1.25-2.75v0a1.19,1.19,0,0,1,.42-1.42c0.88-.51,2-1.21,3.06-1.89a39.64,39.64,0,0,1-9.88-1.74A4.68,4.68,0,0,1,475.41,156.24Zm-4.27.88a3.5,3.5,0,0,1-.51,0,3.82,3.82,0,0,1-1.91-1,3.5,3.5,0,0,1-1-3,0.32,0.32,0,0,0,0-.18,0.38,0.38,0,0,1,0-.18,4.06,4.06,0,0,1,.87-1.57,3.43,3.43,0,0,1,1.73-1h0.21a3.5,3.5,0,0,1,3,1,3.66,3.66,0,0,1,.38.46,3.48,3.48,0,0,1,.59,1.67,0.34,0.34,0,0,0,0,.13,0.39,0.39,0,0,1,0,.12s0,0.08,0,.1a3.5,3.5,0,0,1-.86,2.27,0.61,0.61,0,0,1-.15.11,0.35,0.35,0,0,0-.07.1,0.38,0.38,0,0,1-.08.11A3.49,3.49,0,0,1,471.15,157.13Zm-2.63-4.26a1,1,0,0,1,0,.41,2.73,2.73,0,0,0,.77,2.31,3.06,3.06,0,0,0,1.51.77,2.73,2.73,0,0,0,2.18-.65,1.05,1.05,0,0,1,.26-0.29l0.06,0a2.7,2.7,0,0,0,.64-1.74,0.83,0.83,0,0,1-.06-0.32,2.72,2.72,0,0,0-.46-1.27,2.67,2.67,0,0,0-2.65-1.12,0.78,0.78,0,0,1-.25,0,2.7,2.7,0,0,0-1.29.73A3.29,3.29,0,0,0,468.52,152.86Zm7.11-.29a0.38,0.38,0,0,1-.35-0.24l-0.06-.13-0.07-.14c0-.1-0.08-0.18-0.12-0.27l0-.05a9.09,9.09,0,0,0-.75-1.3,4.33,4.33,0,0,0-2.59-1.15,0.37,0.37,0,0,1-.32-0.5c1.39-4,2.59-6.65,3.57-7.86a2,2,0,0,1,2.6-.52,2.93,2.93,0,0,1,1.09,1.22l0.82,1.78a3.26,3.26,0,0,0,2,1.86l0.76,0.25a2.63,2.63,0,0,1,1.77,2v0.31a1.79,1.79,0,0,1-.87,1.42c-1.57,1.21-5.29,2.6-7.38,3.32Zm0.07-1.12c0,0.09.09,0.18,0.13,0.27,2.08-.73,5.43-2,6.85-3.1a1.05,1.05,0,0,0,.57-0.82v-0.26a1.9,1.9,0,0,0-1.28-1.33l-0.75-.25a4,4,0,0,1-2.48-2.27l-0.82-1.77a2.13,2.13,0,0,0-.78-0.88,1.2,1.2,0,0,0-1.64.35c-0.89,1.09-2,3.51-3.29,7.2a5,5,0,0,1,2.62,1.35,9.4,9.4,0,0,1,.86,1.45l-0.34.16Z" transform="translate(-456.21 -138.64)"/>
		</symbol>

		<symbol id="icon-camera" viewBox="0 0 65 55">
			<path d="M74.22,77.5H25.78a8.31,8.31,0,0,1-8.28-8.31V38.81a8.31,8.31,0,0,1,8.28-8.31h0.06l2.09-4.18a6.87,6.87,0,0,1,6.18-3.82H47.89a6.87,6.87,0,0,1,6.18,3.82l2.09,4.18H74.22a8.31,8.31,0,0,1,8.28,8.31V69.19A8.31,8.31,0,0,1,74.22,77.5Zm-48.44-40a1.28,1.28,0,0,0-1.28,1.31V69.19a1.28,1.28,0,0,0,1.28,1.31H74.22a1.28,1.28,0,0,0,1.28-1.31V38.81a1.28,1.28,0,0,0-1.28-1.31H51.84l-4-8-13.65,0-4,8H25.78ZM42,66.55a13.3,13.3,0,1,1,13.3-13.3A13.32,13.32,0,0,1,42,66.55Zm0-19.61a6.3,6.3,0,1,0,6.3,6.3A6.31,6.31,0,0,0,42,46.95Z" transform="translate(-17.5 -22.5)"/>
		</symbol>

		<symbol id="icon-kurdale" viewBox="0 0 34.69 34.84">
			<path class="cls-1" d="M490.53,155.36a1.2,1.2,0,0,0-.07-0.41,0.74,0.74,0,0,0-.77-0.55,37.62,37.62,0,0,1-12-1.32,29.27,29.27,0,0,0,6.11-2.94,2.75,2.75,0,0,0,1.33-2.34,1.66,1.66,0,0,0-.06-0.53,3.68,3.68,0,0,0-2.52-2.85l-0.77-.26a2.21,2.21,0,0,1-1.33-1.21l-0.83-1.79a3.64,3.64,0,0,0-1.59-1.72,3.13,3.13,0,0,0-4.06.8c-1.74,2.13-3.46,7.25-4,8.84a4.72,4.72,0,0,0-2.1,1.2,4.29,4.29,0,0,0-1.24,2.17c-1.66.52-6.75,2.25-8.8,4a3.12,3.12,0,0,0-.8,4.06,3.63,3.63,0,0,0,1.72,1.59l1.79,0.83a2.21,2.21,0,0,1,1.21,1.33L462,165a3.68,3.68,0,0,0,2.85,2.51,2.87,2.87,0,0,0,2.87-1.26,28.56,28.56,0,0,0,2.88-6,37.65,37.65,0,0,1,1.31,12,0.76,0.76,0,0,0,.55.76,0.82,0.82,0,0,0,.9-0.31l0.09-.13c2-2.93,3.06-4.65,3.63-5.6,0.62,0.27,2.43,1.06,5.27,2.45a0.81,0.81,0,0,0,.91-0.15,0.7,0.7,0,0,0,.23-0.54v-0.32h-0.06c-0.07-.12-0.27-0.47-0.49-0.86-0.69-1.23-1.92-3.41-3.17-5.31,2.45,1.61,5.64,3.42,6.15,3.65h0a0.71,0.71,0,0,0,.86-0.08,0.81,0.81,0,0,0,.15-0.91l-1.28-2.75c-0.54-1.14-.93-2-1.17-2.53,2.2-1.33,5.61-3.59,5.75-3.74A0.93,0.93,0,0,0,490.53,155.36Zm-15.19-3.79a9.6,9.6,0,0,0-.78-1.35,4.68,4.68,0,0,0-2.86-1.3c1.37-4,2.56-6.57,3.51-7.74a1.59,1.59,0,0,1,2.14-.42,2.51,2.51,0,0,1,.92,1l0.83,1.79a3.62,3.62,0,0,0,2.25,2.06l0.76,0.25a2.26,2.26,0,0,1,1.52,1.68v0.26a1.43,1.43,0,0,1-.72,1.12c-1.52,1.17-5.16,2.53-7.27,3.27,0-.09-0.09-0.19-0.13-0.29S475.4,151.68,475.35,151.57Zm-4.69-1a3.12,3.12,0,0,1,2.7.86,3.2,3.2,0,0,1,.34.41,3.1,3.1,0,0,1,.52,1.48,0.62,0.62,0,0,0,.06.23s0,0.06,0,.09a3.1,3.1,0,0,1-.77,2l-0.08,0a0.67,0.67,0,0,0-.19.23,3.12,3.12,0,0,1-2.56.78,3.41,3.41,0,0,1-1.69-.87,3.12,3.12,0,0,1-.87-2.69,0.62,0.62,0,0,0,0-.32,3.66,3.66,0,0,1,.79-1.42,3,3,0,0,1,1.53-.85A0.45,0.45,0,0,0,470.66,150.55Zm3,6.91a31.62,31.62,0,0,1,4.33,5c1.07,1.57,2.17,3.44,2.88,4.71-2.33-1.1-3.8-1.75-3.82-1.76a0.75,0.75,0,0,0-1,.31c-0.59,1.12-1.68,2.79-2.52,4.07a42.09,42.09,0,0,0-1.94-11.62A4.61,4.61,0,0,0,473.69,157.46Zm2.07-3.29a39.29,39.29,0,0,0,11.41,1.89l-0.17.11c-1.32.84-2.82,1.79-3.89,2.42a0.82,0.82,0,0,0-.3,1h0c0.09,0.26.68,1.54,1.75,3.82a48.3,48.3,0,0,1-4.71-2.88,28.78,28.78,0,0,1-4.92-4.23A4.4,4.4,0,0,0,475.76,154.17Zm-9.13.48a4.52,4.52,0,0,0,1.23,2.34,4.33,4.33,0,0,0,1.91,1.15,30.64,30.64,0,0,1-3.26,7.26,1.28,1.28,0,0,1-1.35.66,2.15,2.15,0,0,1-1.69-1.51l-0.26-.83a3.82,3.82,0,0,0-2-2.25l-1.8-.84a1.91,1.91,0,0,1-1-.91,1.67,1.67,0,0,1-.24-0.9,1.37,1.37,0,0,1,.72-1.18c1.17-1,3.77-2.13,7.72-3.5A4.52,4.52,0,0,0,466.62,154.65Z" transform="translate(-456.21 -138.64)"/><path class="cls-1" d="M472.7,173.48a1.63,1.63,0,0,1-.31,0,1.14,1.14,0,0,1-.85-1.13,36.66,36.66,0,0,0-1-10.81,24.57,24.57,0,0,1-2.5,5,3.27,3.27,0,0,1-3.24,1.41,4,4,0,0,1-3.14-2.76l-0.26-.77a1.84,1.84,0,0,0-1-1.11l-1.8-.84a4,4,0,0,1-1.9-1.77,3.48,3.48,0,0,1,.89-4.51c2.19-1.83,7.5-3.58,8.73-4a4.64,4.64,0,0,1,1.29-2.15,5.05,5.05,0,0,1,2.06-1.23c0.56-1.74,2.25-6.65,4-8.77a3.49,3.49,0,0,1,4.53-.89,4,4,0,0,1,1.75,1.9l0.83,1.79a1.83,1.83,0,0,0,1.1,1l0.78,0.26a4.06,4.06,0,0,1,2.77,3.15,1.81,1.81,0,0,1,.07.59,3.13,3.13,0,0,1-1.49,2.65,25,25,0,0,1-5.1,2.56,36.53,36.53,0,0,0,10.79,1h0a1.11,1.11,0,0,1,1.16.85,1.42,1.42,0,0,1,.07.48,1.28,1.28,0,0,1-.4.87c-0.2.2-3.48,2.36-5.54,3.62,0.24,0.53.59,1.27,1,2.22l0.1,0.22,1.18,2.53a1.18,1.18,0,0,1-.23,1.33,1.09,1.09,0,0,1-1.33.13c-0.54-.26-2.67-1.46-4.73-2.74,0.93,1.51,1.77,3,2.27,3.9l0.43,0.75a0.36,0.36,0,0,1,.17.29v0.32a1.08,1.08,0,0,1-.34.8,1.17,1.17,0,0,1-1.35.22c-2.45-1.19-4.13-2-5-2.31-0.6,1-1.66,2.66-3.47,5.34l-0.09.13A1.17,1.17,0,0,1,472.7,173.48Zm-2.1-13.57h0a0.38,0.38,0,0,1,.34.28,38,38,0,0,1,1.32,12.15,0.38,0.38,0,0,0,.27.37,0.44,0.44,0,0,0,.51-0.17l0.08-.12c2-2.94,3.06-4.66,3.62-5.58a0.37,0.37,0,0,1,.47-0.15c0.65,0.28,2.47,1.08,5.28,2.46a0.44,0.44,0,0,0,.48-0.08,0.33,0.33,0,0,0,.12-0.27v-0.11h0c-0.07-.11-0.25-0.43-0.46-0.81-0.7-1.25-1.94-3.45-3.19-5.36A0.38,0.38,0,0,1,480,162c2.39,1.57,5.59,3.39,6.1,3.62l0.08,0a0.34,0.34,0,0,0,.36,0,0.43,0.43,0,0,0,.08-0.48l-1.18-2.53-0.1-.22c-0.54-1.14-.93-2-1.17-2.54a0.38,0.38,0,0,1,.15-0.47c2.21-1.33,5.47-3.51,5.69-3.69a0.56,0.56,0,0,0,.17-0.32,0.82,0.82,0,0,0,0-.29,0.38,0.38,0,0,0-.42-0.29,37.94,37.94,0,0,1-12.14-1.33,0.38,0.38,0,0,1,0-.71,29.7,29.7,0,0,0,6-2.89,2.38,2.38,0,0,0,1.18-2,1.34,1.34,0,0,0,0-.41,3.35,3.35,0,0,0-2.29-2.62l-0.77-.25a2.59,2.59,0,0,1-1.55-1.42l-0.83-1.79a3.25,3.25,0,0,0-1.42-1.54,2.74,2.74,0,0,0-3.61.71c-1.71,2.09-3.41,7.2-3.9,8.71a0.37,0.37,0,0,1-.26.25,4.36,4.36,0,0,0-1.94,1.1,3.94,3.94,0,0,0-1.14,2,0.38,0.38,0,0,1-.25.27c-2.37.73-6.86,2.38-8.67,3.9a2.76,2.76,0,0,0-.71,3.59,3.25,3.25,0,0,0,1.55,1.43l1.8,0.83a2.6,2.6,0,0,1,1.41,1.57l0.25,0.76a3.3,3.3,0,0,0,2.55,2.26,2.49,2.49,0,0,0,2.52-1.13,29.15,29.15,0,0,0,2.82-5.86A0.37,0.37,0,0,1,470.61,159.91Zm3,10.26h-0.1a0.38,0.38,0,0,1-.27-0.35,42,42,0,0,0-1.92-11.5,0.38,0.38,0,0,1,.31-0.49,4.21,4.21,0,0,0,1.89-.66,0.37,0.37,0,0,1,.46,0,32.21,32.21,0,0,1,4.38,5.07c1.34,2,2.56,4.13,2.9,4.74a0.37,0.37,0,0,1-.49.52c-2.33-1.1-3.79-1.75-3.8-1.75a0.38,0.38,0,0,0-.52.16c-0.61,1.16-1.74,2.88-2.53,4.08A0.38,0.38,0,0,1,473.57,170.18Zm-1.45-11.69a42.61,42.61,0,0,1,1.78,10.12c0.67-1,1.41-2.2,1.86-3.06a1.13,1.13,0,0,1,1.46-.49s1,0.45,2.77,1.26c-0.55-1-1.39-2.34-2.28-3.65a31.72,31.72,0,0,0-4.06-4.75A4.9,4.9,0,0,1,472.12,158.49Zm-6.72,8a1.55,1.55,0,0,1-.34,0,2.52,2.52,0,0,1-2-1.77l-0.26-.83a3.44,3.44,0,0,0-1.85-2l-1.8-.84a2.25,2.25,0,0,1-1.2-1.08,2,2,0,0,1-.28-1.06,1.74,1.74,0,0,1,.9-1.5c1.16-1,3.8-2.15,7.8-3.54a0.38,0.38,0,0,1,.5.31,4.19,4.19,0,0,0,.08.47v0a4.13,4.13,0,0,0,1.13,2.14,4,4,0,0,0,1.75,1.05,0.37,0.37,0,0,1,.25.48,30.93,30.93,0,0,1-3.32,7.37A1.82,1.82,0,0,1,465.4,166.45Zm0.83-11.82c-3.68,1.29-6.1,2.4-7.19,3.29a1,1,0,0,0-.58.89,1.29,1.29,0,0,0,.18.7,1.55,1.55,0,0,0,.83.75l1.83,0.85a4.19,4.19,0,0,1,2.25,2.47l0.26,0.84a1.77,1.77,0,0,0,1.39,1.25,0.91,0.91,0,0,0,1-.52,29.12,29.12,0,0,0,3.08-6.79,4.78,4.78,0,0,1-1.7-1.12,4.9,4.9,0,0,1-1.33-2.52Zm18.33,9.13a0.38,0.38,0,0,1-.18,0,47.37,47.37,0,0,1-4.75-2.9,29.18,29.18,0,0,1-5-4.28,0.38,0.38,0,0,1,0-.48,4,4,0,0,0,.75-1.93,0.37,0.37,0,0,1,.17-0.27,0.37,0.37,0,0,1,.32,0,39.21,39.21,0,0,0,11.3,1.87,0.37,0.37,0,0,1,.2.69l-0.17.11c-1.32.84-2.82,1.79-3.91,2.43a0.43,0.43,0,0,0-.15.48l0,0.05c0,0.14.33,0.82,1.74,3.78A0.38,0.38,0,0,1,484.56,163.76Zm-9.15-7.52a28.54,28.54,0,0,0,4.65,4,40.79,40.79,0,0,0,3.66,2.29c-1-2.12-1.2-2.6-1.25-2.75v0a1.19,1.19,0,0,1,.42-1.42c0.88-.51,2-1.21,3.06-1.89a39.64,39.64,0,0,1-9.88-1.74A4.68,4.68,0,0,1,475.41,156.24Zm-4.27.88a3.5,3.5,0,0,1-.51,0,3.82,3.82,0,0,1-1.91-1,3.5,3.5,0,0,1-1-3,0.32,0.32,0,0,0,0-.18,0.38,0.38,0,0,1,0-.18,4.06,4.06,0,0,1,.87-1.57,3.43,3.43,0,0,1,1.73-1h0.21a3.5,3.5,0,0,1,3,1,3.66,3.66,0,0,1,.38.46,3.48,3.48,0,0,1,.59,1.67,0.34,0.34,0,0,0,0,.13,0.39,0.39,0,0,1,0,.12s0,0.08,0,.1a3.5,3.5,0,0,1-.86,2.27,0.61,0.61,0,0,1-.15.11,0.35,0.35,0,0,0-.07.1,0.38,0.38,0,0,1-.08.11A3.49,3.49,0,0,1,471.15,157.13Zm-2.63-4.26a1,1,0,0,1,0,.41,2.73,2.73,0,0,0,.77,2.31,3.06,3.06,0,0,0,1.51.77,2.73,2.73,0,0,0,2.18-.65,1.05,1.05,0,0,1,.26-0.29l0.06,0a2.7,2.7,0,0,0,.64-1.74,0.83,0.83,0,0,1-.06-0.32,2.72,2.72,0,0,0-.46-1.27,2.67,2.67,0,0,0-2.65-1.12,0.78,0.78,0,0,1-.25,0,2.7,2.7,0,0,0-1.29.73A3.29,3.29,0,0,0,468.52,152.86Zm7.11-.29a0.38,0.38,0,0,1-.35-0.24l-0.06-.13-0.07-.14c0-.1-0.08-0.18-0.12-0.27l0-.05a9.09,9.09,0,0,0-.75-1.3,4.33,4.33,0,0,0-2.59-1.15,0.37,0.37,0,0,1-.32-0.5c1.39-4,2.59-6.65,3.57-7.86a2,2,0,0,1,2.6-.52,2.93,2.93,0,0,1,1.09,1.22l0.82,1.78a3.26,3.26,0,0,0,2,1.86l0.76,0.25a2.63,2.63,0,0,1,1.77,2v0.31a1.79,1.79,0,0,1-.87,1.42c-1.57,1.21-5.29,2.6-7.38,3.32Zm0.07-1.12c0,0.09.09,0.18,0.13,0.27,2.08-.73,5.43-2,6.85-3.1a1.05,1.05,0,0,0,.57-0.82v-0.26a1.9,1.9,0,0,0-1.28-1.33l-0.75-.25a4,4,0,0,1-2.48-2.27l-0.82-1.77a2.13,2.13,0,0,0-.78-0.88,1.2,1.2,0,0,0-1.64.35c-0.89,1.09-2,3.51-3.29,7.2a5,5,0,0,1,2.62,1.35,9.4,9.4,0,0,1,.86,1.45l-0.34.16Z" transform="translate(-456.21 -138.64)"/>
		</symbol>

		<symbol id="icon-heart" viewBox="0 0 100 87.41">
			<path d="M70.62,0A26,26,0,0,0,50,9.56,26,26,0,0,0,29.38,0C11.81,0,0,16.28,0,31.49,0,53.62,42.52,83.29,47.37,86.6a4.68,4.68,0,0,0,5.27,0c4.85-3.31,47.37-33,47.37-55.11C100,16.28,88.19,0,70.62,0" transform="translate(0 0)"/>
		</symbol>

		<symbol id="icon-like" viewBox="0 0 11.75 9.28">
			<path id="icon_like" class="cls-1" d="M634.88,1518.52c-1.522-3.21-5.85-2.46-5.878,1.27-0.016,2.05,2.061,2.82,3.443,3.64a6.256,6.256,0,0,1,2.444,2.34,6.94,6.94,0,0,1,2.432-2.36c1.357-.87,3.459-1.59,3.443-3.64C640.735,1516.03,636.333,1515.44,634.88,1518.52Z" transform="translate(-629 -1516.5)" />
		</symbol>
	</defs>
</svg>
@endsection
@push('scripts')
	@if(Session::has('workshop_register'))
		<script>
			$('#workshop-register-success').modal('show');
		</script>
	@endif
	{!! Html::script('/assets/_website/js/infinite.min.js') !!}
	<script>
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>
<script type="text/javascript">
	// Beğenme işlemleri
	function addFavorite(item) {
			var w_id = $(item).attr('value');
			var data = { id: w_id };
			@if(Auth::check())
				
				$.ajax({
					type: 'POST',
					url: "{{ route('favorite.add') }}",
					data: data,
					success: function(response){

						if(response.action == "add"){
							$('#favorite_'+w_id).addClass('increase');
							$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html())+1);
						}else{
							$('#favorite_'+w_id).removeClass('increase');
							$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html())-1);
						}
					}
				});
			@else
				if($('#favorite_'+w_id).hasClass('increase')) {
					$('#favorite_'+w_id).removeClass('increase');
					$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html())-1);
					$.ajax({
						type: 'POST',
						url: "{{ route('favorite.destroy') }}",
						data: data,
					});
				}else{
					$('#favorite_'+w_id).addClass('increase');
					$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html())+1);
					$.ajax({
						type: 'POST',
						url: "{{ route('favorite.add') }}",
						data: data,
					});
				}
			@endif
	}


	// Infinity Scroll **
	var $container = $('#articles').infiniteScroll({
	  path: '.pagination li.active + li a',
	  append: '#article',
	  hideNav: 'ul.pagination',
	  status: '.load-more',
	  button: '.view-more-button',
	  history: false,
      onInit: function() {
          this.on( 'append', function() {
              $('.workshop-pp-name').each(function() {
                  var dataText = $(this).data('name');
                  var arr = dataText.toString().substr(0,1);
                  $(this).text(arr)
              });
          })
      }
	});

	var $viewMoreButton = $('.view-more-button');
	var infScroll = $container.data('infiniteScroll');
	$container.on( 'load.infiniteScroll', onPageLoad );
	function onPageLoad() {
	  if ( infScroll.loadCount == 4 ) {
	    $container.infiniteScroll( 'option', {
	      loadOnScroll: false,
	    });
	    $viewMoreButton.show();
	    $container.off( 'load.infiniteScroll', onPageLoad );
	  }
	}
</script>
@endpush