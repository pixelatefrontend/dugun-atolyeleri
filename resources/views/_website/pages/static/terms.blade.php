@extends('_website.layouts.master')
@section('content')
@section('title', 'Kullanıcı Sözleşmesi | Düğün Atölyeleri')

	<section class="base-page sign-in-up-page sign-modal contact-page sss-page">
		<div class="sss-container">
			<div class="container">
				<div class="row">
					<div class="col-lg-10 col-lg-offset-1">
	                	<h2>Kullanıcı Sözleşmesi</h2><br><br>
	                	<p>İşbu Üyelik Sözleşmesi (kısaca “Sözleşme”), Düğünatölyeleri.com (PİXELATE REKLAM ve İLETİŞİM HİZ. TİC. LTD. ŞTİ.) ile www.dugunatolyeleri.com alan adlı web sitesine (İnternet Sitesi) üye olma talebinde bulunan kullanıcı arasında, kullanıcının Site’ye üye olması amacıyla ve Sözleşme’nin elektronik ortamda kullanıcı tarafından onaylanması anında düzenlenmiştir.<br><br>
Üye, Site’ye üye olarak, işbu Sözleşme’nin tamamını okuduğunu, içeriğini bütünü ile anladığını ve tüm hükümlerini onayladığını kabul, beyan ve taahhüt eder.<br><br>
Üye, ayrıca, Site’de erişilebilen ve işbu Sözleşme’nin ayrılmaz bir parçasını oluşturan Gizlilik Politikası’nın tamamını okuduğunu, içeriğini bütünü ile anladığını ve tüm hükümlerini onayladığını kabul, beyan ve taahhüt eder.<br><br>
<strong>1. TANIMLAR</strong><br><br>
Düğün Atölyeleri: Düğünatölyeleri.com (PİXELATE REKLAM ve İLETİŞİM HİZ. TİC. LTD. ŞTİ.)<br><br>
Site: PİXELATE REKLAM ve İLETİŞİM HİZ. TİC. LTD. ŞTİ.’ye ait www.dugunatolyeleri.com alan adından ve bu alan adına bağlı alt alan adlarından oluşan web sitesini,<br><br>
Üye(ler): Site'ye Düğün Atölyeleri tarafından belirlenen yöntemlerle kayıt olan ve Site'de sunulan Düğün Atölyeleri Hizmetleri’nden işbu Sözleşme’de belirtilen koşullar dahilinde yararlanan gerçek veya tüzel kişiyi,<br><br>
Düğün Atölyeleri Hizmetleri: Satıcılar tarafından tedarik edilecek olan malların Site’de yayınlanması/ilan edilmesi; <br><br>
Sisteme Erişim Araçları: Üye'nin, yalnızca Üye tarafından bilinen, Site’ye erişimini sağlayan, kullanıcı adı, parola ve/veya şifre v.b. bilgilerini,<br><br>
Gizlilik Politikası: Site’de erişilebilen ve işbu Sözleşme’nin ayrılmaz bir parçasını oluşturan Gizlilik Politikasını,
ifade etmektedir.<br><br>
<strong>2. SÖZLEŞME’NİN KONUSU VE KAPSAMI</strong><br><br>
İşbu Sözleşme’nin konusu, Site'de mevcut bulunan ve ileride sunulacak tüm Düğün Atölyeleri Hizmetleri’nin, bunlardan yararlanma şartlarının ve tarafların hak ve yükümlülüklerinin tespitidir.<br><br>
İşbu Sözleşme’nin kapsamı, Sözleşme ile Site içerisinde yer alan kullanıma, üyeliğe ve Düğün Atölyeleri Hizmetleri’ne ilişkin olarak, Düğün Atölyeleri tarafından yapılmış olan bilcümle uyarı, yazı ve açıklama gibi beyanlardır. Üye, işbu Sözleşme hükümlerini kabul etmekle, Site içerisinde yer alan kullanıma, üyeliğe ve Düğün Atölyeleri Hizmetleri’ne ilişkin olarak, Düğün Atölyeleri tarafından açıklanan her türlü beyanı da kabul etmiş olmaktadır. Üye, bahsi geçen beyanlarda belirtilen her türlü hususa uygun olarak davranacağını kabul, beyan ve taahhüt eder.<br><br>

<strong>3. ÜYE HAK VE YÜKÜMLÜLÜKLERİ</strong><br><br>
<strong>3.1. Genel Olarak</strong><br>
3.1.1. Üye, üyelik prosedürlerini yerine getirirken, Site'deki Düğün Atölyeleri Hizmetleri'nden faydalanırken ve Site'deki Düğün Atölyeleri Hizmetleri ile ilgili herhangi bir işlemi yerine getirirken, işbu Sözleşme içerisinde yer alan tüm şartlara, Site'nin ilgili yerlerinde belirtilen kurallara ve yürürlükteki tüm mevzuata uygun hareket edeceğini; yukarıda belirtilen tüm şart ve kuralları anladığını ve onayladığını kabul, beyan ve taahhüt eder.<br><br>
3.1.2. Üye, Düğün Atölyeleri’nin yürürlükteki emredici mevzuat hükümleri gereğince resmi makamlara açıklama yapmakla yükümlü olduğu durumlar içerisinde, resmi makamlarca usulü dairesinde bu bilgilerin talep edilmesi halinde, Üyeler'e ait gizli/özel/ticari bilgileri resmi makamlara açıklamaya yetkili olacağını ve bu sebeple kendisinden her ne nam altında olursa olsun tazminat talep edilemeyeceğini kabul, beyan ve taahhüt eder.<br><br>
3.1.3. Üyeler'in, Site aracılığıyla Düğün Atölyeleri tarafından sunulan Düğün Atölyeleri Hizmetleri'nden yararlanabilmek amacıyla kullandıkları Sisteme Erişim Araçları'nın (kullanıcı ismi, şifre v.b.) güvenliği, saklanması, üçüncü kişilerin bilgisinden uzak tutulması, kullanılması ile ilgili sorumluluk tamamen kendine aittir.<br><br>
3.1.4. Düğün Atölyeleri'nin sunduğu Düğün Atölyeleri Hizmetleri’nden yararlananlar ve Site'yi kullananlar, yalnızca hukuka uygun amaçlarla Site üzerinde işlem yapabilirler. Kullanıcıların, Site dahilinde yaptığı her işlem ve eylemdeki hukuki ve cezai sorumluluk kendilerine aittir. Her Üye ve Marka, Site dahilinde bulunan, Düğün Atölyeleri ve/veya başka bir üçüncü şahsın ayni veya şahsi haklarına, malvarlığına tecavüz teşkil edecek nitelikteki resimleri, metinleri, görsel ve işitsel imgeleri, video kliplerini, dosyaları, veritabanlarını, katalogları ve listeleri çoğaltmayacağını, kopyalamayacağını, dağıtmayacağını, işlemeyeceğini; gerek bu eylemleri ile gerekse de başka yollarla, Düğün Atölyeleri ile doğrudan ve/veya dolaylı olarak rekabete girmeyeceğini kabul, beyan ve taahhüt etmektedir. Üyeler'in Site üzerinde, işbu Sözleşme hükümlerine ve hukuka aykırı olarak gerçekleştirdikleri faaliyetleri nedeniyle üçüncü kişilerin uğradıkları veya uğrayabilecekleri zararlardan dolayı Düğün Atölyeleri doğrudan ve/veya dolaylı olarak hiçbir şekilde sorumlu tutulamaz.<br><br>
3.1.5. Düğün Atölyeleri'nin izni dışında, Üyeler'in, Site üzerinden yapacakları herhangi bir işlemde, başka bir web sitesine, bir web sitesinin içeriğine veya ağ üzerindeki herhangi bir veriye link vermeleri yasaktır.<br><br>
<strong>3.2. Site Üzerinden Mal Alımı İçin İletişim ile İlgili Olarak</strong><br>
3.2.1. Alıcı ve Satıcı, Site üzerinden sadece iletişim kurar, satış site üzerinden gerçekleşmez. Bu kapsamda, Alıcı ve Satıcı arasındaki herhangi bir ticari uyuşmazlık, hiçbir şekilde Düğün Atölyeleri'ni bağlamaz, Düğün Atölyeleri, bu uyuşmazlık ve sorunlardan sorumlu tutulamaz. <br><br>

<strong>4. DÜĞÜN ATÖLYELERİ HAK VE YÜKÜMLÜLÜKLERİ</strong><br><br>
4.1. Düğün Atölyeleri, Site’de sunulan Düğün Atölyeleri Hizmetleri'ni ve içerikleri her zaman değiştirebilme hakkını saklı tutmaktadır. Düğün Atölyeleri, bu hakkını hiçbir bildirimde bulunmadan ve önel vermeden kullanabilir.<br><br>
4.2. Site içerisinde, yalnızca Düğün Atölyeleri tarafından, referans kolaylığı veya çeşitli sebepler nedeniyle, Düğün Atölyeleri kontrolünde olmayan bir takım web sitelerine veya içeriklere linkler verilebilir. Bu linkler, bahsi geçen web sitesini veya içerik sahibini desteklemek amacıyla olmayıp, web sitesi veya içerikte yer alan bilgilere yönelik herhangi bir türde bir beyan veya garanti niteliği de taşımamaktadır. Site üzerindeki linkler vasıtasıyla erişilen portallar, web siteleri, dosyalar ve içerikler, bu linkler vasıtasıyla erişilen portallar veya web sitelerinden sunulan hizmetler veya mallar veya bunların içerikleri hakkında, Düğün Atölyeleri'nin herhangi bir sorumluluğu yoktur.<br><br>
4.3. Teknik sorunlar sebebiyle Site'ye erişimde yaşanan kesintilerden dolayı, Üye'nin yaşayacağı sorunlardan Düğün Atölyeleri sorumlu tutulamaz.<br><br>
4.4. Düğün Atölyeleri, Site içerisinde ortaya konulan uygulamalar üzerinde Üyeler'in işbu Sözleşme içerisinde tanımlı olan iş ve işlemleri daha etkin şekilde gerçekleştirebilmelerini sağlamak üzere, dilediği zaman Düğün Atölyeleri Hizmetleri'nde değişiklikler ve/veya uyarlamalar yapabilir. Düğün Atölyeleri tarafından yapılan bu değişiklikler ve/veya uyarlamalarla ilgili Üyeler'in uymakla yükümlü olduğu kural ve koşullar, Düğün Atölyeleri tarafından ilgili Düğün Atölyeleri Hizmetleri'nin kullanımıyla ilgili açıklamaların bulunduğu web sayfasından Üyeler'e duyurulur.<br><br>
4.5. Düğün Atölyeleri, dilediği zaman, Site'de bildirmek suretiyle, Düğün Atölyeleri Hizmetleri'ni ücretli veya sair yollarla sağlayabilir. Site'de bildirimin yapılmasından itibaren, söz konusu Düğün Atölyeleri Hizmetleri, bildirilen şart ve koşullarda sağlanacak ve söz konusu Düğün Atölyeleri Hizmetleri'nden değişiklikten önce faydalanan Üyeler'e de değişiklikle yeni şart ve koşullar uygulanacaktır.<br><br>
<strong>5. GİZLİLİK POLİTİKASI</strong><br><br>
Düğün Atölyeleri, Site'de, Üyeler'le ilgili bilgileri, işbu Sözleşmenin kapsamı dışında, işbu Sözleşme’nin ayrılmaz bir parçası olan Gizlilik Politikası kapsamında kullanabilir. Düğün Atölyeleri, Üyeler'e ait gizli bilgileri, işbu Sözleşme’nin kapsamı dışında, ancak Gizlilik Politikası'nda belirtilen koşullar dahilinde üçüncü kişilere açıklayabilir veya kullanabilir.<br><br>
<strong>6. DİĞER HÜKÜMLER</strong><br><br>
<strong>6.1. FİKRİ MÜLKİYET HAKLARI</strong><br><br>
6.1.1. Site'nin tasarım, metin, imge, html kodu ve diğer kodlar da dahil ve fakat bunlarla sınırlı olmamak kaydıyla, tüm unsurları (bundan böyle “Düğün Atölyeleri’nin telif haklarına tabi çalışmalar” olarak anılacaktır) Düğün Atölyeleri'ne ait ve/veya Düğün Atölyeleri tarafından üçüncü bir kişiden alınan lisans hakkı altında kullanılmaktadır. Üyeler, Düğün Atölyeleri Hizmetleri'ni, Düğün Atölyeleri bilgilerini ve Düğün Atölyeleri'nin telif haklarına tabi çalışmalarını yeniden satamaz, paylaşamaz, dağıtamaz, sergileyemez veya başkasının Düğün Atölyeleri Hizmetleri'ne erişmesine veya kullanmasına izin veremez. Aksi takdirde, lisans verenler de dahil üçüncü kişilerin uğradıkları zararlardan dolayı Düğün Atölyeleri'nden talep edilen tazminat miktarını, mahkeme masrafları ve avukatlık ücreti de dahil olmak üzere karşılamakla yükümlü olacaklardır. Üyeler, Düğün Atölyeleri'nin telif haklarına tabi çalışmalarını çoğaltamaz, dağıtamaz veya bunlardan türemiş çalışmalar yapamaz veya hazırlayamaz.<br><br>
6.1.2 Düğün Atölyeleri'nin, Düğün Atölyeleri Hizmetleri, Düğün Atölyeleri bilgileri, Düğün Atölyeleri telif haklarına tabi çalışmaları, Düğün Atölyeleri ticari markaları, Düğün Atölyeleri ticari görünümü veya Site vasıtasıyla sahip olduğu her tür maddi ve fikri mülkiyet hakları da dahil tüm malvarlığı, ayni ve şahsi hakları, ticari bilgi ve know-how'a yönelik tüm hakları saklıdır.<br><br>
<strong>6.2. SÖZLEŞME DEĞİŞİKLİKLERİ</strong><br><br>
Düğün Atölyeleri, tamamen kendi takdirine bağlı ve tek taraflı olarak, işbu Sözleşme’yi, uygun göreceği herhangi bir zamanda, Site'de ilan ederek değiştirebilir. İşbu Sözleşme’nin değişen hükümleri, ilan edildikleri tarihte geçerlilik kazanacak; geri kalan hükümler aynen yürürlükte kalarak hüküm ve sonuçlarını doğurmaya devam edecektir. İşbu Sözleşme, Üyeler'in tek taraflı beyanları ile değiştirilemez.<br><br>
<strong>6.3. MÜCBİR SEBEPLER</strong><br><br>
Hukuken mücbir sebep sayılan tüm durumlarda, Düğün Atölyeleri, işbu Sözleşme ile belirlenen edimlerinden herhangi birini geç veya eksik ifa etme veya ifa etmeme nedeniyle yükümlü değildir. Bu ve bunun gibi durumlar, Düğün Atölyeleri için, gecikme, eksik ifa etme veya ifa etmeme veya temerrüt addedilmeyecek veya bu durumlar için Düğün Atölyeleri'nden herhangi bir nam altında tazminat talep edilemeyecektir. "Mücbir sebep" terimi, doğal afet, isyan, savaş, grev, iletişim sorunları, altyapı ve internet arızaları, elektrik kesintisi ve kötü hava koşulları da dahil ve fakat bunlarla sınırlı olmamak kaydıyla, ilgili tarafın makul kontrolü haricinde ve Düğün Atölyeleri'nin gerekli özeni göstermesine rağmen önleyemediği, kaçınılamayacak olaylar olarak yorumlanacaktır.<br><br>
<strong>6.4. UYGULANACAK HUKUK VE YETKİ</strong><br><br>
İşbu Sözleşme’nin uygulanmasında, yorumlanmasında ve işbu Sözleşme dahilinde doğan hukuki ilişkilerin yönetiminde Türk Hukuku uygulanacaktır. İşbu Sözleşme’den dolayı doğan veya doğabilecek her türlü ihtilafın hallinde İstanbul Mahkemeleri ve İcra Daireleri yetkilidir.<br><br>
<strong>6.5. SÖZLEŞMENİN FESHİ</strong><br><br>
İşbu Sözleşme, Üye'nin üyeliği iptal edilinceye kadar yürürlükte kalacak ve taraflar arası hüküm ve sonuçlarını doğurmaya devam edecektir.<br><br>
Üye, Düğün Atölyeleri’ne Sözleşme’nin 3.2.1 numaralı maddesinde belirtilen temsil yetkisini vermeyi kabul etmediği veya verdiği yetkiyi geri aldığı takdirde, Sözleşme kendiliğinden feshedilmiş sayılacak, Üye’nin üyeliği iptal edilecektir. Düğün Atölyeleri, Üyeler'in işbu Sözleşme’de belirtilen hükümleri ve Site içinde yer alan kullanıma, üyeliğe ve Düğün Atölyeleri Hizmetleri'ne ilişkin benzeri kuralları ihlal etmeleri durumunda, Sözleşme’yi tek taraflı olarak feshedebilecek ve Üyeler fesih sebebiyle Düğün Atölyeleri'nin uğradığı tüm zararları tazmin etmekle yükümlü olacaktır.<br><br>
<strong>6.6. DÜĞÜN ATÖLYELERİ KAYITLARININ GEÇERLİLİĞİ</strong><br><br>
Üye, işbu Sözleşme’den doğabilecek ihtilaflarda, Düğün Atölyeleri'nin kendi veritabanında veya sunucularında tuttuğu elektronik kayıtların ve sistem kayıtlarının, ticari kayıtlarının ve bilgisayar kayıtlarının muteber, bağlayıcı, kesin ve münhasır delil teşkil edeceğini; Düğün Atölyeleri'ni yemin teklifinden ber'i kıldığını ve bu maddenin HMK 193. madde anlamında delil sözleşmesi niteliğinde olduğunu kabul, beyan ve taahhüt eder.<br><br>
<strong>6.7. YÜRÜRLÜK</strong><br><br>
İşbu Sözleşme Üye'nin elektronik olarak onay vermesi ile karşılıklı olarak kabul edilerek yürürlüğe girmiştir.

</p>
					</div>
				</div>                
			</div>
		</div>
	</section>
@endsection


                    