@extends('_website.layouts.master')
@section('content')
@section('title', 'Biz Kimiz | Düğün Atölyeleri')

	<section class="base-page sign-in-up-page sign-modal contact-page sss-page hakkimizda-page">
	<div class="contact-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h2>BİZ KİMİZ</h2>
					<p>Hayatın en güzel anlarını, daha güzel ve unutulmaz yapmak için çalışan tatlış bir ekibiz. İşimizi çok seviyoruz çünkü içinde "aşk" var, "heyecan" var, "ilham" var. Instagram'da fark yaratan yüzlerce atölyeyi sizler için bir araya getirdik. Nişan, kına, düğün, doğum ve doğum günü gibi hayatımızın en güzel ve anlamlı günleri için özel tasarımlar üreten yüzlerce butik marka hizmetinizde.
</p>
				</div>
			</div>
		</div>
	</div>
	<div class="sss-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<article>
						<h3>Kısa Hikayemiz</h3>
						<p>düğünatölyeleri.com kendi düğününde bazı aksaklıklar yaşayan bir gelinin, diğer çiftlere destek olma arzusuyla yola çıktı. <strong>"Her şey çok güzel olsun"</strong> dileğiyle başladık.</p>
						<!--<h3>Neredeyiz?</h3>
						<p>düğünatölyeleri.com'un merkezi Ataşehir'de. Ama siz hangi atölyemizden hizmet alıyorsanız, biz oradayız!</p>-->
						<h3>Çalışma Prensibimiz</h3>
						<p>düğünatölyeleri.com, bir dijital pazar yeridir. Atölyeler ile ürün / hizmet arayan çiftleri buluşturuyoruz. Aradığınız hizmet ya da ürün kategorisinde, onlarca atölyeyi bir arada bulabilirsiniz. Atölyelerimizin ürünlerini, diğer çiftlerin yorumlarını görerek fikir edinebilir; atölyelerimizden teklif isteyerek, ayrıntılı bilgi alabilirsiniz. Tüm atölyeleri bir arada görerek, listeler oluşturabilir, ne istediğinize çok daha kolay ve hızlı karar verebilirsiniz. 
</p>
					</article>

				</div>
			</div>
		</div>
	</div>
	<div class="about-foot">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="about-foot-inner">
						<div class="left">
							<svg class="wedding-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/wedding-icon.svg#wedding-icon"></use></svg>
							<h4>ÇİFTLER İÇİN <br><span>NE İFADE EDİYORUZ?</span></h4>
							<p>Aradığınız her şeyi bulabileceğiniz keyifli bir platform. Instagram'da sayfalar dolusu, yüzlerce profili gezmek yerine, hepsine tek adresten ulaşabilirsiniz.
                            
                            Instagram'da öne çıkan, onlarca çifte özel ürün tasarlamış yüzlerce düğün atölyesi burada! Nişan, kına, düğün, doğum ve doğum günü gibi özel günlerinizde, ihtiyaç duyduğunuz özel tasarları düğünatölyeleri.com'da bulabilirsiniz.</p>
                            <div class="form-group">
								<a href="{{ route('workshop.all') }}">
									<button class="btn all-workshop-button">TÜM ATÖLYELER</button>
								</a>
							</div>
						</div>
						<div class="middle">
							<hr>
						</div>
						<div class="right">
							<svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>
							<h4>ATÖLYELER İÇİN <br><span>NE İFADE EDİYORUZ?</span></h4>
							<p>Çiftlerin hayallerini gerçeğe dönüştürme gücüne ve yeteneğine sahipsiniz. Ürettiğiniz özgün tasarımlar ve hizmetlerle Instagram hesabınızdan bugüne kadar onlarca çifte ulaştınız.
                            
                            düğünatölyeleri.com ile artık daha fazla çifte ulaşabilirsiniz. Profilleriniz ile siz de burada yerinizi alarak bilinirliğinizi artırın. Daha fazla çifte ulaşın, avantajlarımızdan faydalanın ve daha fazla kazanın.</p>
                            <div class="form-group">
								<a href="{{ route('workshop.register') }}">
									<button class="btn all-workshop-button">ATÖLYE KAYIT</button>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection