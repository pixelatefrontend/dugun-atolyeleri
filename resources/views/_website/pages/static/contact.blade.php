@extends('_website.layouts.master')
@section('content')
@section('title', 'İletişim | Düğün Atölyeleri')
<section class="base-page sign-in-up-page sign-modal contact-page">
	<div class="contact-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<div class="row mobile-flex">
						<div class="col-sm-6 col-md-6 col-lg-6 order-2">
							<div class="sign-in-form contact-form">
								<h3>İLETİŞİM FORMU</h3>
								{!! Form::open([ 'url' => route('post.contact'), 'id' => 'contact-form']) !!}
									<div class="row row-inputs">
										<div class="col-lg-12">
											<div class="input__group input-container input-control">
												<label>ADINIZ</label>
												<input type="text" placeholder="Adınız" name="name">
												<div class="error-message" id="name">Ad hatalı. Lütfen boş alan bırakmadığınızdan emin olun</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="input__group input-container input-control">
												<label>E - POSTA</label>
												<input type="text" placeholder="örnekmail@örnek.com" name="email">
												<div class="error-message" id="email">E-posta hatalı. Lütfen boş alan bırakmadığınızdan emin olun</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="input__group input-container input-control">
												<label>MESAJINIZ</label>
												<textarea placeholder="Mesajınız" name="message"></textarea>
												<div class="error-message" id="message">Mesaj alanı hatalı. Lütfen boş alan bırakmadığınızdan emin olun</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="input__group submit__group">
												<button id="contact-send-button" type="submit" name="register_ajax" class="button button--large button--block button--pink">GÖNDER<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></button>
											</div>
										</div>
									</div>
									<div class="row row-success">
										<div class="bid-success-modal">
										<div class="success-icon">
					                        <span><svg class="success-icon-pink"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/success-icon.svg#success-icon"></use></svg></span>
					                    </div>
					                    <h4>Mesajınız <br>Başarıyla İletilmiştir</h4>
					                    </div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 order-1">
							<div class="contact-wrap">
								<h3>İLETİŞİM BİLGİLERİ</h3>
								<h4>ADRES</h4>
								<address>Barbaros Mah. Ak Zambak Sok. Uphill Towers A Blok<br>Daire: 145 Ataşehir, İstanbul</address>
								<h4>E- POSTA ADRESİ</h4>
								<span>bilgi@dugunatolyeleri.com</span>
								<a href="tel:+902165042262">
									<figure>
										<svg class="comment-phone-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/comment-phone-icon.svg#comment-phone-icon"></use></svg>
									</figure>
									<div class="text">
										<strong>MÜŞTERİ HİZMETLERİ</strong>
										<strong>0 <span>(216)</span> 504 22 62</strong>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@push('scripts')
<script>
	$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
</script>
@endpush