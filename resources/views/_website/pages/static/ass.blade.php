@extends('_website.layouts.master')
@section('content')
@section('title', 'Atölyeler İçin Sık Sorulanlar | Düğün Atölyeleri')
	<section class="base-page sign-in-up-page sign-modal contact-page sss-page">
	
	<div class="sss-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
                <h2>ATÖLYELER İÇİN SIK SORULANLAR</h2><br><br>
					<div id="accordion"> 
						<h3>Düğünatölyeleri.com'un amacı ne?</h3>
						<div>
							<p>İşinizi kolaylaştırmak! Instagram'da satış yapan atölyeleri doğru hedef kitleye ulaştırmak. Butik Düğün Atölyelerinin bilinirliğini ve kazancını arttırmak.</p>
						</div>
						<h3>Düğünatölyeleri.com'un atölyeme katkısı nedir?</h3>
						<div>
							<p>Atölyeniz daha büyük ve doğru hedef kitleye ulaşır. Çiftlerin Instagram'da sizi tanımadığı ve isminizi bilmediği halde bulması oldukça zordur. Doğrudan hedef kitleniz olan çiftlerimiz ihtiyaçları doğrultusunda kategorilerimizi gezerler ve size ulaşırlar.</p>
						</div>
						<h3>Nasıl atölye profili oluştururum? / Nasıl atölye kaydı yapabilirim? </h3>
						<div>
							<p>Atölye Kayıt sayfamızda yer alan formu doldurarak başvurabilir, <strong>0264 504 22 62</strong> destek hattımızı arayabilir, <strong>bilgi@dugunatolyeleri.com</strong> e-posta adresimize bilgilerinizi ileterek atölye profilinizi oluşturabilirsiniz.</p>
						</div>
						<h3>Instagram hesabım olmadan üye olabilir miyim?</h3>
						<div>
							<p>düğünatölyeleri.com, Instagram'da satış yapan butik atölyelerin yer aldığı dijital bir pazar yeridir. Instagram hesabınız olmadan üye olmak için atölyenize ait bilgileri bizimle paylaşınız. Uzmanlarımız sizinle iletişime geçerek yardımcı olacaklardır.</p>
						</div>
						<h3>Markamı Düğünatölyeleri.com'da öne çıkarmak için ne yapmalıyım?</h3>
						<div>
							<p>Premium Üyelik ayrıcalıklarından faydalanarak markanızı öne çıkarabilirsiniz.</p>
						</div>
						<h3>Standart Üyelik nedir?</h3>
						<div>
							<p>Standart Üyelik, süresiz ve tamamen ücretsiz şekilde atölyelerin istedikleri kategorilerde profillerinin yer almasına olanak sağlar. Standart üyelerimizin Atölye Adı, Atölye Logosu, Atölye Kısa Tanıtım Yazısı, Çift Yorumları ve Seçecekleri 5 Fotoğrafı profillerinde yayınlama hakları vardır. yer alabileceğiniz standart atölye profilleridir.</p>
						</div>
						<h3>Standart Üyelik kapsamı ne içerir?</h3>
						<div>
							<p>Standart üyelerimizin Atölye Adı, Atölye Logosu, Atölye Kısa Tanıtım Yazısı, Çift Yorumları ve Seçecekleri 5 Fotoğrafı profillerinde yayınlama hakları vardır. yer alabileceğiniz standart atölye profilleridir.</p>
						</div>
						<h3>Premium Üyelik nedir?</h3>
						<div>
							<p>Premium Üyelik, düğünatölyeleri.com&#39;un özel fırsat ve avantajlarının sunulduğu özel üyelik paketidir.</p>
						</div>
						<h3>Premium Üyelik avantajları nelerdir?</h3>
						<div>
							<p>Premium üyelerimizin profillerinde Atölye Adı, Atölye Logosu, Atölye Tanıtım Yazısı, Fiyat ve Teslimat Bilgisi, Sıkça Sorulan Sorular, İletişim Bilgileri, Çift Yorumları ve Sınırsız Fotoğraf / Video  yayınlama hakları vardır. Premium Üyelerimiz ile editörlerimizin yapacağı özel röportaj Düğün Atölyeleri Blog bölümünde yayınlanır ve premium üyelerimiz Blog bölümü için diledikleri zaman bülten gönderebilirler. Premium Üyelerimiz arama ve kategori listelerinde üst sırada yer almakla birlikte <strong>Düğün Atölyeleri Premium Rozeti</strong>ne sahip olurlar.</p>
						</div>
						<h3>Premium Üye olmak için ne yapmalıyım?</h3>
						<div>
							<p>Atölye Kayıt sayfamızda yer alan formu doldurarak başvurabilir, <strong>0264 504 22 62</strong> destek hattımızı arayabilir, <strong>bilgi@dugunatolyeleri.com</strong> e-posta adresimize bilgilerinizi ileterek atölye profilinizi oluşturabilirsiniz.<br><br>
							Atölye profiliniz onaylandıktan sonra dilediğiniz zaman profilinizi Premium Üyeliğe yükseltebilirsiniz. </p>
						</div>
						<h3>Premium Üyelik ödeme koşulları nelerdir?</h3>
						<div>
							<p>Aylık veya Yıllık Premium üyelik oluşturabilirsiniz. Hangi paketi seçerseniz seçin bir ay ücretsiz kullanırsınız. Yıllık premium paketinde, aylık pakete göre %20 daha ekonomik, her ay ödeme derdi olmadan daha uzun süreli kullanım sağlarsınız. Ödemenizi Kredi kartı veya Havale (EFT) ile yapabilirsiniz.</p>
						</div>
						<h3>Birden fazla kategoride yer alabilir miyim?</h3>
						<div>
							<p>Birden fazla kategoride aynı profil bilgileriniz ile farklı ürün fotoğraflarınız ile yer alabilirsiniz. Bunun için yer almak istediğiniz kategorilere özel ürün fotoğraflarınızı lütfen bizimle paylaşın</p>
						</div>
						<h3>Birden fazla atölye ile üye olabilir miyim?</h3>
						<div>
							<p>düğünatölyeleri.com'da istediğiniz kadar standart veya premium üyelik oluşturabilirsiniz.</p>
						</div>
						
                        </div>
                              
					</div>
                    
				</div>
				<div class="row">
					 <div class="sss-bottom">
                    	<a href="{{ route('get.sss') }}">
                    		<button class="sss-button">ÇİFTLER İÇİN SIK SORULANLAR</button>
                    	</a>
                    </div>
				</div>
                
			</div>
		
		</div>
   
</section>
@endsection


                    