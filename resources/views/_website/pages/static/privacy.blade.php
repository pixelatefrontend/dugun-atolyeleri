@extends('_website.layouts.master')
@section('content')
@section('title', 'Gizlilik Politikası | Düğün Atölyeleri')

	<section class="base-page sign-in-up-page sign-modal contact-page sss-page">
	
	<div class="sss-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
                	<h2>Gizlilik Politikası</h2><br><br>
                	<p>Gizlilik politikası üyelerimizin, ziyaretçilerimizin ve atölyelerimizin web sitemizi kullanımına ilişkin hükümleri düzenlemektedir. Web sitemizi ziyaret ederek aşağıdaki koşulları uygulamayı kabul etmiş sayılmaktasınız.<br><br>

Web sitemizde yer alan Atölye Kayıt, Kullanıcı Kayıt formları ve üyelik bilgileriniz (isim, e-posta adresi, posta adresi, fotoğraf vb), pazarlama izni prensipleri dışında üçüncü kişi ve kurumlarla asla paylaşılmamaktadır. Teklif İste formu, sadece iletişim kurmak istediğiniz marka sahiplerine iletilmekte, diğer marka ve kurumlarla paylaşılmamaktadır. <br><br>

Üyelik bilgileriniz kampanya duyuru ve bilgilendirme amacıyla kullanılabilmektedir. Atölye Kayıt ve Kullanıcı Kayıt bilgilerinizi, Hesabım > Hesap Ayarları bölümünden değiştirebilirsiniz.<br><br>
Atölye profil sayfalarında yer alan görsel ve yazılı materyallere,  düğünatölyeleri.com konseptine uygun olarak atölye sahipleri karar vermektedir. Üyeliği biten ve silinen atölye bilgileri, tarafımızca saklanmaktadır. Atölye profil sayfalarına eklenen görsel ve yazılı materyaller, tarafımızca saklanmakta, üçüncü kişi ve kurumlarla paylaşılmamaktadır. <br><br>

düğünatölyeleri.com'a kayıt olan atölyeler, web sitemizde yayınladıkları ürün fotoğraflarının ve ürün bilgilerinin, düğünatölyeleri.com tarafından reklam amacıyla kullanılmasına izin vermiş sayılmaktadır.<br><br>

düğünatölyeleri.com'da yer alan atölyelerin görsel ve yazılı materyalleri korunmaktadır fakat diğer kişilerce web sitemizden çekilen görsellerin başka platformlarda kullanılmasından düğünatölyeleri.com sorumlu tutulamaz. </p>

				</div>
			</div>                
		</div>
	</div>
   
</section>
@endsection


                    