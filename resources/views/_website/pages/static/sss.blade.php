@extends('_website.layouts.master')
@section('content')
@section('title', 'Çiftler İçin Sık Sorulanlar | Düğün Atölyeleri')

	<section class="base-page sign-in-up-page sign-modal contact-page sss-page">
	
	<div class="sss-container">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
                <h2>ÇİFTLER İÇİN SIK SORULANLAR</h2><br><br>
					<div id="accordion"> 
						<h3>Düğünatölyeleri.com'un amacı ne?</h3>
						<div>
							<p>İşinizi kolaylaştırmak! Bir sayfa üzerinden yüzlerce atölyeye ulaşabilmenizi sağlıyoruz. Web Sitemize üye olduktan sonra beğendiğiniz atölyeler ve ürünlerden favori listelerinizi  oluşturabilirsiniz.</p>
						</div>
						<h3>Düğünatölyeleri.com'da neler var?</h3>
						<div>
							<p>Davetiye tasarımı, düğün pastası, nikah şekeri, gelin çiçeği, düğün fotoğrafçıları, bebek fotoğrafçıları, bebek süsleri, düğün çikolatası, nişan süsleri gibi kategoriler altında onlarca atölye ve ürünü burada bulabilirsiniz.</p>
						</div>
						<h3>Aradığım ürünleri nasıl bulurum?</h3>
						<div>
							<p>İlgili kategori sayfalarımızda atölyeleri, her atölyenin profil sayfasında en güzel ürünlerini görebilirsiniz.</p>
						</div>
                        <h3>Atölyelerle nasıl iletişime geçerim?</h3>
						<div>
							<p>Atölye profil sayfalarından teklif isteyerek iletişime geçebilir, yorum yapabilirsiniz.</p>
						</div>
                        <h3>Düğünatölyeleri.com'dan sipariş verebilir miyim?</h3>
						<div>
							<p>düğünatölyeleri.com, şimdilik bir online katalog işlevine sahiptir. Siparişlerinizi sizler için atölyelerimize iletiyoruz. Sipariş vermek istediğiniz atölyenin profil sayfasından teklif isteyebilirsiniz.</p>
						</div>
                        <h3>Satın aldığım üründen memnun kalmazsam ne yapmalıyım?</h3>
						<div>
							<p>Satın aldığınız ürün ve hizmetlerle ilgili atölyelerimizle iletişime geçmeniz gerekmektedir. Atölye profil sayfaları aracılığıyla yorumlarınızı atölye ve diğer çiflerimizle paylaşabilirsiniz.</p>
						</div>
                         <h3>Düğünatölyeleri.com güvence veriyor mu?</h3>
						<div>
							<p>düğünatölyeleri.com ürün/hizmetlerle ilgili garanti ya da güvence vermemektedir. Ancak birlikte çalıştığımız atölyelerimize güveniyor, sorun yaşanması durumunda atölyelerimizle iletişime geçerek sizlere destek oluyoruz.</p>
						</div>
                        <h3>'Teklif İste' nedir?</h3>
						<div>
							<p>Detaylı bilgi almak istediğiniz ürün / hizmetlerle ilgili atölye profil sayfalarından teklif isteyebilirsiniz. Ürün detayları, miktar ve fiyat bilgilerini, teklif isteyerek edinebilirsiniz. Teklif istediğinizde iletişim bilgilerinizi atölyemiz ile paylaşırız ve size ulaşmalarını sağlarız.</p>
						</div>
                        </div>
                                 
					</div>
                    
				</div>
				<!-- <div class="row">
					 <div class="sss-bottom">
                    	<a href="{{ route('get.sss.workshop') }}">
                    		<button class="sss-button">ATÖLYELER İÇİN SIK SORULANLAR</button>
                    	</a>
                    </div>
				</div> -->
                
			</div>
		</div>
   
</section>
@endsection


                    