@extends('_website.layouts.master')
@section('content')
@include('_website.partials.modals.register-modal')
@section('title', $item->title.' | Düğün Atölyeleri')
<section class="base-page home-page invitation-design-page">
	<div class="page-header" style="background-image: url('{{ $item->cover_picture }}')">
		<div class="container">
			<div class="container-inner">
				<article>
					<div class="breadcrump-inner">
						{{-- <span><a href="">Anasayfa</a> / <a href="">Atölyeler</a> / Davetiye Tasarımı Atölyeleri</span> --}}
					</div>
					<h2>{{ $item->title }}</h2>
					<p>{{ $item->description }}</p>
					<div class="list-length">
						<svg class="icon">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-search-list"></use>
						</svg>
						<span><strong>{{ $item->workshops->total() }}</strong> ATÖLYE LİSTELENDİ</span>
					</div>
					@if($item->adWorkshop)
						<div class="design-by">
							<div class="design-by-inner">
								<a href="{{ route('workshop.single', $item->adWorkshop->slug ) }}">
									<svg class="shop-icon">
										<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use>
									</svg>
									<span>{{ $item->adWorkshop->title }}</span>
								</a>
							</div>
						</div>
					@endif
				</article>
			</div>
		</div>
	</div>
	{{-- <div class="home-page-week-design-list">
		<div class="container">
			<div class="row week-design-container">
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
					<div class="title-container">
						<h3>Öne Çıkan Atölyeler</h3>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
					<div class="week-design-list">
						<div class="week-design-list-inner">
							<div class="row">
								<div class="week-design-list-slider">
									<div class="item-slider">
										@foreach($item->workshops->chunk(3) as $items)
											@foreach($items as $workshop)
												<div class="{{{ $workshop->doping == 1 ? 'col-sm-12 col-md-8 col-lg-8' : 'col-md-4 col-lg-4 col-sm-4 col-xs-4 single' }}}">
													<div class="week-design-list-item">
														@if($workshop->type != 'normal')
															<div class="ribbon-badge">
																<div class="kurdele-inner">
																	<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg>
																</div>
															</div>
															<div class="info-badge">
																<div class="info-badge-item">
																	<div class="info-badge-item-inner">
																		<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-video"></use></svg>
																		<span>{{ $workshop->videos->count() }}</span>
																	</div>
																</div>
																<div class="info-badge-item">
																	<div class="info-badge-item-inner">
																		<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-camera"></use></svg>
																		<span>{{ $workshop->images->count() }}</span>
																	</div>
																</div>
															</div>
														@endif
														<figure class="main-figure">
															<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">
																<div class="click-detail-inner">
																	<span>ATÖLYEYE GİT</span>
																</div>
															</a>
															@if($workshop->latestImage)
																<img src="           rename('uploads/workshops/'.$item->slug, 'uploads/workshops/'.$item->id);
/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->filename }}">
															@else
																<img src="/assets/_website/img/week-design-image-1.jpg" alt="">
															@endif
														</figure>
														<div class="footer">
															<figure class="avatar-figure">
																<div class="hover-svg">
																	<div class="hover-svg-inner">

																		<button
																				value="{{ $workshop->id }}"
																				class="no-button"
																				type="button"
																				onclick="addFavorite(this)">
																			<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
																		</button>
																	</div>
																</div>
																@if($workshop->avatar)
																	<img src="{{ $workshop->avatarPath() }}">
																@else
																	<div class="no-image">
																		<span>{{ $workshop->noImageAvatar() }}</span>
																	</div>
																@endif
															</figure>
															<div class="footer-inner">
																<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->getShortTitle() }}</h4></a>
																@if(Auth::check())
																	<a href="javascript:void(0)" id="favorite_{{$workshop->id}}" class={{{ $workshop->isFavorited() ? 'increase' : '' }}}>
																		@else
																			<a href="javascript:void(0)" id="favorite_{{$workshop->id}}">
																				@endif
																				<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
																				<span>{{ $workshop->like }}</span>
																			</a>
																	</a>
															</div>
														</div>
													</div>
												</div>
											@endforeach
										@endforeach
									</div>
								</div>


							</div>
						</div>
					</div>


				</div>
			</div>
		</div>
	</div> --}}
	<div class="home-page-design-list">
		<div class="container">
			<div class="row design-list design-list-grid" id="articles">
				@foreach($workshops as $workshop)
					<div class="col-xs-12 col-sm-6 {{{ $workshop->doping == 1 ? 'col-md-6 col-lg-6' : 'col-md-3 col-sm-4 col-xs-6 col-lg-3' }}} design-list-grid-item" id="article">
					<div class="week-design-list-item">
						@if($workshop->type != "normal")
						<div class="ribbon-badge">
							<div class="kurdele-inner">
								<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg>
							</div>
						</div>
						<div class="info-badge">
							<div class="info-badge-item">
								<div class="info-badge-item-inner">
									<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-video"></use></svg>
									<span>{{ count($workshop->videos) }}</span>
								</div>
							</div>
							<div class="info-badge-item">
								<div class="info-badge-item-inner">
									<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-camera"></use></svg>
									<span>{{ count($workshop->images) }}</span>
								</div>
							</div>
						</div>
						@endif
						<figure class="main-figure">
							<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">
								<div class="click-detail-inner">
									<span>ATÖLYEYE GİT</span>
								</div>
							</a>
							@if($workshop->first_picture)
								<img src="{{ $workshop->first_picture }}" alt="">
							@elseif($workshop->latestImage)
								<img src="/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->getFilename()}}" alt="">
							@else
								<img src="/assets/_website/img/week-design-image-1.jpg" alt="">
							@endif
						</figure>
						<div class="footer">
							<figure class="avatar-figure">
								<div class="hover-svg">
									<div class="hover-svg-inner">
										<button 
											value="{{ $workshop->id }}" 
											class="no-button" 
											type="button" 
											onclick="addFavorite(this)">
										<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg></button>
									</div>
								</div>
								@if($workshop->avatar)
									<img src="{{ $workshop->avatarPath() }}">
								@else
									<div class="no-image">
										<span class="workshop-pp-name" style="text-transform: uppercase;" data-name="{{ $workshop->title }}"></span>
									</div>
								@endif
							</figure>
							<div class="footer-inner">
								<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->getShortTitle() }}</h4></a>
								@if(Auth::check())
									<a href="#" id="favorite_{{$workshop->id}}" class={{{ $workshop->isFavorited() ? 'increase' : '' }}}>
								@else
									<a href="#" id="favorite_{{$workshop->id}}">
								@endif
									<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
									<span>{{ $workshop->like }}</span>
								</a>
									</a>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">

					<div class="week-design-list-item add-design-item">

						<a href="{{ route('workshop.register') }}" class="add-design-item-container">

							<div class="add-design-item-inner">

								<figure>

									<svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

									<div class="icon">

										<i class="fa fa-plus" aria-hidden="true"></i>

									</div>

								</figure>

								<h4>ATÖLYE KAYIT</h4>

								<span>FİRMANIZ BURADA <br>YER ALSIN</span>

							</div>

						</a>

					</div>

				</div>
			</div>
		</div>
	</div>
	@include('_website.partials.layouts.pagination', ['paginator' => $workshops])

</section>
@endsection
@push('scripts')
	<script>
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
	</script>
<script type="text/javascript">
	function addFavorite(item) {
		var w_id = $(item).attr('value');
		var data = { id: w_id };
		@if(Auth::check())
			
			$.ajax({
				type: 'POST',
				url: "{{ route('favorite.add') }}",
				data: data,
				success: function(response){
					if(response.action == "add"){
						$('#favorite_'+w_id).addClass('increase');
						$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
					}else{
						$('#favorite_'+w_id).removeClass('increase');
						$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
					}
				}
			});
		@else
			if($('#favorite_'+w_id).hasClass('increase')) {
				$('#favorite_'+w_id).removeClass('increase');
				$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
				$.ajax({
					type: 'POST',
					url: "{{ route('favorite.destroy') }}",
					data: data,
				});
			}else{
				$('#favorite_'+w_id).addClass('increase');
				$('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
				$.ajax({
					type: 'POST',
					url: "{{ route('favorite.add') }}",
					data: data,
				});
			}
		@endif
	}
</script>
@endpush