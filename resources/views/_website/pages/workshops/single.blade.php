@extends('_website.layouts.master')
@section('title', $item->title.' | '.$item->categories->last()->title.' | Düğün Atölyeleri')
@section('content')
	<section class="base-page premium-detail-page workshop-detail-page ">

		@if(!Auth::check())
			@include('_website.partials.modals.register-modal')
		@else
			@include('_website.partials.modals.add-list-modal')
			@include('_website.partials.modals.fav-modal')
		@endif
		@include('_website.partials.modals.offer-modal')
		@include('_website.partials.modals.offer-success-modal')
		@include('_website.partials.modals.comment-modal')
		@include('_website.partials.modals.comment-success-modal')
		@if($item->type != 'normal')
			@include('_website.partials.workshops.premium_workshop')
		@else
			@include('_website.partials.workshops.normal_workshop')
		@endif
		@include('_website.partials.layouts.comments')
	</section>
@endsection
@push('scripts')
	<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	</script>
    <script>
        $(function(){
            var workshop_id = {{ $item->id }};
            var date = new Date();
             var minutes = 30;
             date.setTime(date.getTime() + (minutes * 60 * 1000));
            if($.cookie('visitor') == undefined || $.cookie('visitor') != {{ $item->id }})
            {
                $.ajax({
                    method: 'POST',
                    url: '{{ route("workshop.visitor.count") }}',
                    data: { workshop_id: workshop_id },
                });
                $.cookie('visitor', {{ $item->id }}, { expires: date });
            }
        });
    </script>
	<script>
        $('#stop').click(function() {
            alert();
            $('#youtube-iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
        });

        $('form#commentForm').submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            $('#commentButton').attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                url: "{{ route('add.comment', $item->slug) }}",
                data: data,
                success: function(response){
                    if(response.success){
                        $('#comment-modal').modal('hide');
                        $('#comment-success-modal').modal('show');
                        $('input:not(.form-control-button)').val('');
                    }
                }
            });
        });
        $('#offerForm').submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            $(this).find('#offerSend').attr('disabled', 'disabled').val('');
            $('#loadStatus').show();
            $.ajax({
                method: 'POST',
                url: "{{ route('add.offer.post', $item->slug) }}",
                data: data,
                success: function(response) {
                    if(response.success) {
                        $('#offer-modal').modal('hide');
                        $('#loadStatus').hide();
                        $('#offer-success-modal').modal('show');
                        $('input:not(.form-control-button)').val('');
                        $('textarea').val('');
                        $('#offerSend').removeAttr('disabled').val('Teklif İste');
                    }
                }
            });
        });

        function itemAdd(item){
            var data = $(item).attr('value');
            $.ajax({
                type: 'POST',
                url: "{{ route('add.list.content', $item->slug) }}",
                data: { list_id: data },
                success: function(response){
                    if(response.success)
                    {
                        $(item).html('Eklendi &nbsp; <i class="fa fa-check"> </i> ');
                        $('#count_' + data).html(+1);
                        $(item).fadeOut('slow', function(){ $(item).remove(); })
                    }
                }
            });
        }

        function addFavorite(item) {
            var w_id = $(item).attr('value');
            var data = { id: w_id };
			@if(Auth::check())
            $.ajax({
                type: 'POST',
                url: "{{ route('favorite.add') }}",
                data: data,
                success: function(response) {
                    if(response.action == "add") {
                        $('.like-counter').addClass('like-counter-selected');
                        $('.like-button').addClass('liked');
                        $('.like-counter span').html(parseInt($('.like-counter span').html(), 10)+1);
                    }else{
                        $('.like-counter').removeClass('like-counter-selected');
                        $('.like-button').removeClass('liked');
                        $('.like-counter span').html(parseInt($('.like-counter span').html(), 10)-1);
                    }
                }
            });
			@else
            if($('.like-counter').hasClass('like-counter-selected')){
                $('.like-counter').removeClass('like-counter-selected');
                $('.like-button').removeClass('liked');
                $('.like-counter span').html(parseInt($('.like-counter span').html(), 10)-1)
                $.ajax({
                    type: 'POST',
                    url: "{{ route('favorite.destroy') }}",
                    data: data,
                });
            }else{
                $('.like-counter').addClass('like-counter-selected');
                $('.like-button').addClass('liked');
                $('.like-counter span').html(parseInt($('.like-counter span').html(), 10)+1)
                $.ajax({
                    type: 'POST',
                    url: "{{ route('favorite.add') }}",
                    data: data,
                });
            }
			@endif

        }
	</script>
@endpush