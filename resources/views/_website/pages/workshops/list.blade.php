@extends('_website.layouts.master')
@section('content')
@section('title', 'Tüm Atölyeler | Düğün Atölyeleri')
	<section class="base-page home-page invitation-design-page all-workshop-page">
		<div class="page-header">
			<div class="container">
				<div class="container-inner">
					<article>
						<div class="breadcrump-inner">

						</div>
						<h2>TÜM ATÖLYELER</h2>
						<p>Davetiye tasarımı, düğün pastası, nikah şekeri, gelin çiçeği, düğün fotoğrafçıları, bebek fotoğrafçıları, bebek süsleri, düğün çikolatası, nişan süsleri gibi kategoriler altında onlarca atölye ve ürün düğünatölyeleri.com'da!</p>
					</article>
				</div>
			</div>
		</div>
		@foreach($categories as $category)
			<div class="home-page-design-list">
				<div class="container">
					<div class="row design-list">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 title">
									<div class="inner">
										<svg class="{{ $category->icon }}-icon">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#{{ $category->icon }}"></use>
										</svg>
										<h3>{{ $category->title }}</h3>
									</div>
									<a href="{{ route('category.single', $category->slug) }}">KATEGORİYE GİT</a>
								</div>
							</div>
							<div class="row category-slider">
								{{--<div class="item-slider">--}}
									{{----}}
								{{--</div>--}}
								@foreach($category->workshops as $workshop)
									<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 single">
										<div class="week-design-list-item">
											@if($workshop->type != 'normal')
												<div class="ribbon-badge">
													<div class="kurdele-inner">
														<svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg>
													</div>
												</div>
												<div class="info-badge">
													<div class="info-badge-item">
														<div class="info-badge-item-inner">
															<svg class="video-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-video"></use></svg>
															<span>{{ $workshop->videos->count() }}</span>
														</div>
													</div>
													<div class="info-badge-item">
														<div class="info-badge-item-inner">
															<svg class="photo-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-camera"></use></svg>
															<span> {{ $workshop->images->count() }} </span>
														</div>
													</div>
												</div>
											@endif
											<figure class="main-figure">
												<a href="{{ route('workshop.single', $workshop->slug) }}" class="click-detail">
													<div class="click-detail-inner">
														<span>ATÖLYEYE GİT</span>
													</div>
												</a>
												@if($workshop->first_picture)
													<img src="{{ $workshop->first_picture }}" alt="">
												@elseif($workshop->latestImage)
													<img src="/uploads/workshops/{{$workshop->id}}/{{ $workshop->latestImage->getFilename()}}" alt="">
												@else
													<img src="/assets/_website/img/week-design-image-1.jpg" alt="">
												@endif
											</figure>
											<div class="footer">
												<figure class="avatar-figure">
													<div class="hover-svg">
														<div class="hover-svg-inner">
															<button
																	value="{{ $workshop->id }}"
																	class="no-button"
																	type="button"
																	onclick="addFavorite(this)">
																<svg class="avatar-heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
															</button>
														</div>
													</div>
													@if($workshop->avatar)
														<img src="{{ $workshop->avatarPath() }}">
													@else
														<div class="no-image">
															<span class="workshop-pp-name" style="text-transform: uppercase;" data-name="{{ $workshop->title }}"></span>
														</div>
													@endif
												</figure>
												<div class="footer-inner">
													<a href="{{ route('workshop.single', $workshop->slug) }}"><h4>{{ $workshop->getShortTitle() }}</h4></a>
													<a href="" id="favorite_{{$workshop->id}}">
														<svg class="heart-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-heart"></use></svg>
														<span>{{ $workshop->like }}</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</section>
@endsection
@push('scripts')
	<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	</script>
	<script>
        function addFavorite(item) {
            var w_id = $(item).attr('value');
            var data = { id: w_id };
			@if(Auth::check())

            $.ajax({
                type: 'POST',
                url: "{{ route('favorite.add') }}",
                data: data,
                success: function(response){
                    if(response.action == "add"){
                        $('#favorite_'+w_id).addClass('increase');
                        $('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
                    }else{
                        $('#favorite_'+w_id).removeClass('increase');
                        $('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
                    }
                }
            });
			@else
            if($('#favorite_'+w_id).hasClass('increase')) {
                $('#favorite_'+w_id).removeClass('increase');
                $('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )-1);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('favorite.destroy') }}",
                    data: data,
                });
            }else{
                $('#favorite_'+w_id).addClass('increase');
                $('#favorite_'+w_id+' span').html(parseInt($('#favorite_'+w_id+' span').html(), )+1);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('favorite.add') }}",
                    data: data,
                });
            }
			@endif
        }
	</script>
@endpush