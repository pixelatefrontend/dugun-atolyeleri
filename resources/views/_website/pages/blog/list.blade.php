@extends('_website.pages.blog.layouts.master')
@section('content')
	{{-- Code Blog --}}
    <div class="blog-wrapper">
        <div class="blog-header">
            <img src="/assets/_website/img/blog.jpg" class="blog-header__img">
            <div class="overlay">
                <h1 class="blog-header__title text-center">BLOG</h1>
                <p class="blog-header__txt">Hemen ücretsiz profil oluşturun. Memnun kalırsanız premium paket seçin!</p>

            </div>
        </div>

        <div class="blog-content">
            <div class="container">
                <div class="row">
                    @if($items->count() <= 0)
                            {{-- Eğer hiç blog yazısı yoksa burası gözükecek  --}}
                    @endif
                    @foreach($items as $row)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="blog__item">
                            <div class="blog__item-top">
                                <img src="{{ $row->cover }}" alt="blog alt" class="blog__item-img">
                                {{-- <div class="ribbon-badge">
                                    <div class="kurdele-inner">
                                        <svg class="kurdele-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-kurdale"></use></svg>
                                    </div>
                                </div> --}}
                                <div class="blog__item-category">{{ $row->category->title }}</div>
                                <a href="{{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}" class="overlay">
                                    <span class="overlay-txt">Detaya Git</span>
                                </a>
                                <div class="blog__item-share">
                                    <div class="btn-share">
                                        <svg class="icon icon-share"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-share"></use></svg>
                                    </div>
                                    <div class="share-wrp">
                                        <div class="share-group">
                                            <a href="https://plus.google.com/share?url={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                class="share__item google">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-google-plus"></use></svg>
                                            </a>
                                            <a href="https://twitter.com/share?url={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}&via=dugunatolyeleri&related=twitterapi%2Ctwitter&hashtags=dugunatolyeleri&text={{ $row->title }}" target="_blank"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                              class="share__item twitter">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-twitter"></use></svg>
                                            </a>

                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}" target="_blank" class="share__item facebook"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-facebook"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog__item-content">
                                <a href="#" class="blog__item-title">{{ $row->title }}</a>
                                <div class="blog__item-bottom">
                                    <span class="blog__item-date">{{  $row->created_at->diffForHumans() }}</span>
                                    <div class="blog__item-meta">
                                        <span class="meta__item">
                                            <span class="meta__item-icon">
                                                <svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use></svg>
                                            </span>
                                            <span class="meta__item-count">
                                                {{ $row->view_count }}
                                            </span>
                                        </span>
                                        <span class="meta__item">
                                            <span class="meta__item-icon ">
                                                <svg class="icon icon-comment-open"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-comment"></use></svg>
                                            </span>
                                                    <span class="meta__item-count">
                                                {{ $row->comments->count() }}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
     <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endpush