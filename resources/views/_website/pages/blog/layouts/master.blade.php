<!DOCTYPE html>
<html class lang="tr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="svg/xml" />
	<meta name="p:domain_verify" content="07e75480d9124bc7b806a6f3612b1e23"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ $item->title or 'Blog' }} | Düğün Atölyeleri</title>
	<!-- Main Meta Data -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<!-- Main Meta Data End -->
	<!-- Twitter Card Data -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="{{ config('app.twitter') }}">
	<meta name="twitter:title" content="{{ $item->title or config('app.title') }}">
	<meta name="twitter:description" content="{{ $item->description or config('app.description') }}">
	<meta name="twitter:image" content="{{ $item->image or config('app.image') }}">
	<meta name="twitter:url" content="{{ URL::current() }}">
	<!-- Twitter Card Data End -->
	<!-- Open Graph Data -->
	<meta property="og:title" content="{{ $item->title or config('app.title') }} | Düğün Atölyeleri" />
	<meta property="og:type" content="{{ config('app.type') }}" />
	<meta property="og:url" content="{{ URL::current() }}" />
	<meta property="og:image" content="{{ $item->image or config('app.image') }}" />
	<meta property="og:description" content="{{ $item->description or config('app.description') }}" /> 
	<meta property="og:site_name" content="{{ config('app.title') }}" />
	<!-- Open Graph Data End -->
	<!-- Meta Tag Data Start -->
	<meta name="title" content="{{ $item->title or config('app.title') }}" />
	<meta name="description" content="{{ $item->description or config('app.description') }}" />
	<meta name="keywords" content="{{ $item->keywords or config('app.keywords') }}" />
	<meta name="copyright" content="{{ config('app.copyright') }}" />
	<!-- Meta Tag Data End -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />

	<!-- Icon End -->
	<!-- Styles -->
 	{!! Html::style('assets/_website/fancybox/jquery.fancybox.css') !!}
 	{!! Html::style('assets/_website/fancybox/helpers/jquery.fancybox-buttons.css') !!}
 	{!! Html::style('assets/_website/fancybox/helpers/jquery.fancybox-thumbs.css') !!}
	{!! Html::style('assets/_website/css/jquery-ui.css') !!}
	{!! Html::style('assets/_website/css/bootstrap.min.css') !!}
	{!! Html::style('assets/_website/css/font-awesome.min.css') !!}
	{!! Html::style('assets/_website/css/select2.min.css') !!}
	{!! Html::style('assets/_website/css/easy-autocomplete.min.css') !!}
	{!! Html::style('assets/_website/css/easy-autocomplete.themes.min.css') !!}
	{{ Html::style('assets/_website/css/main.css') }}
	{{ Html::style('assets/_website/css/blog.css') }}
	<!-- Styles End -->
	

@if(env('APP_ENV') == 'production')
<!-- Yandex.Metrika counter --> 
	<script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46142220 = new Ya.Metrika({ id:46142220, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");
	</script>
	<!-- /Yandex.Metrika counter -->
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-87812758-29"></script>
	<script>
	 window.dataLayer = window.dataLayer || [];
	 function gtag(){dataLayer.push(arguments);}
	 gtag('js', new Date());

	 gtag('config', 'UA-87812758-29');
	</script>
@endif
</head>
<body>
	@include('_website.pages.blog.layouts.header')
	@yield('content')
	@include('_website.pages.blog.layouts.footer')
<!-- Scripts -->
{!! Html::script('assets/_website/js/jquery.min.js') !!}
{!! Html::script('assets/_website/js/jquery-ui.js') !!}
{!! Html::script('assets/_website/js/bootstrap.min.js') !!}
{!! Html::script('assets/_website/js/isotope.pkgd.min.js') !!}
{!! Html::script('assets/_website/js/slick.min.js') !!}
{!! Html::script('assets/_website/js/select2.min.js') !!}
{!! Html::script('assets/_website/js/tweenmax.min.js') !!}
{{ Html::script('assets/_website/fancybox/jquery.fancybox.pack.js') }}
{{ Html::script('assets/_website/fancybox/helpers/jquery.fancybox-buttons.js') }}
{{ Html::script('assets/_website/fancybox/helpers/jquery.fancybox-media.js') }}
{{ Html::script('assets/_website/fancybox/helpers/jquery.fancybox-thumbs.js') }}
{!! Html::script('assets/_website/js/jquery.easy-autocomplete.min.js') !!}
{!! Html::script('assets/_website/js/jquery.inputmask.bundle.min.js') !!}
@stack('scripts')
{!! Html::script('assets/_website/js/main.js') !!}

<script>
	var options = {
	  	url: "{{ route('blog.search') }}",
	    getValue: "title",
		list: {	
			match: {
			  enabled: true
			},
			showAnimation: {type: "fade", time: 400},
	        hideAnimation: {type: "fade", time: 400}
		},
	    template: {type: "links", fields: {link: "url"}}
	};
	$("#blogSearch").easyAutocomplete(options);
</script>

</body>
</html>