<footer>

	<div class="row big-nav-container">

		<div class="container">

			<div class="big-nav">

				<div class="row">

					<div class="row-inner col-md-3 col-lg-3 col-sm-6 col-xs-12">

						<div class="logo-section-container">

							<div class="logo-section">

								<figure><a href=""><img src="/assets/_website/img/logo-pink-white.svg" ></a></figure>

							</div>

							<div class="call-section-wrp">
								<a href="tel:+90 216 504 22 62" class="call-section">

									<div class="call-section-inner">

										<figure>

											<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/phone-icon.svg#phone-icon"></use></svg>

										</figure>

										<div class="text">

											<strong>BİZİ ARAYIN</strong>

											<strong><span>0 (216)</span> 504 22 62</strong>

										</div>

									</div>

								</a>
							</div>

							<nav class="logo-under-menu">

								<ul>

									<li><a href="{{ route('get.blog') }}">BLOG</a></li>

									<li><a href="{{ route('get.whoarewe') }}">BİZ KİMİZ</a></li>

									<li><a href="{{ route('get.sss') }}">SIK SORULANLAR</a></li>

									{{-- <li><a href="{{ route('get.blog') }}">BLOG</a></li> --}}

									<li><a href="{{ route('get.contact') }}">İLETİŞİM</a></li>



								</ul>

							</nav>

						</div>

					</div>

					<div class="row-inner col-md-6 col-sm-6 col-lg-6 hidden-xs">



						<div class="workshop-section-container">

							<h3>ATÖLYELER</h3>

							<nav class="workshop-menu">

								<ul>

									@foreach($all_categories as $category)
										<li><a href="{{ route('category.single', $category->slug) }}">{{ $category->title }}</a></li>
									@endforeach

								</ul>

							</nav>

						</div>

					</div>

					<div class="row-inner col-md-3 col-sm-12 col-lg-3 col-xs-12">



						<div class="social-section-container">



							<div class="workshop-register">

								<a href="{{ route('workshop.register') }}" class="workshop-button" href=""><span><svg class="shop-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg><strong>ATÖLYE KAYIT</strong></span></a>

							</div>

							<div class="social-follow">

								<h3>BİZİ TAKİP EDİN</h3>
								
								<a href="https://www.facebook.com/dugunatolyeleri" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>

								<a href="https://twitter.com/dugunatolyeleri" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>

								<a href="https://www.instagram.com/dugunatolyeleri/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>

								<a href="http://pinterest.com/dugunatolyeleri" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>

							</div>

						</div>

					</div>

					<div class="footer-bottom">
						<ul>

							<li><a href="{{ route('get.privacy') }}">Gizlilik Politikası</a></li>

							<li><a href="{{ route('get.terms') }}">Kullanıcı Sözleşmesi</a></li>

						</ul>
					</div>

				</div>

			</div>

		</div>

	</div>

	<div class="row small-nav-container">

		<div class="container">

			<div class="small-nav">

				<span>© 2017 <strong>düğünatölyeleri.com</strong> Tüm hakları saklıdır. </span>

				<ul>

					<li><a href="{{ route('get.privacy') }}">Gizlilik Politikası</a></li>

					<li><a href="{{ route('get.terms') }}">Kullanıcı Sözleşmesi</a></li>

				</ul>

			</div>

		</div>

	</div>

</footer>

@if(Session::has('loginMessage'))
	<div class="loginMessage">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			<p>{{ Session::get('loginMessage') }}</p>
		</div>
	</div>
@endif