<header class="header-blog">
	<div class="row middle-nav-container hidden-xs hidden-sm visible-md visible-lg">
		<div class="container">
			<nav class="middle-nav">
				<div class="row">
					<div class="row-inner col-md-4 col-lg-4">
						<div class="logo-section">
							<h1><a href="{{ route('get.blog') }}"><img alt="Düğün Atölyeleri" src="/assets/_website/img/logo_blog.svg"></a></h1>
						</div>
					</div>
					<div class="row-inner col-md-4 col-lg-4">
						<div class="search-section">
							<div class="search-section-inner">
								<svg class="search-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#search-icon"></use></svg>
								{!! Form::open([ 'url' => route('get.blog'), 'method' => 'GET' ]) !!}
									<input id="blogSearch" type="text" name="ara" placeholder="Blog içerisinde ara...">
									<button class="no-button search-button" type="submit">
										<span>ARA</span>
										<svg class="right-long-arrow-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/right-long-arrow.svg#right-long-arrow"></use></svg>
									</button>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					<div class="row-inner col-md-4 col-lg-4">
					@if(!Auth::check())
						<div class="login-section">
							<div class="login-section-inner">
								<a href="{{ route('user.register') }}" id="sign-up-button" class="button button-border">ÜYE OL</a>
								<a href="{{ route('website.home') }}" class="workshop-button">
									<span>
										<svg class="shop-icon">
											<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use>
										</svg>
										<strong>ATÖLYELER</strong>
									</span>
								</a>
							</div>
						</div>
						@else
						<div class="user-section">
							<div class="user-section-inner">
								<a href="{{ route('user.dashboard') }}">
									<div class="user-section-inner-sub">
										<figure>
											@if(!Auth::user()->image)
													<span>{{ strtoupper(Auth::user()->name[0]) }}</span>
											@else
												<img src="{{ Auth::user()->image }}">
											@endif
										</figure>
										<span class="name">{{ Auth::user()->name.' '.Auth::user()->lname }}</span>
									</div>
								</a>
							</div>
							<div class="user-section-menu">
								<ul>
									<li><a href="{{ route('user.offers') }}">TEKLİFLERİM</a></li>
									<li><a href="{{ route('user.lists') }}">LİSTELERİM</a></li>
									<li><a href="{{ route('user.favorites') }}">BEĞENDİKLERİM</a></li>
									<li><a href="{{ route('user.comments') }}">YORUMLARIM</a></li>
									<li><a href="{{ route('user.settings') }}">HESABIM</a></li>
									<li><a href="{{ route('user.logout') }}"><svg width="13" height="13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/close-door-icon.svg#close_door"></use></svg> ÇIKIŞ YAP</a></li>
								</ul>

							</div>

						</div>
						@endif

					</div>

				</div>

			</nav>

		</div>

	</div>

	<div class="row big-nav-container hidden-xs hidden-sm visible-md visible-lg">

		<div class="container">

			<nav class="big-nav">
				<ul>
					@foreach($blogCategories as $category)
					<li class="{{ setActive('blog/kategori/'.$category->slug, 'selected') }}">
						<a href="{{route('get.blog.category', $category->slug)}}">
							<svg class="icon {{$category->icon}}">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#{{ $category->icon }}"></use>
							</svg>
							<span>{{ $category->title }}</span>
						</a>
					</li>
					@endforeach
				</ul>
			</nav>

		</div>

	</div>

	<div class="row mobil-nav visible-sm visible-xs hidden-md hidden-lg">

		<div class="container">

			<div class="button-wrap">
				
				<a href="#" id="hamburger-button" class="hamburger-button">

					<svg class="close-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/close-icon.svg#close-icon"></use></svg>

					<svg class="hamburger-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/hamburger-icon.svg#hamburger-icon"></use></svg>

				</a>

				<a href="{{ route('get.blog') }}">

					<img alt="Düğün Atölyeleri" src="/assets/_website/img/logo_blog.svg" >

				</a>

				<a href="#" id="account-button" class="account-button">

					<svg class="close-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/close-icon.svg#close-icon"></use></svg>

					<svg class="account-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/account-icon.svg#account-icon"></use></svg>

				</a>

			</div>

			<div class="search-wrap">
				
				<div class="search-section">

					<div class="search-section-inner">
						{!! Form::open([ 'url' => route('get.blog'), 'method' => 'GET' ]) !!}

						<button type="submit" class="mobile-search-btn"><svg class="search-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#search-icon"></use></svg></button>

						<input id="blogSearchMobile" type="text" name="ara" placeholder="Blog içerisinde ara...">

						{!! Form::close() !!}

					</div>

				</div>

			</div>

			<div class="left-mobil-menu">

				<ul>
					@foreach($blogCategories as $category)
					<li class="{{ setActive('blog/kategori/'.$category->slug, 'selected') }}">
						<a href="{{ route('get.blog.category', $category->slug) }}">
							<figure>
								<svg class="davetiye-icon">
									<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/files/svg/icons.svg#{{ $category->icon }}"></use>
								</svg>
							</figure>
							<span>{{ $category->title }}</span>
						</a>
					</li>
					@endforeach

				</ul>

				<div class="social">

					<h3>BİZİ TAKİP EDİN</h3>

					<ul>

						<li><a href="https://www.facebook.com/dugunatolyeleri" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

						<li><a href="https://twitter.com/dugunatolyeleri" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>

						<li><a href="https://www.instagram.com/dugunatolyeleri/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>

						<li><a href="http://pinterest.com/dugunatolyeleri" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>

					</ul>

				</div>

			</div>



			<div class="right-mobil-menu">

				@if(!Auth::check())

					<div class="sign-modal sign-modal-popup sign-modal-right-menu">
						<div class="sign-in-form sign-in-form-2">
							<h3>GİRİŞ YAP</h3>
							 {!! Form::open([ 'url' => route('user.login.post') ]) !!}
								<div class="row">
									<div class="col-lg-12">
										<div class="input__group">
											<label>E - POSTA</label>
											<input type="text" placeholder="örnekmail@örnek.com" name="email">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input__group">
											<label>ŞİFRE</label>
											<input type="password" placeholder="Şifreniz" name="password">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="remember-wrap">
					<span>
					<input type="checkbox" id="registerCheck" name="formcheck_register">
					<label for="registerCheck">Oturumu açık tut</label>
					</span>
											<a href="">Şifremi Unuttum</a>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="input__group submit__group">
											<button type="submit" name="register_ajax"
													class="button button--large button--block button--pink">GİRİŞ
												YAP<span class="icon--arrow-right"><svg class="icon"><use
																xmlns:xlink="http://www.w3.org/1999/xlink"
																xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span>
											</button>
										</div>
									</div>
								</div>
							{!! Form::close() !!}
						</div>

						<div class="sign-up-wrap sign-up-wrap-right sign-in-wrp">
							<h3>Üye değil misiniz?</h3>
							<p>Üyelerimize özel ayrıcalıklardan faydalanabilmek için üye olun.</p>
							<a href="#" class="register-button button button--pink btn-singup">KAYIT OL<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></a>

						</div>

						<div class="sign-in-form sign-up-form sign-up-form-mobile">
							@if ($errors->any())
								<div class="alert alert-danger">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<h3>KAYIT OL</h3>
							{!! Form::open([ 'url' => route('user.register.post') ]) !!}
							<div class="row">
								<div class="col-lg-12">
									<h4>KİŞİSEL BİLGİLERİNİZ</h4>
								</div>
								<div class="col-lg-6">
									<div class="input__group input-container input-control">
										<input type="text" placeholder="Adınız" name="name">
										<div class="error-message">Adınız hatalı.</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="input__group input-container input-control">
										<input type="text" placeholder="Soyadınız" name="lname">
										<div class="error-message">Soyadınız hatalı.</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="input__group input__phone__group input-container input-control phone-control">
										<input type="text" placeholder="(+90) XXX XXX XX XX" name="phone" class="phone-mask">
										<span>( Teklif geri dönüşleri için )</span>
										<div class="error-message">Telefon hatalı.</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="input__group input-container input-control mail-control">
										<input type="email" placeholder="E-mail" name="email">
										<div class="error-message">Mail hatalı.</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="input__group input-container input-control">
										<input type="password" placeholder="Şifreniz" name="password">
										<div class="error-message">Şifre hatalı.</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="input__group input-container input-control">
										<input type="password" placeholder="Şifre Tekrarı" name="confirm-password">
										<div class="error-message">Şifre hatalı.</div>
									</div>
								</div>
								<div class="col-lg-12">
									<h4>DÜĞÜN BİLGİLERİNİZ</h4>
								</div>
								<div class="col-lg-6">
									<div class="input__group input__date__group input-container input-control">
										<input class="input__control datepicker" name="date" id="date" placeholder="Tarih" readonly="readonly" required="" data-date-format="dd/mm/yyyy">
										<svg class="date-picker-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/date-picker-icon.svg#date-picker-icon"></use></svg>
										<div class="error-message">Tarih hatalı.</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="input__group custom-select select-container input-control">
										<select placeholder="Şehir" class="city-category-select" name="city">
												<option value="" disabled="" selected="">Şehir</option>
				                               @foreach ($cities as $city)
				                               		<option value="{{$city->city}}">{{ $city->city }}</option>
				                               @endforeach
				                            </select>
										<div class="error-message">Şehir hatalı.</div>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="checkbox-container input-control">
										<input type="checkbox" id="registerCheck-mb" name="formcheck_register">
										<label for="registerCheck-mb"><a href="#" data-toggle="modal" data-target="#termsModal">Üyelik Sözleşmesini ve Gizlilik Politikasını</a> kabul ediyorum</label>
										<div class="error-message" id="termsCheck">Sözleşme şartlarını kabul etmelisiniz.</div>
									</div>
								</div>
								<div class="col-lg-12">
									<button type="submit"  style="margin-top: 25px;" class="button button--large button--block button--pink form-control-button">KAYIT OL<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></button>
								</div>
							</div>
							{!! Form::close() !!}
								<div class="sign-up-wrap sign-up-wrap-right">
									<h3>Zaten üye misiniz ?</h3>
									{{--<p>Üyelerimize özel ayrıcalıklardan faydalanabilmek için üye olun.</p>--}}
									<a href="#" class="register-button button button--pink btn-singin">GİRİŞ YAP<span class="icon--arrow-right"><svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-arrow-right"></use></svg></span></a>

								</div>
						</div>


					</div>
				@else
					<div class="avatar-wrap">

						<figure>

							@if(Auth::user()->image)
								<img src="{{ Auth::user()->image }}">
							@else
								<div class="no-image">
									<span>
										{{ Auth::user()->noImageAvatar() }}
									</span>
								</div>
							@endif

						</figure>

						<h4>{{ Auth::user()->getFullName() }}</h4>

					</div>

					<ul>

						<li><a href="{{ route('user.offers') }}">TEKLİFLERİM</a></li>

						<li><a href="{{ route('user.lists') }}">LİSTELERİM</a></li>

						<li><a href="{{ route('user.favorites') }}">BEĞENDİKLERİM</a></li>

						<li><a href="{{ route('user.comments') }}">YORUMLARIM</a></li>

						<li><a href="{{ route('user.settings') }}">HESABIM</a></li>

					</ul>

					<a href="{{ route('user.logout') }}" class="exit-button"><svg class="exit-icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/exit-icon.svg#exit-icon"></use></svg><span>ÇIKIŞ YAP</span></a>
				@endif

			</div>

		</div>
		
	</div>

</header>