@extends('_website.pages.blog.layouts.master')
@section('content')
	{{-- Code Blog --}}

    <div class="search-result-page">
        <div class="profil-head">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="result-title">
                            <h2><span>“</span> {{ Request::input('ara') }} <span>”</span></h2>
                        </div>
                        @if($items->count() > 0)
                            <div class="result-count-title">
                                <div class="list-length">
                                    <svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-search-list"></use></svg>
                                    <span><strong>{{ $items->count() }} </strong> BLOG LİSTELENDİ</span>
                                </div>
                            </div>
                        @endif
                        @if($items->count() <= 0)
                            <span class="empty-message">
                                Aradığınız sonuç bulunamadı.
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="blog-wrapper">
        {{--<div class="blog-header">--}}
            {{--<img src="/files//home-slider-image.jpg" class="blog-header__img">--}}
            {{--<div class="overlay">--}}
                {{--<h1 class="blog-header__title text-center">BLOG</h1>--}}
                {{--<p class="blog-header__txt">Hemen ücretsiz profil oluşturun. Memnun kalırsanız premium paket seçin!</p>--}}

            {{--</div>--}}
        {{--</div>--}}

        <div class="blog-content">

            <div class="container">
                <div class="row">
                    @foreach($items as $row)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="blog__item">
                            <div class="blog__item-top">
                                <img src="{{ $row->cover }}" alt="blog alt" class="blog__item-img">
                                <a href="{{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}" class="overlay">
                                    <span class="overlay-txt">Detaya Git</span>
                                </a>
                                <div class="blog__item-share">
                                    <div class="btn-share">
                                        <svg class="icon icon-share"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-share"></use></svg>
                                    </div>
                                    <div class="share-wrp">
                                        <div class="share-group">
                                            <a href="https://plus.google.com/share?url={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                class="share__item google">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-google-plus"></use></svg>
                                            </a>
                                            <a href="https://twitter.com/share?url={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}&via=dugunatolyeleri&related=twitterapi%2Ctwitter&hashtags=dugunatolyeleri&text={{ $row->title }}" target="_blank"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                              class="share__item twitter">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-twitter"></use></svg>
                                            </a>

                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('get.blog.detail', [ 'slug' => $row->slug, 'id' =>  $row->id]) }}" target="_blank" class="share__item facebook"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-facebook"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog__item-content">
                                <a href="#" class="blog__item-title">{{ $row->title }}</a>
                                <div class="blog__item-bottom">
                                    <span class="blog__item-date">{{  $row->created_at->diffForHumans() }}</span>
                                    <div class="blog__item-meta">
                                        <span class="meta__item">
                                            <span class="meta__item-icon">
                                                <svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use></svg>
                                            </span>
                                            <span class="meta__item-count">
                                                {{ $row->view_count }}
                                            </span>
                                        </span>
                                        <span class="meta__item">
                                            <span class="meta__item-icon ">
                                                <svg class="icon icon-comment-open"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-comment"></use></svg>
                                            </span>
                                                    <span class="meta__item-count">
                                                {{ $row->comments->count() }}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
     <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endpush