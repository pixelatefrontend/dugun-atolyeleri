<div class="blog-comments">
	<div class="comments-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					@if($item->comments->count() > 0)
					<div class="title">
						<h3>YORUMLAR</h3>
						<span>( {{ count($item->comments) }} YORUM )</span>
					</div>
					<ul class="comment-list">
						@foreach($item->comments as $comment)
							<li>
								<div class="profil-area">
									<figure>
										<!-- <img src="/assets/_website/img/avatar.jpg"> -->
										{{-- <span>{{ $comment->getShortName() }}</span> --}}
										<span class="blog-comment-pp" style="text-transform: uppercase;" data-name="{{ $comment->name }}" data-lname="{{ $comment->lname }}"></span>
										{{--<span class="comment-pp-name" style="text-transform: uppercase;" data-female="22" data-male="aa"></span>--}}

									</figure>
									<strong>{{ $comment->name }} {{ $comment->lname }} </strong>
								</div>
								<div class="comment-area">
									<strong>{{ $comment->title }}</strong>
									<p>{{ $comment->content }}</p>
									<div class="comment-bottom">
										<span>{{ $comment->getCreatedAt() }}</span>
									</div>

								</div>
							</li>
						@endforeach
					</ul>
					<div class="buttons">
						<a href="#" id="comment-button" data-toggle="modal" data-target="#comment-modal" class="add-comment"> <i class="fa fa-plus" aria-hidden="true"></i> YORUM YAP</a>
					</div>
					@else
						<div class="title">
							<h3>YORUMLAR</h3>
						</div>
						<h4>Buraya henüz yorum yapılmadı. İlk Yorumu Sen Yap!</h4>

						<br>
                        <div class="buttons no-comment">
                            <a href="#" id="comment-button" data-toggle="modal" data-target="#comment-modal" class="add-comment"> <i class="fa fa-plus" aria-hidden="true"></i> YORUM YAP</a>
                        </div>
					@endif
				</div>
			</div>
		</div>
	</div>

	<div id="comment-modal" class="modal fade sign-modal bid-modal fav-modal comment-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>Yorum Yap</h2>
            </div>
            <div class="modal-body">
				{!! Form::open([ 'url' => route('add.comment.blog', $item->id), 'id' => 'commentForm' ] ) !!}
				<div class="comments-add-wrap">
					<div class="row">
						<div class="col-md-6">
							<div class="input__group input-container input-control">
								<input type="text" name="name"  placeholder="Ad" value="{{{ Auth::check() ? Auth::user()->name : '' }}}">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input__group input-container input-control">
								<input type="text" name="lname"  placeholder="Soyad" value="{{{ Auth::check() ? Auth::user()->lname : '' }}}">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="input__group input-container input-control">
								<input type="text" name="email"  placeholder="E-posta" value="{{{ Auth::check() ? Auth::user()->email : '' }}}">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="col-md-12">
							{{--<input type="text" name="title"  placeholder="Yorum Başlığı">--}}
							<div class="input__group input-container input-control">
								<input type="text" name="title"  placeholder="Yorum Başlığı">
								<div class="error-message">Bu alanı boş bırakamazsınız.</div>
							</div>
						</div>

						<div class="col-md-12">
							<div class="input__group input-container input-control textarea-control">
								<textarea name="content" placeholder="Mesajınızı buraya giriniz."></textarea>
								<div class="error-message">Burayı boş bırakamazsınız.</div>
							</div>
						</div>
						<div class="foot col-md-12">
							<span>Mesajınız sistemimize iletildikten sonra yüklenecektir</span>
							<button type="submit" class="no-button form-control-button" id="commentButton">GÖNDER </button>
							<div class="loading" id="loadStatus">
								<img src="/assets/_website/img/load.gif" alt="">
							</div>
						</div>
					</div>
				</div>
				{!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div id="comment-success-modal" class="modal fade sign-modal bid-modal bid-success-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2>YORUM GÖNDERİLDİ</h2>
            </div>
            <div class="modal-body">
				<div class="row">
					<div class="success-icon">
                        <span><svg class="success-icon-pink"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/success-icon.svg#success-icon"></use></svg></span>
                    </div>
                    <h4>Yorumunuz incelendikten sonra yayınlanacaktır!</h4>
                    <span style="font-size: 17px; color: #999eab">Teşekkür Ederiz</span>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
