@extends('_website.pages.blog.layouts.master')
@section('content')
    <div class="blog-details-wrapper">
        <div class="blog-details-header">
            <img src="{{ $item->cover }}">
            <div class="overlay">
                <div class="details-content">
                    <div class="blog-details-header-title">{{ $item->title }}</div>
                    <div class="blog-category">{{ $item->category->title }}</div>
                </div>
            </div>
        </div>

        <div class="blog-details-content">
            <div class="container">
                <div class="row">
                    <div class="content-header">

                        <div class="content-header-meta">

                            <div class="content-header-item">
                                <span class="blog__item-date">
                                    <span class="meta__item-icon">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                                        {{ $item->created_at->diffForHumans() }}
                                </span>
                            </div>

                            <div class="content-header-item">
                                <span class="meta__item">
                                    <span class="meta__item-icon">
                                        <svg class="icon icon-comment-open"><use
                                                    xmlns:xlink="http://www.w3.org/1999/xlink"
                                                    xlink:href="/assets/_website/img/icons.svg#icon-comment"></use></svg>
                                    </span>
                                            <span class="meta__item-count">
                                        {{ $item->comments->count() }}
                                    </span>
                                </span>

                                <span class="meta__item">
                                    <span class="meta__item-icon">
                                        <svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                               xlink:href="/assets/_website/img/icons.svg#icon-eye"></use></svg>
                                    </span>
                                    <span class="meta__item-count">
                                        {{ $item->view_count }}
                                    </span>

                                </span>
                            </div>

                            <div class="content-header-item">

                                 <span class="meta__item blog-share">
                                    <span class="meta__item-icon">
                                        <svg class="icon icon-share"><use xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                          xlink:href="/assets/_website/img/icons.svg#icon-share"></use></svg>
                                    </span>
                                    <span class="meta__item-count">
                                        Paylaş
                                    </span>
                                </span>
                              <div class="share-wrp">
                                        <div class="share-group">
                                            <a href="https://plus.google.com/share?url={{ route('get.blog.detail', [ 'slug' => $item->slug, 'id' =>  $item->id]) }}"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                class="share__item google">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-google-plus"></use></svg>
                                            </a>
                                            <a href="https://twitter.com/share?url={{ route('get.blog.detail', [ 'slug' => $item->slug, 'id' =>  $item->id]) }}&via=dugunatolyeleri&related=twitterapi%2Ctwitter&hashtags=dugunatolyeleri&text={{ $item->title }}" target="_blank"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                              class="share__item twitter">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-twitter"></use></svg>
                                            </a>
                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('get.blog.detail', [ 'slug' => $item->slug, 'id' =>  $item->id]) }}" target="_blank" class="share__item facebook"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-facebook"></use></svg>
                                            </a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="blog-details-inner">
                        {!! $item->content !!}
                        @if($item->images->count() > 0)
                        <div class="slider-wrp">
                            <span class="blog-slider-arrow blog-details-slider-prev">
                                <i class="fa fa-chevron-left" aria-hidden="true"></i>
                            </span>
                            <span class="blog-slider-arrow blog-details-slider-next">
                                <i class="fa fa-chevron-right" aria-hidden="true"></i>
                            </span>
                            <div class="blog-details-slider">
                                @foreach($item->images as $image)
                                <div class="blog-details-slider-item">
                                    <img src="/uploads/blog/{{ $image->filename }}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="blog-comments">
            @include('_website.pages.blog.partials.comments')
        </div>
        <div class="blog-others">
            <div class="container">
                <div class="row">
                    @if($posts->count() >= 0)
                        <div class="title">BENZER YAZILAR</div>
                    @endif
                    @foreach($posts as $post)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="blog__item">
                            <div class="blog__item-top">
                                <img src="{{$post->cover}}" alt="" class="blog__item-img">
                                <a href="{{ route('get.blog.detail', [ 'slug' => $post->slug, 'id' =>  $post->id]) }}" class="overlay">
                                    <span class="overlay-txt">Detaya Git</span>
                                </a>
                                <div class="blog__item-share">
                                    <div class="btn-share">
                                        <svg class="icon icon-share"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-share"></use></svg>
                                    </div>

                                    <div class="share-wrp">
                                        <div class="share-group">
                                            <a href="https://plus.google.com/share?url={{ route('get.blog.detail', [ 'slug' => $post->slug, 'id' =>  $post->id]) }}"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                                class="share__item google">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-google-plus"></use></svg>
                                            </a>
                                            <a href="https://twitter.com/share?url={{ route('get.blog.detail', [ 'slug' => $post->slug, 'id' =>  $post->id]) }}&via=dugunatolyeleri&related=twitterapi%2Ctwitter&hashtags=dugunatolyeleri&text={{ $post->title }}" target="_blank"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                              class="share__item twitter">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-twitter"></use></svg>
                                            </a>

                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('get.blog.detail', [ 'slug' => $post->slug, 'id' =>  $post->id]) }}" target="_blank" class="share__item facebook"
                                                onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                                <svg class="icon icon-twitter"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-facebook"></use></svg>
                                            </a>
                                        </div>
                                    </div>

                                </div>


                            </div>

                            <div class="blog__item-content">
                                <a href="#" class="blog__item-title">{{ $post->title }}</a>
                                <div class="blog__item-bottom">
                                    <span class="blog__item-date">
                                        <span class="meta__item-icon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>{{ $post->created_at->diffForHumans() }}</span>
                                    <div class="blog__item-meta">
                                        <span class="meta__item">
                                            <span class="meta__item-icon">
                                                <svg class="icon"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-eye"></use></svg>
                                            </span>
                                            <span class="meta__item-count">
                                                {{ $post->view_count }}
                                            </span>
                                        </span>

                                        <span class="meta__item">
                                            <span class="meta__item-icon ">
                                                <svg class="icon icon-comment-open"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#icon-comment"></use></svg>
                                            </span>
                                                    <span class="meta__item-count">
                                                {{ $post->comments->count() }}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $('form#commentForm').submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            $('#commentButton').attr('disabled', 'disabled');
            $.ajax({
                method: 'POST',
                url: "{{ route('add.comment.blog', $item->id) }}",
                data: data,
                success: function(response){
                    if(response.success){
                        $('#comment-modal').modal('hide');
                        $('#comment-success-modal').modal('show');
                        $('input:not(.form-control-button)').val('');
                    }
                }
            });
        });
    </script>
     <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
@endpush