<!DOCTYPE html>
<html lang="tr" class="no-js">
<head>
    <!-- Meta-Information -->
    <title>Admin Panel | Düğün Atölyeleri</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="svg/xml" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php header("Content-Type: application/json;charset=utf-8"); ?>
    <meta name="robots" content="noindex, nofollow">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />
    {!! Html::style('assets/_panel/css/bootstrap.min.css') !!}
    {!! Html::style('assets/_panel/css/select2.min.css') !!}
    {!! Html::style('assets/_panel/css/sweetalert2.min.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/main.css') !!}
    {!! Html::style('assets/_panel/css/panel.css') !!}
    {!! Html::style('assets/_panel/css/component.css') !!}
    {!! Html::style('assets/_panel/css/icons.css') !!}
    {!! Html::style('assets/_panel/css/dropzone.css') !!}
    {!! Html::style('assets/_panel/css/custom.css') !!}
    {!! Html::style('//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css') !!}
    <script>(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);</script>
</head>
<body>
	@include('_admin.layouts.header')
	@include('_admin.layouts.sidebar')
	@yield('content')

	{{-- <script src="https://maps.google.com/maps/api/js?sensor=false"></script> --}}
	{!! Html::script('assets/_panel/js/jquery-2.1.3.js') !!}
    {!! Html::script('assets/_panel/js/jquery-ui.min.js') !!}
	{!! Html::script('assets/_panel/js/bootstrap.min.js') !!}
	{!! Html::script('assets/_panel/js/app.js') !!}
	{!! Html::script('assets/_panel/js/common.js') !!}
	{!! Html::script('assets/_panel/js/main.js') !!}
  	{!! Html::script('assets/_panel/js/sweetalert2.min.js') !!}
  	{!! Html::script('assets/_panel/js/select2.min.js') !!}
	{!! Html::script('assets/_panel/js/slugify.js') !!}
    {!! Html::script('assets/_panel/js/dropzone.js') !!}
    {!! Html::script('assets/_panel/js/jquery.friendurl.min.js') !!}
	{!! Html::script('assets/_panel/js/custom-file-input.js') !!}
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

    <script>
      var editor_config = {
        path_absolute : "/files",
        selector: "textarea#blogContent",
        height: 500,
        theme: 'modern',
        content_css : '/assets/_panel/css/panel.css',
        plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern",
          "wordcount",
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor | mybutton",
        relative_urls: true,
        remove_script_host: false,
        file_browser_callback : function(field_name, url, type, win) {
          var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
          var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

          var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
          if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
          } else {
            cmsURL = cmsURL + "&type=Files";
          }

          tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
          });
        },
        setup: function(editor) {
            editor.addButton('mybutton', {
              type: 'splitbutton',
              text: 'Kampanya Ekle',
              icon: false,
              onclick: function() {
                  $('.blog-popup__title').text('KAMPANYA EKLE');
                $('.blog-popup').addClass('active');
                $('.blog-popup__overlay').addClass('active');
                $('.blog-popup__inner').hide();
                $('#campaignPopup').show();
              },
              menu: [{
                text: 'Kampanya Ekle',
                onclick: function() {
                    $('.blog-popup__title').text('KAMPANYA EKLE');
                    $('.blog-popup').addClass('active');
                    $('.blog-popup__overlay').addClass('active');
                }
              }, {
                  text: 'Atölye Ekle',
                  onclick: function() {
                      $('.blog-popup__title').text('ATÖLYE EKLE');
                      $('.blog-popup').addClass('active');
                      $('.blog-popup__overlay').addClass('active');
                      $('.blog-popup__inner').hide();
                      $('#workshopPopup').show();
                  }
              }, {
                  text: 'Video Ekle',
                  onclick: function() {
                      $('.blog-popup__title').text('VİDEO EKLE');
                      $('.blog-popup').addClass('active');
                      $('.blog-popup__overlay').addClass('active');
                      $('.blog-popup__inner').hide();
                      $('#videoPopup').show();
                  }
              }]
            });
          }
      };

      tinymce.init(editor_config);

      $('#campaignPopup .blog-popup__submit').click(function (e) {
          e.preventDefault();
          var   campaignTitle = $('#campaignTitle').val(),
              campaignLinkTitle = $('#campaignLinkTitle').val(),
              campaignLink = $('#campaignLink').val();

          $('.blog-popup').removeClass('active');
          $('.blog-popup__overlay').removeClass('active');
          tinyMCE.activeEditor.insertContent('<div class="blog-campaign"><h2 class="blog-campaign__title">' + campaignTitle + '</h2><a href="' + campaignLink + '" class="blog-campaign__btn">' + campaignLinkTitle + '</a></div><br/>');
      });

      $('#workshopPopup .blog-popup__submit').click(function (e) {
          e.preventDefault();
          var workshopLink = $('#workshopLink').val();
          $('.blog-popup').removeClass('active');
          $('.blog-popup__overlay').removeClass('active');
          tinyMCE.activeEditor.insertContent('<a href="'+ workshopLink +'" class="workshop-link">Atölyeye Git</a><br/><br/>');
      });

      $('#videoPopup .blog-popup__submit').click(function (e) {
          e.preventDefault();
          var videoID = $('#videoID').val(),
              videoType = $('#videoType').val();
          $('.blog-popup').removeClass('active');
          $('.blog-popup__overlay').removeClass('active');
          if(videoType == "youtube") {
              tinyMCE.activeEditor.insertContent('<div class="video-popup__iframe"><iframe id="ytplayer" type="text/html" width="720" height="405" src="https://www.youtube.com/embed/'+ videoID +'?showinfo=0" frameborder="0" allowfullscreen></iframe></div><br/><br/>');
          }

          else if(videoType == "vimeo") {
              tinyMCE.activeEditor.insertContent('<div class="video-popup__iframe"><iframe id="ytplayer" type="text/html" width="720" height="405" src="https://player.vimeo.com/video/'+ videoID +'?autoplay=0&loop=1&title=0&byline=0&portrait=0" frameborder="0" allowfullscreen></iframe></div><br/><br/>');
          }
      });

      function test(editor) {
          tinyMCE.activeEditor.insertContent('&nbsp;<em>You clicked menu item 2!</em>');
      }

    </script>

    {!! Html::script('assets/_panel/js/custom.js') !!}
	@stack('script')
    {!! Html::script('assets/_panel/js/panel.js') !!}
    <script>
       setTimeout(function(){
            $('#mceu_39').hide();
            $('#mceu_40').hide();
            $('#mceu_41').hide();
            $('#mceu_42').hide();
            $('#mceu_43').hide();
            $('#mceu_44').hide();
            $('#mceu_45').hide();
        }, 2000);
            
    </script>

	<!--[if lte IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>      
	<![endif]-->
</body>
</html>
