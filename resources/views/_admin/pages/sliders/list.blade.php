@extends('_admin.master')
@section('content')
<div class="main-content">
     <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                <a href="{{ route('sliders.create') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-plus"></i></span>
                                <h3>Slider Ekle</h3>
                                <p>Yeni Slider Oluştur</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Slider Tablosu</h3>
                            <span>{{count($items)}} Slider Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Başlık</th>
                                        <th>Kapak Resmi</th>
                                        <th>Durum</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{$item->sort}}</td>
                                        <td>{{ $item->title }}</td>
                                        <td><img src="{{ $item->image }}" alt="" height="50"></td>
                                        <td>
                                        @if($item->active)
                                            <label class="label label-success">Yayında</label>
                                        @else
                                            <label class="label label-warning">Beklemede</labe>
                                        @endif
                                        </td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td class="f-r b-none">
                                            <a href="{{route('sliders.edit', $item->id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ 'sliders.destroy', $item->id]) !!}
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Slider Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection