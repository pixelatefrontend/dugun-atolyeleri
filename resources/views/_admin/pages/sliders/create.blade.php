@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Slider Ekle</h3>
                        </div>
                        {!! Form::open([ 'url' => route('sliders.store'), 'class' => 'sec', 'role' => 'form' ]) !!}
                            <div class="row">
                                <div class="col-md-12 mb">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                                <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                            </a>
                                        </span>
                                        <input id="cover_picture" class="form-control custom-input" type="text" name="image" placeholder="Kapak Resmi">
                                    </div>
                                    <img id="cover_preview" style="margin-top:15px;max-height:100px;">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="Başlık" class="form-control custom-input" name="title">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="Buton Yazısı" class="form-control custom-input" name="button_text">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" class="form-control custom-input" name="url" placeholder="Link">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <select name="active" id="access" class="form-control custom-input" placeholder="Seçiniz">
                                            <option value="" disabled selected hidden>Durum</option>
                                            <option value="1">Aktif</option>
                                            <option value="0">Pasif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="description" id=""  rows="5" class="form-control custom-input" placeholder="Açıklama"></textarea>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="record-buttons">
                                        <button class="btn btn-edit" type="submit">
                                            <i class="fa fa-plus"></i> Slider Ekle
                                        </button>
                                        <a href="{{ route('sliders.index') }}">
                                            <button class="btn btn-trash" type="button">
                                                <i class="fa fa-remove"></i> İptal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush