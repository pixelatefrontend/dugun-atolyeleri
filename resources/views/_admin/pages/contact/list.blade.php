@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>İletişim Tablosu</h3>
                            <span>{{count($items)}} İletişim Formu Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ad</th>
                                        <th>Email</th>
                                        <th>Gönderim Tarihi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr value="{{ $item->id }}">
                                        <td>{{$item->id}}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td> 
                                            <button data-toggle="modal" data-target="#contactForm{{$loop->iteration}}"  class="btn btn-edit">Detayları Görüntüle</button>
                                            {!! stuff_destroy([ 'contact.destroy', $item->id]) !!}
                                            @if($item->active == 1)
                                                <i class="fa fa-eye"></i>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Data Yok</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@forelse($items as $item)
    <div id="contactForm{{$loop->iteration}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Teklif Detayları</h4>
          </div>
          <div class="modal-body">
            <h5>Adı</h5>
            <p>{{ $item->name }}</p>
            <h5>E-mail</h5>
            <p>{{ $item->email }}</p>
            <h5>Mesaj</h5>
            <p>{{ $item->message }}</p>
            <h5>Gönderim Tarihi</h5>
            <p>{{ $item->created_at->diffForHumans() }}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
@empty
@endforelse
@endsection
@push('script')
{{-- <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function read(item){
        var id = $(item).parent().parent().val();
        console.log(id);
        $.ajax({
            method: 'POST',
            url: '/contact-form/'+id,
            data: { id }, 
        });
        $(item).appendTo(" <i class='fa fa-eye'></i> ");
    }
</script> --}}
@endpush