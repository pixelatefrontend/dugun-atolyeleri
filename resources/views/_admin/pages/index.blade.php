@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="mini-stats-sec">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="red-skin"><i class="fa fa-eye"></i></span>
                                    <p>Anlık Ziyaretçi</p>
                                    <h3>{{ $activeUser }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="sky-skin"><i class="fa fa-code-fork"></i></span>
                                    <p>Atölye Sayısı</p>
                                    <h3>{{ $workshops->where('active', 1)->count() }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="purple-skin"><i class="fa fa-users"></i></span>
                                    <p>Kullanıcı Sayısı</p>
                                    <h3>{{ $users->count() }}</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="widget">
                                <div class="widget-controls">
                                    <span class="refresh-content"><i class="fa fa-refresh"></i></span>
                                </div><!-- Widget Controls -->
                                <div class="mini-stats ">
                                    <span class="pink-skin"><i class="fa fa-envelope"></i></span>
                                    <p>Teklifler</p>
                                    <h3>{{ $offers->count() }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- Mini stats Sec -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>En çok ziyaret edilen sayfalar</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>URL</th>
                                        <th>Görüntülenme</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                @foreach($analytics as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><a href="{{ env('URL')}}{{ $item['url'] }}" target="_blank">{{ $item['url'] }}</a></td>
                                        <td>{{ $item['pageViews'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>En çok ziyaret edilen atölyeler</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Atölye</th>
                                        <th>Görüntülenme</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                @foreach($workshops->sortByDesc('view_count')->slice(0, 20) as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->view_count }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>En çok beğenilen atölyeler</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Atölye</th>
                                        <th>Beğenme</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                @foreach($workshops->sortByDesc('like')->slice(0, 20) as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->like }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Kategoriye göre en çok etkileşim alan atölye</h3><br>
                            {{ Form::select('category', $categories, null, ['id' => 'workshopByCategory' ,'class' => 'form-control custom-input', 'placeholder' => 'Kategori Seçiniz']) }}
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped" id="table-content">
                                <thead>
                                    <tr id="tableDesc">
                                        <th>#</th>
                                        <th>Atölye</th>
                                        <th>Beğenme</th>
                                        <th>Görüntülenme</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                @foreach($workshops->sortByDesc('like')->sortByDesc('view_count')->slice(0, 20) as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td id="title" >{{ $item->title }}</td>
                                        <td id="like" >{{ $item->like }}</td>
                                        <td id="view_count" >{{ $item->view_count }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
    <script>
        $('select#workshopByCategory').change(function(){
            var data = $(this).val();
            var items = [];
            $('#table-content tr:not(#tableDesc)').remove();
            $.ajax({
                method: 'POST',
                url: "{{ route('workshopByCategory') }}",
                data: {category_id:data},
                success: function(response){
                    $.each(response.items[0].workshops, function(i,tutorial){
                        json_data = '<tr>'+
                            '<td>'+ i +'</td>'+
                            '<td>'+tutorial.title+'</td>'+
                            '<td>'+tutorial.like+'</td>'+
                            '<td>'+tutorial.view_count+'</td>'+
                            '</tr>';
                        $(json_data).appendTo('#table-content');
                        });
                }
            });
        });
    </script>
@endpush