@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Kategori Düzenle</h3>
                        </div>
                        {!! Form::open([ 'url' => route('categories.update', $item->id), 'class' => 'sec', 'role' => 'form' ]) !!}
                        {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6 mb">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                                <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                            </a>
                                        </span>
                                        <input id="cover_picture" class="form-control custom-input" type="text" name="cover_picture" placeholder="Kapak Resmi" value="{{ $item->cover_picture }}">
                                    </div>
                                    <img src="{{$item->cover_picture}}" id="cover_preview" style="margin-top:15px;max-height:100px; width: 100%;">
                                </div>
                                <div class="col-md-6 mb">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="icon" data-input="icon_image" data-preview="icon_preview" class="btn btn-trash" style="height: 45px;">
                                                <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i>
                                            </a>
                                        </span>
                                        <input id="icon_image" class="form-control custom-input" type="text" name="icon_image" placeholder="İcon Resmi (SVG)" value="{{ $item->icon }}">
                                    </div>
                                    <img src="{{$item->icon}}" id="icon_preview" style="margin-top:15px;max-height:100px;">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Başlık</label>
                                        <input type="text" placeholder="Başlık" class="form-control custom-input" name="title" id="title" value="{{ $item->title }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>SEF Link (SEO)</label>
                                        <input type="text" placeholder="SEF Link" class="form-control custom-input" name="slug" id="slug" value="{{ $item->slug }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Açıklama</label>
                                        <textarea name="description" id=""  rows="5" class="form-control custom-input" placeholder="Açıklama">{{ $item->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Keywords</label>
                                        <textarea name="keywords" id=""  rows="5" class="form-control custom-input" placeholder="Açıklama">{{ $item->keywords }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                            <label>Durum</label>
                                            {!! Form::select('active',
                                                [ '1' => 'Aktif', '0' => 'Pasif' ],
                                                $item->active,
                                                ['class' => 'form-control custom-input']) !!}
                                    </div>
                                </div>
                                <div class="col-md-12 m-b">
                                    <div class="form-group">
                                            <label>Reklam Panosuna Gelecek Atölye</label>
                                            {!! Form::select('workshop',
                                                $workshops,
                                                $item->workshop_id,
                                                ['class' => 'form-control custom-input', 'placeholder' => 'Seçiniz...']) !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="record-buttons">
                                        <button class="btn btn-edit" type="submit">
                                            <i class="fa fa-edit"></i> Kategori Düzenle 
                                        </button>
                                        <a href="{{ route('categories.index') }}">
                                            <button class="btn btn-trash" type="button">
                                                <i class="fa fa-remove"></i> İptal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush 