@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
                <div class="widget">
                 <div class="form-elements-sec">
                    <div class="widget-title">
                        <h3>Blog Ekle</h3>
                    </div>
                    {!! Form::open([ 'url' => route('blogs.store'), 'class' => 'sec', 'role' => 'form', 'files' => true ]) !!}
                    <div class="row">
                        <div class="col-md-12 mb">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                        <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                    </a>
                                </span>
                                <input id="cover_picture" class="form-control custom-input" type="text" name="cover" placeholder="Kapak Resmi">
                            </div>
                            <img id="cover_preview" style="margin-top:15px;max-height:100px;">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <input type="text" placeholder="Başlık" class="form-control custom-input" name="title">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <input type="text" placeholder="Kısa Açıklama" class="form-control custom-input" name="description">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                {!! Form::select('category',
                                $categories,
                                null,
                                ['class' => 'form-control custom-input', 'placeholder' => 'Blog Kategorisi']) !!}
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <select name="active" id="access" class="form-control custom-input" placeholder="Seçiniz">
                                    <option value="" disabled selected hidden>Durum</option>
                                    <option value="1">Aktif</option>
                                    <option value="0">Pasif</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="content" id="blogContent"  rows="5" class="form-control custom-input"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="form-elements-sec">
                    <label class="row">
                        <input type="checkbox" id="galleryCheck" name="imageCheck"> Resim Galerisi Ekle
                    </label>
                    <hr>
                    <div class="row" id="galleryImages" style="display: none">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="file" name="images[]" class="form-control custom-input" multiple>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="record-buttons">
                <button class="btn btn-edit" type="submit">
                    <i class="fa fa-plus"></i> Blog Ekle
                </button>
                <a href="{{ route('blogs.index') }}">
                    <button class="btn btn-trash" type="button">
                        <i class="fa fa-remove"></i> İptal
                    </button>
                </a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div><!-- Panel Content -->
</div><!-- Main Content -->
    <div class="blog-popup__overlay"></div>
    <div class="blog-popup">
        <div class="blog-popup__header">
            <h2 class="blog-popup__title">KAMPANYA EKLE</h2>
            <a href="#" class="blog-popup__close">
                <i class="fa fa-times"></i>
            </a>
        </div>

        <div class="blog-popup__inner" id="campaignPopup">

            <form action="">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="input-group">
                        <input type="text" placeholder="Başlık" class="form-input" id="campaignTitle">
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="input-group">
                        <input type="text" placeholder="Link Başlığı" class="form-input" value="FİRMALARI İNCELE" id="campaignLinkTitle">
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="input-group">
                        <input type="text" placeholder="Link" class="form-input" id="campaignLink">
                    </div>
                </div>

                <button class="blog-popup__submit">EKLE</button>
            </form>

        </div>



        {{--video popup--}}

        <div class="blog-popup__inner" id="videoPopup">

            <form action="">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="input-group">
                        <select name="" id="videoType" class="form-input">
                            <option value="" selected hidden disabled>Video Türü</option>
                            <option value="youtube">YouTube</option>
                            <option value="vimeo">Vimeo</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="input-group">
                        <input type="text" placeholder="Video ID" class="form-input" id="videoID">
                    </div>
                </div>

                <button class="blog-popup__submit">EKLE</button>
            </form>

        </div>

        {{--workshop popup--}}

        <div class="blog-popup__inner" id="workshopPopup">

            <form action="">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="input-group">
                        <input type="text" placeholder="Link" class="form-input" id="workshopLink">
                    </div>
                </div>

                <button class="blog-popup__submit">EKLE</button>
            </form>

        </div>

    </div>

@endsection
@push('script')
<script>
    $('#galleryCheck').change(function(){
        if(this.checked){
            $('#galleryImages').show();
        }else{
            $('#galleryImages').hide();
        }
    });
</script>

@endpush