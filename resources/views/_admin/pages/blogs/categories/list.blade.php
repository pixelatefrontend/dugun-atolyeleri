@extends('_admin.master')
@section('content')
<div class="main-content">
     <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                <a href="{{ route('blog-categories.create') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-plus"></i></span>
                                <h3>Blog Kategorisi Ekle</h3>
                                <p>Yeni Kategori Oluştur</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Kategori Tablosu</h3>
                            <span>{{count($items)}} Kategori Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Başlık</th>
                                        <th>Kapak Resmi</th>
                                        <th>Durum</th>
                                        <th>İşlemler</th>
                                    </tr>
                                </thead>
                                <tbody id="sortable">
                                @forelse($items as $item)
                                    <tr id="{{ $item->id }}">
                                        <td>{{$item->sort}}</td>
                                        <td>{{ $item->title }}</td>
                                        <td><img src="{{ $item->cover}}" alt="" height="50"></td>
                                        <td>
                                        @if($item->active)
                                            <label class="label label-success">Yayında</label>
                                        @else
                                            <label class="label label-warning">Beklemede</label>
                                        @endif
                                        </td>
                                        <td >
                                            <a href="{{route('blog-categories.edit', $item->id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ 'blog-categories.destroy', $item->id]) !!}
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Kategori Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
<div class="custom-alert alert alert-success" id="notification">
   <i class="fa fa-check"></i>
   <span>Kaydedildi</span>
   <p> &nbsp; &nbsp;&nbsp; Sıralama başarılı bi şekilde değiştirildi</p>
</div>
@endsection
@push('script')
  <script>
    $("#sortable").sortable({
        update: function( event, ui ) {
            var order = $(this).sortable('toArray');
            console.log(order);
            $.post('{{route('blog.categorie.sortable')}}', { order: order })
            .done(function(){
              $('.custom-alert').fadeIn(1000).delay(3000).fadeOut('slow');
            });
        }
    });
  </script>
@endpush