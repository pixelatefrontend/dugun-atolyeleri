@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Kategori Düzenle</h3>
                        </div>
                        {!! Form::open([ 'url' => route('blog-categories.update', $item->id), 'class' => 'sec', 'role' => 'form' ]) !!}
                        {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6 mb">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                                <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                            </a>
                                        </span>
                                        <input id="cover_picture" class="form-control custom-input" type="text" name="cover" placeholder="Kapak Resmi" value="{{ $item->cover }}">
                                    </div>
                                    <img src="{{$item->cover}}" id="cover_preview" style="margin-top:15px;max-height:100px; width: 100%;">
                                </div>
                                <div class="col-md-6 mb">
                                <div class="custom-admin-select">
                                    <div class="image-show">

                                        <svg class="icon davetiye">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/files/svg/icons.svg#{{ $item->icon  }}"></use>
                                        </svg>

                                    </div>

                                    <div class="select-inner">
                                        {!! Form::select('icon',
                                                [ 
                                                    'blog-dugunzedeler' => 'DÜĞÜNZEDELER',
                                                    'blog-haberler' => 'HABERLER',
                                                    'blog-roportajlar' => 'RÖPORTOJLAR',
                                                    'blog-oneriler' => 'ÖNERİLER',
                                                    'blog-secmeler' => 'SEÇMELER',
                                                    'blog-galeriler' => 'GALERİLER',
                                                    'blog-mutlu-ciftler' => 'MUTLU ÇİFTLER',
                                                ],
                                                $item->icon,
                                                ['class' => 'form-control custom-input', 'id' => 'admin-category-select']) !!}

                                    </div>

                                </div>
                            </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Başlık</label>
                                        <input type="text" placeholder="Başlık" class="form-control custom-input" name="title" id="title" value="{{ $item->title }}">
                                    </div>
                                </div>

                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Açıklama</label>
                                        <textarea name="description" id=""  rows="5" class="form-control custom-input" placeholder="Açıklama">{{ $item->description }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                            <label>Durum</label>
                                            {!! Form::select('active',
                                                [ '1' => 'Aktif', '0' => 'Pasif' ],
                                                $item->active,
                                                ['class' => 'form-control custom-input']) !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="record-buttons">
                                        <button class="btn btn-edit" type="submit">
                                            <i class="fa fa-edit"></i> Kategori Düzenle 
                                        </button>
                                        <a href="{{ route('categories.index') }}">
                                            <button class="btn btn-trash" type="button">
                                                <i class="fa fa-remove"></i> İptal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush 