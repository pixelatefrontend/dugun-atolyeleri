@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
                <div class="widget">
                 <div class="form-elements-sec">
                    <div class="widget-title">
                        <h3>Yorum Düzenle</h3>
                    </div>
                    {!! Form::open([ 'url' => route('comments.update', $item->id), 'class' => 'sec', 'role' => 'form' ]) !!}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <label>Gelin</label>
                                <input type="text" value="{{ $item->female }}" placeholder="Başlık" class="form-control custom-input" name="female">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <label>Damat</label>
                                <input type="text" value="{{ $item->male }}" placeholder="Buton Yazısı" class="form-control custom-input" name="male">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <label>Başlık</label>
                                <input type="text" value="{{ $item->title }}" class="form-control custom-input" name="title" placeholder="Başlık">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <label>Durum</label>
                                {!! Form::select('active',
                                    [ '1' => 'Aktif', '0' => 'Pasif' ],
                                    $item->active,
                                    ['class' => 'form-control custom-input']) !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Yorum</label>
                                    <textarea name="content" id=""  rows="5" class="form-control custom-input" placeholder="Açıklama">{{ $item->content }}</textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="col-md-12">
                                <div class="record-buttons">
                                    <button class="btn btn-edit" type="submit">
                                        <i class="fa fa-edit"></i> Yorum Düzenle
                                    </button>
                                    <a href="{{ route('comments.index') }}">
                                        <button class="btn btn-trash" type="button">
                                            <i class="fa fa-remove"></i> İptal
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush