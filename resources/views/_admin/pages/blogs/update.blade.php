@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
                <div class="widget">
                 <div class="form-elements-sec">
                    <div class="widget-title">
                        <h3>Blog Düzenle</h3>
                    </div>
                    {!! Form::open([ 'url' => route('blogs.update', $item->id), 'class' => 'sec', 'role' => 'form', 'files' => true]) !!}
                    {{ method_field('PUT') }}
                     <div class="row">
                        <div class="col-md-12 mb">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                        <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                    </a>
                                </span>
                                <input id="cover_picture" class="form-control custom-input" type="text" name="cover" value="{{ $item->cover }}">
                            </div>
                            <img id="cover_preview" style="margin-top:15px;max-height:100px;" src="{{ $item->cover }}">
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <input type="text" placeholder="Başlık" class="form-control custom-input" name="title" value="{{ $item->title }}">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                <input type="text" placeholder="Kısa Açıklama" class="form-control custom-input" name="description" value="{{ $item->description }}">
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                {!! Form::select('category',
                                $categories,
                                $item->category->id,
                                ['class' => 'form-control custom-input']) !!}
                            </div>
                        </div>
                        <div class="col-md-6 m-b">
                            <div class="form-group">
                                {!! Form::select('active',
                                [ '1' => 'Aktif', '0' => 'Pasif' ],
                                $item->active,
                                ['class' => 'form-control custom-input']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="content" id="blogContent"  rows="5" class="form-control custom-input">{{ $item->content }}</textarea>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="form-elements-sec">
                        <label class="row">
                            <input type="checkbox" id="galleryCheck" name="imageCheck" {{{ count($item->images) > 0 ? 'checked' : ''}}}> Resim Galerisi Ekle
                        </label>
                        <hr>
                        <div class="row" id="galleryImages" {{{ count($item->images) > 0 ? '' : 'style=display:none;' }}} >
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="file" name="images[]" class="form-control custom-input" multiple>
                                </div>
                            </div>
                        </div>
                        <hr>
                        @foreach($item->images as $image)
                            <div class="col-md-3" style="margin-bottom: 10px;">
                                <img src="/uploads/blog/{{$image->filename}}" alt="" class="img-responsive">
                                <button type="button"
                                    class="btn btn-danger"
                                    style="width: 100%;" onclick="imageDestroy(this)" data-img-id="{{ $image->id }}"><i class="fa fa-trash"></i></button>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="record-buttons">
                    <button class="btn btn-edit" type="submit">
                        <i class="fa fa-edit"></i> Blog Düzenle
                    </button>
                    <a href="{{ route('blogs.index') }}">
                        <button class="btn btn-trash" type="button">
                            <i class="fa fa-remove"></i> İptal
                        </button>
                    </a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div><!-- Panel Content -->
</div><!-- Main Content -->
<div class="blog-popup__overlay"></div>
<div class="blog-popup">
    <div class="blog-popup__header">
        <h2 class="blog-popup__title">KAMPANYA EKLE</h2>
        <a href="#" class="blog-popup__close">
            <i class="fa fa-times"></i>
        </a>
    </div>

    <div class="blog-popup__inner" id="campaignPopup">

        <form action="">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="input-group">
                    <input type="text" placeholder="Başlık" class="form-input" id="campaignTitle">
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="input-group">
                    <input type="text" placeholder="Link Başlığı" class="form-input" value="FİRMALARI İNCELE" id="campaignLinkTitle">
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    <input type="text" placeholder="Link" class="form-input" id="campaignLink">
                </div>
            </div>

            <button class="blog-popup__submit">EKLE</button>
        </form>

    </div>



    {{--video popup--}}

    <div class="blog-popup__inner" id="videoPopup">

        <form action="">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="input-group">
                    <select name="" id="videoType" class="form-input">
                        <option value="" selected hidden disabled>Video Türü</option>
                        <option value="youtube">YouTube</option>
                        <option value="vimeo">Vimeo</option>
                    </select>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="input-group">
                    <input type="text" placeholder="Video ID" class="form-input" id="videoID">
                </div>
            </div>

            <button class="blog-popup__submit">EKLE</button>
        </form>

    </div>

    {{--workshop popup--}}

    <div class="blog-popup__inner" id="workshopPopup">

        <form action="">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="input-group">
                    <input type="text" placeholder="Link" class="form-input" id="workshopLink">
                </div>
            </div>

            <button class="blog-popup__submit">EKLE</button>
        </form>

    </div>

</div>
@endsection
@push('script')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
<script>
    $('#galleryCheck').change(function(){
        if(this.checked){
            $('#galleryImages').show();
        }else{
            $('#galleryImages').hide();
        }
    });
    function imageDestroy(item){
        var data = $(item).data('img-id');
        $.ajax({
            method: 'POST',
            url: '{{ route("blog.image.destroy") }}',
            data: { id : data },
            success: function(response){
                if(response.success){
                    $(item).parent().slideUp().remove();
                }
            }
        });
    }
</script>
@endpush