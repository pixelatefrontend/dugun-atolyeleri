@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                @if(Auth::guard('admin')->user()->isAdmin())
                    <a href="{{ route('admins.create') }}">
                @else
                    <a href="javascript:void(0)" onclick="return false;" style="cursor: not-allowed;">
                @endif
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-plus"></i></span>
                                <h3>Admin Ekle</h3>
                                <p>Yeni Kullanıcı Oluştur</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Admin Tablosu</h3>
                            <span>{{count($items)}} Admin Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ad Soyad</th>
                                        <th>E-mail</th>
                                        <th>İş Tanımı</th>
                                        <th>Yetki</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th>İşlemler</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name.' '.$item->lname }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->job_title }}</td>
                                        <td>{{ $item->role->name }}</td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td>
                                            @if(Auth::guard('admin')->user()->role->name == 'Admin' || Auth::guard('admin')->user()->id == $item->id)
                                            <a href="{{route('admins.edit', $item->id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ 'admins.destroy', $item->id]) !!}
                                            @else
                                                <small>Kullanıcı düzenlemek için yetkiniz yok!</small>
                                            @endif
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Kullanıcı Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection