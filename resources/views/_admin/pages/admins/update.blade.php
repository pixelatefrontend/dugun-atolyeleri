@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Admin Düzenle</h3>
                        </div>
                        {!! Form::open([ 'url' => route('admins.update', $item->id), 'class' => 'sec', 'role' => 'form', 'autocomplete' => 'false' ]) !!}
                        {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="text" autocomplete="false" placeholder="E-mail" class="form-control custom-input" name="email" id="email" value="{{ $item->email }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Şifre</label>
                                        <input type="password" autocomplete="false" placeholder="Şifre" class="form-control custom-input" name="password" id="password">
                                    </div>
                                </div>
                                <div class="col-md-4 m-b">
                                    <div class="form-group">
                                        <label>Ad Soyad</label>
                                        <input type="text" name="name" class="custom-input form-control" value="{{ $item->name }}">
                                    </div>
                                </div>
                                <div class="col-md-4 m-b">
                                    <div class="form-group">
                                        <label>İş Tanımı</label>
                                        <input type="text" name="job_title" class="custom-input form-control" value="{{ $item->job_title }}">
                                    </div>
                                </div>
                                <div class="col-md-4 m-b">
                                    <div class="form-group">
                                        <label>Yetki</label>
                                         {{ Form::select('role',
                                            $roles,
                                            $item->role->id,
                                            ['class' => 'form-control custom-input', 'placeholder' => 'Seçiniz...']) }}
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="record-buttons">
                                        <button class="btn btn-edit" type="submit">
                                            <i class="fa fa-edit"></i> Admin Düzenle 
                                        </button>
                                        <a href="{{ route('users.index') }}">
                                            <button class="btn btn-trash" type="button">
                                                <i class="fa fa-remove"></i> İptal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush 