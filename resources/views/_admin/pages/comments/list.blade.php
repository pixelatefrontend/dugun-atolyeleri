@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                <a href="{{ route('get.active.comments') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-check"></i></span>
                                <h3>Yorumlar</h3>
                                <p>Onaylanmış Yorumlar Listesi</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Yorum Tablosu</h3>
                            <span>{{count($items)}} Yorum Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Atölye</th>
                                        <th>Başlık</th>
                                        <th>İçerik</th>
                                        <th>Durum</th>
                                        <th>Tarih</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{ $item->workshop->title }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->content }}</td>
                                        <td>
                                        @if($item->active)
                                            <label class="label label-success">Yayında</label>
                                        @else
                                            <label class="label label-warning">Beklemede</labe>
                                        @endif
                                        </td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td class="f-r b-none">
                                            <a href="{{route('comments.active', $item->id)}}">
                                                <button class="btn btn-warning">
                                                    {{ $item->active == 1 ? 'Yayından Al' : 'Onayla' }}
                                                    <i class="fa {{{ $item->active == 1 ? 'fa-eye-slash' : 'fa-check' }}}"></i>
                                                </button>
                                            </a>
                                            <a href="{{route('comments.edit', $item->id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ 'comments.destroy', $item->id]) !!}
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Yorum Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection