@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Kampanya Tablosu</h3>
                            <span>{{count($items)}} Kampanya Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kampanya Adı</th>
                                        <th>Atölye</th>
                                        <th>Kampanya Tarihi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->workshop->title }}</td>
                                        <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                        <td>
                                            <button data-toggle="modal" data-target="#campaignModal{{$loop->iteration}}" class="btn btn-edit">Kampanya Detayları</button>
                                            <a href="{{ route('get.campaigns.update', $item->id) }}" target="_blank">
                                                <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Kampanya Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@forelse($items as $item)
    <div id="campaignModal{{$loop->iteration}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Teklif Detayları</h4>
          </div>
          <div class="modal-body">
            <h5>Kampanya Başlığı</h5>
            <p>{{ $item->title }}</p>
            <h5>Atölye</h5>
            <p>{{ $item->workshop->title  }}</p>
            <h5>Tarih</h5>
            <p>{{ $item->created_at->format('d.m.Y') }} | ({{ $item->created_at->diffForHumans() }})</p>
            <h5>Kampanya İçeriği</h5>
            <p> {!! $item->content !!} </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          </div>
        </div>
      </div>
    </div>
@empty
@endforelse
@endsection
