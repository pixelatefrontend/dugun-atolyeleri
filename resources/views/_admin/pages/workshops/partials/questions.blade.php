{!! Form::open([ 'url' => route('post.questions', $item->id) ]) !!}
<div class="widget">
    <div class="form-elements-sec">
        <div class="widget-title">
           <h3></h3>
           <button class="btn btn-edit pull-right" id="addItem" type="button"><i class="fa fa-plus"></i></button>
        </div>
        @if(count($item->questions) == 0)
	        <div id="items">
	            <input type="hidden" id="itemCounter" name="itemCounter" value="1">
	            <div class="item">
	              <div class="row">
	                    <div class="col-md-6">
	                        <div class="form-group">
	                            <label for="question">Soru</label>
	                            <input type="text" name="questions[]" class="form-control custom-input">
	                        </div>
	                    </div>
	                    <div class="col-md-5">
	                        <div class="form-group">
	                            <label for="answer">Cevap</label>
	                            <input type="text" name="replies[]" class="form-control custom-input">
	                        </div>
	                    </div>
	                    <div class="col-md-1">
	                        <div class="form-group">
	                            <label for="">&nbsp;</label>
	                            <button type="button" id="destroy-clone" class="btn btn-trash form-control" onclick="remove(this)" style="margin-top: 5px;"><i class="fa fa-trash"></i></button>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
        @else
            <div id="items">
              <input type="hidden" id="itemCounter" name="itemCounter" value="{{ count($item->questions) }}">
              @for($i=0,$c=count($item->questions);$i<$c;$i++)
              <div class="item">
                  <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="question">Soru</label>
                                <input type="text" name="questions[]" class="form-control custom-input" value="{{ $item->questions[$i]->question }}">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="answer">Cevap</label>
                                <input type="text" name="replies[]" class="form-control custom-input" value="{{ $item->questions[$i]->reply }}">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label for="">&nbsp;</label>
                                <button type="button" id="destroy-clone" class="btn btn-trash form-control" onclick="remove(this)" style="margin-top: 5px;"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
        @endif
        <div class="row">
        	<div class="form-group">
        		<div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
        	</div>
        </div>
    </div>
</div>
{!! Form::close() !!}