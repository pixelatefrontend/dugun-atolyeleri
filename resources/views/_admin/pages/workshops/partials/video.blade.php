{!! Form::open([ 'url' => route('post.video', $item->id), 'files' => true, 'class' => 'sec', 'role' => 'form' ]) !!}
    {{ method_field('PUT') }}
    <div class="widget">
       <div class="form-elements-sec">
            <div class="widget-title">
                <h3></h3>
            </div>
            @foreach($item->videos as $video)
            <div class="row" id="videoCol">
                <button class="btn btn-danger pull-right" style="margin-top: -34px;" onclick="colDestroy(this)" value="{{ $video->id }}"> <i class="fa fa-trash"></i></button>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="site">Site</label>
                        {!! Form::select('site[]',
                                [ 'youtube' => 'Youtube', 'vimeo' => 'Vimeo' ],
                                $video->site,
                                ['class' => 'form-control custom-input']) !!}
                    </div>
                    <div class="form-group">
                        <label for="url">Video Linki <small>(youtube)</small></label>
                        <input type="text" placeholder="Youtube Link" class="form-control custom-input" name="url[]" value="{{$video->url or ''}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Video Açıklaması</label>
                        <textarea placeholder="Açıklama" class="form-control custom-input" name="description[]" rows="6">{{$video->description or ''}}</textarea>
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Video Önizleme</label>
                        <iframe style="width: 100%; height: 300px;" src="{{ $video->strUrl() }}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            @endforeach
            <hr>
            <div class="row">
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="site">Site</label>
                        {!! Form::select('site[]',
                                [ 'youtube' => 'Youtube', 'vimeo' => 'Vimeo' ],
                                '',
                                ['class' => 'form-control custom-input', 'placeholder' => 'Seçiniz']) !!}
                    </div>
                    <div class="form-group">
                        <label for="site">Kapak Fotoğrafı</label>
                        <input type="file" name="videoCover" class="form-control custom-input">
                    </div>
                    <div class="form-group">
                        <label for="url">Video Linki <small></small></label>
                        <input type="text" placeholder="URL" class="form-control custom-input" name="url[]" >
                    </div>
                    <div class="form-group">
                        <label for="description">Video Açıklaması</label>
                        <textarea placeholder="Açıklama" class="form-control custom-input" name="description[]" rows="6"></textarea>
                    </div>
                </div>
                <div class="col-md-6 m-b video-bg">
                    <div class="form-group">
                        <label for="">Video Önizleme</label>
                        {{ Html::image('assets/_panel/images/video-bg.png', 'Default Video Image', ['class' => 'img-responsive']) }}
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
   </div>               
{!! Form::close() !!}