{!! Form::open([ 'url' => route('post.images', $item->id), 'files' => true, 'class' => 'sec' ]) !!}
    {{ method_field('PUT') }}

<div class="widget">
    <div class="form-elements-sec">
        <div class="widget-title">
           <h3></h3>
        </div>
        <div class="">
            <div class="col-md-6">
                <h3>Avatar</h3>
                <div class="box">
                    <div class="js--image-preview" style="background-image: url({{ $item->avatarPath() }}) "></div>
                        <div class="upload-options">
                          <label>
                            <input type="file" class="image-upload" accept="image/*"  name="avatar"/>
                        </label>
                    </div>
                </div>
            </div>
            @if($item->type != 'normal')
            <div class="col-md-6">
                <h3>Kapak</h3>
                <div class="box">
                    <div class="js--image-preview" style="background-image: url({{ $item->coverPath() }}) "></div>
                    <div class="upload-options">
                      <label>
                        <input type="file" class="image-upload" accept="image/*" name="cover" />
                    </label>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="col-md-12">
        <div class="record-buttons">
            <button class="btn btn-edit" type="submit">
                <i class="fa fa-plus"></i> Düzenle
            </button>
            <a href="{{ route('workshops.index') }}">
                <button class="btn btn-trash" type="button">
                    <i class="fa fa-remove"></i> İptal
                </button>
            </a>
        </div>
    </div>
    </div>
</div>
{!! Form::close() !!}