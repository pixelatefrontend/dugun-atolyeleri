{!! Form::open([ 'url' => route('post.details', $item->id), 'class' => 'sec' ]) !!}
    {{ method_field('PUT') }}
    <div class="widget">
       <div class="form-elements-sec">
            <div class="widget-title">
                <h3></h3>
            </div>
            <div class="row">
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Yetkili Kişi</label>
                        <input type="text" placeholder="Yetkili Kişi" class="form-control custom-input" name="member" value="{{ $item->member }}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Telefon</label>
                        <input type="text" placeholder="Telefon" class="form-control custom-input" name="phone" value="{{$item->phone}}">
                    </div>
                </div>
                <div class="col-md-12 m-b">
                    <div class="form-group">
                        <label for="">Adres</label>
                        <textarea rows="5" placeholder="Adres" class="form-control custom-input" name="address">{{ $item->address }}</textarea>
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Harita Koordinatı (x)</label>
                        <input type="text" class="form-control custom-input" name="x" value="{{ $item->map->x or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Harita Koordinatı (y)</label>
                        <input type="text" class="form-control custom-input" name="y" value="{{ $item->map->y or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                    <label for="">Min Fiyat (TL)</label>
                        <input type="text" name="price" id="numeric" class="form-control custom-input"
                        value="{{$item->services->price or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="">Teslim Süresi</label>
                        {!! Form::select('delivery_time',
                                [ 
                                    '1-3' => '1-3 Gün',
                                    '3-5' => '3-5 Gün',
                                    '7-10' => '7-10 Gün',
                                    '10-15' => '10-15 Gün',
                                    '15-30' => '15-30 Gün',
                                ],
                                $item->services ? $item->services->delivery_time : null,
                                ['class' => 'form-control custom-input', 'placeholder' => 'Seçiniz']) !!}
                    </div>
                </div>
                <hr>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
{!! Form::close() !!}