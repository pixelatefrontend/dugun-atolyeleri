{!! Form::open([ 'url' => route('create.acc', $item->id), 'files' => true, 'class' => 'sec', 'role' => 'form' ]) !!}
    {{ method_field('PUT') }}
    <div class="widget">
       <div class="form-elements-sec">
            <div class="widget-title">
                <h3></h3>
            </div>
            <div class="row">
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="username">Kullanıcı Adı</label>
                        <input type="text" placeholder="Kullanıcı Adı" class="form-control custom-input" name="username" value="{{ $item->username }}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="password">Şifre</label>
                        <input type="password" placeholder="Şifre" class="form-control custom-input" name="password">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
   </div>               
{!! Form::close() !!}