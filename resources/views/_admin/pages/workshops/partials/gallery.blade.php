 <svg class="hidden">
  <defs>
    <symbol id="icon-images" viewBox="0 0 36 32">
    <title>images</title>
    <path d="M34 4h-2v-2c0-1.1-0.9-2-2-2h-28c-1.1 0-2 0.9-2 2v24c0 1.1 0.9 2 2 2h2v2c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-24c0-1.1-0.9-2-2-2zM4 6v20h-1.996c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v1.996h-24c-1.1 0-2 0.9-2 2v0zM34 29.996c-0.001 0.001-0.002 0.003-0.004 0.004h-27.993c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v23.993z"></path>
    <path d="M30 11c0 1.657-1.343 3-3 3s-3-1.343-3-3 1.343-3 3-3 3 1.343 3 3z"></path>
    <path d="M32 28h-24v-4l7-12 8 10h2l7-6z"></path>
    </symbol>
  </defs>
</svg>
<div class="gallery-sec">
    <div class="row">
        <div class="col-md-12">
            {!! Form::open([ 'url' => route('post.gallery', $item->id), 'files' => true, 'class' => 'sec dropzone', 'id' => 'my-awesome-dropzone' ]) !!}
            {{ method_field('PUT') }}
            <div class="form-content">
                <svg class="icon icon-images"><use xlink:href="#icon-images"></use></svg>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
<hr>
<div class="gallery-sec">
    <div class="row" id="imageSortable">
        @foreach($item->images as $image)
        <div class="col-md-3 imageDes" id="{{ $image->id }}">
            <div class="gallery-box">
                <img src="/uploads/workshops/{{$item->id}}/{{ $image->filename }}" id="img" alt="" data-img-id="{{ $image->id }}">
                <div class="gallery-inner">
                    <div class="gallery-padding">
                        <div class="gallery-info">
                            <ul>
                                <li>
                                    <button onclick="firstPic(this)" data-path="/uploads/workshops/{{$item->id}}/{{ $image->filename }}" value="{{ $image->id }}"><i class="fa fa-desktop"></i></button>
                                </li>
                                <li><button onclick="destroyImage(this)" value="{{ $image->id }}"><i class="fa fa-trash-o"></i></button></li>
                            </ul>
                        </div><!-- Gallery Info -->
                    </div>
                </div>
            </div><!-- Gallery Box -->
        </div>
        @endforeach
    </div>
</div><!-- gallery Section -->
<div class="custom-alert alert alert-success" id="notification">
   <i class="fa fa-check"></i>
   <span>Kaydedildi</span>
   <p> &nbsp; &nbsp;&nbsp; Öne çıkarılan görsel değiştirildi</p>
</div>