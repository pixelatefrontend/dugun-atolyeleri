{!! Form::open([ 'url' => route('post.other', $item->id), 'files' => true, 'class' => 'sec', 'role' => 'form' ]) !!}
    {{ method_field('PUT') }}
    <div class="widget">
       <div class="form-elements-sec">
            <div class="widget-title">
                <h3></h3>
            </div>
            <div class="row">
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="ad">Facebook Link</label>
                        <input type="text" placeholder="Facebook" class="form-control custom-input" name="facebook" value="{{$item->other->facebook or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="twitter">Twitter Link</label>
                        <input type="text" placeholder="Twitter" class="form-control custom-input" name="twitter" value="{{$item->other->twitter or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="website">Website Link</label>
                        <input type="text" placeholder="Website" class="form-control custom-input" name="website" value="{{$item->other->website or ''}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="note">Atölye Notu</label>
                        <textarea name="note" rows="5" class="form-control custom-input">{{ $item->other->note or '' }}</textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
   </div>               
{!! Form::close() !!}