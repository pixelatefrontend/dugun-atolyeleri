{!! Form::open([ 'url' => route('workshops.update', $item->id), 'files' => true, 'class' => 'sec', 'role' => 'form' ]) !!}
    {{ method_field('PUT') }}
    <div class="widget">
       <div class="form-elements-sec">
            <div class="widget-title">
                <h3></h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="type">Atölye Tipi</label>
                        <select name="type" class="form-control custom-input">
                            @foreach($types as $type)
                                <option {{ $item->type == $type ? 'selected=selected' : '' }} value="{{$type}}">{{ucwords($type)}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                 <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="instagram">Instagram Kullanıcı Adı</label>
                        <input type="text" placeholder="Instagram Kullanıcı Adı" class="form-control custom-input" name="instagram_username" value="{{$item->instagram_username}}">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="ad">Atölye Adı</label>
                        <input type="text" placeholder="Atölye Adı" class="form-control custom-input" name="title" id="workshopName" value="{{$item->title}}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="instagram">Slug (SEO URL)</label>
                        <input type="text" placeholder="Slug" id="workshopSlug" class="form-control custom-input" name="slug" value="{{$item->slug}}">
                    </div>
                </div>
                <div class="col-md-12 m-b">
                    <div class="form-group">
                        <label for="desc">Açıklama</label>
                        <textarea rows="5" placeholder="Atölye Açıklaması" class="form-control custom-input" name="description">{{ $item->description }}</textarea>
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" placeholder="E-mail" class="form-control custom-input" name="email" value="{{ $item->email }}">
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="city">Şehir</label>
                        {{ Form::select('city', $cities, $item->city, ['class' => 'form-control custom-input', 'placeholder' => 'Şehir']) }}
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <div class="custom-select">
                            <label for="category">Kategori</label>
                            {{ Form::select('workshop_categories[]', $categories, $item->categories, ['multiple' => 'true', 'class' => 'workshop-category-select form-control custom-input', 'placeholder' => 'Atölye Kategorisi']) }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6 m-b">
                    <div class="form-group">
                        <label for="status">Durumu</label>
                        {!! Form::select('active',
                                [ '1' => 'Aktif', '0' => 'Pasif' ],
                                $item->active,
                                ['class' => 'form-control custom-input']) !!}
                    </div>
                </div>
                <hr>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="record-buttons">
                        <button class="btn btn-edit" type="submit">
                            <i class="fa fa-plus"></i> Düzenle
                        </button>
                        <a href="{{ route('workshops.index') }}">
                            <button class="btn btn-trash" type="button">
                                <i class="fa fa-remove"></i> İptal
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
   </div>               
{!! Form::close() !!}