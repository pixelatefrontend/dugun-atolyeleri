@extends('_admin.master')
@section('content')
<div class="main-content">
     <div class="heading-sec">
        <div class="mini-stats-sec">
            <div class="row">
                <a href="{{ route('workshops.create') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="red-skin"><i class="fa fa-plus"></i></span>
                                <h3>Atölye Ekle</h3>
                                <p>Yeni Atölye Oluştur</p>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('disabled.workshops') }}">
                    <div class="col-md-4 pull-right">
                        <div class="widget">
                            <div class="mini-stats">
                                <span class="orange-skin"><i class="fa fa-eye-slash"></i></span>
                                <h3>Atölyeler</h3>
                                <p>Onay bekleyen atölyeler</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div><!-- Mini stats Sec -->
    </div><!-- Top Bar Chart -->
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Atölye Tablosu</h3>
                            <span>Atölye Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped" id="workshops-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Atölye</th>
                                        <th>Instagram</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th>İşlemler</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
  <script>
    $('#workshops-table').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('workshops.data') !!}',
      columns: [
        {data: 'id', name: 'id'},
        {data: 'title', name: 'title'},
        {data: 'instagram_username', name: 'instagram_username'},
        {data: 'created_at', name: 'created_at'},
        {data: 'operations', name: 'operations', orderable: false, searchable: false}
      ]
    });
  </script>
@endpush