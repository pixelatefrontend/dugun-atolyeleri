<!DOCTYPE html>
<html lang="tr" class="no-js">
<head>
    <!-- Meta-Information -->
    <title>Admin Panel | Düğün Atölyeleri</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="svg/xml" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php header("Content-Type: application/json;charset=utf-8"); ?>
    <meta name="robots" content="noindex, nofollow">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />
    {!! Html::style('assets/_panel/css/bootstrap.min.css') !!}
    {!! Html::style('assets/_panel/css/select2.min.css') !!}
    {!! Html::style('assets/_panel/css/sweetalert2.min.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/main.css') !!}
    {!! Html::style('assets/_panel/css/component.css') !!}
    {!! Html::style('assets/_panel/css/icons.css') !!}
    {!! Html::style('assets/_panel/css/custom.css') !!}
    <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
</head>
<body>
<div class="main-content" style="padding: 10px !important;">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Atölye Tablosu</h3>
                            <span>{{count($items)}} Atölye Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Atölye</th>
                                        <th>Telefon</th>
                                        <th>E-mail</th>
                                        <th>Oluşturulma Tarihi</th>
                                        {{-- <th>Arandı mı?</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                       {{--  <td>
                                            <label class="switch">
                                              <input type="checkbox"
                                              data-id="{{ $item->id }}"
                                              onclick="readCheck(this)"
                                              {{{ $item->call_active == 1 ? 'checked' : '' }}} >
                                              <span class="slider round"></span>
                                            </label>
                                        </td> --}}
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Atölye Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
    {{-- <script src="https://maps.google.com/maps/api/js?sensor=false"></script> --}}
    {!! Html::script('assets/_panel/js/jquery-2.1.3.js') !!}
    {!! Html::script('assets/_panel/js/jquery-ui.min.js') !!}
    {!! Html::script('assets/_panel/js/bootstrap.min.js') !!}
    {!! Html::script('assets/_panel/js/angular.min.js') !!}
    {!! Html::script('assets/_panel/js/app.js') !!}
    {!! Html::script('assets/_panel/js/common.js') !!}
    {!! Html::script('assets/_panel/js/main.js') !!}
    {!! Html::script('assets/_panel/js/sweetalert2.min.js') !!}
    {!! Html::script('assets/_panel/js/select2.min.js') !!}
    {!! Html::script('assets/_panel/js/slugify.js') !!}
    {!! Html::script('assets/_panel/js/custom-file-input.js') !!}
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
        <script>
        function readCheck(item){
        var id = $(item).attr('data-id');
        $.ajax({
            method: 'POST',
            url: '{{ route('post.workshop.call') }}',
            data: { 'data-id' : id },
            success: function(response) {
                console.log(response);
            }
        });
    }
    </script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    {!! Html::script('assets/_panel/js/custom.js') !!}
    {!! Html::script('assets/_panel/js/panel.js') !!}

    <!--[if lte IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.2/jquery.xdomainrequest.min.js"></script>      
    <![endif]-->

</body>
</html>