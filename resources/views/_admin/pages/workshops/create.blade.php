@extends('_admin.master')
@section('content')
{!! Form::open([ 'url' => route('workshops.store'), 'files' => true, 'class' => 'sec', 'role' => 'form', ]) !!}
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Atölye Ekle</h3>
                        </div>
                            <div class="row">
                                <div class="col-md-6 mb">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <a id="cover" data-input="cover_picture" data-preview="cover_preview" class="btn btn-edit" style="height: 45px;">
                                                <i class="fa fa-picture-o" style="vertical-align: -webkit-baseline-middle;"></i> 
                                            </a>
                                        </span>
                                        <input id="cover_picture" class="form-control custom-input" type="text" name="avatar" placeholder="Profil Resmi">
                                    </div>
                                    <img id="cover_preview" style="margin-top:15px;max-height:100px;">
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="type" class="form-control custom-input">
                                            <option value="" selected hidden disabled>Atölye Üyelik Tipi</option>
                                            <option value="premium">Premium</option>
                                            <option value="gold">Gold</option>
                                            <option value="normal">Normal</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="Instagram Kullanıcı Adı" class="form-control custom-input" name="instagram_username">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="Atölye Adı" id="workshopName" class="form-control custom-input" name="title">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="SEO URL" class="form-control custom-input" id="workshopSlug" name="slug">
                                    </div>
                                </div>
                                <div class="col-md-12 m-b">
                                    <div class="form-group">
                                        <textarea rows="5" placeholder="Atölye Açıklaması" class="form-control custom-input" name="description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <input type="text" placeholder="E-mail" class="form-control custom-input" name="email">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <select name="city" id="city" class="form-control custom-input">
                                        <option selected disable hidden value="">Şehir</option>
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->city }}">{{ $city->city }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <div class="custom-select">
                                            <select placeholder="Atöyle Kategorisi" 
                                                class="workshop-category-select form-control custom-input" multiple="true" name="workshop_categories[]">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <select name="active" id="access" class="form-control custom-input" placeholder="Seçiniz">
                                            <option value="" disabled selected hidden>Durum</option>
                                            <option value="1">Aktif</option>
                                            <option value="0">Pasif</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    </div>
               </div>
               <hr>
               <div class="widget">
                    <div class="form-elements-sec">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="record-buttons">
                                    <button class="btn btn-edit" type="submit">
                                        <i class="fa fa-plus"></i> Atölye Ekle
                                    </button>
                                    <a href="{{ route('workshops.index') }}">
                                        <button class="btn btn-trash" type="button">
                                            <i class="fa fa-remove"></i> İptal
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
{!! Form::close() !!}

@endsection
@push('script')
<script>
    $(function(){
        $('#workshopName').friendurl({id : 'workshopSlug', divider: '-', transliterate: true});
    });
</script>
@endpush