@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#general">Genel Bilgiler</a></li>
                            <li><a data-toggle="tab" href="#image">Atölye Görseli</a></li>
                            <li><a data-toggle="tab" href="#details">Atölye Detay</a></li>
                            <li><a data-toggle="tab" href="#gallery">Resim Galerisi</a></li>
                            <li><a data-toggle="tab" href="#questions">Sorular</a></li>
                            <li><a data-toggle="tab" href="#video">Video</a></li>
                            <li><a data-toggle="tab" href="#other">Diğer</a></li>
                            <li><a data-toggle="tab" href="#acc">Kullanıcı Oluştur</a></li>
                            <a href="{{ env('URL').'/atolye/'.$item->slug }}" target="_blank">
                                <li style="float: right; "> <button class="btn btn-primary"><i class="fa fa-eye"></i></button></li>
                            </a>
                            <a href="https://www.instagram.com/{{$item->instagram_username}}" target="_blank">
                                <li style="float: right; margin-right: 10px;"> <button class="btn btn-warning"><i class="fa fa-instagram"></i></button></li>
                            </a>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="general" class="tab-pane fade in active">
                        @include('_admin.pages.workshops.partials.general')
                    </div>

                    <div id="image" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.images')
                    </div>

                    <div id="details" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.details')
                    </div>

                    <div id="gallery" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.gallery')
                    </div>

                    <div id="questions" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.questions')
                    </div>

                    <div id="video" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.video')
                    </div>

                    <div id="other" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.others')
                    </div>

                    <div id="acc" class="tab-pane fade">
                        @include('_admin.pages.workshops.partials.acc')
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
    $("#imageSortable").sortable({
        update: function( event, ui ) {
            var order = $(this).sortable('toArray');
            console.log(order);
            $.post('{{route('image.sortable')}}', { order: order })
            .done(function(){
              $('.custom-alert').fadeIn(1000).delay(3000).fadeOut('slow');
            });
        }
    });
  </script>
  <script type="text/javascript">
        Dropzone.options.myAwesomeDropzone = {
          maxFilesize: 20, // Size in MB
          dictDefaultMessage: "Dosya yüklemek için tıklayın veya sürükleyin",
          dictInvalidFileType: 'Sadece .jpg, .png, .jpeg dosyalarına izin verilmektedir',
          addRemoveLinks: true,
          renameFile: true,
          acceptedFiles: ".png,.jpg,.jpeg",
          dictRemoveFile: 'Resmi Sil',
          removedfile: function(file) { 
              var fileRef;
              return (fileRef = file.previewElement) != null ? 
              fileRef.parentNode.removeChild(file.previewElement) : void 0;
          },
      };
</script>
<script>
    $('.conf').click(function() {
        $(this).parents('.form-elements-item').find('.conf-inner').toggle();
    });

    $(function(){
      $('#workshopName').friendurl({id : 'workshopSlug', divider: '-', transliterate: true});
    });



    destroyImage = function(item){
        var data = $(item).attr('value');

        $.ajax({
            type: "POST",
            url: "/image/destroy/"+data,
            data: {id: data},
            success: function(response){
                if(response.success)
                {
                    $(item).closest('.imageDes').fadeOut(300, function(){ $(item).remove() });
                }else{
                    console.log('HATA!');
                }
            }
        });
    }
    
    firstPic = function(item){
        var data = $(item).data('path');
        var id = {{ $item->id }};
        $.ajax({
            type: 'POST',
            url: '/image/conf/'+id,
            data: { id: id, path: data },
            success: function(response){
                if(response.success){
                    $('.custom-alert').fadeIn(1000).delay(3000).fadeOut('slow');
                }else{
                    console.log('HATA');
                }
            }
        });
        $(item).parents('.form-elements-item').find('.conf-inner').hide();
    }

    $("#numeric").keypress(function (e){
      var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
    });
</script>
  <script>
    function initImageUpload(box) {
  let uploadField = box.querySelector('.image-upload');

  uploadField.addEventListener('change', getFile);

  function getFile(e){
    let file = e.currentTarget.files[0];
    checkType(file);
  }
  
  function previewImage(file){
    let thumb = box.querySelector('.js--image-preview'),
        reader = new FileReader();

    reader.onload = function() {
      thumb.style.backgroundImage = 'url(' + reader.result + ')';
    }
    reader.readAsDataURL(file);
    thumb.className += ' js--no-default';
  }

  function checkType(file){
    let imageType = /image.*/;
    if (!file.type.match(imageType)) {
      throw 'Datei ist kein Bild';
    } else if (!file){
      throw 'Kein Bild gewählt';
    } else {
      previewImage(file);
    }
  }
  
}

// initialize box-scope
var boxes = document.querySelectorAll('.box');

for(let i = 0; i < boxes.length; i++) {
  let box = boxes[i];
  initDropEffect(box);
  initImageUpload(box);
}



/// drop-effect
function initDropEffect(box){
  let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
  
  // get clickable area for drop effect
  area = box.querySelector('.js--image-preview');
  area.addEventListener('click', fireRipple);
  
  function fireRipple(e){
    area = e.currentTarget
    // create drop
    if(!drop){
      drop = document.createElement('span');
      drop.className = 'drop';
      this.appendChild(drop);
    }
    // reset animate class
    drop.className = 'drop';
    
    // calculate dimensions of area (longest side)
    areaWidth = getComputedStyle(this, null).getPropertyValue("width");
    areaHeight = getComputedStyle(this, null).getPropertyValue("height");
    maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

    // set drop dimensions to fill area
    drop.style.width = maxDistance + 'px';
    drop.style.height = maxDistance + 'px';
    
    // calculate dimensions of drop
    dropWidth = getComputedStyle(this, null).getPropertyValue("width");
    dropHeight = getComputedStyle(this, null).getPropertyValue("height");
    
    // calculate relative coordinates of click
    // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
    x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
    y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
    
    // position drop and animate
    drop.style.top = y + 'px';
    drop.style.left = x + 'px';
    drop.className += ' animate';
    e.stopPropagation();
    
  }
}
  </script>
<script>
    $(function(){
        if($('#itemCounter').val() == 0 || $('#itemCounter').val() == 1)
        {
          $('#destroy-clone').hide();
        }else if($('#itemCounter').val() >= 6){
            $('#addItem').prop('disabled', true);
        }
    });
function remove(div){
  $(div).parent().parent().parent().slideUp('slow', function(){
    $(this).remove();
  });
  $('#itemCounter').val(Number($('#itemCounter').val()) - 1);

  if($('#itemCounter').val() == 1)
  {
    $('#destroy-clone').hide();
  }

  if($('#itemCounter').val() >= 6){
    $('#addItem').prop('disabled', true);
  }else{
    $('#addItem').prop('disabled', false);
  }
}

// Accepts an element and a function
function childRecursive(element, func){
    // Applies that function to the given element.
    func(element);
    var children = element.children();
    if (children.length > 0) {
        children.each(function (){
            // Applies that function to all children recursively
            childRecursive($(this), func);
        });
    }
}

// Expects format to be xxx-#[-xxxx] (e.g. item-1 or item-1-name)
function getNewAttr(str, newNum){
    // Split on -
    var arr = str.split('-');
    // Change the 1 to wherever the incremented value is in your id
    arr[1] = newNum;
    // Smash it back together and return
    return arr.join('-');
}

// Written with Twitter Bootstrap form field structure in mind
// Checks for id, name, and for attributes.
function setCloneAttr(element, value){
    // Check to see if the element has an id attribute
    if (element.attr('id') !== undefined){
        // If so, increment it
        element.attr('id', getNewAttr(element.attr('id'),value));
    } else { /*If for some reason you want to handle an else, here you go */}
    // Do the same with name...
    // And don't forget to show some love to your labels.
    if (element.attr('data-inputid') !== undefined){
        element.attr('data-inputid', getNewAttr(element.attr('data-inputid'),value));
    } else {}
}

// Sets an element's value to ''
function clearCloneValues(element){
    if (element.attr('value') !== undefined){
        element.val('');
    }
}
     
    var say = 0;
    
//$(document).ready(function(){
    $('#addItem').click(function(){
      $('#destroy-clone').show();
        //increment the value of our counter
        $('#itemCounter').val(Number($('#itemCounter').val()) + 1);
        if($('#itemCounter').val() >= 6){
            $('#addItem').prop('disabled', true);
        }
        //clone the first .item element
        var newItem = $('div.item').first().clone();
        //recursively set our id, name, and for attributes properly
        childRecursive(newItem, 
            // Remember, the recursive function expects to be able to pass in
            // one parameter, the element.
            function(e){
                setCloneAttr(e, $('#itemCounter').val());
        });
        // Clear the values recursively
        childRecursive(newItem, 
            function(e){
                clearCloneValues(e);
        });
        // Finally, add the new div.item to the end
        newItem.appendTo($('#items'));
        say+1;

    });
//});

    function colDestroy(item){
      $(item).closest('#videoCol').slideUp('slow').remove();
    }

</script>
@endpush