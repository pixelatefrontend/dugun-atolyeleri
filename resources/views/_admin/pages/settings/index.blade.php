@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Site Ayarları</h3>
                        </div>
                        <div class="row">
                            <form method="POST" accept-charset="UTF-8" class="sec" role="form">
                            {{ Form::open([ 'url' => 'post.setting' ])}}
                            {{ method_field('PUT') }}
                            <div class="col-md-6 mb">
                                <div class="form-group">
                                    <label for="title">Site Başlığı</label>
                                    <input type="text" class="form-control custom-input" name="title">
                                </div>
                            </div>
                            <div class="col-md-6 mb">
                                <div class="form-group">
                                    <label for="title">Anahtar Kelimeler <small>( * Kelimelerin arasına virgül koyunuz. )</small></label>
                                    <input type="text" class="form-control custom-input" name="keywords">
                                </div>
                            </div>
                            <div class="col-md-6 mb">
                                <div class="form-group">
                                    <label for="title">Site Linki <small>(* URL)</small> </label>
                                    <input type="text" class="form-control custom-input" name="url">
                                </div>
                            </div>
                            <div class="col-md-6 mb">
                                <div class="form-group">
                                    <label for="title">Site E-mail</label>
                                    <input type="text" class="form-control custom-input" name="email">
                                </div>
                            </div>
                            <div class="col-md-12 mb">
                                <div class="form-group">
                                    <label for="title">Site Tanımı</label>
                                    <textarea type="text" class="form-control custom-input" rows="5" name="description"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    <button class="btn btn-edit" type="submit">
                                        <i class="fa fa-edit"></i> Düzenle
                                    </button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
{{-- <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    function read(item){
        var id = $(item).parent().parent().val();
        console.log(id);
        $.ajax({
            method: 'POST',
            url: '/contact-form/'+id,
            data: { id }, 
        });
        $(item).appendTo(" <i class='fa fa-eye'></i> ");
    }
</script> --}}
@endpush