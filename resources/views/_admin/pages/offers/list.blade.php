@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Teklifler Tablosu</h3>
                            <span>{{count($items)}} Teklif Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kullanıcı</th>
                                        <th>Atölye</th>
                                        <th>Teklif Tarihi</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr class="section" data-title="asdasd">
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ ucwords($item->user->name.' '.$item->user->lname) }}</td>
                                        <td>{{ $item->workshop->title }}</td>
                                        <td>{{ $item->created_at->format('d-m-Y') }} ({{ $item->created_at->diffForHumans() }})</td>
                                        <td> <button data-toggle="modal" data-target="#offerModal{{$loop->iteration}}" class="btn btn-edit">Teklif Detayları</button> </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Teklif Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@forelse($items as $item)
    <div id="offerModal{{$loop->iteration}}" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Teklif Detayları</h4>
          </div>
          <div class="modal-body">
            <h5>Adı Soyadı</h5>
            <p>{{ $item->user->getFullName() }}</p>
            <h5>E-mail</h5>
            <p>{{ $item->user->email  }}</p>
            <h5>Telefon</h5>
            <p>{{ $item->user->phone }}</p>
            <h5>Düğün Tarihi</h5>
            <p>{{ $item->user->date->format('d.m.Y') }}</p>
            <h5>Teklif Açıklaması</h5>
            <p>{{ $item->description }}</p>
            <?php $image = DB::table('images')->where('id', $item->image_id)->first();  ?>
            @if($image)
            <h5>Ürün</h5>
            <p><img src="/uploads/workshops/{{ $item->workshop->id }}/{{ $image->filename }}" class="img-responsive" alt=""></p>
            @endif
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
          </div>
        </div>
      </div>
    </div>
@empty
@endforelse
@endsection
@push('script')
    <script>
        $(function(){
            var titles =[] ;
            $.each ($('tr.section'), function (index, elt) {
                var title =$(elt).attr('data-title') ;
                if ( $.inArray(title, titles) == -1 )
                    $('<td colspan="3">' + title + '</td>').afterBefore($(elt)) ;
                titles.push(title) ;
            }) ;
        });
    </script>
@endpush