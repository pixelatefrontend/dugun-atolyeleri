@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-sec">
                    <div class="row">
                        <div class="col-md-12">
                            @include('_admin.layouts.notifications')
                        </div>
                    </div>
                </div>
               <div class="widget">
                   <div class="form-elements-sec">
                        <div class="widget-title">
                            <h3>Kullanıcı Düzenle</h3>
                        </div>
                        {!! Form::open([ 'url' => route('users.update', $item->id), 'class' => 'sec', 'role' => 'form' ]) !!}
                        {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="text" placeholder="E-mail" class="form-control custom-input" name="email" id="email" value="{{ $item->email }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Şifre</label>
                                        <input type="password" placeholder="Şifre" class="form-control custom-input" name="password" id="password">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Ad</label>
                                        <input type="text" name="name" class="custom-input form-control" value="{{ $item->name }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Soyad</label>
                                        <input type="text" name="lname" class="custom-input form-control" value="{{ $item->lname }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Telefon</label>
                                        <input type="text" name="phone" class="custom-input form-control" value="{{ $item->phone }}">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Şehir</label>
                                        {!! Form::select('city', $cities, $item->city, [ 'class' => 'custom-input form-control' ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                        <label>Düğün Tarihi</label>
                                        <input type="text" value="{{ $item->date->format('d.m.Y') }}" class="form-control custom-input" name="date">
                                    </div>
                                </div>
                                <div class="col-md-6 m-b">
                                    <div class="form-group">
                                            <label>Durum</label>
                                            {!! Form::select('active',
                                                [ '1' => 'Aktif', '0' => 'Pasif' ],
                                                $item->active,
                                                ['class' => 'form-control custom-input']) !!}
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="record-buttons">
                                        <button class="btn btn-edit" type="submit">
                                            <i class="fa fa-edit"></i> Kullanıcı Düzenle 
                                        </button>
                                        <a href="{{ route('users.index') }}">
                                            <button class="btn btn-trash" type="button">
                                                <i class="fa fa-remove"></i> İptal
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
               </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection
@push('script')
<script>

</script>
@endpush 