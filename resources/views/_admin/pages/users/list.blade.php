@extends('_admin.master')
@section('content')
<div class="main-content">
    <div class="panel-content">
        <div class="row">
            <div class="col-md-12">
                <div class="widget">
                    <div class="table-area">
                        <div class="widget-title">
                            <h3>Kullanıcı Tablosu</h3>
                            <span>{{count($items)}} Kullanıcı Listelendi</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Ad Soyad</th>
                                        <th>E-mail</th>
                                        <th>Durum</th>
                                        <th>Oluşturulma Tarihi</th>
                                        <th>İşlemler</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($items as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name.' '.$item->lname }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>
                                        @if($item->active)
                                            <label class="label label-success">Aktif</label>
                                        @else
                                            <label class="label label-warning">Pasif</labe>
                                        @endif
                                        </td>
                                        <td>{{ $item->created_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="{{route('users.edit', $item->id)}}">
                                                <button class="btn btn-edit">
                                                    Düzenle <i class="fa fa-edit"></i>
                                                </button>
                                            </a>
                                            {!! stuff_destroy([ 'users.destroy', $item->id]) !!}
                                        </td>
                                    </tr>
                                @empty
                                    <h3 class="text-center" style="color: #ff6b6b"> <i class="fa fa-exclamation" aria-hidden="true"></i> Kullanıcı Bulunamadı</h3>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Panel Content -->
</div><!-- Main Content -->
@endsection