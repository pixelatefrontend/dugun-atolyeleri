<!DOCTYPE html>
<html>
<head>
    <title>Düğün Atölyeleri Admin</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Html::style('assets/_panel/css/bootstrap.min.css') !!}
    {!! Html::style('assets/_panel/css/icons.css') !!}
    {!! Html::style('assets/_panel/css/main.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/responsive.css') !!}
    {!! Html::style('assets/_panel/css/custom.css') !!}
    <style>
    	.panel-logo {
    		padding: 20px;
    	}
    </style>
</head>
<body>
<div class="main-content">
<div class="account-user-sec">
    <div class="account-sec">
        <div class="account-top-bar">
            <div class="container">
                 <div class="text-center panel-logo">
                    <a href="#" title=""><img src="assets/_website/img/logo-pink-black.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="acount-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="contact-sec">
                            <div class="widget-title">
                                <h3>Düğün Atölyeleri</h3>
                                <span>Yönetim Paneli Girişi</span>
                            </div><!-- Widget title -->
                            <form action="{{ route('admin.post.login') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input type="text" name="email" class="form-control custom-input" placeholder="E-mail">
                                </div>
                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" name="password" class="form-control custom-input" placeholder="Şifre">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-edit pull-right">Giriş Yap</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Account Sec -->
</div>

</div><!-- Main Content -->

<!-- Vendor: Javascripts -->

{!! Html::script('assets/_panel/js/jquery-2.1.3.js') !!}
{!! Html::script('assets/_panel/js/bootstrap.min.js') !!}


<!-- Our Website Javascripts -->
</body>
</html>