<header class="side-header light-skin opened-menu">
    <div class="admin-details">
        <span><img src="/assets/_panel/images/avatar.png" alt="" /></span>
        <h3>{{ Auth::guard('admin')->user()->name }}</h3>
        <i>{{ Auth::guard('admin')->user()->job_title }}</i>
    </div>
    <div class="menu-scroll">
        <div class="side-menus">
            <nav>
                <ul>
                    <li><a href="/" title=""><i class="fa fa-dashboard"></i> <span>Ana Sayfa</a></li>
                    @if(Auth::guard('admin')->user()->role->name != 'Blog Editör')
                    <li><a href="{{ route('sliders.index') }}" title=""><i class="fa fa-bolt"></i> <span>Slider Yönetimi</a></li>
                    <li><a href="{{ route('categories.index' )}}" title=""><i class="fa fa-bars"></i> <span>Kategoriler</a></li>
                    <li><a href="{{ route('workshops.index' )}}" title=""><i class="fa fa-code-fork"></i> <span>Atölyeler</a></li>
                    <li><a href="{{ route('users.index' )}}" title=""><i class="fa fa-users"></i> <span>Kullanıcılar</a></li>
                    <li><a href="{{ route('offers.index' )}}" title=""><i class="fa fa-envelope"></i> <span>Teklifler</a></li>
                    <li><a href="{{ route('comments.index' )}}" title=""><i class="fa fa-comments"></i> <span>Yorumlar</a></li>
                    <li><a href="{{ route('get.campaigns' )}}" title=""><i class="fa fa-gift"></i> <span>Kampanyalar</a></li>
                    <li><a href="{{ route('get.contact-list' )}}" title=""><i class="fa fa-bell"></i> <span>İletişim Formu</a></li>
                    @endif
                    <li class="menu-item-has-children">
                        <a href="" title=""><i class="fa fa-rss"></i> <span>Blog</span></a>
                        <ul>
                            <li><a href="{{ route('blogs.index') }}" title=""><i class="fa fa-list"></i> <span>Listeler</span></a></li>
                            <li><a href="{{ route('blog-categories.index') }}" title=""><i class="fa fa-files-o"></i> <span>Kategoriler</span></a></li>
                            <li><a href="{{ route('blog-comments.index') }}" title=""><i class="fa fa-comments"></i> <span>Yorumlar</span></a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div><!-- Menu Scroll -->
</header>