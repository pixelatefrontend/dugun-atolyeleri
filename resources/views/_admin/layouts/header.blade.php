<div class="top-bar">
    <div class="logo">
        <a href="/" title=""><img alt="Düğün Atölyeleri" src="/assets/_website/img/logo-pink-black.png" srcset="/assets/_website/img/logo-pink-black-2x.png 2x"></a>
         <div class="menu-options"><span class="menu-action"><i></i></span></div>
    </div>
    <ul>
       {{--  <li><a title=""><i class="fa fa-plus"></i>Yeni Görev</a></li> --}}
        <li><a title="" id="files"><i class="fa fa-cloud-upload"></i>Dosya Yükle</a></li>
        <li><a href="{{ route('admins.index') }}" title=""><i class="fa fa-users"></i>Panel Kullanıcıları</a></li>
        {{-- <li><a href="{{ route('get.settings') }}" title=""><i class="fa fa-cog"></i>Site Ayarları</a></li> --}}
    </ul>
    {{-- <div class="quick-links">
        <ul>
            <li><a title="" class="sky-skin"><i class="fa fa-info"></i></a>
                <div class="dialouge notification" style="display: none;">
                    <span>You have 6 New Notification</span>
                    <a title="" href="/"><img alt="" src="http://placehold.it/40x40">Server 3 is Over Loader Pleas
                    toe Check. <p><i class="fa fa-clock-o"></i>3:45pm</p></a>
                    <a title="" href="/"><img alt="" src="http://placehold.it/40x40">Server 10 is Over Loader Pleas
                    toe Check. <p><i class="fa fa-clock-o"></i>1:40am</p></a>
                    <a title="" href="/"><img alt="" src="http://placehold.it/40x40">New User Registered Please Check This <p><i class="fa fa-clock-o"></i>4 Hours ago</p></a>
                    <a class="view-all" href="#">VIEW ALL NOTIFICATIONS</a>
                </div>
            </li>
            <li><a title="" class="purple-skin"><i class="fa fa-comment-o"></i></a>
                <div class="dialouge notification" style="display: none;">
                    <span>You have 3 New Messages</span>
                    <a title="" href="/">Hey! How are You Diana. I waiting for you.
                    toe Check. <p><i class="fa fa-clock-o"></i>3:45pm</p></a>
                    <a title="" href="/">Please Can you Submit A file. I am From Korea
                    toe Check. <p><i class="fa fa-clock-o"></i>1:40am</p></a>
                    <a title="" href="/">Hey Today is Party So you Will Have to Come <p><i class="fa fa-clock-o"></i>4 Hours ago</p></a>
                    <a class="view-all" href="inbox.html">VIEW ALL MESSAGE</a>
                </div>
            </li>
            <li><a title="" class="show-stats red-skin"><i class="fa fa-cog"></i></a></li>
            <li><a title="" id="toolFullScreen" class="pink-skin"><i class="fa fa-arrows-alt"></i></a></li>
        </ul>
    </div> --}}
</div><!-- Top Bar -->