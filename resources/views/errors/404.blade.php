<!DOCTYPE html>
<html class="error-page-show">
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="googlebot" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<title>Düğün Atölyeleri | Aradığınız Sayfa Bulunamadı!</title>
	<!-- Icon -->

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/favicon/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="/favicon/mstile-310x310.png" />

<!-- Icon End -->



<!-- Styles -->

<link rel="stylesheet" type="text/css" href="/assets/_website/css/jquery-ui.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/select2.min.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/easy-autocomplete.min.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/easy-autocomplete.themes.min.css">

<link rel="stylesheet" type="text/css" href="/assets/_website/css/main.css">

<!-- Styles End -->
</head>
<body>
<header class="login-success">
	<div class="row middle-nav-container">

		<div class="container">

			<nav class="middle-nav">

				<div class="row">

					<div class="row-inner col-md-4 col-lg-3 col-sm-4 col-xs-6">

						<div class="logo-section">

							<h1><a href="{{ env('APP_URL') }}"><img alt="Düğün Atölyeleri" src="/assets/_website/img/logo-pink-black.png" srcset="/assets/_website/img/logo-pink-black-2x.png 2x"></a></h1>

						</div>

					</div>

					<div class="row-inner col-md-4 col-lg-6 col-sm-4 hidden-xs">

					</div>

					<div class="row-inner col-md-3 col-lg-3 col-sm-4 col-xs-6">

						<a href="tel:+90 538 970 48 68" class="call-section">

							<div class="call-section-inner">

								<figure>

									<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/phone-icon.svg#phone-icon"></use></svg>

								</figure>

								<div class="text">

									<strong>BİZİ ARAYIN</strong>

									<strong><span>0 (216)</span> 504 22 62</strong>

								</div>

							</div>

						</a>

					</div>

				</div>

			</nav>

		</div>

	</div>

	<div class="row big-nav-container">

		<div class="container">

		</div>

	</div>

</header>
<section class="error-page">
	
	<div class="container">

		<div class="row">

			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<div class="message">

					<div class="icon-animation">

						<svg class="shop-icon-1"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-3"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-4"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

					</div>

					<div class="icon-animation icon-animation-2">

						<svg class="shop-icon-1"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-2"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-3"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

						<svg class="shop-icon-4"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/icons.svg#studio-icon"></use></svg>

					</div>

					<svg class="image"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/404-image.svg#404-image"></use></svg>

					<h3>HAY AKSİ!</h3>

					<p>Aradığınız sayfaya giderken bir terslik oldu. Daha sonra tekrar deneyin.</p>

					<a href="{{ route('website.home') }}"><svg class="left-long-arrow"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/_website/img/left-long-arrow.svg#left-long-arrow"></use></svg><span>GERİ DÖN</span></a>

				</div>

			</div>
			
		</div>

	</div>

</section>
</body>
</html>