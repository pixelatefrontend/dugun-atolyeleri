<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Models\Slider;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        Slider::truncate();
        foreach(range(1, 3) as $index){
            \DB::table('sliders')->insert([
                'image' => 'assets/_website/img/home-slider-image.jpg',
                'title' => $faker->title,
                'description' => 'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet',
                'button_text' => 'Tüm Atölyeler',
                'url' => '#',
                'active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
