<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        User::truncate();
        foreach(range(1, 30) as $index){
            \DB::table('users')->insert([
                'name' => $faker->firstName,
                'lname' => $faker->lastName,
                'phone' => $faker->phoneNumber,
                'email' => $faker->unique()->email,
                'password' => \Hash::make('secret'),
                'date' => Carbon::createFromDate(null, 12, 25),
                'city' => $faker->city, 
                'active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
