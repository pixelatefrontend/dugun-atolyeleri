<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('admins')->delete();
        DB::table('admins')->insert([
            'name' => 'Pixelate Creative',
            'email' => 'pixelate@example.com',
            'job_title' => 'Web Developer',
            'password' => \Hash::make('secret'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
