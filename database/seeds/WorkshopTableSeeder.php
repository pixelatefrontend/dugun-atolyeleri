<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;
use App\Models\Workshop;

class WorkshopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Workshop::truncate();
		factory(App\Models\Workshop::class, 30)->create()->each(function ($u) {
		    $u->categories()->sync([1, 2]);
		});
    }
}
