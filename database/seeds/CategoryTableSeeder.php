<?php

use Illuminate\Database\Seeder;
use App\Models\Category;
use Carbon\Carbon;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::truncate();
        \DB::table('categories')->insert([
        	'title' => 'DAVETİYE TASARIMI',
        	'slug' => 'davetiye-tasarimi',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'davetiye',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'DÜĞÜN PASTASI',
        	'slug' => 'dugun-pastasi',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'dugun-pastasi',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'NİKAH ŞEKERİ',
        	'slug' => 'nikah-sekeri',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'nikah-sekeri',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'GELİN ÇİÇEĞİ',
        	'slug' => 'gelin-cicegi',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'gelin-cicegi',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'DÜĞÜN-BEBEK FOTOĞRAFÇILIĞI',
        	'slug' => 'dugun-bebek-fotografciligi',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'dugun-bebek-fotograf',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'BEBEK SÜSLERİ',
        	'slug' => 'bebek-susleri',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'bebek-susleri',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'DÜĞüN ÇİKOLATASI',
        	'slug' => 'dugun-cikolatasi',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'dugun-cikolatasi',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        \DB::table('categories')->insert([
        	'title' => 'NİŞAN SÜSLERİ',
        	'slug' => 'nisan-susleri',
        	'description' => 'Kategori Açıklaması',
        	'icon' => 'nisan-susleri',
        	'cover_picture' => '/files/_page-header.jpg',
        	'active' => 1,
        	'view' => 0,
        	'sort' => 0,
        	'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
