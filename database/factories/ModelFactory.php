<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Carbon\Carbon;

$factory->define(App\Models\Workshop::class, function (Faker\Generator $faker) {
    static $password;

	$types = ['normal', 'premium', 'gold'];
	shuffle($types);
    return [
        		'type' => $types[0],
                'title' => $faker->name,
                'description' => $faker->text,
                'slug' => $faker->slug,
                'instagram_username' => $faker->userName,
                'member' => $faker->name,
                'phone' => $faker->phoneNumber,
                'username' => $faker->userName,
                'email' => $faker->unique()->email,
                'password' => \Hash::make('secret'),
                'address' => $faker->address,
                'city' => $faker->city, 
                'active' => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    ];
});
