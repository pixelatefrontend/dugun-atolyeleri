<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workshop_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->text('keywords');
            $table->string('icon');
            $table->string('cover_picture');
            $table->boolean('active');
            $table->integer('view')->default(0);
            $table->integer('sort')->default(0);
            $table->timestamps();
        });

        Schema::create('category_workshop', function(Blueprint $table){
            $table->integer('workshop_id');
            $table->integer('category_id');
            $table->primary([ 'workshop_id', 'category_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('category_workshop');
    }
}
