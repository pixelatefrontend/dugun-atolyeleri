<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('workshop_id');
            $table->string('card_user_key');
            $table->string('card_token');
            $table->string('card_alias')->nullable();
            $table->integer('bin_number');
            $table->string('card_association');
            $table->string('card_family');
            $table->string('card_bank_name')->nullable();
            $table->integer('cvc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
