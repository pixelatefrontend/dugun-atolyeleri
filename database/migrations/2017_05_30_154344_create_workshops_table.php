<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->default('normal');
            $table->string('avatar')->nullable();
            $table->string('first_picture')->nullable();
            $table->string('cover')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('slug')->nullable();
            $table->string('instagram_username')->nullable();
            $table->string('member')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('city')->nullable();
            $table->text('address')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('view_count')->default(0);
            $table->integer('like')->default(0);
            $table->integer('point')->default(0)->nullable();
            $table->integer('call_active')->default(0)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workshops');
    }
}
