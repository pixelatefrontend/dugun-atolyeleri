const { mix } = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
		'public/assets/_website/css/main.css',
	], 'public/assets/_website/css/all.css').version();

mix.js([
		'public/assets/_website/js/main.js',
	], 'public/assets/_website/js/all.js').version();
